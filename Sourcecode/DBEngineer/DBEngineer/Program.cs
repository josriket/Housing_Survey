﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBEngineer.library.service;
using SendEmailService.library.util;

namespace DBEngineer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. Drop Standard FK");
            Console.WriteLine("2. Create Standard FK");
            Console.WriteLine("3. Rebuild Standard FK (Drop+Create)");
            Console.WriteLine("4. Drop All Index Key");
            Console.WriteLine("5. Generate FK Index Key");
            Console.WriteLine("6. Drop Full Foreign Key !!!!!!!!!!!!!");
            Console.WriteLine("7. Drop All Table !!!!!!!!!!!!!");
            Console.WriteLine("8. Compair table 2 database");
            Console.WriteLine("9. Create standard field if not exists");
            Console.WriteLine("10. Create complex FK");
            Console.WriteLine("11. Reverse Engineer SQLServer ->Postgres");

            string menuId = Console.ReadLine();
            if (menuId == "1")
            {
                new SQLServerRebuildStandardFK().StartRebuild(true, false);
            }
            else if (menuId == "2")
            {
                new SQLServerRebuildStandardFK().StartRebuild(false, true);
            }
            else if (menuId == "3")
            {
                new SQLServerRebuildStandardFK().StartRebuild(true,true);
            }
            else if (menuId == "4")
            {
                new SQLServerIndexService().DropAllIndex();
            }
            else if (menuId == "5")
            {
                new SQLServerIndexService().CreateFKIndex();
            }
            else if (menuId == "6")
            {
                new SQLServerRebuildStandardFK().DropFullFK();
            }
            else if (menuId == "7")
            {
                new SQLServerRebuildStandardFK().DropAllTable();
            }
            else if (menuId == "8")
            {
                new CompairSchemaService().Start();
            }
            else if (menuId == "9")
            {
                new SQLServerCreateStandardColumnService().Start();
            }
            else if (menuId == "10")
            {
                new SQLServerRebuildComplexFK().Start();
            }
            else if (menuId == "11")
            {
                new SQLserverToPgSQLService().StartReverseEngineering();
            }
            else
            {
                Console.WriteLine("Command not found !!!!");
            }


            // ถ้าต้องการใช้postgres ให้ลง mpgsql แล้ว include เข้ามา แล้วเปิด แล้ว include PGConnection 

            //new SQLserverToPgSQLService().StartReverseEngineering();

            // new SQLServerToAccessService().StartReverseEngineering();
            
          

           // new SQLServerToMySqlService().StartReverseEngineering();

            //new ScriptDatabaseService().GenerateScript();
        }

    }
}
