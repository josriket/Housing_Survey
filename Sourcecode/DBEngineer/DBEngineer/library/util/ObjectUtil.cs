﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SendEmailService.library.util
{
    public class ObjectUtil
    {
        public static void CopyTo(object S, object T)
        {
            foreach (var pS in S.GetType().GetProperties())
            {
                foreach (var pT in T.GetType().GetProperties())
                {
                    if (pT.Name != pS.Name) continue;
                    (pT.GetSetMethod()).Invoke(T, new object[] { pS.GetGetMethod().Invoke(S, null) });
                }
            };
        }

        public static T Clone<T>(T obj)
        {
            byte[] tmp = Serialize<T>(obj);
            T result = Deserialize<T>(tmp);

            return result;
        }

        // Deserial  binary data to Object
        public static TData Deserialize<TData>(byte[] b)
        {
            MemoryStream stream = new MemoryStream(b);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                stream.Seek(0, SeekOrigin.Begin);
                return (TData)formatter.Deserialize(stream);
            }
            finally
            {
                stream.Close();
            }
        }

        // Serial Object to binary
        public static byte[] Serialize<TData>(TData settings)
        {
            MemoryStream stream = new MemoryStream();
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, settings);
                stream.Flush();
                stream.Position = 0;
                return stream.ToArray();
            }
            finally
            {
                stream.Close();
            }
        }

        public static string GetProperties(object criteria)
        {
            BindingFlags bindingFlags = BindingFlags.Public
                //| BindingFlags.NonPublic 
                            | BindingFlags.Instance
                //| BindingFlags.CreateInstance
                //| BindingFlags.Static
                            ;

            string objType = criteria.GetType().FullName;
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            Type typeOfClass = thisAssembly.GetTypes().Where(t => t.FullName == objType).First();

            string properties = string.Empty;
            bool isSecond = false;
            foreach (FieldInfo field in typeOfClass.GetFields(bindingFlags))
            {
                if (isSecond) properties += " ,";
                object o = null;
                if (typeOfClass.GetField(field.Name) != null) o = typeOfClass.GetField(field.Name).GetValue(criteria);
                properties += field.Name + ":" + ((o == null) ? "" : o.ToString());
                isSecond = true;
            }
            return properties;
        }

        public static string SerializeBase64(object o)
        {
            // Serialize to a base 64 string
            byte[] bytes;
            long length = 0;
            MemoryStream ws = new MemoryStream();
            BinaryFormatter sf = new BinaryFormatter();
            sf.Serialize(ws, o);
            length = ws.Length;
            bytes = ws.GetBuffer();
            string encodedData = bytes.Length + ":" + Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None);
            return encodedData;
        }

        public static object DeserializeBase64(string s)
        {
            // We need to know the exact length of the string - Base64 can sometimes pad us by a byte or two
            int p = s.IndexOf(':');
            int length = Convert.ToInt32(s.Substring(0, p));

            // Extract data from the base 64 string!
            byte[] memorydata = Convert.FromBase64String(s.Substring(p + 1));
            MemoryStream rs = new MemoryStream(memorydata, 0, length);
            BinaryFormatter sf = new BinaryFormatter();
            object o = sf.Deserialize(rs);
            return o;
        }

        /// <summary>
        /// Writes the given object instance to a binary file.
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the XML file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the XML file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file.
        /// </summary>
        /// <typeparam name="T">The type of object to read from the XML.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (Stream stream = File.Open(filePath, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    return (T)binaryFormatter.Deserialize(stream);
                }
            }
            else
            {
                return default(T);
            }
        }
    }
}
