﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.util
{
    public class AppCofig
    {
        public static string GetString(string key, string defaultValue = null)
        {
            if (ConfigurationManager.AppSettings[key] != null)
            {
                return ConfigurationManager.AppSettings[key].ToString();
            }
            return defaultValue;
        }

        public static string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[GetString(key)].ToString();
        }
    }
}
