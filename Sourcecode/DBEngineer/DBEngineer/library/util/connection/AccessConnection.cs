﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.OleDb;

namespace SendEmailService.library.util
{
    public class AccessConnection : IDisposable
    {
        public OleDbConnection conn;
        public OleDbTransaction tran;


        // Example : DBConnection conn = DBConnection.NewGWConnection;
        public static AccessConnection DefaultConnection
        {
            get
            {
                AccessConnection conn = new AccessConnection(ConfigurationManager.ConnectionStrings["AccessConnection"].ToString());
                return conn;
            }
        }

        public static AccessConnection CustomConnection(string connections)
        {
            return new AccessConnection(connections);
        }
        // New connection for Choose Database Connection "CRM","GW"
        // Example : DBConnection conn = new DBConnection(DBConnection.CRM);
        private AccessConnection(string connectionstring)
        {
            openCon(connectionstring);
        }


        private void openCon(string connectionstring)
        {
            if (conn != null) throw new Exception("Connection not yet close, Please close connection before re-create.");

            conn = new OleDbConnection(connectionstring);
            conn.Open();
        }


        public DataSet executeDataSet(string query)
        {
            return executeDataSet(query, new List<OleDbParameter>());
        }

        public DataSet executeStoredProcedure(string storedName, Dictionary<string, object> lstParam)
        {
            using (OleDbCommand cmd = new OleDbCommand(storedName, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                if (tran != null)
                {
                    cmd.Transaction = tran;
                }

                if (lstParam != null)
                {
                    foreach (KeyValuePair<string, object> p in lstParam)
                    {
                        cmd.Parameters.Add("@" + p.Key, SqlDbType.VarChar).Value = p.Value;
                        //cmd.Parameters.Add(new OleDbParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                    }
                }

                OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                ds.Dispose();
                return ds;
            }
        }

        public DataSet executeDataSet(string query, OleDbParameter param)
        {
            OleDbCommand cmd = new OleDbCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                cmd.Parameters.Add(new OleDbParameter(param.ParameterName, param.Value));
            }
            OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public DataSet executeDataSet(string query, List<OleDbParameter> param)
        {
            OleDbCommand cmd = new OleDbCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (OleDbParameter p in param)
                {
                    cmd.Parameters.Add(new OleDbParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public object executeScalar(string query,params OleDbParameter[] param)
        {
            OleDbCommand cmd = new OleDbCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (OleDbParameter p in param)
                {
                    cmd.Parameters.Add(new OleDbParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            return cmd.ExecuteScalar();
        }

        public OleDbDataReader executeDataReader(string query, List<OleDbParameter> param)
        {
            OleDbCommand cmd = new OleDbCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (OleDbParameter p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            return cmd.ExecuteReader();
        }

        public string QueryDataString(string query, List<OleDbParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return null;
            }
        }

        public byte[] QueryDataBinary(string query, List<OleDbParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (byte[])ds.Tables[0].Rows[0][0];
            }
            else
            {
                return null;
            }
        }

        public decimal QueryDataDecimal(string query, List<OleDbParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (decimal)ds.Tables[0].Rows[0][0];
            }
            else
            {
                return 0;
            }
        }

        public int QueryDataInt(string query, List<OleDbParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public Int64? QueryDataInt64(string query, List<OleDbParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt64(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return null;
            }
        }


        public DateTime QueryDataDateTime(string query, List<OleDbParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return DateTime.MinValue;
            }
        }


        public int executeUpdate(string query, List<OleDbParameter> param, ref Int64 generateKey)
        {
            OleDbCommand cmd = new OleDbCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                //cmd.CommandText = "SET autocommit = 0";
                //cmd.ExecuteNonQuery();
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (OleDbParameter p in param)
                {
                    cmd.Parameters.Add(new OleDbParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            // SQL SERVER
            cmd.CommandText = "SELECT @@IDENTITY";
            generateKey = Convert.ToInt64(cmd.ExecuteScalar());

            // MYSQL SQL
            //generateKey =cmd.LastInsertedId;

            return result;

        }

        public int executeUpdate(string query)
        {
            return executeUpdate(query, null);
        }

        public int executeUpdate(string query, List<OleDbParameter> param)
        {
            OleDbCommand cmd = new OleDbCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (OleDbParameter p in param)
                {
                    cmd.Parameters.Add(new OleDbParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            return result;

        }

        public DataSet getSchema(string tableName)
        {
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM " + tableName + " WHERE 1<>1");
            cmd.Connection = conn;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public Int32 getGenerateKey(string tableName, string columnName)
        {
            DataSet ds = executeDataSet("SELECT MAX(" + columnName + ") FROM " + tableName);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            throw new Exception(String.Format("Not found generate key from table name:{0}, Column:{1}", tableName, columnName));
        }

        public void beginTrans()
        {
            if (conn == null) throw new Exception("Can't begin tranaction, Please initial connection before use.");
            tran = conn.BeginTransaction();
        }

        public void commitTrans()
        {
            if (conn == null && tran == null) throw new Exception("Can't commit tranaction, Please begin transaction before use.");
            if (tran != null)
            {
                tran.Commit();
                tran = null;
            }
        }

        public void rollbackTrans()
        {
            if (tran != null)
            {
                tran.Rollback();
                tran = null;
            }
        }

        public void closeCon()
        {
            if (tran != null)
            {
                rollbackTrans();
            }
            if (conn != null) conn.Close();
            conn = null;
        }


        /* Delete data for search page*/
        public void delete(string sqlDelete, string[] values)
        {
            executeUpdate(String.Format(sqlDelete, StringUtil.arrToString(values)), null);
        }

        public static void Close(AccessConnection conn)
        {
            if (conn != null) conn.closeCon();
        }

        public DataSet find(string unqid, string query, string columnName)
        {
            List<OleDbParameter> lstParams = new List<OleDbParameter>();
            lstParams.Add(new OleDbParameter(columnName, Int32.Parse(unqid)));
            DataSet ds = executeDataSet(query, lstParams);
            if (ds.Tables[0].Rows.Count == 1)
            {
                return ds;
            }
            else if (ds.Tables[0].Rows.Count > 1)
            {
                throw new Exception("Found multiple(" + ds.Tables[0].Rows.Count + ") record for UNIQ_ID:" + unqid);
            }
            else
            {
                throw new Exception("Not found record for UNIQ_ID:" + unqid);
            }
        }

        public void Dispose()
        {
            AccessConnection.Close(this);
        }

        public bool IsExistsData(string sql, params OleDbParameter[] param)
        {
            OleDbCommand command = new OleDbCommand(sql, conn);
            if (tran != null)
            {
                command.Transaction = tran;
            }

            if (param != null)
            {
                foreach (OleDbParameter p in param)
                {
                    command.Parameters.Add(new OleDbParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return Convert.ToInt32(command.ExecuteScalar())>0;
        }
    }
}
