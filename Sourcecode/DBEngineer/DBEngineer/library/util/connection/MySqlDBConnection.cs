﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using SendEmailService.library.util;
using MySql.Data.MySqlClient;

namespace DBEngineer.library.util
{
    public class MySqlDBConnection : IDisposable
    {
        public MySqlConnection conn;
        public MySqlTransaction tran;


        // Example : PGConnection conn = PGConnection.NewGWConnection;
        public static MySqlDBConnection DefaultConnection
        {
            get
            {
                MySqlDBConnection conn = new MySqlDBConnection(ConfigurationManager.ConnectionStrings["MySQLConnection"].ToString());
                return conn;
            }
        }

        public static MySqlDBConnection CustomConnection(string connections)
        {
            return new MySqlDBConnection(connections);
        }
        // New connection for Choose Database Connection "CRM","GW"
        // Example : PGConnection conn = new PGConnection(PGConnection.CRM);
        private MySqlDBConnection(string connectionstring)
        {
            openCon(connectionstring);
        }


        private void openCon(string connectionstring)
        {
            if (conn != null) throw new Exception("Connection not yet close, Please close connection before re-create.");

            conn = new MySqlConnection(connectionstring);
            conn.Open();
        }


        public DataSet executeDataSet(string query)
        {
            return executeDataSet(query, new List<MySqlParameter>());
        }

        public DataSet executeStoredProcedure(string storedName, Dictionary<string, object> lstParam)
        {
            using (MySqlCommand cmd = new MySqlCommand(storedName, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                if (tran != null)
                {
                    cmd.Transaction = tran;
                }

                if (lstParam != null)
                {
                    foreach (KeyValuePair<string, object> p in lstParam)
                    {
                        cmd.Parameters.Add("@" + p.Key, MySqlDbType.VarChar).Value = p.Value;
                        //cmd.Parameters.Add(new MySqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                    }
                }

                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                ds.Dispose();
                return ds;
            }
        }

        public DataSet executeDataSet(string query, MySqlParameter param)
        {
            MySqlCommand cmd = new MySqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                cmd.Parameters.Add(new MySqlParameter(param.ParameterName, param.Value));
            }
            MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public DataSet executeDataSet(string query, List<MySqlParameter> param)
        {
            MySqlCommand cmd = new MySqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (MySqlParameter p in param)
                {
                    cmd.Parameters.Add(new MySqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public object executeScalar(string query,params MySqlParameter[] param)
        {
            MySqlCommand cmd = new MySqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (MySqlParameter p in param)
                {
                    cmd.Parameters.Add(new MySqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            return cmd.ExecuteScalar();
        }

        public MySqlDataReader executeDataReader(string query, List<MySqlParameter> param)
        {
            MySqlCommand cmd = new MySqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (MySqlParameter p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            return cmd.ExecuteReader();
        }

        public string QueryDataString(string query, List<MySqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return null;
            }
        }

        public byte[] QueryDataBinary(string query, List<MySqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (byte[])ds.Tables[0].Rows[0][0];
            }
            else
            {
                return null;
            }
        }

        public decimal QueryDataDecimal(string query, List<MySqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (decimal)ds.Tables[0].Rows[0][0];
            }
            else
            {
                return 0;
            }
        }

        public int QueryDataInt(string query, List<MySqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public Int64? QueryDataInt64(string query, List<MySqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt64(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return null;
            }
        }


        public DateTime QueryDataDateTime(string query, List<MySqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return DateTime.MinValue;
            }
        }


        public int executeUpdate(string query, List<MySqlParameter> param, ref Int64 generateKey)
        {
            MySqlCommand cmd = new MySqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                //cmd.CommandText = "SET autocommit = 0";
                //cmd.ExecuteNonQuery();
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (MySqlParameter p in param)
                {
                    cmd.Parameters.Add(new MySqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            // MySql SERVER
            cmd.CommandText = "SELECT @@IDENTITY";
            generateKey = Convert.ToInt64(cmd.ExecuteScalar());

            // MYMySql MySql
            //generateKey =cmd.LastInsertedId;

            return result;

        }

        public int executeUpdate(string query)
        {
            return executeUpdate(query, null);
        }

        public int executeUpdate(string query, List<MySqlParameter> param)
        {
            MySqlCommand cmd = new MySqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (MySqlParameter p in param)
                {
                    cmd.Parameters.Add(new MySqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            return result;

        }

        public DataSet getSchema(string tableName)
        {
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM " + tableName + " WHERE 1<>1");
            cmd.Connection = conn;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public Int32 getGenerateKey(string tableName, string columnName)
        {
            DataSet ds = executeDataSet("SELECT MAX(" + columnName + ") FROM " + tableName);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            throw new Exception(String.Format("Not found generate key from table name:{0}, Column:{1}", tableName, columnName));
        }

        public void beginTrans()
        {
            if (conn == null) throw new Exception("Can't begin tranaction, Please initial connection before use.");
            tran = conn.BeginTransaction();
        }

        public void commitTrans()
        {
            if (conn == null && tran == null) throw new Exception("Can't commit tranaction, Please begin transaction before use.");
            if (tran != null)
            {
                tran.Commit();
                tran = null;
            }
        }

        public void rollbackTrans()
        {
            if (tran != null)
            {
                tran.Rollback();
                tran = null;
            }
        }

        public void closeCon()
        {
            if (tran != null)
            {
                rollbackTrans();
            }
            if (conn != null) conn.Close();
            conn = null;
        }


        /* Delete data for search page*/
        public void delete(string MySqlDelete, string[] values)
        {
            executeUpdate(String.Format(MySqlDelete, StringUtil.arrToString(values)), null);
        }

        public static void Close(MySqlDBConnection conn)
        {
            if (conn != null) conn.closeCon();
        }

        public DataSet find(string unqid, string query, string columnName)
        {
            List<MySqlParameter> lstParams = new List<MySqlParameter>();
            lstParams.Add(new MySqlParameter(columnName, Int32.Parse(unqid)));
            DataSet ds = executeDataSet(query, lstParams);
            if (ds.Tables[0].Rows.Count == 1)
            {
                return ds;
            }
            else if (ds.Tables[0].Rows.Count > 1)
            {
                throw new Exception("Found multiple(" + ds.Tables[0].Rows.Count + ") record for UNIQ_ID:" + unqid);
            }
            else
            {
                throw new Exception("Not found record for UNIQ_ID:" + unqid);
            }
        }

        public void Dispose()
        {
            MySqlDBConnection.Close(this);
        }

        public bool IsExistsData(string MySql, params MySqlParameter[] param)
        {
            MySqlCommand command = new MySqlCommand(MySql, conn);
            if (tran != null)
            {
                command.Transaction = tran;
            }

            if (param != null)
            {
                foreach (MySqlParameter p in param)
                {
                    command.Parameters.Add(new MySqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return Convert.ToInt32(command.ExecuteScalar())>0;
        }
    }
}
