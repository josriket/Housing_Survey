﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SendEmailService.library.util
{
    public class SQLServerConnection : IDisposable
    {
        public SqlConnection conn;
        public SqlTransaction tran;


        // Example : DBConnection conn = DBConnection.NewGWConnection;
        public static SQLServerConnection DefaultConnection
        {
            get
            {
                SQLServerConnection conn = new SQLServerConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ToString());
                return conn;
            }
        }

        public static SQLServerConnection CustomConnection(string connections)
        {
            return new SQLServerConnection(connections);
        }
        // New connection for Choose Database Connection "CRM","GW"
        // Example : DBConnection conn = new DBConnection(DBConnection.CRM);
        private SQLServerConnection(string connectionstring)
        {
            openCon(connectionstring);
        }


        private void openCon(string connectionstring)
        {
            if (conn != null) throw new Exception("Connection not yet close, Please close connection before re-create.");

            conn = new SqlConnection(connectionstring);
            conn.Open();
        }


        public DataSet executeDataSet(string query)
        {
            return executeDataSet(query, new List<SqlParameter>());
        }

        public DataSet executeStoredProcedure(string storedName, Dictionary<string, object> lstParam)
        {
            using (SqlCommand cmd = new SqlCommand(storedName, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                if (tran != null)
                {
                    cmd.Transaction = tran;
                }

                if (lstParam != null)
                {
                    foreach (KeyValuePair<string, object> p in lstParam)
                    {
                        cmd.Parameters.Add("@" + p.Key, SqlDbType.VarChar).Value = p.Value;
                        //cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                    }
                }

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                ds.Dispose();
                return ds;
            }
        }

        public DataSet executeDataSet(string query, SqlParameter param)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                cmd.Parameters.Add(new SqlParameter(param.ParameterName, param.Value));
            }
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public DataSet executeDataSet(string query, List<SqlParameter> param)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public object executeScalar(string query,params SqlParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            return cmd.ExecuteScalar();
        }

        public SqlDataReader executeDataReader(string query, List<SqlParameter> param)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            return cmd.ExecuteReader();
        }

        public string QueryDataString(string query, List<SqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return null;
            }
        }

        public byte[] QueryDataBinary(string query, List<SqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (byte[])ds.Tables[0].Rows[0][0];
            }
            else
            {
                return null;
            }
        }

        public decimal QueryDataDecimal(string query, List<SqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (decimal)ds.Tables[0].Rows[0][0];
            }
            else
            {
                return 0;
            }
        }

        public int QueryDataInt(string query, List<SqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public Int64? QueryDataInt64(string query, List<SqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt64(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return null;
            }
        }


        public DateTime QueryDataDateTime(string query, List<SqlParameter> lstParam)
        {
            DataSet ds = executeDataSet(query, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return DateTime.MinValue;
            }
        }


        public int executeUpdate(string query, List<SqlParameter> param, ref Int64 generateKey)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                //cmd.CommandText = "SET autocommit = 0";
                //cmd.ExecuteNonQuery();
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            // SQL SERVER
            cmd.CommandText = "SELECT @@IDENTITY";
            generateKey = Convert.ToInt64(cmd.ExecuteScalar());

            // MYSQL SQL
            //generateKey =cmd.LastInsertedId;

            return result;

        }

        public int executeUpdate(string query)
        {
            return executeUpdate(query, null);
        }

        public int executeUpdate(string query, List<SqlParameter> param)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            return result;

        }

        public DataSet getSchema(string tableName)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM " + tableName + " WHERE 1<>1");
            cmd.Connection = conn;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public Int32 getGenerateKey(string tableName, string columnName)
        {
            DataSet ds = executeDataSet("SELECT MAX(" + columnName + ") FROM " + tableName);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            throw new Exception(String.Format("Not found generate key from table name:{0}, Column:{1}", tableName, columnName));
        }

        public void beginTrans()
        {
            if (conn == null) throw new Exception("Can't begin tranaction, Please initial connection before use.");
            tran = conn.BeginTransaction();
        }

        public void commitTrans()
        {
            if (conn == null && tran == null) throw new Exception("Can't commit tranaction, Please begin transaction before use.");
            if (tran != null)
            {
                tran.Commit();
                tran = null;
            }
        }

        public void rollbackTrans()
        {
            if (tran != null)
            {
                tran.Rollback();
                tran = null;
            }
        }

        public void closeCon()
        {
            if (tran != null)
            {
                rollbackTrans();
            }
            if (conn != null) conn.Close();
            conn = null;
        }


        /* Delete data for search page*/
        public void delete(string sqlDelete, string[] values)
        {
            executeUpdate(String.Format(sqlDelete, StringUtil.arrToString(values)), null);
        }

        public static void Close(SQLServerConnection conn)
        {
            if (conn != null) conn.closeCon();
        }

        public DataSet find(string unqid, string query, string columnName)
        {
            List<SqlParameter> lstParams = new List<SqlParameter>();
            lstParams.Add(new SqlParameter(columnName, Int32.Parse(unqid)));
            DataSet ds = executeDataSet(query, lstParams);
            if (ds.Tables[0].Rows.Count == 1)
            {
                return ds;
            }
            else if (ds.Tables[0].Rows.Count > 1)
            {
                throw new Exception("Found multiple(" + ds.Tables[0].Rows.Count + ") record for UNIQ_ID:" + unqid);
            }
            else
            {
                throw new Exception("Not found record for UNIQ_ID:" + unqid);
            }
        }

        public void Dispose()
        {
            SQLServerConnection.Close(this);
        }

        public bool IsExistsData(string sql, params SqlParameter[] param)
        {
            SqlCommand command = new SqlCommand(sql, conn);
            if (tran != null)
            {
                command.Transaction = tran;
            }

            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    command.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return Convert.ToInt32(command.ExecuteScalar())>0;
        }
    }
}
