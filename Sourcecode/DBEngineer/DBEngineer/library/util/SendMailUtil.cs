﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Mail;
using System.IO;
using System.Data;
using SendEmailService.library.model;
using System.Configuration;

namespace SendEmailService.library.util
{
    public class SendMailUtil
    {
        private string emailTO;
        private string emailCC;
        private string subject;
        private string message;
        private List<AttachFile> lstStream;

        private SendMailUtil(string emailTO,string emailCC, string subject, string message, List<AttachFile> lstStream)
        {
            this.emailTO = emailTO;
            this.emailCC = emailCC;
            this.subject = subject;
            this.message = message;
            this.lstStream = lstStream;
        }

        public static void Send(string emailTO, string emailCC, string subject, string message, List<AttachFile> lstStream)
        {
            if (emailTO != null && emailTO.Length > 0)
            {
                SendMailUtil sender = new SendMailUtil(emailTO, emailCC, subject, message, lstStream);
                Thread oThread = new Thread(new ThreadStart(sender.Sendemail));
                oThread.Start();
            }
        }

        private void Sendemail()
        {
            string status = "";
            for (int i = 0; i < 5; i++)
            {
                status = TryToSend(i + 1);
                if (status.Equals("Success"))
                {
                    break;
                }
                else
                {
                    Thread.Sleep(1000 * 30);
                }
            }

        }

        public string TryToSend(int round)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["smtp-server"].ToString());
                mail.From = new MailAddress(ConfigurationManager.AppSettings["smtp-sender"].ToString(), ConfigurationManager.AppSettings["smtp-displayname"].ToString());
                
                foreach (string _emailTo in emailTO.Replace(",", ";").Replace(":", ";").Split(';'))
                {
                    if (!string.IsNullOrEmpty(_emailTo) && _emailTo.Trim().Length > 2)
                    {
                        mail.To.Add(new MailAddress(_emailTo.Trim(), _emailTo.Trim()));
                    }
                }
                foreach (string _emailCc in emailCC.Replace(",", ";").Replace(":", ";").Split(';'))
                {
                    if (!string.IsNullOrEmpty(_emailCc)&&_emailCc.Trim().Length>2)
                    {
                        mail.CC.Add(new MailAddress(_emailCc.Trim(), _emailCc.Trim()));
                    }
                }

                if (lstStream != null && lstStream.Count>0)
                {
                    foreach (AttachFile ms in lstStream)
                    {
                        Attachment attach = new Attachment(ms.Stream, ms.FileName);
                        mail.Attachments.Add(attach);
                    }
                }


                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;

                SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp-port"].ToString());
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["smtp-password"].ToString()))
                {
                    SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["smtp-sender"].ToString(), ConfigurationManager.AppSettings["smtp-password"].ToString());
                    SmtpServer.EnableSsl = true;
                }

                SmtpServer.Send(mail);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}