﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Collections;

namespace SendEmailService.library.util
{
    public class QueryUtil
    {
        public static void Find(SQLServerConnection conn, string tableName, string pkName, string value, object domain)
        {
            string sql = "select * from [" + tableName + "] with(nolock) where [" + pkName + "]=@PkValue";
            List<SqlParameter> lstParam = new List<SqlParameter>();
            lstParam.Add(new SqlParameter("PkValue", value));

            SetModel(conn.executeDataSet(sql, lstParam), domain);
        }

        public static void Find(SQLServerConnection conn, string sql, object domain)
        {
            SetModel(conn.executeDataSet(sql), domain);
        }


        public static void Find(SQLServerConnection conn, string sql, List<SqlParameter> lstParam, object domain)
        {
            SetModel(conn.executeDataSet(sql,lstParam), domain);
        }

        public static void SetModel(DataSet ds, object domain)
        {
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];
                SetModel(dt, row, domain);
            }
        }

        public static void SetModel(DataTable dt, DataRow row, object domain)
        {
            PropertyInfo[] properties = ReflectionUtil.getProperties(domain);
            foreach (PropertyInfo prop in properties)
            {
                if (dt.Columns.Contains(prop.Name) && row[prop.Name] != DBNull.Value)
                {
                    ReflectionUtil.setProperty(domain, prop.Name, row[prop.Name]);
                }
            }
        }

        public static List<object> FindList(SQLServerConnection conn, string tableName, string pkName, string value, string SortingBy, object template)
        {
            string sql = "select * from [" + tableName + "] where [" + pkName + "]=@PkValue " + SortingBy;
            List<SqlParameter> lstParam = new List<SqlParameter>();
            lstParam.Add(new SqlParameter("PkValue", value));
            DataSet ds = conn.executeDataSet(sql, lstParam);

            return SetList(ds, template);
        }

        public static List<T> SetList<T>(DataSet ds, T template)
        {
            List<T> Lst = new List<T>();
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    T domain = ObjectUtil.Clone<T>(template);

                    PropertyInfo[] properties = ReflectionUtil.getProperties(domain);
                    foreach (PropertyInfo prop in properties)
                    {
                        if (dt.Columns.Contains(prop.Name) && row[prop.Name] != DBNull.Value)
                        {
                            ReflectionUtil.setProperty(domain, prop.Name, row[prop.Name]);
                        }
                    }

                    Lst.Add(domain);
                }
            }
            return Lst;
        }

        public static List<object> FindList(SQLServerConnection conn, string sql, object template)
        {
            return SetList(conn.executeDataSet(sql), template);
        }

        public static List<T> FindList<T>(SQLServerConnection conn, string sql, List<SqlParameter> lstParam, T template)
        {
            return SetList(conn.executeDataSet(sql, lstParam), template);
        }

        public static List<T> FindList<T>(SQLServerConnection conn, string storedName, Dictionary<string, object> lstParam, T template)
        {
            DataSet dsResult = conn.executeStoredProcedure(storedName, lstParam);
            return SetList(dsResult, template);
        }

        public static byte[] GetByteData(SQLServerConnection conn, string tableName, string pkName, string value, string fieldName)
        {
            string sql = "select [" + fieldName + "] from [" + tableName + "] where [" + pkName + "]=@PkValue";
            List<SqlParameter> lstParam = new List<SqlParameter>();
            lstParam.Add(new SqlParameter("PkValue", value));
            DataSet ds = conn.executeDataSet(sql, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];

                if (row[fieldName] != DBNull.Value)
                {
                    return (byte[])row[fieldName];
                }
            }
            return null;
        }

        public static string GetStringData(SQLServerConnection conn, string tableName, string pkName, string value, string fieldName)
        {
            string sql = "select [" + fieldName + "] from [" + tableName + "] where [" + pkName + "]=@PkValue";
            List<SqlParameter> lstParam = new List<SqlParameter>();
            lstParam.Add(new SqlParameter("PkValue", value));
            DataSet ds = conn.executeDataSet(sql, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];

                if (row[fieldName] != DBNull.Value)
                {
                    return row[fieldName].ToString();
                }
            }
            return null;
        }

        public static Int64? GetInt64Data(SQLServerConnection conn, string tableName, string pkName, string value, string fieldName)
        {
            string sql = "select [" + fieldName + "] from [" + tableName + "]";
            if (!string.IsNullOrEmpty(pkName))
            {
                sql += " where [" + pkName + "]=@PkValue";
            }
            List<SqlParameter> lstParam = new List<SqlParameter>();
            lstParam.Add(new SqlParameter("PkValue", value));
            DataSet ds = conn.executeDataSet(sql, lstParam);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];

                if (row[fieldName] != DBNull.Value)
                {
                    return Convert.ToInt64(row[fieldName]);
                }
            }
            return null;
        }

        public static int Delete(SQLServerConnection conn, string tableName, string pkName, string[] ids)
        {
            string sql = "delete from [" + tableName + "] where [" + pkName + "] in(" + StringUtil.arrToStringSingleQuote(ids) + ")";
            return conn.executeUpdate(sql, null);
        }

        public static Dictionary<string, DataTable> _DtMapping = new Dictionary<string, DataTable>();
        public static long InsertOrUpdate(SQLServerConnection conn,string tableName, string pkName, object domain)
        {
            DataTable dt = null;
            if (_DtMapping.ContainsKey(tableName))
            {
                dt = _DtMapping[tableName];
            }
            else
            {
                dt = conn.executeDataSet("select * from [" + tableName + "] with(nolock) where 1<>1").Tables[0];
                try
                {
                    _DtMapping.Add(tableName, dt);
                }
                catch
                {
                }
            }

            List<string> colNames = new List<string>();
            List<string> colValues = new List<string>();
            List<string> sqlTmpUpdate = new List<string>();

            PropertyInfo[] properties = ReflectionUtil.getProperties(domain);
            List<SqlParameter> lstParam = new List<SqlParameter>();
            foreach (PropertyInfo prop in properties)
            {
                if (dt.Columns.Contains(prop.Name))
                {
                    object valSource = ReflectionUtil.getPropertyValue(domain, prop.Name);

                    bool isCommand = false;
                    if (!prop.Name.Equals(pkName))
                    {
                        isCommand = true;
                    }


                    if (isCommand)
                    {
                        colValues.Add("@" + prop.Name);
                        colNames.Add("@" + prop.Name);
                        sqlTmpUpdate.Add("[" + tableName + "]." + prop.Name + "=@" + prop.Name);
                    }

                    lstParam.Add(new SqlParameter(prop.Name, valSource == null ? DBNull.Value : valSource));
                }
            }
            Int64 genkey = 0;
            string sql = "update [" + tableName + "] set " + string.Join(",", sqlTmpUpdate) + " where [" + pkName + "]=@" + pkName;
            object pk = ReflectionUtil.GetPropertyValue(domain, pkName);
            if (conn.executeUpdate(sql, lstParam) < 1)
            {
                sql = "insert into [" + tableName + "](" + string.Join(",", colNames).Replace("@", "[" + tableName + "].") + ") VAlUES(" + string.Join(",", colValues) + ")";

                conn.executeUpdate(sql, lstParam, ref genkey);
                pk = genkey;
            }
            else
            {
                return Convert.ToInt64(pk);
            }

            return genkey;
        }

        public static bool IsExists(SQLServerConnection conn,string sql, params SqlParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(sql, conn.conn);
            if (conn.tran != null)
            {
                cmd.Transaction = conn.tran;
            }

            if (param != null)
            {
                foreach (SqlParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return (int)cmd.ExecuteScalar() > 0;
        }

    }
}
