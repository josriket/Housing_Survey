﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SendEmailService.library.util
{
    public class StringUtil
    {
        /**
	    * Convert string array to string by commas for database
	    */
        public static string ArrToString(String[] arr)
        {
            String ids = "";
            foreach (string s in arr) ids += ",'" + s + "'";

            if (!string.IsNullOrEmpty(ids))
            {
                ids = ids.Substring(1);
            }
            return ids;
        }

        /**
        * Convert string array to string by commas for database
        */
        public static string ArrNumberToString(String[] arr)
        {
            String ids = "";
            foreach (string s in arr) ids += "," + s;

            if (!string.IsNullOrEmpty(ids))
            {
                ids = ids.Substring(1);
            }
            return ids;
        }

        public static bool VerifyCitizenID(string citizenId)
        {
            try
            {
                //ตรวจสอบว่าทุก ๆ ตัวอักษรเป็นตัวเลข

                if (citizenId.ToCharArray().All(c => char.IsNumber(c)) == false)

                    return false;

                //ตรวจสอบว่าข้อมูลมีทั้งหมด 13 ตัวอักษร

                if (citizenId.Trim().Length != 13)

                    return false;



                int sumValue = 0;

                for (int i = 0; i < citizenId.Length - 1; i++)

                    sumValue += int.Parse(citizenId[i].ToString()) * (13 - i);

                int v = 11 - (sumValue % 11);
                v = v % 10;

                return citizenId[12].ToString() == v.ToString();
            }
            catch
            {
                return false;
            }
        }

        public static bool notEmpty(string input)
        {
            if (input == null || input.Trim().Length == 0)
            {
                return false;
            }
            return true;
        }

        public static string encdb(string input)
        {
            if (input != null)
            {
                return input.Trim().Replace("'", "''");
            }
            return null;
        }

        public static string arrToString(string[] arr)
        {
            String ids = "";
            foreach (String s in arr)
                ids += "," + Convert.ToInt64(s);
            if (!ids.Equals(""))
            {
                ids = ids.Substring(1);
            }
            return ids;
        }

        public static string arrToString(int[] arr)
        {
            String ids = "";
            foreach (int s in arr)
                ids += "," + Convert.ToInt64(s);
            if (!ids.Equals(""))
            {
                ids = ids.Substring(1);
            }
            return ids;
        }

        public static string arrToStringTXT(string[] arr)
        {
            String ids = "";
            foreach (String s in arr)
                ids += "," + s;
            if (!ids.Equals(""))
            {
                ids = ids.Substring(1);
            }
            return ids;
        }

        public static String arrToStringSingleQuote(String[] arr)
        {
            String ids = "";
            foreach (String s in arr)
            {
                ids += "','" + ReplaceSpecial(s);
            }
            if (!ids.Equals(""))
            {
                ids = ids.Substring(2);
                ids = ids + "'";
            }
            return ids;
        }

        public static string ReplaceSpecial(string CARD_NO)
        {
            return CARD_NO == null ? "" : CARD_NO.Replace("'", "''").Trim();
        }

        public static bool IsNumber(string text)
        {
            Regex regex = new Regex("[^0-9]");
            //Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(text);
        }

        public static string AlertMSG(string input)
        {
            return input.Replace("\r\n", "\\n").Replace("\n", "\\n").Replace("'", " ").Replace("\\u", "\\\\u");
            //return input.Replace("(?<!\r)\n", "\r\n");
        }


        public static string ScriptOnload(string p)
        {
            return "jQuery(document).ready(function () {" + p + "})";
        }

        public static string ToTitleCase(string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(str) && str.Length > 1)
            {
                return str.Substring(0, 1).ToUpper() + str.Substring(1).ToLower();
            }
            return str;
        }


        public static bool IsValidEngCharactor(string data)
        {
            char[] ch = data.ToCharArray();
            foreach (int c in ch)
            {
                if (c != ((int)' ') && (c < 65 || c > 90))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValidEngCharactorNonSpace(string data)
        {
            char[] ch = data.ToCharArray();
            foreach (int c in ch)
            {
                if ((c < 65 || c > 90))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValidEmail(string email)
        {
            bool isvalid = false;
            string errorMSG = string.Empty;
            try
            {

                string[] sp = email.Split('@');
                if (sp.Length == 2)
                {
                    string[] dot = sp[1].Split('.');
                    if (dot.Length > 1)
                    {
                        //isvalid = true;
                        isvalid = IsValidACIIEmail(email);
                    }
                }

            }
            catch (Exception ex)
            {
                errorMSG = ex.ToString();
                return false;
            }
            finally
            {
                if (!isvalid)
                {
                    //SysLogcss.Info("StringUtil", "IsValidEmail", email, null, errorMSG, null, null, null);
                }
            }

            return isvalid;
        }


        public static string RepaireEmail(string emails)
        {
            string s = "";
            foreach (int c in emails)
            {
                if (IsValidACIIChar(c))
                {
                    s += "" + (char)c;
                }
            }

            return s;
        }

        public static bool IsValidACIIEmail(string emails)
        {

            foreach (int c in emails)
            {
                if (!IsValidACIIChar(c))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsValidACIIChar(int c)
        {
            //48-57
            //45,46
            //64-90
            //95
            //97-122
            if ((c >= 48 && c <= 57)
                || (c >= 45 && c <= 46)
                || (c >= 64 && c <= 90)
                || (c >= 97 && c <= 122)
                || c == 95
                || c == 38)
            {
                return true;
            }
            return false;
        }


        public static bool IsValidThaiCharactor(string str)
        {
            foreach (int c in str)
            {
                if (c >= 32 && c <= 127)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValidThaiCharactorWithSpace(string str)
        {
            foreach (int c in str)
            {
                if (c >= 33 && c <= 127) // && c!=46) // 46(.)
                {
                    return false;
                }
            }
            return true;
        }

        public static string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            //sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }

            return sb.ToString();
        }

        public static string FormatInt(decimal? value, string OutPutNumberFormat)
        {
            // Return number with commas
            return value.HasValue ? string.Format(OutPutNumberFormat, value) : string.Empty;
        }

        public static string FormatDecimal(object value,string OutPutMoneyFormat)
        {
            // Return number with commas
            return (value != null && value != DBNull.Value) ? string.Format(OutPutMoneyFormat, value) : string.Empty;
        }

        public static Int32? ParseInt(string value)
        {
            // Convert input to nummber
            if (string.IsNullOrEmpty(value)) return null;

            value = value.Replace(",", "");
            return Convert.ToInt32(value);
        }

        public static decimal? ParseDecimal(string value)
        {
            // Convert input to nummber
            if (string.IsNullOrEmpty(value)) return null;

            value = value.Replace(",", "");
            return Convert.ToDecimal(value);
        }

        public static String FormatDateTime(object p, string OutPutDateTimeFormat, IFormatProvider CultureInfo)
        {
            if (p != DBNull.Value && p != null)
            {
                if (p.GetType() == typeof(DateTime))
                {
                    DateTime dt = (DateTime)p;
                    if (dt == DateTime.MinValue)
                    {
                        return "";
                    }
                    return dt.ToString(OutPutDateTimeFormat, CultureInfo);
                }
                else
                {
                    return p.ToString();
                }
            }
            else
            {
                return "";
            }
        }

        public static String FormatTime(object p, string OutPutTimeFormat, IFormatProvider CultureInfo)
        {
            if (p != DBNull.Value && p != null)
            {
                if (p.GetType() == typeof(DateTime))
                {
                    DateTime dt = (DateTime)p;
                    if (dt == DateTime.MinValue)
                    {
                        return "";
                    }
                    return dt.ToString(OutPutTimeFormat, CultureInfo);
                }
                else
                {
                    return p.ToString();
                }
            }
            else
            {
                return "";
            }
        }



        public static DateTime? ParseTime(DateTime? ActivateDate, string data, string OutPutTimeFormat, IFormatProvider CultureInfo)
        {
            try
            {
                if (StringUtil.notEmpty(data) && ActivateDate.HasValue)
                {
                    DateTime _TmpDate = DateTime.ParseExact(data, OutPutTimeFormat, CultureInfo);
                    ActivateDate=ActivateDate.Value.AddHours(_TmpDate.Hour);
                    ActivateDate = ActivateDate.Value.AddMinutes(_TmpDate.Minute);
                    ActivateDate = ActivateDate.Value.AddSeconds(_TmpDate.Second);

                    return ActivateDate;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static string FormatDate(object p, string OutPutDateFormat, IFormatProvider CultureInfo)
        {
            if (p != DBNull.Value && p != null)
            {
                if (p.GetType() == typeof(DateTime))
                {
                    DateTime dt = (DateTime)p;
                    if (dt == DateTime.MinValue)
                    {
                        return "";
                    }
                    return dt.ToString(OutPutDateFormat, CultureInfo);
                }
                else
                {
                    return p.ToString();
                }
            }
            else
            {
                return "";
            }
        }

        public static DateTime? ParseDate(string data, string OutPutDateFormat, IFormatProvider CultureInfo)
        {
            try
            {
                if (StringUtil.notEmpty(data))
                {
                    return DateTime.ParseExact(data, OutPutDateFormat, CultureInfo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static string ThaiBaht(decimal amount)
        {
            string Prefix = string.Empty;
            if (amount < 0)
            {
                amount = -1 * amount;
                Prefix = "ลบ";
            }
            string bahtTxt, n, bahtTH = "";
            bahtTxt = amount.ToString("####.00");
            string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
            string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
            string[] temp = bahtTxt.Split('.');
            string intVal = temp[0];
            string decVal = temp[1];
            if (Convert.ToDouble(bahtTxt) == 0)
            {
                bahtTH = "ศูนย์บาทถ้วน";
            }
            else
            {
                for (int i = 0; i < intVal.Length; i++)
                {
                    n = intVal.Substring(i, 1);
                    if (n != "0")
                    {
                        if ((i == (intVal.Length - 1)) && (n == "1"))
                            bahtTH += "เอ็ด";
                        else if ((i == (intVal.Length - 2)) && (n == "2"))
                            bahtTH += "ยี่";
                        else if ((i == (intVal.Length - 2)) && (n == "1"))
                            bahtTH += "";
                        else
                            bahtTH += num[Convert.ToInt32(n)];
                        bahtTH += rank[(intVal.Length - i) - 1];
                    }
                }

                bahtTH += "บาท";
                if (decVal == "00")
                    bahtTH += "ถ้วน";
                else
                {
                    for (int i = 0; i < decVal.Length; i++)
                    {
                        n = decVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == decVal.Length - 1) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (decVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (decVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(decVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "สตางค์";
                }
            }
            return Prefix+bahtTH;
        }

        public static string ToStringBase64(byte[] TakeImage)
        {
            return TakeImage==null?null: Convert.ToBase64String(TakeImage,Base64FormattingOptions.InsertLineBreaks);
        }

        public static byte[] Base64ToByteArray(string value)
        {
            return string.IsNullOrEmpty(value)? null: Convert.FromBase64String(value);
        }


    }
}
