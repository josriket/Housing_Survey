﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mime;

namespace SendEmailService.library.model
{
    public class AttachFile
    {
        public Stream Stream { get; set; }
        public string FileName { get; set; }
        public ContentType ContentType { get; set; }

        public AttachFile(byte[] byteData, string fileName, string fileType)
        {
            this.Stream = new MemoryStream(byteData);
            this.FileName = fileName;
            this.ContentType = GetContentType(fileType);
        }

        public static ContentType GetContentType(string type)
        {
            string contentType = string.Empty;
            if (type.ToUpper().Equals("XLS"))
            {
                contentType = "application/vnd.ms-excel";
            }
            else if (type.ToUpper().Equals("JPG"))
            {
                contentType = "image/jpeg";
            }
            else if (type.ToUpper().Equals("PDF"))
            {
                contentType = "application/pdf";
            }
            return new ContentType(contentType);
        }
    }
}