﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SendEmailService.library.util;

namespace DBEngineer.library.service
{
    public class SQLServerRebuildStandardFK
    {
        public void StartRebuild(bool isDrop, bool isCreate)
        {
            Console.Write("Please enter to start..");
            Console.ReadLine();
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                if (isDrop)
                {
                    DropFK(conn);
                }

                if (isCreate)
                {
                    CreateFK(conn);
                }

            }
            Console.WriteLine("\nRebuild FK completed, please enter to exit..");
            Console.ReadLine();
        }

        public void CreateFK(SQLServerConnection conn)
        {
            string sql = @"select *
                        from INFORMATION_SCHEMA.COLUMNS
                        where COLUMN_NAME in('ActiveStatusId','CreatedBy','ModifiedBy')
                        and TABLE_NAME+'Id'<>COLUMN_NAME
                        order by TABLE_NAME";

            DataSet ds = conn.executeDataSet(sql);

            string TABLE_NAME = null;
            string COLUMN_NAME = null;
            string FK_TableName = null;
            string FK_PKName = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                TABLE_NAME = row["TABLE_NAME"].ToString();
                COLUMN_NAME = row["COLUMN_NAME"].ToString();

                if (COLUMN_NAME == "CreatedBy")
                {
                    FK_TableName = "SystemUser";
                    FK_PKName = "SystemUserId";
                }
                else if (COLUMN_NAME == "ModifiedBy")
                {
                    FK_TableName = "SystemUser";
                    FK_PKName = "SystemUserId";
                }
                else if (COLUMN_NAME == "ActiveStatusId")
                {
                    FK_TableName = "ActiveStatus";
                    FK_PKName = "ActiveStatusId";
                }
                Console.WriteLine("- Create FK_{0}_{1}", TABLE_NAME, COLUMN_NAME);

                sql = string.Format("ALTER TABLE [dbo].[{0}]  WITH CHECK ADD  CONSTRAINT [FK_{0}_{1}] FOREIGN KEY([{1}]) REFERENCES [dbo].[{2}] ([{3}])"
                    , TABLE_NAME, COLUMN_NAME, FK_TableName, FK_PKName);
                conn.executeUpdate(sql);

                sql = string.Format("ALTER TABLE [dbo].[{0}] CHECK CONSTRAINT [FK_{0}_{1}]", TABLE_NAME, COLUMN_NAME);
                conn.executeUpdate(sql);
            }
        }

        public void DropFK(SQLServerConnection conn)
        {
            string sql = @"SELECT RC.CONSTRAINT_NAME FK_Name
                                , KF.TABLE_SCHEMA FK_Schema
                                , KF.TABLE_NAME FK_Table
                                , KF.COLUMN_NAME FK_Column
                                , RC.UNIQUE_CONSTRAINT_NAME PK_Name
                                , KP.TABLE_SCHEMA PK_Schema
                                , KP.TABLE_NAME PK_Table
                                , KP.COLUMN_NAME PK_Column
                                , RC.MATCH_OPTION MatchOption
                                , RC.UPDATE_RULE UpdateRule
                                , RC.DELETE_RULE DeleteRule
                                FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC
                                JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KF ON RC.CONSTRAINT_NAME = KF.CONSTRAINT_NAME
                                JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KP ON RC.UNIQUE_CONSTRAINT_NAME = KP.CONSTRAINT_NAME
                                where KF.COLUMN_NAME in('ActiveStatusId','CreatedBy','ModifiedBy')";
            DataSet ds = conn.executeDataSet(sql);

            string FK_Table = null;
            string FK_Name = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                FK_Table = row["FK_Table"].ToString();
                FK_Name = row["FK_Name"].ToString();

                Console.WriteLine("- Drop FK:" + FK_Name);

                sql = string.Format("ALTER TABLE [dbo].[{0}] DROP CONSTRAINT [{1}]", FK_Table, FK_Name);
                conn.executeUpdate(sql);
            }
        }


        public void DropFullFK()
        {
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                string sql = @"SELECT RC.CONSTRAINT_NAME FK_Name
                                , KF.TABLE_SCHEMA FK_Schema
                                , KF.TABLE_NAME FK_Table
                                , KF.COLUMN_NAME FK_Column
                                , RC.UNIQUE_CONSTRAINT_NAME PK_Name
                                , KP.TABLE_SCHEMA PK_Schema
                                , KP.TABLE_NAME PK_Table
                                , KP.COLUMN_NAME PK_Column
                                , RC.MATCH_OPTION MatchOption
                                , RC.UPDATE_RULE UpdateRule
                                , RC.DELETE_RULE DeleteRule
                                FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC
                                JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KF ON RC.CONSTRAINT_NAME = KF.CONSTRAINT_NAME
                                JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KP ON RC.UNIQUE_CONSTRAINT_NAME = KP.CONSTRAINT_NAME";
                DataSet ds = conn.executeDataSet(sql);

                string FK_Table = null;
                string FK_Name = null;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    FK_Table = row["FK_Table"].ToString();
                    FK_Name = row["FK_Name"].ToString();

                    Console.WriteLine("- Drop FK:" + FK_Name);

                    sql = string.Format("ALTER TABLE [dbo].[{0}] DROP CONSTRAINT [{1}]", FK_Table, FK_Name);
                    conn.executeUpdate(sql);
                }
            }
        }

        public void DropAllTable()
        {
            Console.Write("Please enter to start..");
            Console.ReadLine();
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                DataSet ds = null;
                string sql = null;
                while (true)
                {
                    sql = "SELECT TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME not in('sysdiagrams')";
                    ds = conn.executeDataSet(sql);
                    if (DataSetUtil.IsNotEmpty(ds))
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            try
                            {
                                string name = row["TABLE_NAME"].ToString();
                                sql = "DROP TABLE [dbo].[" + name + "]";
                                Console.WriteLine(sql);

                                conn.executeUpdate(sql);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("DropFail->" + ex.ToString());
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                sql = "SELECT name FROM dbo.sysobjects WHERE (type = 'P')";
                ds = conn.executeDataSet(sql);
                if (DataSetUtil.IsNotEmpty(ds))
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        try
                        {
                            string name = row["name"].ToString();
                            sql = "DROP PROCEDURE [dbo].[" + name + "]";
                            Console.WriteLine(sql);

                            conn.executeUpdate(sql);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("DropFail->" + ex.ToString());
                        }
                    }
                }
            }
        }
    }
}