﻿using DBEngineer.library.model;
using DBEngineer.library.util;
using SendEmailService.library.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.service
{
    public class CompairSchemaService
    {
        public void Start()
        {
            try
            {
                List<string> TargetTable = new List<string>();
                List<string> DestinationTable = new List<string>();
                string connectionString = null;

                connectionString = AppCofig.GetConnectionString("DB_SRC");
                Console.WriteLine("Source Database is: " + connectionString);
                Console.Write("Please enter to load source of table schema : ");
                Console.ReadLine();

                SQLServerConnection connSrc = SQLServerConnection.CustomConnection(connectionString);
                TargetTable = GetListOfTables(connSrc, null);
                Dictionary<string, ColumnInfo> srcInfo = GetSchemaInfo(connSrc, TargetTable);
                Console.WriteLine("Load source of table completed");
                Console.WriteLine("");

                connectionString = AppCofig.GetConnectionString("DB_DES");
                Console.WriteLine("Taget Database is: " + connectionString);
                Console.Write("Please enter to load taget of table schema : ");
                Console.ReadLine();
                SQLServerConnection connDes = SQLServerConnection.CustomConnection(connectionString);
                DestinationTable = GetListOfTables(connDes, null);
                Dictionary<string, ColumnInfo> desInfo = GetSchemaInfo(connDes, DestinationTable);
                Console.WriteLine("Load destination of table completed");
                Console.WriteLine("");

                Console.Write("Please enter to begin compair : ");
                Console.ReadLine();

                StringBuilder alterScript = new StringBuilder();
                foreach (KeyValuePair<string, ColumnInfo> pair in srcInfo)
                {
                    if (desInfo.ContainsKey(pair.Key))
                    {
                        desInfo.Remove(pair.Key);
                    }
                    else
                    {
                        Console.WriteLine("Missing: " + pair.Key);

                        alterScript.AppendLine(CreateAlterScript(pair.Key));
                    }
                }

                foreach (KeyValuePair<string, ColumnInfo> pair in desInfo)
                {
                    Console.WriteLine("Above: " + pair.Key);
                }

                Console.WriteLine("Compair Schema success.. Please enter to exit");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Application error, please enter to exit : ");
                Console.ReadLine();
            }
        }

        private string CreateAlterScript(string p)
        {
            return string.Empty;
        }

        public Dictionary<string, ColumnInfo> GetSchemaInfo(SQLServerConnection conn, List<string> TargetTable)
        {
            Dictionary<string, ColumnInfo> allInfo = new Dictionary<string, ColumnInfo>();

            foreach (string TableName in TargetTable)
            {
                string sql = "SELECT column_name,is_nullable FROM information_schema.columns WHERE table_schema='dbo' AND table_name='" + TableName + "'";
                DataSet ds = conn.executeDataSet(sql);
                Dictionary<String, bool> ColumnNullable = new Dictionary<string, bool>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ColumnNullable.Add(row["column_name"].ToString(), row["is_nullable"].ToString().Equals("YES"));
                }


                sql = "select * From " + "[" + TableName + "] where 1<>1";
                SqlDataReader reader = null;
                try
                {
                    reader = conn.executeDataReader(sql, null);
                    Console.WriteLine("Load Schema Infor: " + TableName);
                    DataTable tableInfo = reader.GetSchemaTable();

                    foreach (DataRow row in tableInfo.Rows)
                    {
                        string ColumnName = row["ColumnName"].ToString();
                        string DataType = row["DataType"].ToString().Replace("System.", "");
                        bool AllowDBNull = ColumnNullable[ColumnName];
                        int ColumnSize = Convert.ToInt32(row["ColumnSize"]);
                        if (DataType == "DateTime" && ColumnSize == 4)
                        {
                            DataType = "Date";
                        }

                        ColumnInfo colInfo = new ColumnInfo();
                        colInfo.TableName = TableName;
                        colInfo.ColumnName = ColumnName;
                        colInfo.DataType = DataType;
                        colInfo.AllowDBNull = AllowDBNull;
                        colInfo.ColumnSize = ColumnSize;

                        allInfo.Add(colInfo.TableName + " - " + colInfo.ColumnName, colInfo);
                    }
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }
            return allInfo;
        }

        public List<string> GetListOfTables(SQLServerConnection conn, List<string> IgnoreTable)
        {
            string sql = @"select TABLE_NAME 
                    from INFORMATION_SCHEMA.TABLES 
                    where TABLE_SCHEMA='dbo'
                    and table_name not like 'sysdiagrams'
                    ";
            if (IgnoreTable != null && IgnoreTable.Count > 0)
            {
                sql += string.Format(" and TABLE_NAME not in({0})", string.Join(",", IgnoreTable));
            }
            sql += " order by TABLE_NAME";

            DataSet ds = conn.executeDataSet(sql);
            List<string> lstTable = new List<string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                lstTable.Add(row["TABLE_NAME"].ToString());
            }

            return lstTable;
        }
    }
}
