﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DBEngineer.library.util;
using SendEmailService.library.util;

namespace DBEngineer.library.service
{
    public class SQLserverToPgSQLService
    {
        public void StartReverseEngineering()
        {
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                using (PGConnection pgConn = PGConnection.DefaultConnection)
                {
                    HashSet<string> ExistingTable = GetPGExistingTable(pgConn);
                    DataSet dsTable = conn.executeDataSet("select name From sys.tables where [type]='U' and name<>'sysdiagrams' order by name");
                    foreach (DataRow _row in dsTable.Tables[0].Rows)
                    {
                        string tableName = _row["name"].ToString();
                        if (!ExistingTable.Contains(tableName.ToLower()))
                        {
                            Console.WriteLine(" - generate " + tableName);
                            CreateSequence(pgConn, tableName);

                            CreateTable(conn, pgConn, tableName);
                        }
                        else
                        {
                            Console.WriteLine(" - Existing " + tableName);
                        }
                    }
                }
            }
            Console.WriteLine("\nGenerate completed, please enter to exit..");
            Console.ReadLine();
        }

        private HashSet<string> GetPGExistingTable(PGConnection pgConn)
        {
            string sql = @"SELECT tablename FROM pg_catalog.pg_tables where schemaname='public' order by tablename";
            HashSet<string> result = new HashSet<string>();
            DataSet ds = pgConn.executeDataSet(sql);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                result.Add(row["tablename"].ToString().ToLower());
            }

            return result;

        }

        private void CreateTable(SQLServerConnection Conn ,PGConnection PgConn, string TableName)
        {
            string sequenceName = GetSequenceName(TableName);

            string sql=string.Empty;

            sql+="CREATE TABLE public.\"{0}\"";
            sql += "\n(\"{0}Id\" bigint NOT NULL DEFAULT nextval('\"" + sequenceName + "\"'::regclass),";

            sql += "\n" + GetColumnScript(Conn, TableName);


            sql += "\nCONSTRAINT \"{0}PKId\" PRIMARY KEY (\"{0}Id\")";
            sql += "\n)WITH (OIDS=FALSE);";
            sql = string.Format(sql, TableName);
            PgConn.executeUpdate(sql);

            sql = "ALTER TABLE public.\"" + TableName + "\" OWNER TO postgres;";
            PgConn.executeUpdate(string.Format(sql, sequenceName));

        }

        private string GetColumnScript(SQLServerConnection Conn, string TableName)
        {

            string _sqlTmp = string.Format(@"select COLUMN_NAME,IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,NUMERIC_SCALE
                            --select * 
                             from INFORMATION_SCHEMA.COLUMNS 
                             where table_name='{0}'
                             and ORDINAL_POSITION<>1
                             order by ORDINAL_POSITION", TableName);
            DataSet ds = Conn.executeDataSet(_sqlTmp);

            StringBuilder sql = new StringBuilder();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string ColumnName = row["COLUMN_NAME"].ToString();
                bool IsNullable = row["IS_NULLABLE"].ToString().Equals("YES");
                string DataType = row["DATA_TYPE"].ToString();
                int MaxLength = row["CHARACTER_MAXIMUM_LENGTH"] == DBNull.Value ? 0 : Convert.ToInt32(row["CHARACTER_MAXIMUM_LENGTH"]);
                int NumericPrecision = row["NUMERIC_PRECISION"] == DBNull.Value ? 0 : Convert.ToInt32(row["NUMERIC_PRECISION"]);
                int NumericScale = row["NUMERIC_SCALE"] == DBNull.Value ? 0 : Convert.ToInt32(row["NUMERIC_SCALE"]);

                sql.Append("\"" + ColumnName + "\" ");

                if (DataType.Equals("varchar"))
                {
                    if (MaxLength == -1)
                    {
                        sql.Append(" text");
                    }
                    else
                    {
                        sql.Append(" character varying(" + MaxLength + ")");
                    }
                }
                else if (DataType.Equals("int"))
                {
                    sql.Append(" integer");
                }
                else if (DataType.Equals("bigint"))
                {
                    sql.Append(" bigint");
                }
                else if (DataType.Equals("decimal"))
                {
                    sql.Append(" numeric(" + NumericPrecision + "," + NumericScale + ")");
                }
                else if (DataType.Equals("datetime"))
                {
                    sql.Append(" timestamp without time zone");
                }
                else if (DataType.Equals("date"))
                {
                    sql.Append(" date");
                }
                else if (DataType.Equals("time"))
                {
                    sql.Append(" time without time zone");
                }
                else if (DataType.Equals("varbinary"))
                {
                    sql.Append(" bytea");
                }
                else if (DataType.Equals("uniqueidentifier"))
                {
                    sql.Append(" uuid");
                }
                else if (DataType.Equals("bit"))
                {
                    sql.Append(" boolean");
                }
                else if (DataType.Equals("float"))
                {
                    sql.Append(" float");
                }
                else
                {
                    throw new Exception("Unknow data type for "+DataType);
                }

                if (!IsNullable)
                {
                    sql.Append(" NOT NULL");
                }
                sql.Append(",");
                sql.Append("\n");
            }

            return sql.ToString();
        }

        private void DropTable(PGConnection PgConn, string TableName)
        {
            string sql = string.Format("DROP TABLE IF EXISTS public.\"{0}\";",TableName);
            PgConn.executeUpdate(sql);
        }


        private string GetSequenceName(string TableName){
        
            return string.Format("{0}Id_Seq", TableName);
        }

        private string CreateSequence(PGConnection PgConn, string TableName)
        {
            DropTable(PgConn, TableName);

            //Case_CaseId_seq
            string sequenceName = GetSequenceName(TableName);

            string sql = string.Format("DROP SEQUENCE IF EXISTS public.\"{0}\";", sequenceName);
            PgConn.executeUpdate(sql);

            sql = "CREATE SEQUENCE public.\"{0}\"";
            sql += "\nINCREMENT 1";
            sql += "\nMINVALUE 1";
            sql += "\nMAXVALUE 9223372036854775807";
            sql += "\nSTART 1";
            sql += "\nCACHE 1;";

            PgConn.executeUpdate(string.Format(sql, sequenceName));

            sql = "ALTER TABLE public.\"{0}\"";
            sql += "\nOWNER TO postgres;";
            PgConn.executeUpdate(string.Format(sql, sequenceName));

            return sequenceName;
        }
    }
}
