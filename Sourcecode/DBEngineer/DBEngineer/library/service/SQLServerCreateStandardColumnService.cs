﻿using SendEmailService.library.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.service
{
    public class SQLServerCreateStandardColumnService
    {
        public void Start()
        {
            List<string> StandardColumn = new List<string>() { "ActiveStatusId", "CreatedOn", "CreatedBy", "ModifiedOn","ModifiedBy" };

            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                HashSet<string> TBColumn = LoadColumn(conn);
                string sql = "SELECT TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_NAME not in('sysdiagrams')";
                DataSet ds = conn.executeDataSet(sql);
                if (DataSetUtil.IsNotEmpty(ds))
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string tableName = row["TABLE_NAME"].ToString();

                        foreach (string col in StandardColumn)
                        {
                            if (!TBColumn.Contains(tableName.ToUpper() + "__" + col.ToUpper()))
                            {
                                if (col == "ActiveStatusId")
                                {
                                    sql = "Alter Table {0} Add ActiveStatusId bigint not null";
                                }
                                else if (col == "CreatedOn")
                                {
                                    sql = "Alter Table {0} Add CreatedOn datetime not null";
                                }
                                else if (col == "CreatedBy")
                                {
                                    sql = "Alter Table {0} Add CreatedBy bigint not null";
                                }
                                else if (col == "ModifiedOn")
                                {
                                    sql = "Alter Table {0} Add ModifiedOn datetime";
                                }
                                else if (col == "ModifiedBy")
                                {
                                    sql = "Alter Table {0} Add ModifiedBy bigint";
                                }

                                conn.executeUpdate(string.Format(sql, tableName));
                            }
                        }
                    }
                }
            }
        }

        private HashSet<string> LoadColumn(SQLServerConnection conn)
        {
            HashSet<string> hash = new HashSet<string>();
            string sql = @"Select TABLE_NAME+'__'+COLUMN_NAME ColumnInfo from INFORMATION_SCHEMA.COLUMNS";

            DataSet ds = conn.executeDataSet(sql);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                hash.Add(row["ColumnInfo"].ToString().ToUpper());
            }

            return hash;
        }
    }
}