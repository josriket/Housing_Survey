﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DBEngineer.library.util;
using MySql.Data.MySqlClient;
using SendEmailService.library.util;

namespace DBEngineer.library.service
{
    public class SQLServerToMySqlService
    {
        public void StartReverseEngineering()
        {
            try
            {
                Console.WriteLine("ReverseEngineering from SQLServer to MySq start..");
                using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
                {
                    using (MySqlDBConnection mysqlConn = MySqlDBConnection.DefaultConnection)
                    {

                        DropAllForeignKey(mysqlConn);

                        DataSet dsTable = conn.executeDataSet("select name From sys.tables where [type]='U' and name<>'sysdiagrams' and name not like '[_]%'  order by name");
                        foreach (DataRow _row in dsTable.Tables[0].Rows)
                        {
                            string tableName = _row["name"].ToString();
                            Console.WriteLine(" - drop table : " + tableName);
                            DropTableIIfExists(mysqlConn, tableName);

                        }

                        foreach (DataRow _row in dsTable.Tables[0].Rows)
                        {
                            string tableName = _row["name"].ToString();
                            Console.WriteLine(" - Create Table : " + tableName);
                            CreateTable(conn, mysqlConn, tableName);
                        }

                        foreach (DataRow _row in dsTable.Tables[0].Rows)
                        {
                            string tableName = _row["name"].ToString();
                            Console.WriteLine(" - Copy data : " + tableName);
                            CopyData(conn, mysqlConn, tableName);
                        }

                        foreach (DataRow _row in dsTable.Tables[0].Rows)
                        {
                            string tableName = _row["name"].ToString();
                            Console.WriteLine(" - Initial FK : " + tableName);
                            GenerateForeignKey(conn, mysqlConn, tableName);
                        }
                    }
                }
                Console.WriteLine("\nGenerate completed, please enter to exit..");
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }

        private void CreateTable(SQLServerConnection Conn, MySqlDBConnection mysqlConn, string TableName)
        {
            string sql = string.Empty;

            sql += "CREATE TABLE `{0}` (";
            sql += "\n`{0}Id` bigint(20) NOT NULL AUTO_INCREMENT,";

            sql += "\n" + GetColumnScript(Conn, TableName);


            sql += "PRIMARY KEY (`{0}Id`)";
            sql += "\n) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            sql = string.Format(sql, TableName);
            mysqlConn.executeUpdate(sql);
        }

        private void GenerateForeignKey(SQLServerConnection Conn,MySqlDBConnection mysqlConn, string TableName)
        {
            DataSet dsFk = Conn.executeDataSet(@"SELECT   
                                                                   OBJECT_NAME(f.parent_object_id) AS table_name  
                                                                   ,COL_NAME(fc.parent_object_id, fc.parent_column_id) AS constraint_column_name  
                                                                   ,OBJECT_NAME (f.referenced_object_id) AS referenced_object  
                                                                   ,COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS referenced_column_name  
                                                                   ,f.delete_referential_action_desc
                                                                FROM sys.foreign_keys AS f  
                                                                INNER JOIN sys.foreign_key_columns AS fc   
                                                                   ON f.object_id = fc.constraint_object_id   
                                                                WHERE OBJECT_NAME(f.parent_object_id)='" + TableName + "'");

            string sql = null;
            foreach (DataRow rowFk in dsFk.Tables[0].Rows)
            {
                string constraint_column_name = rowFk["constraint_column_name"].ToString();
                string referenced_object = rowFk["referenced_object"].ToString();
                string referenced_column_name = rowFk["referenced_column_name"].ToString();
                string delete_referential_action_desc = rowFk["delete_referential_action_desc"].ToString();

                string FKName = "FK_" + TableName + "_" + constraint_column_name;

                sql = string.Format("ALTER TABLE `{0}` ADD CONSTRAINT `{1}` FOREIGN KEY (`{2}`) REFERENCES `{3}`(`{4}`) ON DELETE {5};"
                    , TableName
                    , FKName
                    , constraint_column_name
                    , referenced_object
                    , referenced_column_name
                    , delete_referential_action_desc.ToString().Replace("_", " ")
                    );

                mysqlConn.executeUpdate(sql);

                string IndexName = "Index_" + TableName + "_" + constraint_column_name;
                sql = string.Format("ALTER TABLE `{0}` ADD KEY `{1}` (`{2}`)", TableName, IndexName, constraint_column_name);

                mysqlConn.executeUpdate(sql);

            }

        }

        private string GetColumnScript(SQLServerConnection Conn, string TableName)
        {

            string _sqlTmp = string.Format(@"select COLUMN_NAME,IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,NUMERIC_SCALE
                             from INFORMATION_SCHEMA.COLUMNS 
                             where table_name='{0}'
                             and ORDINAL_POSITION<>1
                             order by ORDINAL_POSITION", TableName);
            DataSet ds = Conn.executeDataSet(_sqlTmp);

            StringBuilder sql = new StringBuilder();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string ColumnName = row["COLUMN_NAME"].ToString();
                bool IsNullable = row["IS_NULLABLE"].ToString().Equals("YES");
                string DataType = row["DATA_TYPE"].ToString();
                int MaxLength = row["CHARACTER_MAXIMUM_LENGTH"] == DBNull.Value ? 0 : Convert.ToInt32(row["CHARACTER_MAXIMUM_LENGTH"]);
                int NumericPrecision = row["NUMERIC_PRECISION"] == DBNull.Value ? 0 : Convert.ToInt32(row["NUMERIC_PRECISION"]);
                int NumericScale = row["NUMERIC_SCALE"] == DBNull.Value ? 0 : Convert.ToInt32(row["NUMERIC_SCALE"]);

                sql.Append("`" + ColumnName + "` ");

                if (DataType.Equals("varchar"))
                {
                    if (MaxLength < 0)
                    {
                        sql.Append(" longtext");
                    }
                    else
                    {
                        sql.Append(" varchar(" + MaxLength + ")");
                    }
                }
                else if (DataType.Equals("int"))
                {
                    sql.Append(" int(11)");
                }
                else if (DataType.Equals("bigint"))
                {
                    sql.Append(" bigint(20)");
                }
                else if (DataType.Equals("decimal"))
                {
                    sql.Append(" decimal(" + NumericPrecision + "," + NumericScale + ")");
                }
                else if (DataType.Equals("datetime"))
                {
                    sql.Append(" datetime");
                }
                else if (DataType.Equals("date"))
                {
                    sql.Append(" date");
                }
                else if (DataType.Equals("time"))
                {
                    sql.Append(" time");
                }
                else if (DataType.Equals("varbinary"))
                {
                    sql.Append(" longblob");
                }
                else if (DataType.Equals("uniqueidentifier"))
                {
                    sql.Append(" varchar(64)");
                }
                else if (DataType.Equals("bit"))
                {
                    sql.Append(" BOOLEAN");
                }
                else
                {
                    throw new Exception("Unknow data type for " + DataType);
                }

                if (!IsNullable)
                {
                    sql.Append(" NOT NULL");
                }
                sql.Append(",");
                sql.Append("\n");
            }

            return sql.ToString();
        }

        private void DropTableIIfExists(MySqlDBConnection mysqlConn, string TableName)
        {
            string sql = string.Format("DROP TABLE IF EXISTS `{0}`;", TableName);
            mysqlConn.executeUpdate(sql);
        }

        public void DropAllForeignKey(MySqlDBConnection mysqlConn)
        {
            DataSet ds = mysqlConn.executeDataSet(@"SELECT CONCAT('ALTER TABLE ', TABLE_NAME, ' DROP FOREIGN KEY ', CONSTRAINT_NAME, ';') Command
                                        ,TABLE_NAME
                                        FROM information_schema.key_column_usage 
                                        WHERE 1=1
                                        AND CONSTRAINT_SCHEMA = 'BoatTicketDB' 
                                        AND REFERENCED_TABLE_NAME IS NOT NULL");
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Console.WriteLine(" - drop FK : " + row["TABLE_NAME"].ToString());

                string command = row["Command"].ToString();
                mysqlConn.executeUpdate(command);
            }
        }

        public void CopyData(SQLServerConnection Conn, MySqlDBConnection mysqlConn, string tableName)
        {
            try
            {
                string PreFixBegin = "`";
                string PreFixEnd = "`";

                string _sqlTmp = string.Format(@"select COLUMN_NAME,DATA_TYPE
                             from INFORMATION_SCHEMA.COLUMNS 
                             where table_name='{0}'
                             order by ORDINAL_POSITION", tableName);
                DataSet ds = Conn.executeDataSet(_sqlTmp);

                List<string> colNames = new List<string>();
                List<string> colValue = new List<string>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string ColumnName = row["COLUMN_NAME"].ToString();
                    string DataType = row["DATA_TYPE"].ToString();

                    colNames.Add(PreFixBegin + ColumnName + PreFixEnd);
                    colValue.Add("@" + ColumnName);
                }

                string selectSql = string.Format("select * From {0}", tableName);
                ds = Conn.executeDataSet(selectSql);

                List<MySqlParameter> mysqlParam = new List<MySqlParameter>();
                string sql = "insert into " + PreFixBegin + tableName + PreFixEnd + "(" + string.Join(",", colNames) + ") VAlUES(" + string.Join(",", colValue) + ")";

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    mysqlParam.Clear();
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        mysqlParam.Add(new MySqlParameter(col.ColumnName, row[col.ColumnName]));
                    }
                    mysqlConn.executeUpdate(sql, mysqlParam);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
