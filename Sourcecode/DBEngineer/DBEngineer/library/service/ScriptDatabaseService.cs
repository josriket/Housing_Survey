﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;
using SendEmailService.library.util;

namespace DBEngineer.library.service
{
    public class ScriptDatabaseService
    {
        public static string FolderPath = @"D:\VSMWorkspace\PTG\PTG_Mobile_Web_Service_20161125\Gnerate_Script\";
        public void GenerateScript()
        {
            bool genTableScript = false;
            bool genStoredScript = false;
            bool genViewScript = true;
            bool genUserDefinedScript = true;

            try
            {
                ServerConnection srvConn = new ServerConnection();
                srvConn.ServerInstance = "172.17.110.32";
                srvConn.LoginSecure = false;
                srvConn.Login = "sa";
                srvConn.Password = "sql.2012";

                Server server = new Server(srvConn);

                Database databse = server.Databases["vtms"];

                ScriptingOptions scriptOptions = new ScriptingOptions();
                scriptOptions.ScriptDrops = false;
                //scriptOptions.WithDependencies = false;
                scriptOptions.IncludeHeaders = false;
                scriptOptions.ScriptSchema = true;
                scriptOptions.ScriptData = false;
                scriptOptions.IncludeIfNotExists = true;
                scriptOptions.Indexes = true;
                scriptOptions.ClusteredIndexes = true;
                //scriptOptions.DriAll = true;
                scriptOptions.DriNonClustered = true;
                scriptOptions.DriPrimaryKey = true;
                scriptOptions.DriUniqueKeys = true;
                scriptOptions.DriWithNoCheck = false;
                scriptOptions.DriForeignKeys = true;
                scriptOptions.DriChecks = true;
                scriptOptions.DriClustered = true;
                scriptOptions.DriDefaults = true;
                scriptOptions.DriIncludeSystemNames = true;
                scriptOptions.DriIndexes = true;

                string scrs = string.Empty;

                if(genTableScript)
                {
                    foreach (Table myTable in databse.Tables)
                    {
                        if (!myTable.IsSystemObject)
                        {
                            GenerateScriptToFile("Table." + myTable.Name + ".sql", myTable.Script(scriptOptions));
                        }
                    }
                }
               
                if (genStoredScript)
                {
                    foreach (StoredProcedure mySP in databse.StoredProcedures)
                    {
                        if (!mySP.IsSystemObject)
                        {
                            GenerateScriptToFile("StoredProcedure." + mySP.Name + ".sql", mySP.Script(scriptOptions));
                        }
                    }
                }
               
                if (genViewScript)
                {
                    foreach (View myView in databse.Views)
                    {
                        if (!myView.IsSystemObject)
                        {
                            GenerateScriptToFile("View." + myView.Name + ".sql", myView.Script(scriptOptions));
                        }
                    }
                }

                if (genUserDefinedScript)
                {
                    foreach (Microsoft.SqlServer.Management.Smo.UserDefinedFunction userF in databse.UserDefinedFunctions)
                    {
                        if (!userF.IsSystemObject)
                        {
                            GenerateScriptToFile("UserDefinedFunction." + userF.Name + ".sql", userF.Script(scriptOptions));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Please enter to exit...");
                Console.ReadLine();
            }
        }

        private void GenerateScriptToFile(string name, StringCollection tableScripts)
        {
            string fileName = FolderPath + name;
            if (File.Exists(fileName)) File.Delete(fileName);
            string scrs = string.Empty;
            foreach (string script in tableScripts)
            {
                scrs += script + "\n\n";
            }
            File.WriteAllText(fileName, scrs, Encoding.UTF8);

            Console.WriteLine("Generated script : " + name);
        }

    }
}
