﻿using SendEmailService.library.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.service
{
    public class SQLServerIndexService
    {
        public void DropAllIndex()
        {
            Console.Write("Please enter to start..");
            Console.ReadLine();
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                string sql = @"SELECT ind.name IdxName,t.name TableName
                            FROM  sys.indexes ind 
                            INNER JOIN sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
                            INNER JOIN sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id 
                            INNER JOIN sys.tables t ON ind.object_id = t.object_id 
                            WHERE ind.is_primary_key = 0 
                                 AND ind.is_unique = 0 
                                 AND ind.is_unique_constraint = 0 
                                 AND t.is_ms_shipped = 0 
                            ORDER BY t.name, ind.name, ind.index_id, ic.index_column_id";
                DataSet ds = conn.executeDataSet(sql);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string indexName = row["IdxName"].ToString();
                    string TableName = row["TableName"].ToString();

                    sql = string.Format("DROP INDEX [{0}] ON [dbo].[{1}]", indexName, TableName);
                    Console.WriteLine(" - " + sql);
                    conn.executeUpdate(sql);
                }
            }
            Console.WriteLine("\nDrop index completed, please enter to exit..");
            Console.ReadLine();
        }

        public void CreateFKIndex()
        {
            Console.Write("Please enter to start..");
            Console.ReadLine();
            DropAllIndex();
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                string sql = @"
                            SELECT 'CREATE INDEX [Index_' + f.name + '] ON ' + OBJECT_NAME(f.parent_object_id) + '(' + COL_NAME(fc.parent_object_id, fc.parent_column_id) + ')' IndexName
                            FROM sys.foreign_keys AS f 
                            INNER JOIN sys.foreign_key_columns AS fc 
                            ON f.OBJECT_ID = fc.constraint_object_id";
                DataSet ds = conn.executeDataSet(sql);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string IndexName = row["IndexName"].ToString();

                    Console.WriteLine(" - " + IndexName);
                    conn.executeUpdate(IndexName);
                }

                sql = @"select name from sys.tables where name not in('sysdiagrams') order by name";
                ds = conn.executeDataSet(sql);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string name = row["name"].ToString();

                    sql = string.Format("CREATE INDEX Index_SH_{0}_Name ON {0}({0}Name)", name);
                    Console.WriteLine(" - " + sql);
                    conn.executeUpdate(sql);
                }

            }
            Console.WriteLine("\nCreate index completed, please enter to exit..");
            Console.ReadLine();
        }

      
    }
}