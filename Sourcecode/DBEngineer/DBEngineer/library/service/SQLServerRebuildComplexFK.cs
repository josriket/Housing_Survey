﻿using SendEmailService.library.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.service
{
    public class SQLServerRebuildComplexFK
    {
        public void Start()
        {
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                string sql = @"
						Select TABLE_NAME,COLUMN_NAME
                        From INFORMATION_SCHEMA.COLUMNS
						Where COLUMN_NAME like '%Id'
						and COLUMN_NAME not in('ActiveStatusId','CreatedBy','ModifiedBy','TokenId')
						and TABLE_NAME not in('sysdiagrams')
						and TABLE_NAME+'Id'<>COLUMN_NAME";

                DataSet ds = conn.executeDataSet(sql);

                string TABLE_NAME = null;
                string COLUMN_NAME = null;
                string FK_TableName = null;
                string FK_PKName = null;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    try
                    {
                        TABLE_NAME = row["TABLE_NAME"].ToString();
                        COLUMN_NAME = row["COLUMN_NAME"].ToString();

                        FK_TableName = COLUMN_NAME;
                        FK_TableName = FK_TableName.Substring(0, FK_TableName.Length - 2);
                        FK_PKName = COLUMN_NAME;

                        Console.WriteLine("- Create FK_{0}_{1}", TABLE_NAME, COLUMN_NAME);

                        sql = string.Format("ALTER TABLE [dbo].[{0}]  WITH CHECK ADD  CONSTRAINT [FK_{0}_{1}] FOREIGN KEY([{1}]) REFERENCES [dbo].[{2}] ([{3}])"
                            , TABLE_NAME, COLUMN_NAME, FK_TableName, FK_PKName);
                        conn.executeUpdate(sql);

                        sql = string.Format("ALTER TABLE [dbo].[{0}] CHECK CONSTRAINT [FK_{0}_{1}]", TABLE_NAME, COLUMN_NAME);
                        conn.executeUpdate(sql);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(sql);
                        if (ex.ToString().StartsWith("System.Data.SqlClient.SqlException (0x80131904): There is already an object"))
                        {
                        }
                        else
                        {
                            Console.WriteLine(ex.ToString());
                            Console.ReadLine();
                            //throw ex;
                        }
                    }
                }
            }
        }
    }
}
