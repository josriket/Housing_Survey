﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DBEngineer.library.util;
using SendEmailService.library.util;

namespace DBEngineer.library.service
{
    public class SQLServerToAccessService
    {
        public void StartReverseEngineering()
        {
            Console.WriteLine("Please enter to start..");
            Console.ReadLine();
            using (SQLServerConnection conn = SQLServerConnection.DefaultConnection)
            {
                using (AccessConnection pgConn = AccessConnection.DefaultConnection)
                {
                    DataSet dsTable = conn.executeDataSet("select name From sys.tables where [type]='U' and name<>'sysdiagrams' order by name");
                    foreach (DataRow _row in dsTable.Tables[0].Rows)
                    {
                        string tableName = _row["name"].ToString();

                        Console.WriteLine(" - generate "+tableName);

                        DropTable(pgConn, tableName);

                        CreateTable(conn, pgConn, tableName);
                    }
                }
            }
            Console.WriteLine("\nGenerate completed, please enter to exit..");
            Console.ReadLine();
        }

        private void CreateTable(SQLServerConnection Conn ,AccessConnection PgConn, string TableName)
        {
            string sql=string.Empty;

            sql+="CREATE TABLE {0}";
            sql += "\n({0}Id AUTOINCREMENT,";

            sql += "\n" + GetColumnScript(Conn, TableName);


            sql += "CONSTRAINT {0}_PK PRIMARY KEY({0}Id)";
            sql += "\n)";
            sql = string.Format(sql, TableName);
            PgConn.executeUpdate(sql);
        }

        private string GetColumnScript(SQLServerConnection Conn, string TableName)
        {

            string _sqlTmp = string.Format(@"select COLUMN_NAME,IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,NUMERIC_SCALE
                            --select * 
                             from INFORMATION_SCHEMA.COLUMNS 
                             where table_name='{0}'
                             and ORDINAL_POSITION<>1
                             order by ORDINAL_POSITION", TableName);
            DataSet ds = Conn.executeDataSet(_sqlTmp);

            StringBuilder sql = new StringBuilder();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string ColumnName = row["COLUMN_NAME"].ToString();
                bool IsNullable = row["IS_NULLABLE"].ToString().Equals("YES");
                string DataType = row["DATA_TYPE"].ToString();
                int MaxLength = row["CHARACTER_MAXIMUM_LENGTH"] == DBNull.Value ? 0 : Convert.ToInt32(row["CHARACTER_MAXIMUM_LENGTH"]);
                int NumericPrecision = row["NUMERIC_PRECISION"] == DBNull.Value ? 0 : Convert.ToInt32(row["NUMERIC_PRECISION"]);
                int NumericScale = row["NUMERIC_SCALE"] == DBNull.Value ? 0 : Convert.ToInt32(row["NUMERIC_SCALE"]);

                sql.Append("" + ColumnName + " ");

                if (DataType.Equals("varchar"))
                {
                    sql.Append(" Text");
                }
                else if (DataType.Equals("int"))
                {
                    sql.Append(" Number");
                }
                else if (DataType.Equals("bigint"))
                {
                    sql.Append(" Number");
                }
                else if (DataType.Equals("decimal"))
                {
                    sql.Append(" DECIMAL(" + NumericPrecision + "," + NumericScale + ")");
                    //sql.Append(" Number");
                }
                else if (DataType.Equals("datetime"))
                {
                    sql.Append(" DateTime");
                }
                else if (DataType.Equals("date"))
                {
                    sql.Append(" DateTime");
                }
                else if (DataType.Equals("time"))
                {
                    sql.Append(" DateTime");
                }
                else if (DataType.Equals("varbinary"))
                {
                    sql.Append(" Text");
                }
                else if (DataType.Equals("uniqueidentifier"))
                {
                    sql.Append(" Text");
                }
                else if (DataType.Equals("bit"))
                {
                    sql.Append(" YesNo");
                }
                else
                {
                    throw new Exception("Unknow data type for "+DataType);
                }

                if (!IsNullable)
                {
                    sql.Append(" NOT NULL");
                }
                sql.Append(",");
                sql.Append("\n");
            }

            return sql.ToString();
        }

        private void DropTable(AccessConnection PgConn, string TableName)
        {
            try
            {
                string sql = string.Format("DROP TABLE {0}", TableName);
                PgConn.executeUpdate(sql);
            }
            catch (Exception ex)
            {

            }
        }

       
    }
}
