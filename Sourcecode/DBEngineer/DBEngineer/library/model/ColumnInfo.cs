﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.model
{
    public class ColumnInfo
    {
        public string ColumnName { get; set; }
        public string TableName { get; set; }
        public bool AllowDBNull { get; set; }
        public string DataType { get; set; }
        public int ColumnSize { get; set; }
    }
}
