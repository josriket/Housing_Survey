﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBEngineer.library.model
{
    public class CompairTable
    {
        public string SrcTable { get; set; }
        public string DesTable { get; set; }
        public CompairTable(string SrcTable, string DesTable)
        {
            this.SrcTable = SrcTable;
            this.DesTable = DesTable;
        }

        public override string ToString()
        {
            return string.Format("Table: {0}->{1}", SrcTable, DesTable);
        }
    }
}
