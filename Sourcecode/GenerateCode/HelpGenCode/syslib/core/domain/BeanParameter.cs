﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Globalization;
using SystemLibrary.syslib.core.connection;
using System.Data;
using SystemLibrary.syslib.core.util;
using System.Web;

namespace SystemLibrary.syslib.core.domain
{
    public class BeanParameter
    {
        public string DB_CONNECTION;

        public readonly string OutPutDateFormat = "dd/MM/yyyy";
        public readonly string OutPutDateTimeFormat = "dd/MM/yyyy HH:mm:ss";
        public readonly string OUTPUT_NUMBER_FORMAT = "{0:#,##0}";
        public readonly string OUTPUT_MONEY_FORMAT = "{0:#,##0.00}";
        public readonly string OutPutTimeFormat = "HH:mm:ss";

        public CultureInfo ThaiCultureInfo
        {
            get
            {
                return CultureInfo.CreateSpecificCulture("th-TH");
            }
        }

        public CultureInfo EngCultureInfo
        {
            get
            {
                return CultureInfo.CreateSpecificCulture("en-US");
            }
        }

        public CultureInfo DefaultCultureInfo
        {
            get
            {
                return EngCultureInfo;
            }
        }


        private static BeanParameter param = null;
        public static BeanParameter GetInstance(){
            if (param == null)
            {
                param = new BeanParameter();
            }
            return param;
        }

        private BeanParameter()
        {
            DB_CONNECTION = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }

    }
}
