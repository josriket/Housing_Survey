﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemLibrary.syslib.core.exception
{
    public class DBException: Exception
    {
        public DBException(string msg) : base(msg) { }
    }
}
