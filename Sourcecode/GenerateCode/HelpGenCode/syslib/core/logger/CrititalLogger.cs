﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.syslib.core.logger
{
    public class CrititalLogger
    {
        private static StreamWriter CrititalLog;

        static CrititalLogger()
        {
            CrititalLog = File.AppendText("CrititalLog.log");
        }

        public static void WriteLog(string message)
        {
            CrititalLog.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " {0}", message);
            CrititalLog.Flush();
        }
    }
}
