﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using SystemLibrary.syslib.core.util;
using System.IO;

namespace SystemLibrary.syslib.core.util
{
    public class HttpUtil
    {
        public static string SendRequest(string Uri,string ContentType,string WebMethod,string BodyRequest,Dictionary<string,string> headers)
        {
            string ResponseText = string.Empty;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(Uri);
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> pair in headers)
                {
                    httpWebRequest.Headers.Add(pair.Key, pair.Value);
                }
            }
            httpWebRequest.ContentType = ContentType;
            httpWebRequest.Method = WebMethod;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(BodyRequest);
            }

            HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                ResponseText = streamReader.ReadToEnd();
            }
            return ResponseText;
        }
    }
}
