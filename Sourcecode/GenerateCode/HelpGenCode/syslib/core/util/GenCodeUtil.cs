﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibrary.syslib.core.connection;
using SystemLibrary.syslib.core.domain;

namespace SystemLibrary.syslib.core.util
{
    public class GenCodeUtil
    {
        //public static string GenCodeYearMonth(DBConnection conn, string GenKey, string prefix)
        //{
        //    return GenCode(conn, GenKey, prefix, 4, true);
        //}

        //public static string GenCode(DBConnection conn, string GenKey, string prefix)
        //{
        //    return GenCode(conn, GenKey, prefix, 4, false);
        //}

        public static string GenCode(DBConnection conn, GenCodeKey genKey)
        {
            string yyMMdd = DateTime.Now.ToString("yyMMdd", BeanParameter.GetInstance().EngCultureInfo);

            string sql = string.Format("DECLARE @YYMMDD VARCHAR(6)='{0}'",yyMMdd);
            if (!genKey.YearMonthDay)
            {
                sql = "DECLARE @YYMMDD VARCHAR(1)=''";
            }

            sql += @" declare @Table table (GenCode int);
                        Update GenCode Set GenCode=GenCode+1 
                        OUTPUT INSERTED.GenCode INTO @Table
                        where GenKey=@GenKey and Prefix=@Prefix and YYMMDD=@YYMMDD
                        IF (SELECT COUNT(*) FROM @Table)=0
	                        BEGIN
		                        INSERT INTO GenCode(GenKey,Prefix,YYMMDD,GenCode,Length) VALUES(@GenKey,@Prefix,@YYMMDD,1,@Length)
		                        INSERT INTO @Table VALUES(1)
	                        END
                        SELECT GenCode,@YYMMDD YYMMDD from @Table";

            List<SqlParameter> lstParam = new List<SqlParameter>();
            lstParam.Add(new SqlParameter("GenKey", genKey.GenKey));
            lstParam.Add(new SqlParameter("Prefix", genKey.Prefix));
            lstParam.Add(new SqlParameter("Length", genKey.Length));
            DataSet ds = conn.executeDataSet(sql, lstParam);

            DataRow row = ds.Tables[0].Rows[0];

            return genKey.Prefix + row["YYMMDD"] + Convert.ToInt32(row["GenCode"]).ToString("D" + genKey.Length);
        }

    }
}
