﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SystemLibrary.syslib.core.util
{
    public class FileUtil
    {
        public static string ReadFileToString(string filePath)
        {
            StreamReader streamReader = null;
            try
            {
                streamReader = new StreamReader(filePath);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if (streamReader != null) streamReader.Close();
            }
        }

        public static List<string> ReadLine(string filePath)
        {
            string line;
            List<string> lstLine = new List<string>();
            // Read the file and display it line by line.
            StreamReader file = null;
            try
            {
                file = new StreamReader(filePath);
                while ((line = file.ReadLine()) != null)
                {
                    lstLine.Add(line);
                }
            }
            finally
            {
                file.Close();
            }
            return lstLine;
        }

        // isSkipError =true ในกรณี ที่มี Error ก็จะไม่ Throw Exception
        public static void EmptyFolder(DirectoryInfo directoryInfo, bool isSkipError)
        {
            try
            {
                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo subfolder in directoryInfo.GetDirectories())
                {
                    EmptyFolder(subfolder, isSkipError);
                    subfolder.Delete();
                }
            }
            catch (Exception ex)
            {
                if (!isSkipError)
                {
                    throw ex;
                }
            }
        }

        public static byte[] GetBytesFromFile(string fullFilePath)
        {
            // this method is limited to 2^32 byte files (4.2 GB)

            FileStream fs = File.OpenRead(fullFilePath);
            try
            {
                byte[] bytes = new byte[fs.Length];
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                fs.Close();
                return bytes;
            }
            finally
            {
                fs.Close();
            }
        }

        public static byte[] ConvertImageToByteArray(System.Drawing.Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                // or whatever output format you like
                return ms.ToArray();
            }
        }


        public static byte[] ReadByteFromStream(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
