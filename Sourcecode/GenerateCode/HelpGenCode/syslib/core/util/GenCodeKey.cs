﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemLibrary.syslib.core.util
{
    public class GenCodeKey
    {
        public string GenKey { get; set; }
        public string Prefix { get; set; }
        public int Length { get; set; }
        public bool YearMonthDay { get; set; }

        public GenCodeKey(string GenKey, string Prefix, int Length, bool YearMonthDay)
        {
            this.GenKey = GenKey;
            this.Prefix = Prefix;
            this.Length = Length;
            this.YearMonthDay = YearMonthDay;
        }

        // S000011
        public static GenCodeKey MemberCode(string BranchCode)
        {
            return new GenCodeKey("MemberCode", "M" + BranchCode, 5, false);
        }

        //เลขที่ใบเสร็จสะสมแต้ม = Pbbyymmddxxx	
        public static GenCodeKey SaleReceiveNo(string BranchCode)
        {
            return new GenCodeKey("SaleReceiveNo", "P" + BranchCode, 3, true);
        }

        //เลขที่ใบเสร็จแลกของ = Rbbyymmddxxx	
        public static GenCodeKey RedemptionReceiveNo(string BranchCode)
        {
            return new GenCodeKey("RedemptionReceiveNo", "I" + BranchCode, 3, true);
        }

        //เลขที่ใบเสร็จแลกของ = Rbbyymmddxxx	
        public static GenCodeKey RewardReceiveNo(string BranchCode)
        {
            return new GenCodeKey("RewardReceiveNo", "R" + BranchCode, 3, true);
        }

        //เลขที่ใบนัดรับของ = Abbyymmddxxx
        public static GenCodeKey AppointmentNo(string BranchCode)
        {
            return new GenCodeKey("AppointmentNo", "A" + BranchCode, 3, true);
        }

        public static GenCodeKey CancelReceiveNo(string BranchCode)
        {
            return new GenCodeKey("CancelReceiveNo", "C" + BranchCode, 3, true);
        }

        public static GenCodeKey RewardCode()
        {
            return new GenCodeKey("RewardCode", "P", 3, false);
        }

        public static GenCodeKey RedeemTransactionNo(string BranchCode)
        {
            return new GenCodeKey("RedeemTransactionNo", "R" + BranchCode, 3, true);
        }

        public static GenCodeKey SaleTransactionNo(string BranchCode)
        {
            return new GenCodeKey("SaleTransactionNo", "S" + BranchCode, 3, true);
        }

        public static GenCodeKey BranchCode()
        {
            return new GenCodeKey("BranchCode", "", 2, false);
        }

        internal static GenCodeKey StockCode(string BranchCode)
        {
            return new GenCodeKey("StockCode", "ST" + BranchCode, 3, false);
        }

        public static GenCodeKey PromotionCode(string BranchCode)
        {
            return new GenCodeKey("PromotionCode", "PR" + BranchCode, 3, false);
        }
    }
}
