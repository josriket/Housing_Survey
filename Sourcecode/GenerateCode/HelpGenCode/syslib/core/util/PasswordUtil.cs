﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.syslib.core.util
{
    public class PasswordUtil
    {
        private const string charUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string charLower = "abcdefghijklmnopqrstuvwzyz";
        private const string charNumber = "123456789";
        private const string charSpecial = "!@#$%^&*_";
        private const string charMix = "";

        private static string _DoRandom(Random _rng,int size, string plainText)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = plainText[_rng.Next(plainText.Length)];
            }
            return new string(buffer);
        }

        // Random password 8 charactor
        public static string NewPassword()
        {
            Random _rng = new Random();
            // ตอนนี้ Requirement ให้เป็นเฉพาะ พิมเล็กพิมใหญ่และตัวเลขเท่านั้นไม่ตอ้งมีอักขระพิเศษ
            //charUpper[1] + charSpecial[1] + charLower[3] + charNumber[1] + charLower[2]
            return _DoRandom(_rng,1, charUpper)
                + _DoRandom(_rng, 1, charSpecial)
                + _DoRandom(_rng, 3, charLower)
                + _DoRandom(_rng, 1, charNumber)
                + _DoRandom(_rng, 2, charLower);
        }
    }
}
