﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SystemLibrary.syslib.core.util;
using SystemLibrary.syslib.core.exception;
using SystemLibrary.syslib.core.connection;
using System.Data.SqlClient;

namespace SystemLibrary.syslib.core.util
{
    public class DataSetUtil
    {
        /** Add table column if not exist */
        public static void AddColumnIfNotExist(DataSet ds, string ColumnName, System.Type type)
        {
            if (!ds.Tables[0].Columns.Contains(ColumnName))
            {
                ds.Tables[0].Columns.Add(ColumnName, type);
            }
        }

        /** Check data is not empty in row zero*/
        public static bool IsNotEmpty(DataSet ds)
        {
            return ds != null && ds.Tables.Count > 0 & ds.Tables[0].Rows.Count > 0;
        }
        public static bool IsNotEmpty(DataTable dt)
        {
            return dt !=null && dt.Rows.Count > 0;
        }

        public static bool IsNullOrEmptyString(object col)
        {
            if (col != DBNull.Value && !string.IsNullOrEmpty(col.ToString()))
            {
                return false;
            }
            return true;
        }

        /** Copy column name from dsSrc to dsDes for prepare column data */
        public static void CopyColumnIfNotExist(DataSet dsSrc, DataSet dsDes)
        {
            if (IsNotEmpty(dsSrc) && IsNotEmpty(dsDes))
            {
                DataTable tb = dsSrc.Tables[0];
                foreach (DataColumn col in tb.Columns)
                {
                    AddColumnIfNotExist(dsDes, col.ColumnName, col.DataType);
                }
            }
        }

        public static void CopyColumn(DataSet dsSrc, DataSet dsDes)
        {
            DataTable tb = dsSrc.Tables[0];
            foreach (DataColumn col in tb.Columns)
            {
                AddColumnIfNotExist(dsDes, col.ColumnName, col.DataType);
            }
        }

        /** Copy first row data from  dsSrc to dsDes */
        public static void CopyDataSingleRow(DataSet dsSrc, DataSet dsDes)
        {
            if (IsNotEmpty(dsSrc) && IsNotEmpty(dsDes))
            {
                DataRow srcRow = dsSrc.Tables[0].Rows[0];
                DataRow desRow = dsDes.Tables[0].Rows[0];

                DataTable tb = dsSrc.Tables[0];
                foreach (DataColumn col in tb.Columns)
                {
                    desRow[col.ColumnName] = srcRow[col.ColumnName];
                }
            }
        }

        /** Copy first row data from  dsSrc to dsDes */
        public static void CopyDataRow(DataSet dsSrc, DataRow rowSrc, DataRow rowDes)
        {
            DataTable tb = dsSrc.Tables[0];
            foreach (DataColumn col in tb.Columns)
            {
                rowDes[col.ColumnName] = rowSrc[col.ColumnName];
            }
        }

        public static string ShowString(Object col)
        {
            if (!IsNullOrEmptyString(col))
            {
                return col.ToString();
            }
            return "";
        }

        public static string ShowRealString(Object col)
        {
            if (!IsNullOrEmptyString(col))
            {
                //return col.ToString().Replace("\n", "<br/>").Replace(" ", "&nbsp;");
                return col.ToString();
            }
            return "";
        }


        public static string ShowModifyWebTxt(object text)
        {
            if (text != DBNull.Value)
            {
                string blankTxt = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                string text2 = text.ToString().Trim();

                text2 = text2.Replace("#", blankTxt + "-&nbsp;")
                    .Replace("\n", "<br/>")
                    .Replace("|", blankTxt)
                    .Replace(" ", "&nbsp;");


                return text2;
            }

            return "";
        }

        public static string ShowModifyMobileTxt(object text)
        {
            if (text != DBNull.Value)
            {
                string blankTxt = " ";
                string text2 = text.ToString().Trim();

                text2 = text2.Replace("#", blankTxt + "- ")
                    .Replace("|", blankTxt);


                return text2;
            }

            return "";
        }

        public static bool HasColumn(DataSet ds, string ColumnName)
        {
            return ds.Tables[0].Columns.Contains(ColumnName);
        }

        public static DataSet CopySchema(DataSet dsSrc)
        {
            DataSet _ds = new DataSet();
            DataTable tb = new DataTable();
            foreach (DataColumn dc in dsSrc.Tables[0].Columns)
            {
                tb.Columns.Add(new DataColumn(dc.ColumnName, dc.DataType));
            }
            _ds.Tables.Add(tb);
            return _ds;
        }

        public static DataSet GetEmptyData(DBConnection conn, string tbName)
        {
            bool conisnull = false;
            try
            {
                if (conn == null)
                {
                    conisnull = true;
                    conn = DBConnection.DefaultConnection;
                }

                string sql = "select * from " + tbName + " where 1<>1";
                return conn.executeDataSet(sql);
            }
            finally
            {
                if (conisnull && conn != null) conn.closeCon();
            }
        }

        public static List<SqlParameter> GetLstSqlParams(DataSet ds)
        {
            List<SqlParameter> lstParam = new List<SqlParameter>();
            
            DataRow row = ds.Tables[0].Rows[0];
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                //if (dc.DataType == typeof(byte[]) && row[dc.ColumnName]==DBNull.Value)
                //{
                //    lstParam.Add(new SqlParameter("@" + dc.ColumnName, SqlDbType.VarBinary, -1));
                //}
                //else
                //{
                    lstParam.Add(new SqlParameter(dc.ColumnName, row[dc.ColumnName]));
               //}
            }
            return lstParam;
        }

        public static string GetValue(object o)
        {
            return o.ToString();
        }

        public static Int64 GetInt64Value(object o)
        {
            if (o != DBNull.Value)
            {
                return (Int64)o;
            }
            return Int64.MinValue;
        }

        public static string GetStringValue(object o)
        {
            if (o != DBNull.Value)
            {
                return o.ToString();
            }
            return null;
        }

        public static DateTime GetDateTimeValue(object o)
        {
            if (o != DBNull.Value)
            {
                return (DateTime)o;
            }
            return DateTime.MinValue;
        }
    }
}
