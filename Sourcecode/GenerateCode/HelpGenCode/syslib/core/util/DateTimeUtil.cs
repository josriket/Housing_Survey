﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SystemLibrary.syslib.core.connection;
using SystemLibrary.syslib.core.domain;

namespace SystemLibrary.syslib.core.util
{
    public class DateTimeUtil
    {
        public static Object strUiToDate(string data)
        {
            try
            {
                if (StringUtil.notEmpty(data))
                {
                    return DateTime.ParseExact(data, BeanParameter.GetInstance().OutPutDateFormat, BeanParameter.GetInstance().DefaultCultureInfo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DBNull.Value;
        }




        public static DateTime CurrentDate(DBConnection conn)
        {
            return (DateTime)conn.executeDataSet("select now() CurrentDate").Tables[0].Rows[0]["CurrentDate"];
            
        }
    }
}
