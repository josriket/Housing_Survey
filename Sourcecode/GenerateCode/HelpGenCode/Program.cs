﻿using HelpGenCode.lib;
//using HelpGenCode.lib.gener;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using SystemLibrary.syslib.core.connection;
using HelpGenCode.lib.model;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator;

namespace HelpGenCode
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Mainxxx()
        {
            // Load all category info and tempolary
            Dictionary<int, CategoryInfo> AllCategory = LoadAllCategoryInfo();

            // Find parent category of product (level1,level2)
            int productCategoryId = 45;
            if (AllCategory.ContainsKey(productCategoryId))
            {
                CategoryInfo catInfo = AllCategory[productCategoryId];

                string cat1Name = catInfo.Cat1Name;
                string cat2Name = catInfo.Cat2Name;
            }

        }

        public static Dictionary<int, CategoryInfo> LoadAllCategoryInfo()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                conn.beginTrans();

                string sql = @"WITH cte AS
                            ( 
	                            select Name,Id CategoryId,ParentCategoryId
	                            from Category
	                            union all
	                            select c.Name,c.Id CategoryId,c.ParentCategoryId
	                            from Category c
	                            inner join cte e on e.CategoryId=c.ParentCategoryId 
                            )
                            select distinct * from cte order by CategoryId";
                DataSet ds = conn.executeDataSet(sql);

                CategoryInfo cat = null;
                Dictionary<int, CategoryInfo> _category = new Dictionary<int, CategoryInfo>();

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    cat = new CategoryInfo();
                    cat.CatName = row["Name"].ToString();
                    cat.CatId = Convert.ToInt32(row["CategoryId"]);
                    cat.ParentCatId = Convert.ToInt32(row["ParentCategoryId"]);

                    _category.Add(cat.CatId,cat);
                }

                string cat2Name = string.Empty;
                foreach (KeyValuePair<int, CategoryInfo> pair in _category)
                {
                    pair.Value.Cat1Name = GetParentCatInfo(_category, pair.Value, ref cat2Name);
                    pair.Value.Cat2Name = cat2Name;
                }
                return _category;
            }


        }

        public static string GetParentCatInfo(Dictionary<int, CategoryInfo> AllCategory, CategoryInfo currentCat, ref string cat2Name)
        {
            CategoryInfo myCat = currentCat;
            cat2Name = string.Empty;
            int i = 0;
            while (true)
            {
                if (i++ > 200) return ""; // For check infinity loop
                if (myCat.ParentCatId == 0)
                {
                    return myCat.CatName;
                }
                else
                {
                    cat2Name = myCat.CatName;
                    myCat = AllCategory[myCat.ParentCatId];
                }
            }
        }

        public class CategoryInfo
        {
            public string CatName { get; set; }
            public int CatId { get; set; }
            public int ParentCatId { get; set; }
            public string Cat1Name { get; set; }
            public string Cat2Name { get; set; }
        }

        [STAThread]
        static void Main()
        {
            try
            {
                Console.WriteLine("Please enter Command to Generate Code:");
                Console.WriteLine(" - Press 1 >> Geneate all C#,MVC code");
                Console.WriteLine(" - Press 2 >> Read destination coding file to clipboard");
                Console.WriteLine(" - Press 3 >> Generate menu in html format to clipboard");

                Console.Write(">>");
                string Command = Console.ReadLine();

                if (Command.Equals("1"))
                {
                    new CodeGenerator().StartGenerate();

                    Console.Write("Geneateate complated...\n");
                }
                else if (Command.Equals("2"))
                {
                    string yourCode = new GencodeHelper().ReadCode(@"D:\Workspace\KhunK\ISQE\Sourcecode\Website\Library.Winplus\syslib\core\domain\DisplayIndex.cs");

                    Clipboard.Clear();
                    Clipboard.SetText(yourCode, TextDataFormat.Text);
                    Console.Write("Your code has copied to clip board\n");
                }
                else if (Command.Equals("3"))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        String html = new GencodeHelper().GenerateMenuInHtml(conn);
                        Clipboard.Clear();
                        Clipboard.SetText(html, TextDataFormat.Text);
                        Console.Write("Your code has copied to clip board\n");
                    }
                }

                Console.WriteLine("\nPlease enter to exit");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }
    }
}
