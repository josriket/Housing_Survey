﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.interf;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel
{
    public class ByteArrayGenerator : IDataGenerator
    {
        public ByteArrayGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "Byte[]";
            this.HtmlInputType = "file";
            this.MVCInputType = "File";
            this.SearchOperator = "=";
        }



        #region Controller.cs

        #endregion



        #region ControllerApi.cs

        #endregion



        #region Model.cs

        public override string GenCode_Model_GetterHtml()
        {
            StringBuilder code = new StringBuilder();

            return code.ToString();
        }

        public override string GenCode_Model_SetterHtml()
        {
            StringBuilder code = new StringBuilder();


            return code.ToString();
        }

        public override string GenCode_Model_DeclareVariable()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("        [DataColumn]");
            code.AppendLine("        [LocalizedDisplayName(\"" + this.VariableName + "\", typeof(MyResources.Winplus.Resource))]");
            code.AppendLine("        public " + this.VariableType + ((this.VariableType == "String" || this.VariableType == "Boolean" || this.VariableType == "Byte[]") ? "" : "?") + " " + this.VariableName + " { get; set; }");
            code.AppendLine("");
            code.AppendLine("        /* Remark: This field must generate after ContentType & FileName */"); 
            code.AppendLine("        public HttpPostedFileBase " + this.VariableName + "Upload");
            code.AppendLine("        {");
            code.AppendLine("            get {");
            code.AppendLine("                return null;");
            code.AppendLine("            }");
            code.AppendLine("            set");
            code.AppendLine("            {");
            code.AppendLine("                string contentType = string.Empty;");
            code.AppendLine("                string fileDataFileName = string.Empty;");
            code.AppendLine("                " + this.VariableName + " = FileUtil.ReadHttpPostFile(value, ref contentType, ref fileDataFileName);");
            code.AppendLine("                " + this.VariableName + "ContentType = contentType;");
            code.AppendLine("                " + this.VariableName + "FileName = fileDataFileName;");
            code.AppendLine("            }");
            code.AppendLine("        }");
            code.AppendLine("");

            return code.ToString();
        }

        #endregion



        #region SearchModel.cs


        public override string GenCode_SearchModel_WhereCondition()
        {
            StringBuilder code = new StringBuilder();

            
            return code.ToString();
        }

        public override string GenCode_SearchModel_ListRender()
        {
            StringBuilder code = new StringBuilder();


            return code.ToString();
        }


        public override string GenCode_SearchModel_ListRenderFK()
        {
            StringBuilder code = new StringBuilder();

            return code.ToString();
        }

        public override string GenCode_SearchModel_ListMappingForm()
        {
            StringBuilder code = new StringBuilder();
            return code.ToString();
        }

        public override String GenCode_SearchModel_SqlJoinTable()
        {
            StringBuilder code = new StringBuilder();

            return code.ToString();
        }

        #endregion



        #region Repository.cs


        public override string GenCode_Repository_SearchBy()
        {
            StringBuilder code = new StringBuilder();


            return code.ToString();
        }

        #endregion



        #region AddEdit.html

        public override string GenCode_AddEdit_Element()
        {
            String requireTxt = this.AllowNull ? "" : "required=\"\"";

            StringBuilder code = new StringBuilder();
            code.AppendLine("	                <div class=\"form-group col-sm-6\">");
            code.AppendLine("		                <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
            code.AppendLine("		                <div class=\"col-sm-8\">");

            code.AppendLine("                     	<input type=\"file\" id=\"" + this.VariableName + "Upload\" name=\"" + this.VariableName + "Upload\"  />");

            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");

            return code.ToString();
        }

        #endregion



        #region Search.html

        public override string GenCode_SearchForm_TableFilter()
        {
            StringBuilder code = new StringBuilder();
            return code.ToString();
        }

        public override string GenCode_SearchForm_JSGridColumn()
        {
            StringBuilder code = new StringBuilder();
            return code.ToString();
        }


        public override string GenCode_SearchForm_Element()
        {
            StringBuilder code = new StringBuilder();


            return code.ToString();
        }

        #endregion


    }
}
