﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.interf;

namespace HelpGenCode.lib.generator.datamodel
{
    public class TimeGenerator : IDataGenerator
    {
        public TimeGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "TimeSpan";
            this.HtmlInputType = "text";
            this.MVCInputType = "TextBox";
            this.SearchOperator = "=";
        }



        public override string GenCode_AddEdit_Element()
        {
            StringBuilder code = new StringBuilder();
            String requireTxt = this.AllowNull ? "" : ",@required=\"\"";

            code.AppendLine("	                <div class=\"form-group col-sm-6\">");
            code.AppendLine("		                <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
            code.AppendLine("		                <div class=\"col-sm-8\">");

            code.AppendLine("                            <div class=\"input-group clockpicker\" data-autoclose=\"true\">");
            code.AppendLine("                                <span class=\"input-group-addon\"><i class=\"fa fa-clock-o\"></i></span>");
            code.AppendLine("                                @Html.TextBoxFor(model => model." + this.ColumnName + "Text, new { @class = \"form-control\" " + requireTxt + " })");
            code.AppendLine("                            </div>");


            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");


            return code.ToString();
        }


        public override string GenCode_Model_DeclareVariable()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("        [DataColumn]");
            code.AppendLine("        [LocalizedDisplayName(\"" + this.VariableName + "\", typeof(MyResources.Winplus.Resource))]");
            code.AppendLine("        public " + this.VariableType + ((this.VariableType == "String" || this.VariableType == "Boolean") ? "" : "?") + " " + this.VariableName + " { get; set; }");
            code.AppendLine("        public string " + this.VariableName + "Text");
            code.AppendLine("        {");
            code.AppendLine("            get { return StringUtil.FormatTimeSpan(" + this.VariableName + "); }");
            code.AppendLine("            set { " + this.VariableName + " = StringUtil.ToTimeSpan(value); }");
            code.AppendLine("        }");

            code.AppendLine("");

            return code.ToString();
        }


        public override string GenCode_SearchForm_Element()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("								    <div class=\"col-lg-4\">");
            code.AppendLine("								        <div class=\"form-group\">");
            code.AppendLine("								            <label><p th:remove=\"tag\" th:text=\"${resource.show('field." + this.ColumnName + "')}\" /></label>");

            code.AppendLine("								            <div class=\"input-group clockpicker\" data-autoclose=\"true\">");
            code.AppendLine("								                <input type=\"text\" id=\"" + this.VariableName + "\" th:placeholder=\"${resource.show('field.hh:mm')}\" class=\"form-control\" />");
            code.AppendLine("								                <span class=\"input-group-addon\"><span class=\"fa fa-clock-o\"></span></span>");
            code.AppendLine("								            </div>");

            code.AppendLine("								        </div>");
            code.AppendLine("								    </div>");

            return code.ToString();

        }

    }
}
