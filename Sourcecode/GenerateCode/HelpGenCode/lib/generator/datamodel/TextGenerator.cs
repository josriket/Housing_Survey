﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.interf;

namespace HelpGenCode.lib.generator.datamodel
{
    public class TextGenerator: IDataGenerator
    {
        public TextGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "String";
            this.HtmlInputType = "hidden";
            this.MVCInputType = "Hidden";
            this.SearchOperator = "like";
        }

        public override string GenCode_Model_DeclareVariable()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("        [DataColumn]");
            code.AppendLine("        [AllowHtml]");
            code.AppendLine("        [LocalizedDisplayName(\"" + this.VariableName + "\", typeof(MyResources.Winplus.Resource))]");
            code.AppendLine("        public " + this.VariableType + ((this.VariableType == "String" || this.VariableType == "Boolean") ? "" : "?") + " " + this.VariableName + " { get; set; }");
            code.AppendLine("");

            return code.ToString();
        }

        public override string GenCode_AddEdit_Element()
        {
            //String requireTxt = this.AllowNull ? "" : "required=\"\"";

            StringBuilder code = new StringBuilder();
            code.AppendLine("	                <div class=\"form-group col-sm-12\">");
            code.AppendLine("		                <label class=\"col-sm-2 control-label\">@Html.DisplayNameFor(model => model."+this.ColumnName+")</label>");
            code.AppendLine("		                <div class=\"col-sm-10\">");
            code.AppendLine("                            <div style=\"border:3px solid #F5F5F5;\">");
            code.AppendLine("                                @Html.HiddenFor(model => model." + this.ColumnName + ")");
            code.AppendLine("			                    <div id=\"" + this.ColumnName + "-Editor\" class=\"full-wysiwyg-editor\" >");
            code.AppendLine("                                    @Html.Raw(Model." + this.ColumnName + ")");
            code.AppendLine("                                </div>");
            code.AppendLine("                            </div>");
            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");


            return code.ToString();
        }
    }
}
