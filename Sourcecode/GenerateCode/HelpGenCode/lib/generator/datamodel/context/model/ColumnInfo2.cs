﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.interf;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel.context.model
{
    public class ColumnInfo2
    {
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        public bool AllowDBNull { get; set; }
        public int ColumnSize { get; set; }
        

        public IDataGenerator iDataGenerator { get; set; }

        public ColumnInfo2(string ColumnName, string DataType, bool AllowDBNull, int ColumnSize)
        {
            this.ColumnName = ColumnName;
            this.DataType = DataType;
            this.AllowDBNull = AllowDBNull;
            this.ColumnSize = ColumnSize;
        }

    }
}
