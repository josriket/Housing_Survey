﻿using System;
using System.Collections.Generic;

namespace HelpGenCode.lib.generator.datamodel.context.model
{
    public class TableInfo2
    {
        public String TableName { get; set; }
        public String TableNameLower { get; set; }
        public String PrimaryKeyName { get; set; }
        public String DisplayKeyName { get; set; }
        public bool IsSelfJoin { get; set; }

        public HashSet<String> AllColumnName { get; set; }
        public HashSet<String> AllDataType { get; set; }
        public List<ColumnInfo2> lstColumnInfo { get; set; }
        public TableInfo2()
        {
            AllDataType = new HashSet<string>();
            AllColumnName = new HashSet<string>();
            lstColumnInfo = new List<ColumnInfo2>();
        }

    }
}
