﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.datamodel.context.util;
using HelpGenCode.lib.generator.datamodel.interf;
using SystemLibrary.syslib.core.connection;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel.context
{
    public class GeneratorContext
    {
        //
        public static HashSet<string> LstStandardColumn = new HashSet<string>() { "DisplayIndex", "CreatedOn", "CreatedBy", "ModifiedOn", "ModifiedBy", "VersionNumber", "OrganizationId", "ActiveStatusId" };
        
        // All list of table Name
        public static List<String> LstTableName;

        // PrimaryKeyName + TableName
        public static Dictionary<String, String> TableKeys = new Dictionary<string, string>();

        // TableName + TableInfo Object
        public static Dictionary<String, TableInfo2> TableInfo = new Dictionary<string, TableInfo2>();
        


        static GeneratorContext()
        {
            try
            {

                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    LstTableName = GeneratorUtil.GetListOfTableName(conn);
                    foreach (String tbName in LstTableName)
                    {
                        TableKeys.Add(tbName + "Id", tbName);
                    }

                    foreach (String tbName in LstTableName)
                    {
                        // Initial Table Context
                        TableInfo2 tbInfo = new TableInfo2();
                        tbInfo.TableName = tbName;
                        tbInfo.TableNameLower = tbInfo.TableName.ToLower();

                        tbInfo.PrimaryKeyName = tbInfo.TableName + "Id";
                        tbInfo.DisplayKeyName = tbInfo.TableName + "Name";
                        tbInfo.lstColumnInfo = GeneratorUtil.getListOfColumnInfo(conn, tbInfo.TableName);
                        int i = 0;
                        foreach (ColumnInfo2 colInfo in tbInfo.lstColumnInfo)
                        {
                            tbInfo.AllColumnName.Add(colInfo.ColumnName);
                            if (colInfo.DataType.ToLower() == "String".ToLower())
                            {
                                if (colInfo.ColumnSize > 2000000)
                                {
                                    colInfo.iDataGenerator = new TextGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                                }
                                else if (colInfo.ColumnName.EndsWith("HyperLink"))
                                {
                                    colInfo.iDataGenerator = new HyperLinkGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                                }
                                else
                                {
                                    colInfo.iDataGenerator = new StringGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                                }

                            }
                            else if (colInfo.DataType.ToLower() == "Int64".ToLower())
                            {
                                colInfo.iDataGenerator = new LongGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "Int32".ToLower())
                            {
                                colInfo.iDataGenerator = new IntegerGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "DateTime".ToLower())
                            {
                                if (colInfo.ColumnSize == 8)
                                {
                                    colInfo.iDataGenerator = new TimestampGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                                }
                                else
                                {
                                    colInfo.iDataGenerator = new DateGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                                }

                            }
                            else if (colInfo.DataType.ToLower() == "Guid".ToLower())
                            {
                                colInfo.iDataGenerator = new UUIDGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "Date".ToLower())
                            {
                                colInfo.iDataGenerator = new DateGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "TimeSpan".ToLower())
                            {
                                colInfo.iDataGenerator = new TimeGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "Boolean".ToLower())
                            {
                                colInfo.iDataGenerator = new BooleanGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "Decimal".ToLower())
                            {
                                colInfo.iDataGenerator = new DoubleGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else if (colInfo.DataType.ToLower() == "Byte[]".ToLower())
                            {
                                colInfo.iDataGenerator = new ByteArrayGenerator(tbName, colInfo.DataType, colInfo.ColumnName, colInfo.AllowDBNull, i);
                            }
                            else
                            {
                                throw new Exception("Inplement datatype for" + colInfo.DataType);
                            }

                            i++;

                            if (!LstStandardColumn.Contains(colInfo.ColumnName))
                            {
                                if (!tbInfo.AllDataType.Contains(colInfo.iDataGenerator.VariableType))
                                {
                                    tbInfo.AllDataType.Add(colInfo.iDataGenerator.VariableType);
                                }
                            }

                            DataSet dsFk = conn.executeDataSet(@"SELECT   
                                                                   OBJECT_NAME(f.parent_object_id) AS table_name  
                                                                   ,COL_NAME(fc.parent_object_id, fc.parent_column_id) AS constraint_column_name  
                                                                   ,OBJECT_NAME (f.referenced_object_id) AS referenced_object  
                                                                   ,COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS referenced_column_name  
                                                                FROM sys.foreign_keys AS f  
                                                                INNER JOIN sys.foreign_key_columns AS fc   
                                                                   ON f.object_id = fc.constraint_object_id   
                                                                WHERE OBJECT_NAME(f.parent_object_id)='" + tbName + "' and COL_NAME(fc.parent_object_id, fc.parent_column_id)='" + colInfo.ColumnName + "'");
                            if (dsFk.Tables[0].Rows.Count>0)
                            {
                                DataRow rowFk = dsFk.Tables[0].Rows[0];
                                TableInfo2 fkTbfkInfo = new TableInfo2();
                                fkTbfkInfo.TableName = rowFk["referenced_object"].ToString();
                                fkTbfkInfo.TableNameLower = fkTbfkInfo.TableName.ToLower();
                                fkTbfkInfo.PrimaryKeyName = fkTbfkInfo.TableName + "Id";
                                fkTbfkInfo.DisplayKeyName = fkTbfkInfo.TableName + "Name";

                                colInfo.iDataGenerator.IsForeignKeyColumn = true;
                                colInfo.iDataGenerator.FKTableInfo = fkTbfkInfo;

                            }
                            else if (colInfo.ColumnName == "ActiveStatusId" && tbName != "ActiveStatus")
                            {
                                TableInfo2 fkTbfkInfo = new TableInfo2();
                                fkTbfkInfo.TableName = "ActiveStatus";
                                fkTbfkInfo.TableNameLower = fkTbfkInfo.TableName.ToLower();
                                fkTbfkInfo.PrimaryKeyName = fkTbfkInfo.TableName + "Id";
                                fkTbfkInfo.DisplayKeyName = fkTbfkInfo.TableName + "Name";

                                colInfo.iDataGenerator.IsForeignKeyColumn = true;
                                colInfo.iDataGenerator.FKTableInfo = fkTbfkInfo;
                            }
                            else if (colInfo.ColumnName == "CreatedBy")
                            {
                                TableInfo2 fkTbfkInfo = new TableInfo2();
                                fkTbfkInfo.TableName = "SystemUser";
                                fkTbfkInfo.TableNameLower = fkTbfkInfo.TableName.ToLower();
                                fkTbfkInfo.PrimaryKeyName = fkTbfkInfo.TableName + "Id";
                                fkTbfkInfo.DisplayKeyName = fkTbfkInfo.TableName + "Name";

                                colInfo.iDataGenerator.IsForeignKeyColumn = true;
                                colInfo.iDataGenerator.FKTableInfo = fkTbfkInfo;
                            }
                            else if (colInfo.ColumnName == "ModifiedBy")
                            {
                                TableInfo2 fkTbfkInfo = new TableInfo2();
                                fkTbfkInfo.TableName = "SystemUser";
                                fkTbfkInfo.TableNameLower = fkTbfkInfo.TableName.ToLower();
                                fkTbfkInfo.PrimaryKeyName = fkTbfkInfo.TableName + "Id";
                                fkTbfkInfo.DisplayKeyName = fkTbfkInfo.TableName + "Name";

                                colInfo.iDataGenerator.IsForeignKeyColumn = true;
                                colInfo.iDataGenerator.FKTableInfo = fkTbfkInfo;
                            }


                            if (colInfo.ColumnName == ("Parent" + tbInfo.TableName+"Id"))
                            {
                                tbInfo.IsSelfJoin=true;
                            }
                        }
                        TableInfo.Add(tbInfo.TableName, tbInfo);
                    }

                    // Prepare self join in fk table
                    foreach (KeyValuePair<string, TableInfo2> keyPair in TableInfo)
                    {
                        foreach (ColumnInfo2 colInfo in keyPair.Value.lstColumnInfo)
                        {
                            if (colInfo.iDataGenerator.FKTableInfo != null)
                            {
                                colInfo.iDataGenerator.FKTableInfo.IsSelfJoin = TableInfo[colInfo.iDataGenerator.FKTableInfo.TableName].IsSelfJoin;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Dictionary<String, int> _TEMP_CHECK_ALIAS_NAME = new Dictionary<string, int>();
        private static String CheckingAliasName(String aliasName)
        {
            if (aliasName == "is") aliasName = "iz";
            if (aliasName == "as") aliasName = "az";

            if (!_TEMP_CHECK_ALIAS_NAME.ContainsKey(aliasName))
            {
                _TEMP_CHECK_ALIAS_NAME.Add(aliasName, 1);
                return aliasName;
            }
            else
            {
                _TEMP_CHECK_ALIAS_NAME[aliasName] = _TEMP_CHECK_ALIAS_NAME[aliasName] + 1;
                return aliasName + "" + _TEMP_CHECK_ALIAS_NAME[aliasName];
            }
        }

    }
}
