﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context.model;
using SystemLibrary.syslib.core.connection;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel.context.util
{
    public class GeneratorUtil
    {
        public static List<String> GetListOfTableName(DBConnection conn)
        {
            string sql = @"SELECT table_name
                           FROM information_schema.tables 
                           WHERE table_schema='dbo' 
                           AND table_type='BASE TABLE' 
                           AND table_name not in('sysdiagrams') 
                           AND cast(table_name as varchar(100)) not like '[_]%' 
                           AND TABLE_NAME not like 'Survey_%'
                           ORDER by table_name";
            DataSet ds = conn.executeDataSet(sql);
            List<String> lstTables = new List<string>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                lstTables.Add(row["table_name"].ToString());
            }

            return lstTables;
        }

        public static List<ColumnInfo2> getListOfColumnInfo(DBConnection conn,String TableName)
        {
            string sql = "SELECT column_name,is_nullable FROM information_schema.columns WHERE table_schema='dbo' AND table_name='"+TableName+"' order by ORDINAL_POSITION";
            DataSet ds = conn.executeDataSet(sql);
            Dictionary<String, bool> ColumnNullable = new Dictionary<string, bool>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ColumnNullable.Add(row["column_name"].ToString(), row["is_nullable"].ToString().Equals("YES"));
            }

            List<ColumnInfo2> lstColumn = new List<ColumnInfo2>();
            sql = "select * From " + QueryUtil.PreFixBegin + TableName + QueryUtil.PreFixEnd;
            SqlDataReader reader = conn.executeDataReader(sql, null);

            DataTable tableInfo = reader.GetSchemaTable();

            foreach (DataRow row in tableInfo.Rows)
            {
                string ColumnName = row["ColumnName"].ToString();


                string DataType = row["DataType"].ToString().Replace("System.", "");
                bool AllowDBNull = ColumnNullable[ColumnName];
                int ColumnSize = Convert.ToInt32(row["ColumnSize"]);
                //if (DataType == "DateTime" && ColumnSize == 4)
                //{
                //    DataType = "Date";
                //}

                ColumnInfo2 colInfo = new ColumnInfo2(ColumnName, DataType, AllowDBNull, ColumnSize);
                lstColumn.Add(colInfo);
            }
            reader.Close();
            return lstColumn;
        }
    }
}
