﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.interf;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel
{
    public class StringGenerator : IDataGenerator
    {
        public StringGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "String";
            this.HtmlInputType = "text";
            if (ColumnName == "Password")
            {
                this.MVCInputType = "Password";
            }
            else
            {
                this.MVCInputType = "TextBox";
            }
            this.SearchOperator = "like";
        }

    }
}
