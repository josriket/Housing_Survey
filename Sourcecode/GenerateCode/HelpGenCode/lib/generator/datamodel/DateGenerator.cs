﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.interf;

namespace HelpGenCode.lib.generator.datamodel
{
    public class DateGenerator : IDataGenerator
    {
        public DateGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "DateTime";
            this.HtmlInputType = "text";
            this.MVCInputType = "TextBox";
            this.SearchOperator = "=";
        }

        public override string GenCode_Model_DeclareVariable()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("        [DataColumn]");
            code.AppendLine("        [LocalizedDisplayName(\"" + this.VariableName + "\", typeof(MyResources.Winplus.Resource))]");
            code.AppendLine("        public " + this.VariableType + ((this.VariableType == "String" || this.VariableType == "Boolean") ? "" : "?") + " " + this.VariableName + " { get; set; }");
            code.AppendLine("        public string " + this.VariableName + "Text");
            code.AppendLine("        {");
            code.AppendLine("            get { return StringUtil.FormatDate(" + this.VariableName + "); }");
            code.AppendLine("            set { " + this.VariableName + " = StringUtil.ToDate(value); }");
            code.AppendLine("        }");

            code.AppendLine("");

            return code.ToString();
        }



        public override string GenCode_AddEdit_Element()
        {
            StringBuilder code = new StringBuilder();
            String requireTxt = this.AllowNull ? "" : ",@required=\"\"";

            code.AppendLine("	                <div class=\"form-group col-sm-6\">");
            code.AppendLine("		                <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
            code.AppendLine("		                <div class=\"col-sm-8\">");

            code.AppendLine("                            <div class=\"input-group date\">");
            code.AppendLine("                                <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>");
            code.AppendLine("                                @Html.TextBoxFor(model => model." + this.ColumnName + "Text, new { @class = \"form-control\" " + requireTxt + " })");
            code.AppendLine("                            </div>");


            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");


            return code.ToString();
        }


        public override string GenCode_SearchForm_Element()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("                                                            <div class=\"col-sm-4\">");
            code.AppendLine("                                                                <div class=\"form-group\">");
            code.AppendLine("                                                                    <label>@Html.DisplayNameFor(model => model.search." + this.VariableName + ")</label>");

            code.AppendLine("                                                                    <div class=\"input-daterange input-group\">");
            code.AppendLine("                                                                        <input type=\"text\" class=\"input-sm form-control input-search\" id=\"search_" + this.ColumnName + "_From\" style=\"height:34px;\" value=\"\"/>");
            code.AppendLine("                                                                        <span class=\"input-group-addon\">to</span>");
            code.AppendLine("                                                                        <input type=\"text\" class=\"input-sm form-control input-search\" id=\"search_" + this.ColumnName + "_To\" style=\"height:34px;\" value=\"\" />");
            code.AppendLine("                                                                    </div>");

            code.AppendLine("                                                                </div>");
            code.AppendLine("                                                            </div>");

            return code.ToString();

        }



        public override string GenCode_SearchForm_JSGridData()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("                { \"name\": \"" + this.ColumnName + "\", \"data\": \"" + this.ColumnName + "\", \"render\": function (data, type, row, meta) { return FormatDate(data, type, row, meta); } },");
            return code.ToString();
        }

        public override string GenCode_SearchModel_WhereCondition()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("                /* Advance Search by " + this.ColumnName + " From*/");
            code.AppendLine("                if (StringUtil.IsContainsKeyValue(SearchKey, \"search_" + this.ColumnName + "_From\", ref text))");
            code.AppendLine("                {");
            code.AppendLine("                    sqlQuery.AppendLine(\"and " + this.TableName + ".`" + this.ColumnName + "` >= @" + this.ColumnName + "_From\");");
            code.AppendLine("                    param.Add(new DBParameter(\"" + this.ColumnName + "_From\", StringUtil.To" + this.VariableType + "(text)));");
            code.AppendLine("                }");
            code.AppendLine("");
            code.AppendLine("                /* Advance Search by " + this.ColumnName + " To*/");
            code.AppendLine("                if (StringUtil.IsContainsKeyValue(SearchKey, \"search_" + this.ColumnName + "_To\", ref text))");
            code.AppendLine("                {");
            code.AppendLine("                    sqlQuery.AppendLine(\"and " + this.TableName + ".`" + this.ColumnName + "` < DATE_ADD(@" + this.ColumnName + "_To,INTERVAL 1 DAY)\");");
            code.AppendLine("                    param.Add(new DBParameter(\"" + this.ColumnName + "_To\", StringUtil.To" + this.VariableType + "(text)));");
            code.AppendLine("                }");

            return code.ToString();
        }
    }
}
