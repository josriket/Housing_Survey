﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel.interf
{
    public abstract class IDataGenerator
    {

        protected IDataGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
        {
            this.TableName = TableName;
            this.DBDataType = DBDataType;
            this.AllowNull = AllowNull;
            this.ColumnName = ColumnName;
            this.VariableName = this.ColumnName;
            this.ColumnIndex = ColumnIndex;
            this.IsStandardColumn = GeneratorContext.LstStandardColumn.Contains(this.ColumnName);
            this.IsForeignKeyColumn = false;
            this.IsPrimaryKey = this.ColumnName.Equals(this.TableName + "Id");
        }

        public String TableName { get; set; }
        public String DBDataType { get; set; }
        public String VariableType { get; set; }
        public String HtmlInputType { get; set; }
        public String MVCInputType { get; set; }
        public String ColumnName { get; set; }
        public String VariableName { get; set; }
        public String SearchOperator { get; set; }
        public Boolean AllowNull { get; set; }
        public int ColumnIndex { get; set; }
        public Boolean IsStandardColumn { get; set; }
        public Boolean IsForeignKeyColumn { get; set; }
        public Boolean IsPrimaryKey { get; set; }
        public TableInfo2 FKTableInfo { get; set; }



        #region Controller.cs

        #endregion



        #region ControllerApi.cs

        #endregion



        #region Model.cs

        public virtual string GenCode_Model_DeclareVariable()
        {
            StringBuilder code = new StringBuilder();

            if (this.VariableName == "ConfigValue")
            {
                code.AppendLine("        [AllowHtml]");
            }
            if (this.VariableName == TableName + "Id")
            {
                code.AppendLine("        [DataColumn(\"PK\")]");
            }
            else
            {
                code.AppendLine("        [DataColumn]");
            }
            code.AppendLine("        [LocalizedDisplayName(\"" + this.VariableName + "\", typeof(MyResources.Winplus.Resource))]");
            code.AppendLine("        public " + this.VariableType + ((this.VariableType == "String" || this.VariableType == "Byte[]" || this.VariableType == "Boolean") ? "" : "?") + " " + this.VariableName + " { get; set; }");
            if (this.VariableName.EndsWith("FileUploadIds"))
            {
                code.AppendLine("        public List<FileUpload> Lst" + this.VariableName.Substring(0, this.VariableName.Length - 3) + " { get; set; }");
                code.AppendLine("        public Int64[] " + this.VariableName.Substring(0, this.VariableName.Length - 1) + "");
                code.AppendLine("        {");
                code.AppendLine("            get { return StringUtil.SplitToLong(',', " + this.VariableName + ");}");
                code.AppendLine("            set { " + this.VariableName + " = StringUtil.Join(\",\", value);}");
                code.AppendLine("        }");


            }
            code.AppendLine("");

            return code.ToString();
        }

        public virtual string GenCode_Model_DeclareVariableFK()
        {
            StringBuilder code = new StringBuilder();

            if (IsForeignKeyColumn)
            {
                code.AppendLine("        [LocalizedDisplayName(\"" + FKTableInfo.TableName + "Name\", typeof(MyResources.Winplus.Resource))]");
                code.AppendLine("        public String " + this.ColumnName + FKTableInfo.TableName + "Name { get; set; }");
                code.AppendLine("");

            }

            return code.ToString();
        }


        public virtual string GenCode_Model_GetterHtml()
        {
            StringBuilder code = new StringBuilder();

            return code.ToString();
        }

        public virtual string GenCode_Model_SetterHtml()
        {
            StringBuilder code = new StringBuilder();


            return code.ToString();
        }



        #endregion



        #region SearchModel.cs


        public virtual string GenCode_SearchModel_WhereCondition()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("                /* Advance Search by " + this.ColumnName + " */");
            code.AppendLine("                if (StringUtil.IsContainsKeyValue(SearchKey, \"search_" + this.ColumnName + "\", ref text))");
            code.AppendLine("                {");
            if (this.SearchOperator == "like")
            {
                code.AppendLine("                    sqlQuery.AppendLine(\"and " + this.TableName + ".[" + this.ColumnName + "] " + this.SearchOperator + " '%'+@" + this.ColumnName + "+'%'\");");
            }
            else
            {
                code.AppendLine("                    sqlQuery.AppendLine(\"and " + this.TableName + ".[" + this.ColumnName + "] " + this.SearchOperator + " @" + this.ColumnName + "\");");
            }

            code.AppendLine("                    param.Add(new DBParameter(\"" + this.ColumnName + "\", StringUtil.To" + this.VariableType + "(text)));");
            code.AppendLine("                }");

            return code.ToString();
        }

        public virtual string GenCode_SearchModel_ListRender()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("		lstRenderType.add(new Render" + this.VariableType + "(\"" + this.ColumnName + "\", StringResourceUtil.show(\"field." + this.ColumnName + "\"), new ColumnFilter" + this.VariableType + "(\"" + this.TableName + ".[" + this.ColumnName + "]\") ,\"" + this.TableName + ".[" + this.ColumnName + "]\"));");

            return code.ToString();
        }


        public virtual string GenCode_SearchModel_ListRenderFK()
        {
            StringBuilder code = new StringBuilder();
            if (IsForeignKeyColumn)
            {
                String specialColumnName = "";
                if (ColumnName == "CreatedBy" || ColumnName == "ModifiedBy")
                {
                    specialColumnName = ColumnName;
                }

                code.AppendLine("		lstRenderType.add(new RenderString(\"" + specialColumnName + FKTableInfo.TableName + "Name" + "\", StringResourceUtil.show(\"field." + FKTableInfo.TableName + "Name" + "\"), new ColumnFilterString(\"" + FKTableInfo.TableName + "_" + this.ColumnName + ".[" + FKTableInfo.TableName + "Name" + "]\") ,\"" + FKTableInfo.TableName + "_" + this.ColumnName + ".[" + FKTableInfo.TableName + "Name" + "]\"));");
            }
            return code.ToString();
        }

        public virtual string GenCode_SearchModel_ListMappingForm()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("		lstMappingForm.add(\"" + this.ColumnName + "\");");

            return code.ToString();
        }

        public virtual String GenCode_SearchModel_SqlJoinTable()
        {
            StringBuilder code = new StringBuilder();
            if (IsForeignKeyColumn)
            {
                code.AppendLine("                Left Join [" + FKTableInfo.TableName + "] " + FKTableInfo.TableName + "_" + this.ColumnName + " on " + FKTableInfo.TableName + "_" + this.ColumnName + ".[" + FKTableInfo.PrimaryKeyName + "]=" + this.TableName + ".[" + this.ColumnName + "]");
            }
            return code.ToString();
        }

        #endregion



        #region Repository.cs


        public virtual string GenCode_Repository_SearchBy()
        {
            StringBuilder code = new StringBuilder();

            code.AppendLine("        public " + this.TableName + " FindBy" + this.ColumnName + "(DBConnection conn, " + this.VariableType + " " + this.VariableName + ", string orderBy=\"\")");
            code.AppendLine("        {");
            code.AppendLine("            String sql = \"Select Top 1 " + this.TableName + ".* From [" + this.TableName + "] " + this.TableName + " Where " + this.TableName + ".[" + this.ColumnName + "]=@" + this.VariableName + " \" + orderBy;");
            code.AppendLine("");
            code.AppendLine("            return QueryUtil.Find<" + this.TableName + ">(conn, sql, new DBParameter(\"" + this.ColumnName + "\", " + this.VariableName + "));");
            code.AppendLine("        }");
            code.AppendLine("");
            code.AppendLine("        public List<" + this.TableName + "> FindsBy" + this.ColumnName + "(DBConnection conn, " + this.VariableType + " " + this.VariableName + ", string orderBy=\"\")");
            code.AppendLine("        {");
            code.AppendLine("            String sql = \"Select " + this.TableName + ".* From [" + this.TableName + "] " + this.TableName + " Where " + this.TableName + ".[" + this.ColumnName + "]=@" + this.VariableName + " \" + orderBy;");
            code.AppendLine("");
            code.AppendLine("            return QueryUtil.FindList<" + this.TableName + ">(conn, sql, new DBParameter(\"" + this.ColumnName + "\", " + this.VariableName + "));");
            code.AppendLine("        }");
            code.AppendLine("");
            code.AppendLine("        public int DeleteBy" + this.ColumnName + "(DBConnection conn, " + this.VariableType + " " + this.VariableName + ", string whereText=\"\")");
            code.AppendLine("        {");
            code.AppendLine("            String sql = \"Delete From [" + this.TableName + "] Where [" + this.ColumnName + "]=@" + this.VariableName + " \" + whereText;");
            code.AppendLine("            return conn.executeUpdate(sql,new DBParameter(\"" + this.ColumnName + "\", " + this.VariableName + "));");
            code.AppendLine("        }");
            code.AppendLine("");

            return code.ToString();
        }

        #endregion

        #region AddEdit.html

        public virtual string GenCode_AddEdit_Element()
        {
            String requireTxt = this.AllowNull ? "" : ",@required=\"\"";

            StringBuilder code = new StringBuilder();

            bool hascondition = false;
            if (this.ColumnName == this.TableName + "Name")
            {
                hascondition = true;
                code.AppendLine("	                @if(!Model.IsHiddenName){");
            }

            if (this.ColumnName.EndsWith("FileUploadIds"))
            {
                string fileUploadId = this.ColumnName.Substring(0, this.ColumnName.Length - 1);
                string fileUpload = this.ColumnName.Substring(0, this.ColumnName.Length - 3);

                code.AppendLine("	                <div class=\"form-group col-sm-12\">");
                code.AppendLine("		                <div class=\"col-sm-2\"></div>");
                code.AppendLine("		                <div class=\"form-group col-sm-10\" style=\"padding-left:25px;\">");
                code.AppendLine("                            <div class=\"panel panel-default\">");
                code.AppendLine("                                <div class=\"panel-heading\">");
                code.AppendLine("                                    @Html.DisplayNameFor(model => model." + this.ColumnName + ")");
                code.AppendLine("                                </div>");
                code.AppendLine("                                <div class=\"panel-body\">");
                code.AppendLine("                                    @{");
                code.AppendLine("                                        var setting" + fileUpload + " = new Library.Winplus.syslib.standard.fileupload.model.FileUploadSetting()");
                code.AppendLine("                                        {");
                code.AppendLine("                                            LstFileUpload = Model.Lst" + fileUpload + ",");
                code.AppendLine("                                            FieldName = \"" + fileUploadId + "\",");
                code.AppendLine("                                            MaxFileSize = 24");
                code.AppendLine("                                        };");
                code.AppendLine("                                    }");
                code.AppendLine("                                    @Html.Partial(\"~/Views/standard/fileupload/onepageupload.cshtml\", setting" + fileUpload + ")");
                code.AppendLine("                                </div>");
                code.AppendLine("                            </div>");
                code.AppendLine("		                </div>");
                code.AppendLine("	                </div>");
            }
            else
            {

                code.AppendLine("	                <div class=\"form-group col-sm-6\">");
                code.AppendLine("		                <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
                code.AppendLine("		                <div class=\"col-sm-8\">");

                if (IsForeignKeyColumn)
                {
                    code.AppendLine("                            <div class=\"input-group\">");
                    if (this.FKTableInfo.IsSelfJoin)
                    {
                        code.AppendLine("                                <span class=\"input-group-addon\" onclick=\"openJTreeDialog('" + this.FKTableInfo.TableName + "','" + this.VariableName + "')\"><i class=\"fa fa-filter\"></i></span>");
                    }
                    else
                    {
                        code.AppendLine("                                <span class=\"input-group-addon\" onclick=\"openChooseDialog('" + this.FKTableInfo.TableName + "','" + this.VariableName + "')\"><i class=\"fa fa-filter\"></i></span>");
                    }
                    code.AppendLine("                                @Html.DropDownListFor(model => model." + this.VariableName + ",Model.GetSelectedItems(\"" + this.FKTableInfo.TableName + "\", Model." + this.VariableName + ",\"\"), new { @class = \"form-control\" })");
                    code.AppendLine("                            </div>");
                }
                else
                {
                    code.AppendLine("			                @Html." + this.MVCInputType + "For(model=>model." + this.VariableName + ", new{@class = \"form-control\",@title = @Html.DisplayNameFor(model => model." + this.VariableName + ")" + requireTxt + "})");
                }

                code.AppendLine("		                </div>");
                code.AppendLine("	                </div>");
            }
            if (hascondition)
            {
                code.AppendLine("	                }");
            }


            return code.ToString();
        }

        #endregion



        #region Search.html

        public virtual string GenCode_SearchForm_TableFilter()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("<th><input type=\"text\" id=\"col_" + this.ColumnIndex + "\" placeholder=\"${resource.show('field." + this.ColumnName + "')}\" class=\"form-control control-search\" /></th>");
            return code.ToString();
        }

        public virtual string GenCode_SearchForm_JSGridColumn()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("{ \"data\": \"" + this.ColumnName + "\" }");
            return code.ToString();
        }

        public virtual string GenCode_SearchForm_JSGridData()
        {
            StringBuilder code = new StringBuilder();
            string DBDataType = this.DBDataType;
            if (this.ColumnName.EndsWith("Latitude") || this.ColumnName.EndsWith("Longitude"))
            {
                DBDataType = "String";
            }
            code.AppendLine("                { \"name\": \"" + this.ColumnName + "\", \"data\": \"" + this.ColumnName + "\", \"render\": function (data, type, row, meta) { return Format" + DBDataType + "(data, type, row, meta); } },");
            return code.ToString();
        }

        public virtual string GenCode_SearchForm_Element()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("                                                            <div class=\"col-sm-4\">");
            code.AppendLine("                                                                <div class=\"form-group\">");
            code.AppendLine("                                                                    <label>@Html.DisplayNameFor(model => model.search." + this.VariableName + ")</label>");
            if (IsForeignKeyColumn)
            {

                code.AppendLine("                                                                <div class=\"input-group\">");
                if (this.FKTableInfo.IsSelfJoin)
                {
                    code.AppendLine("                                                                    <span class=\"input-group-addon\" onclick=\"openJTreeDialog('" + this.FKTableInfo.TableName + "','search_" + this.VariableName + "')\"><i class=\"fa fa-filter\"></i></span>");
                }
                else
                {
                    code.AppendLine("                                                                    <span class=\"input-group-addon\" onclick=\"openChooseDialog('" + this.FKTableInfo.TableName + "','search_" + this.VariableName + "')\"><i class=\"fa fa-filter\"></i></span>");
                }

                code.AppendLine("                                                                    @Html.DropDownListFor(model => model.search." + this.VariableName + ",Model.GetSelectedItems(\"" + this.FKTableInfo.TableName + "\", Model.search." + this.VariableName + ",\"\"), new { @class = \"form-control input-search\" })");
                code.AppendLine("                                                                </div>");

            }
            else
            {
                code.AppendLine("                                                                    @Html.TextBoxFor(model => model.search." + this.ColumnName + ", new { @class = \"form-control input-search\" })");
                // code.AppendLine("											<input type=\"" + this.HtmlInputType + "\" name=\"" + this.ColumnName + "\" id=\"" + this.ColumnName + "\" th:placeholder=\"${resource.show('field." + this.ColumnName + "')}\" class=\"form-control control-search\" />");
            }
            code.AppendLine("                                                                </div>");
            code.AppendLine("                                                            </div>");

            return code.ToString();
        }

        #endregion



    }
}
