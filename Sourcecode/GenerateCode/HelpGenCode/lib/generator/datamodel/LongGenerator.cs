﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.interf;

namespace HelpGenCode.lib.generator.datamodel
{
    public class LongGenerator : IDataGenerator
    {
        public LongGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "Int64";
            this.HtmlInputType = "number";
            this.MVCInputType = "TextBox";
            this.SearchOperator = "=";
        }



        public override string GenCode_AddEdit_Element()
        {
            String requireTxt = this.AllowNull ? "" : ",@required=\"\"";

            StringBuilder code = new StringBuilder();
            code.AppendLine("	                <div class=\"form-group col-sm-6\">");
            code.AppendLine("		                <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
            code.AppendLine("		                <div class=\"col-sm-8\">");

            if (IsForeignKeyColumn)
            {
                code.AppendLine("                            <div class=\"input-group\">");

                if (this.FKTableInfo.IsSelfJoin)
                {
                    code.AppendLine("                                <span class=\"input-group-addon\" onclick=\"openJTreeDialog('" + this.FKTableInfo.TableName + "','" + this.VariableName + "')\"><i class=\"fa fa-filter\"></i></span>");
                }
                else
                {
                    code.AppendLine("                                <span class=\"input-group-addon\" onclick=\"openChooseDialog('" + this.FKTableInfo.TableName + "','" + this.VariableName + "')\"><i class=\"fa fa-filter\"></i></span>");
                }
                code.AppendLine("                                @Html.DropDownListFor(model => model." + this.VariableName + ",Model.GetSelectedItems(\"" + this.FKTableInfo.TableName + "\", Model." + this.VariableName + ",\"\"), new { @class = \"form-control\" })");
                code.AppendLine("                            </div>");

            }
            else
            {
                code.AppendLine("			                @Html." + this.MVCInputType + "For(model=>model." + this.VariableName + ", new{@class = \"form-control\",@title = @Html.DisplayNameFor(model => model." + this.VariableName + "),@type=\"number\"" + requireTxt + "})");
            }

            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");
            return code.ToString();
        }
    }
}
