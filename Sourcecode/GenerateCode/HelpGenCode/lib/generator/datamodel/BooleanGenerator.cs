﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.interf;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel
{
    public class BooleanGenerator : IDataGenerator
    {
        public BooleanGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "Boolean";
            this.HtmlInputType = "text";
            this.MVCInputType = "CheckBox";
            this.SearchOperator = "=";
        }

        public override string GenCode_AddEdit_Element()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("	                <div class=\"form-group col-sm-12\">");
            code.AppendLine("		                <label class=\"col-sm-2 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
            code.AppendLine("		                <div class=\"col-sm-10\">");


            code.AppendLine("                            @Html.CheckBoxFor(model=>model." + ColumnName + ", new { @class = \"form-control i-checks\" })");


            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");



            return code.ToString();
        }

        public override string GenCode_SearchForm_Element()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("                                                            <div class=\"col-sm-4\">");
            code.AppendLine("                                                                <div class=\"form-group\">");

            code.AppendLine("                                                                    <label>@Html.DisplayNameFor(model => model.search." + this.VariableName + ")</label>");

            code.AppendLine("                                                                    <select class=\"form-control input-search\" data-val=\"true\" id=\"search_" + this.ColumnName + "\">");
            code.AppendLine("                                                                        <option value=\"\"></option>");
            code.AppendLine("                                                                        <option value=\"true\">True</option>");
            code.AppendLine("                                                                        <option value=\"false\">False</option>");
            code.AppendLine("                                                                    </select>");

            //code.AppendLine("                                                                    @Html.CheckBoxFor(model => model.search." + this.ColumnName + ", new { @class = \"form-control i-checks input-search\" })"); 
            
            code.AppendLine("                                                                </div>");
            code.AppendLine("                                                            </div>");

            return code.ToString();
        }

    }
}
