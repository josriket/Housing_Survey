﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.interf;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.generator.datamodel
{
    public class HyperLinkGenerator : IDataGenerator
    {
        public HyperLinkGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "String";
            this.HtmlInputType = "text";
            if (ColumnName == "Password")
            {
                this.MVCInputType = "Password";
            }
            else
            {
                this.MVCInputType = "TextBox";
            }
            this.SearchOperator = "like";
        }



        public override string GenCode_AddEdit_Element()
        {
            StringBuilder code = new StringBuilder();
            String requireTxt = this.AllowNull ? "" : ",@required=\"\"";

            code.AppendLine("	                <div class=\"form-group col-sm-6\">");
            code.AppendLine("		                <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model." + this.ColumnName + ")" + (this.AllowNull ? "" : "<span class=\"span-required\">*</span>") + "</label>");
            code.AppendLine("		                <div class=\"col-sm-8\">");

            code.AppendLine("                            <div class=\"input-group hyperlink\">");
            code.AppendLine("                                @Html.TextBoxFor(model=>model." + this.ColumnName + ", new{@class = \"form-control\",@title = @Html.DisplayNameFor(model => model." + this.ColumnName + ")" + requireTxt + "})");
            code.AppendLine("                                <span class=\"input-group-addon\" onclick=\"DisplayImage($('#" + this.ColumnName + "'))\"><i class=\"fa fa-file-image-o\"></i></span>");
            code.AppendLine("                            </div>");

            code.AppendLine("		                </div>");
            code.AppendLine("	                </div>");


            return code.ToString();
        }

        public override string GenCode_SearchForm_JSGridData()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("                { \"name\": \"" + this.ColumnName + "\", \"data\": \"" + this.ColumnName + "\", \"render\": function (data, type, row, meta) { return FormatHyperLink(data, type, row, meta); } },");
            return code.ToString();
        }
    }
}
