﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.interf;

namespace HelpGenCode.lib.generator.datamodel
{
    public class UUIDGenerator : IDataGenerator
    {
        public UUIDGenerator(String TableName, String DBDataType, String ColumnName, Boolean AllowNull, int ColumnIndex)
            :base(TableName, DBDataType, ColumnName, AllowNull, ColumnIndex)
        {
            this.VariableType = "Guid";
            this.HtmlInputType = "text";
            this.MVCInputType = "TextBox";
            this.SearchOperator = "=";
        }

        public override string GenCode_AddEdit_Element()
        {
            return string.Empty;
        }


        public override string GenCode_SearchForm_Element()
        {
            return string.Empty;
        }
    }
}
