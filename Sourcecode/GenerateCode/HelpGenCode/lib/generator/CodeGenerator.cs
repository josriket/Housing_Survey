﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel;
using HelpGenCode.lib.generator.datamodel.interf;
using HelpGenCode.lib.generator.method;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator
{
    public class CodeGenerator
    {
        public void StartGenerate(){
            List<ICodeGenerator> lstGenerator = new List<ICodeGenerator>();

            string destinationFolder = string.Empty;

            destinationFolder = @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Library.Winplus\syslib\standard\<<TableLowerName>>";
            lstGenerator.Add(new CodeGenModel(destinationFolder + @"\model", true));
            lstGenerator.Add(new CodeGenPartialModel(destinationFolder + @"\model", false));
            lstGenerator.Add(new CodeGenSearchModel(destinationFolder + @"\search", true));
            lstGenerator.Add(new CodeGenPartialSearchModel(destinationFolder + @"\search", false));
            lstGenerator.Add(new CodeGenRepository(destinationFolder + @"\repository", true));
            lstGenerator.Add(new CodeGenPartialRepository(destinationFolder + @"\repository", false));

            destinationFolder = @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Controllers\Standard\<<TableLowerName>>";
            lstGenerator.Add(new CodeGenController(destinationFolder, true));
            lstGenerator.Add(new CodeGenControllerImpl(destinationFolder, false));

            destinationFolder = @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Views\standard\<<TableLowerName>>";
            lstGenerator.Add(new CodeGenAddEditForm(destinationFolder, true));
            lstGenerator.Add(new CodeGenSearchForm(destinationFolder, true));
            lstGenerator.Add(new CodeGenImportForm(destinationFolder, true));

            destinationFolder = @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Library.Winplus\syslib\core\domain";
            lstGenerator.Add(new CodeGenDisplayIndex(destinationFolder, true));
            lstGenerator.Add(new CodeGenActiveStatusId(destinationFolder, true));

            foreach (ICodeGenerator _g in lstGenerator)
            {
                _g.Generate();
            }
        }
    }
}
