﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenAddEditForm : ICodeGenerator
    {
        public CodeGenAddEditForm(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("@model Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model." + tbInfo.TableName + "");
                code.AppendLine("");
                code.AppendLine("<div class=\"col-lg-12\">");
                code.AppendLine("    <div class=\"ibox float-e-margins\">");
                code.AppendLine("        <form id=\"addeditform-1\" method=\"post\" class=\"form-horizontal\" action=\"@Url.Content(\"~/" + tbInfo.TableNameLower + "/save\")\" onsubmit=\"_initialSubmitForm();ajaxSubmitForm(event,this,'btn-save',_gotoSave);\" enctype=\"multipart/form-data\">");
                code.AppendLine("            <div class=\"ibox-title\">");
                code.AppendLine("                <div class=\"row\">");
                code.AppendLine("                    <div class=\"col-sm-6\">");
                code.AppendLine("                        <h5>");
                code.AppendLine("                            @if(Model."+tbInfo.TableName+"Id.HasValue){");
                code.AppendLine("                                @MyResources.Winplus.Resource.Update");
                code.AppendLine("                            }else{");
                code.AppendLine("                                @MyResources.Winplus.Resource.Create");
                code.AppendLine("                            }");
                code.AppendLine("                            @Html.DisplayNameFor(model => model." + tbInfo.TableName + "Name)");
                code.AppendLine("                        </h5>");
                code.AppendLine("                    </div>");
                code.AppendLine("                    <div class=\"col-sm-6\" style=\"text-align:right;\">");
                code.AppendLine("                        <div class=\"form-group\" style=\"padding-right:10px;\">");
                code.AppendLine("                            <button class=\"btn btn-white\" type=\"button\" onclick=\"gotoSearch();\">@MyResources.Winplus.Resource.Cancel</button>");
                code.AppendLine("                            @if (Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Add\") || Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Edit\"))");
                code.AppendLine("                            {");
                code.AppendLine("                                <button class=\"btn ladda-button btn-primary\" data-style=\"expand-left\" type=\"submit\" id=\"btn-save\">@MyResources.Winplus.Resource.SaveChange</button>");
                code.AppendLine("                            }");
                code.AppendLine("                        </div>");
                code.AppendLine("                    </div>");
                code.AppendLine("                </div>");
                code.AppendLine("            </div>");
                code.AppendLine("            <div class=\"ibox-content\">");
                code.AppendLine("            <div class=\"row\">");
                code.AppendLine("                    @Html.HiddenFor(model => model." + tbInfo.TableName + "Id)");
                code.AppendLine("");


                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if ((!col.iDataGenerator.IsStandardColumn || col.ColumnName.Equals("DisplayIndex") || col.ColumnName.Equals("ActiveStatusId")) && !col.iDataGenerator.IsPrimaryKey)
                    {
                        code.Append(col.iDataGenerator.GenCode_AddEdit_Element());
                    }
                }

                code.AppendLine("            </div>");

                code.AppendLine("            <div class=\"ibox-footer\">");
                code.AppendLine("                <div class=\"row\">");
                code.AppendLine("                    <div class=\"form-group col-sm-6\">");
                code.AppendLine("    		            <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model.CreatedOn)</label>");
                code.AppendLine("    		            <label class=\"col-sm-8 control-label\" style=\"font-weight:normal;text-align:left\">@Html.DisplayTextFor(model => model.CreatedOn)</label>");
                code.AppendLine("    	            </div>");
                code.AppendLine("    	            <div class=\"form-group col-sm-6\">");
                code.AppendLine("    		            <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model.CreatedBy)</label>");
                code.AppendLine("    		            <label class=\"col-sm-8 control-label\" style=\"font-weight:normal;text-align:left\">@Html.DisplayTextFor(model => model.CreatedBySystemUserName)</label>");
                code.AppendLine("    	            </div>");
                code.AppendLine("    	            <div class=\"form-group col-sm-6\">");
                code.AppendLine("    		            <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model.ModifiedOn)</label>");
                code.AppendLine("    		            <label class=\"col-sm-8 control-label\" style=\"font-weight:normal;text-align:left\">@Html.DisplayTextFor(model => model.ModifiedOn)</label>");
                code.AppendLine("    	            </div>");
                code.AppendLine("    	            <div class=\"form-group col-sm-6\">");
                code.AppendLine("    		            <label class=\"col-sm-4 control-label\">@Html.DisplayNameFor(model => model.ModifiedBy)</label>");
                code.AppendLine("    		            <label class=\"col-sm-8 control-label\" style=\"font-weight:normal;text-align:left\">@Html.DisplayTextFor(model => model.ModifiedBySystemUserName)</label>");
                code.AppendLine("    	            </div>");
                code.AppendLine("                </div>");
                code.AppendLine("              </div>");

                code.AppendLine("        </div>");
                code.AppendLine("        </form>");
                code.AppendLine("    </div>");
                code.AppendLine("</div>");
                code.AppendLine("<script>");
                code.AppendLine("");
                code.AppendLine("    /*  */");
                code.AppendLine("    function _initialAddEditForm() {");
                code.AppendLine("        bindingStandardField();");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.ColumnName.EndsWith("FileUploadIds"))
                    {
                        code.AppendLine("        try { InitialDropZone_" + col.ColumnName.Substring(0, col.ColumnName.Length - 1) + "(); } catch (e) { alert(\"Dropzone[" + col.ColumnName.Substring(0, col.ColumnName.Length - 3) + "] Error->\" + e); }");
                    }
                }
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function _initialSubmitForm() {");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.iDataGenerator is TextGenerator)
                    {
                        code.AppendLine("        $(\"#" + col.iDataGenerator.ColumnName + "\").val($('#" + col.iDataGenerator.ColumnName + "-Editor').summernote('code'));");
                    }
                }
                

                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function _gotoSave(data, status) {");
                code.AppendLine("        if (data.StatusCode == \"0\") {");
                code.AppendLine("            gotoSearch();");
                code.AppendLine("        } else {");
                code.AppendLine("            alert(data.Message);");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("</script>");


                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), "addedit.cshtml", finalCode, IsOverride);
                Console.WriteLine(" - Generated AddEditForm " + tbInfo.TableName);
            }
        }
    }
}
