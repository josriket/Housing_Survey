﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenControllerImpl : ICodeGenerator
    {
        public CodeGenControllerImpl(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("using System;");
                code.AppendLine("using System.Collections.Generic;");
                code.AppendLine("using System.Linq;");
                code.AppendLine("using System.Text;");
                code.AppendLine("using System.Threading.Tasks;");
                code.AppendLine("using System.Web.Mvc;");
                code.AppendLine("using Library.Winplus.syslib.core.connection;");
                code.AppendLine("using Library.Winplus.syslib.customize.loginuser.model;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".search;");
                code.AppendLine("using Library.Winplus.syslib.system.common.model;");
                code.AppendLine("using System.Data;");
                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard."+tbInfo.TableNameLower+".controller");
                code.AppendLine("{");
                code.AppendLine("    public partial class " + tbInfo.TableName + "Controller");
                code.AppendLine("    {");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model, SearchResultModel result)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, " + tbInfo.TableName + " model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, " + tbInfo.TableName + " model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + " model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + " model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<" + tbInfo.TableName + "> lstImport)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<" + tbInfo.TableName + "> lstImport)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        [NonAction]");
                code.AppendLine("        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model, DataSet ds)");
                code.AppendLine("        {");
                code.AppendLine("");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("}");


                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), tbInfo.TableName + "ControllerImpl.cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated ControllerImpl" + tbInfo.TableName);
            }
        }
    }
}
