﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenPartialModel : ICodeGenerator
    {
        public CodeGenPartialModel(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("using System;");
                code.AppendLine("using System.Collections.Generic;");
                code.AppendLine("using System.Linq;");
                code.AppendLine("using System.Text;");
                code.AppendLine("using System.Threading.Tasks;");
                code.AppendLine("using System.Web.Mvc;");
                code.AppendLine("using Library.Winplus.syslib.core.simple;");
                code.AppendLine("using Library.Winplus.syslib.core.util;");
                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model");
                code.AppendLine("{");
                code.AppendLine("    public partial class " + tbInfo.TableName);
                code.AppendLine("    {");

                code.AppendLine("    }");
                code.AppendLine("}");



                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), tbInfo.TableName + "Partial.cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated Model " + tbInfo.TableName);
            }
        }
    }
}
