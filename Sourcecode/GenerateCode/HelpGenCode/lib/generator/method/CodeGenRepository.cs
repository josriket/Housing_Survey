﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenRepository : ICodeGenerator
    {
        public CodeGenRepository(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }


        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();

                code.AppendLine("using System;");
                code.AppendLine("using System.Collections.Generic;");
                code.AppendLine("using Library.Winplus.syslib.core.connection;");
                code.AppendLine("using Library.Winplus.syslib.core.simple;");
                code.AppendLine("using Library.Winplus.syslib.core.util;");
                code.AppendLine("using Library.Winplus.syslib.customize.loginuser.model;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model;");
                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".repository");
                code.AppendLine("{");
                code.AppendLine("    public partial class " + tbInfo.TableName + "Repository : SimpleRepository<" + tbInfo.TableName + ">");
                code.AppendLine("    {");

                code.AppendLine("        #region General Query");
                code.AppendLine("");

                List<string> InsertFieldCol = new List<string>();
                List<string> InsertFieldValue = new List<string>();
                List<string> UpdateField = new List<string>();
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.ColumnName != tbInfo.TableName + "Id")
                    {
                        if (col.ColumnName != "ModifiedOn" && col.ColumnName != "ModifiedBy")
                        {
                            InsertFieldCol.Add("[" + col.ColumnName + "]");
                            if (col.ColumnName == "CreatedOn")
                            {
                                InsertFieldValue.Add("getdate()");
                            }
                            else
                            {
                                InsertFieldValue.Add("@" + col.ColumnName);
                            }
                        }

                        if (col.ColumnName != "CreatedOn" && col.ColumnName != "CreatedBy")
                        {
                            if (col.ColumnName == "ModifiedOn")
                            {
                                UpdateField.Add("[" + col.ColumnName + "]=getdate()");
                            }
                            else
                            {

                                UpdateField.Add("[" + col.ColumnName + "]=@" + col.ColumnName);
                            }
                        }
                    }
                }

                code.AppendLine("        public " + tbInfo.TableName + "Repository()");
                code.AppendLine("        {");
                code.AppendLine("            SqlInsert = @\"Insert Into [" + tbInfo.TableName + "](" + string.Join(", ", InsertFieldCol) + ") Values(" + string.Join(", ", InsertFieldValue) + ")\";");
                code.AppendLine("            SqlUpdate = @\"Update [" + tbInfo.TableName + "] Set " + string.Join(", ", UpdateField) + " Where [" + tbInfo.TableName + "Id]=@" + tbInfo.TableName + "Id\";");
                code.AppendLine("            SqlDelete = @\"Delete From [" + tbInfo.TableName + "] Where [" + tbInfo.TableName + "Id]=@Id\";");

                if (tbInfo.AllColumnName.Contains("ActiveStatusId"))
                {
                    code.AppendLine("            SqlActiveStatus = @\"Update [" + tbInfo.TableName + "] Set [ActiveStatusId]=@ActiveStatusId Where [" + tbInfo.TableName + "Id]=@Id\";");
                }
                code.AppendLine("            SqlFindAll = \"Select sm.*" + ((tbInfo.TableName == "ActiveStatus" || !tbInfo.AllColumnName.Contains("ActiveStatusId")) ? "" : ", az.[ActiveStatusName] as [ActiveStatusName]") + ", su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [" + tbInfo.TableName + "] sm " + ((tbInfo.TableName == "ActiveStatus" || !tbInfo.AllColumnName.Contains("ActiveStatusId")) ? "" : "Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] ") + "Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1\";");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        #endregion");
                code.AppendLine("");

                code.AppendLine("        #region Repository method");
                code.AppendLine("");
                code.AppendLine("        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<" + tbInfo.TableName + "> lstImport)");
                code.AppendLine("        {");
                code.AppendLine("            if (lstImport != null)");
                code.AppendLine("            {");
                code.AppendLine("                foreach (" + tbInfo.TableName + " model in lstImport)");
                code.AppendLine("                {");
                code.AppendLine("                    if (model." + tbInfo.TableName + "Id.HasValue)");
                code.AppendLine("                    {");
                code.AppendLine("                        Update(conn, model);");
                code.AppendLine("                    }");
                code.AppendLine("                    else");
                code.AppendLine("                    {");
                code.AppendLine("                        Create(conn, model);");
                code.AppendLine("                    }");
                code.AppendLine("                }");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        #endregion");
                code.AppendLine("");

                code.AppendLine("        #region /** Method for search/delete data by column name **/");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    code.Append(col.iDataGenerator.GenCode_Repository_SearchBy());

                }
                code.AppendLine("        #endregion");
                code.AppendLine("");



                code.AppendLine("    }");
                code.AppendLine("}");


                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableName), tbInfo.TableName + "Repository.cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated Repository " + tbInfo.TableName);
            }
        }
    }
}
