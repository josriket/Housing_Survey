﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenPartialSearchModel : ICodeGenerator
    {
        public CodeGenPartialSearchModel(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("using System;");
                code.AppendLine("using Library.Winplus.syslib.core.simple;");
                code.AppendLine("using Library.Winplus.syslib.core.util;");
                code.AppendLine("using Library.Winplus.syslib.customize.loginuser.model;");

                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".search");
                code.AppendLine("{");
                code.AppendLine("    public partial class " + tbInfo.TableName + "Search");
                code.AppendLine("    {");
                code.AppendLine("        public bool CustomSearch;");
                code.AppendLine("");
                code.AppendLine("        public void CustomPrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)");
                code.AppendLine("        {");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("    }");
                code.AppendLine("}");


                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), tbInfo.TableName + "SearchPartial.cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated Model " + tbInfo.TableName);
            }
        }

       
    }
}
