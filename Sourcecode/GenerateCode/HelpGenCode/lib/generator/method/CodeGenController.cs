﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenController : ICodeGenerator
    {
        public CodeGenController(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("using System;");
                code.AppendLine("using System.Collections.Generic;");
                code.AppendLine("using System.Linq;");
                code.AppendLine("using System.Threading;");
                code.AppendLine("using System.Web;");
                code.AppendLine("using System.Web.Mvc;");
                code.AppendLine("using Library.Winplus.syslib.core.connection;");
                code.AppendLine("using Library.Winplus.syslib.core.exception;");
                code.AppendLine("using Library.Winplus.syslib.core.simple;");
                code.AppendLine("using Library.Winplus.syslib.customize.loginuser.model;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".repository;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".search;");
                code.AppendLine("using Library.Winplus.syslib.standard.mvcsetting.service;");
                code.AppendLine("using Library.Winplus.syslib.system.common.model;");
                code.AppendLine("using System.Data;");
                code.AppendLine("using Library.Winplus.syslib.core.util;");
                code.AppendLine("using Library.Winplus.syslib.standard.fileupload.repository;");
                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".controller");
                code.AppendLine("{");
                code.AppendLine("    public partial class " + tbInfo.TableName + "Controller : SimpleController<" + tbInfo.TableName + ", " + tbInfo.TableName + "Search>");
                code.AppendLine("    {");
                code.AppendLine("        public override ActionResult _Index(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model)");
                code.AppendLine("        {");
                code.AppendLine("            model.loginUser = loginUser;");
                code.AppendLine("");
                code.AppendLine("            _IndexHandleBefore(conn, loginUser, model);");
                code.AppendLine("");
                code.AppendLine("            model.PrepareSql(Request, loginUser);");
                code.AppendLine("");
                code.AppendLine("            return View(\"~/Views/standard/" + tbInfo.TableNameLower + "/\" + MVCSettingService.CustomView(\"" + tbInfo.TableName + "\", \"search\") + \".cshtml\", model);");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        public override ActionResult _Search(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model)");
                code.AppendLine("        {");
                code.AppendLine("            model.loginUser = loginUser;");
                code.AppendLine("");
                code.AppendLine("            _SearchHandleBefore(conn, loginUser, model);");
                code.AppendLine(""); 
                code.AppendLine("            model.PrepareSql(Request, loginUser);");
                code.AppendLine(""); 
                code.AppendLine("            SearchResultModel result = new " + tbInfo.TableName + "Repository().AdvanceSearch(conn, model);");
                code.AppendLine("");
                code.AppendLine("            _SearchHandleAfter(conn, loginUser, model, result); ");
                code.AppendLine("");
                code.AppendLine("            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(result));");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        public override ActionResult _Find(DBConnection conn, LoginUser loginUser, Int64? id)");
                code.AppendLine("        {");
                code.AppendLine("            " + tbInfo.TableName + " model = new " + tbInfo.TableName + "();");
                code.AppendLine("");
                code.AppendLine("            _FindHandleBefore(conn, loginUser, id);");
                code.AppendLine("");
                code.AppendLine("            if (id.HasValue && id.Value != 0)");
                code.AppendLine("            {");
                code.AppendLine("                model = new " + tbInfo.TableName + "Repository().FindById(conn, id.Value);");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.ColumnName.EndsWith("FileUploadIds"))
                    {
                        code.AppendLine("                model.Lst" + col.ColumnName.Substring(0, col.ColumnName.Length - 3) + " = new FileUploadRepository().FindsByIds(conn, model." + col.ColumnName + ");");
                    }
                }

                code.AppendLine("            }");
                if (tbInfo.AllColumnName.Contains("ActiveStatusId") && tbInfo.TableName != "ActiveStatus")
                {

                    code.AppendLine("            else");
                    code.AppendLine("            {");
                    code.AppendLine("                model.ActiveStatusId = 1;");
                    code.AppendLine("            }");
                    code.AppendLine("");
                }
                code.AppendLine("            model.loginUser = loginUser;");
                code.AppendLine("");
                code.AppendLine("            _FindHandleAfter(conn, loginUser, id, model);");
                code.AppendLine("");
                code.AppendLine("            return View(\"~/Views/standard/" + tbInfo.TableNameLower + "/\" + MVCSettingService.CustomView(\"" + tbInfo.TableName + "\", \"addedit\") + \".cshtml\", model);");
                code.AppendLine("        }");

                code.AppendLine("");
                code.AppendLine("        public override ActionResult _Copy(DBConnection conn, LoginUser loginUser, Int64? id)");
                code.AppendLine("        {");
                code.AppendLine("            " + tbInfo.TableName + " model = new " + tbInfo.TableName + "();");
                code.AppendLine("            _CopyHandleBefore(conn, loginUser, id);");
                code.AppendLine("");
                code.AppendLine("            if (id.HasValue && id.Value != 0)");
                code.AppendLine("            {");
                code.AppendLine("                model = new " + tbInfo.TableName + "Repository().FindById(conn, id.Value);");
                code.AppendLine("            }");
                code.AppendLine("");
                code.AppendLine("            model." + tbInfo.TableName + "Id = null;");
                code.AppendLine("            model.CreatedBy = null;");
                code.AppendLine("            model.CreatedOn = null;");
                code.AppendLine("            model.ModifiedBy = null;");
                code.AppendLine("            model.ModifiedOn = null;");
                code.AppendLine("            model.loginUser = loginUser;");
                if (tbInfo.TableName != "ActiveStatus" && tbInfo.AllColumnName.Contains("ActiveStatusId"))
                {
                    code.AppendLine("            model.ActiveStatusId = 1;");
                }
                code.AppendLine("");
                code.AppendLine("            _CopyHandleAfter(conn, loginUser, id, model);");
                code.AppendLine("");
                code.AppendLine("            return View(\"~/Views/standard/" + tbInfo.TableNameLower + "/\" + MVCSettingService.CustomView(\"" + tbInfo.TableName + "\", \"addedit\") + \".cshtml\", model);");
                code.AppendLine("        }");
                code.AppendLine("");

                code.AppendLine("        public override ActionResult _Save(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + " model)");
                code.AppendLine("        {");
                code.AppendLine("            try");
                code.AppendLine("            {");
                code.AppendLine("                model.loginUser = loginUser;");
                code.AppendLine("                conn.beginTrans();");
                code.AppendLine("");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.ColumnName.EndsWith("FileUploadIds"))
                    {
                        code.AppendLine("                model." + col.ColumnName.Substring(0, col.ColumnName.Length - 3) + "Date = DateTimeUtil.SetFileUploadDate(model." + col.ColumnName.Substring(0, col.ColumnName.Length - 3) + "Date,model." + col.ColumnName + ");");
                    }
                }
                code.AppendLine("");
                code.AppendLine("                _SaveHandleBefore(conn, loginUser, model);");
                code.AppendLine("");
                code.AppendLine("                if (model." + tbInfo.TableName + "Id.HasValue && model." + tbInfo.TableName + "Id.Value != 0)");
                code.AppendLine("                {");
                code.AppendLine("                    // Update");
                code.AppendLine("                    model.ModifiedOn = DateTimeUtil.CurrentDate();");
                code.AppendLine("                    model.ModifiedBy = loginUser.SystemUserId;");
                code.AppendLine("                    new " + tbInfo.TableName + "Repository().Update(conn, model);");
                code.AppendLine("                }");
                code.AppendLine("                else");
                code.AppendLine("                {");
                code.AppendLine("                    // Create");
                code.AppendLine("                    model.CreatedOn = DateTimeUtil.CurrentDate();");
                code.AppendLine("                    model.CreatedBy = loginUser.SystemUserId;");
                code.AppendLine("                    model." + tbInfo.TableName + "Id = new " + tbInfo.TableName + "Repository().Create(conn, model);");
                code.AppendLine("                }");
                code.AppendLine("");
                code.AppendLine("                _SaveHandleAfter(conn, loginUser, model);");
                code.AppendLine("");
                code.AppendLine("                conn.commitTrans();");
                code.AppendLine("                return ResponseJsonSuccess(null, model);");
                code.AppendLine("            }");
                code.AppendLine("            catch (Exception ex)");
                code.AppendLine("            {");
                code.AppendLine("                conn.rollbackTrans();");
                code.AppendLine("                throw ex;");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        public override ActionResult _Active(DBConnection conn, LoginUser loginUser,Int64 activeStatusId, string ids)");
                code.AppendLine("        {");
                code.AppendLine("            try");
                code.AppendLine("            {");
                code.AppendLine("                if (!string.IsNullOrEmpty(ids))");
                code.AppendLine("                {");
                code.AppendLine("                    conn.beginTrans();");
                code.AppendLine("");
                code.AppendLine("                    _ActiveHandleBefore(conn, loginUser, activeStatusId, ids);");
                code.AppendLine("");
                code.AppendLine("                    new " + tbInfo.TableName + "Repository().Active(conn, activeStatusId ,ids.Split(','));");
                code.AppendLine("");
                code.AppendLine("                    _ActiveHandleAfter(conn, loginUser, activeStatusId, ids);");
                code.AppendLine("");
                code.AppendLine("                    conn.commitTrans();");
                code.AppendLine("                }");
                code.AppendLine("                return ResponseJsonSuccess();");
                code.AppendLine("            }");
                code.AppendLine("            catch (Exception ex)");
                code.AppendLine("            {");
                code.AppendLine("                conn.rollbackTrans();");
                code.AppendLine("                throw ex;");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        public override ActionResult _Delete(DBConnection conn, LoginUser loginUser, string ids)");
                code.AppendLine("        {");
                code.AppendLine("            try");
                code.AppendLine("            {");
                code.AppendLine("                if (!string.IsNullOrEmpty(ids))");
                code.AppendLine("                {");
                code.AppendLine("                    conn.beginTrans();");
                code.AppendLine("");
                code.AppendLine("                    _DeleteHandleBefore(conn, loginUser, ids);");
                code.AppendLine("");
                code.AppendLine("                    new " + tbInfo.TableName + "Repository().Delete(conn, ids.Split(','));");
                code.AppendLine("");
                code.AppendLine("                    _DeleteHandleAfter(conn, loginUser, ids);");
                code.AppendLine("");
                code.AppendLine("                    conn.commitTrans();");
                code.AppendLine("                }");
                code.AppendLine("                return ResponseJsonSuccess();");
                code.AppendLine("            }");
                code.AppendLine("            catch (Exception ex)");
                code.AppendLine("            {");
                code.AppendLine("                conn.rollbackTrans();");
                code.AppendLine("                throw ex;");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        public override ActionResult _ExportExcel(DBConnection conn, LoginUser loginUser, " + tbInfo.TableName + "Search model)");
                code.AppendLine("        {");
                code.AppendLine("            _ExportExcelHandleBefore(conn, loginUser, model);");
                code.AppendLine("");
                code.AppendLine("            DataSet ds = new " + tbInfo.TableName + "Repository().ExportExcel(conn, model);");
                code.AppendLine("");
                code.AppendLine("            _ExportExcelHandleAfter(conn, loginUser, model, ds);");
                code.AppendLine("");
                code.AppendLine("            ExcelOutputUtil exporter = new ExcelOutputUtil(rm.GetString(\"" + tbInfo.TableName + "\"), Server.MapPath(\"~/Resource/XLSTemplate.xlsx\"));");
                code.AppendLine("            exporter.ToWorkBook(ds.Tables[0]);");
                code.AppendLine("            System.IO.MemoryStream stream = new System.IO.MemoryStream();");
                code.AppendLine("            exporter.ResponseMemory(stream);");
                code.AppendLine("");
                code.AppendLine("            return File(stream.ToArray(), \"application/ms-excel\", \"" + tbInfo.TableName + "_\" + DateTimeUtil.CurrentDate().Value.ToString(\"yyyyMMddHHmmss\") + \".xlsx\");");
                code.AppendLine("        }");
                code.AppendLine("");
                code.AppendLine("        public override ActionResult _Import(DBConnection conn, LoginUser loginUser, List<" + tbInfo.TableName + "> lstImport)");
                code.AppendLine("        {");
                code.AppendLine("            try");
                code.AppendLine("            {");
                code.AppendLine("                DateTime? CurrentDate = DateTimeUtil.CurrentDate();");
                code.AppendLine("                foreach (" + tbInfo.TableName + " import in lstImport)");
                code.AppendLine("                {");
                code.AppendLine("                    import.CreatedOn = CurrentDate;");
                code.AppendLine("                    import.CreatedBy = loginUser.SystemUserId;");
                code.AppendLine("                    import.ModifiedBy = null;");
                code.AppendLine("                    import.ModifiedOn = null;");
                code.AppendLine("                }");
                code.AppendLine("");
                code.AppendLine("                conn.beginTrans();");
                code.AppendLine("");
                code.AppendLine("                _ImportHandleBefore(conn, loginUser, lstImport);");
                code.AppendLine("");
                code.AppendLine("                new " + tbInfo.TableName + "Repository().ImportExcel(conn, loginUser, lstImport);");
                code.AppendLine("");
                code.AppendLine("                _ImportHandleAfter(conn, loginUser, lstImport);");
                code.AppendLine("");
                code.AppendLine("                conn.commitTrans();");
                code.AppendLine("");
                code.AppendLine("                return ResponseJsonSuccess();");
                code.AppendLine("            }");
                code.AppendLine("            catch (Exception ex)");
                code.AppendLine("            {");
                code.AppendLine("                if (conn != null) conn.rollbackTrans();");
                code.AppendLine("                ViewBag.ErrorMessage = ex.ToString();");
                code.AppendLine("");
                code.AppendLine("                return ResponseJsonError(ex.ToString());");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("}");

                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), tbInfo.TableName + "Controller.cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated Controller " + tbInfo.TableName);
            }
        }
    }
}
