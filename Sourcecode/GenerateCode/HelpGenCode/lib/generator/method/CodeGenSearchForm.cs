﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenSearchForm : ICodeGenerator
    {
        public CodeGenSearchForm(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("@model Library.Winplus.syslib.standard."+tbInfo.TableNameLower+".search."+tbInfo.TableName+"Search");
                code.AppendLine("");
                code.AppendLine("@Html.Partial(\"~/Views/standard/" + tbInfo.TableNameLower + "/import.cshtml\",Model)");
                code.AppendLine(""); 
                code.AppendLine("<div class=\"row\" id=\"addedit-section\">");
                code.AppendLine("");
                code.AppendLine("</div>");
                code.AppendLine("");
                code.AppendLine("<div class=\"row\" id=\"search-section\">");
                code.AppendLine("");
                code.AppendLine("    <div class=\"col-lg-12\">");
                code.AppendLine("        <div class=\"ibox float-e-margins\">");
                code.AppendLine("            <div class=\"ibox-title\">");
                code.AppendLine("                <h5>@MyResources.Winplus.Resource.Search @Html.DisplayNameFor(model => model.search."+tbInfo.TableName+"Name)</h5>");
                code.AppendLine("            </div>");
                code.AppendLine("            <div class=\"ibox-content\">");
                code.AppendLine("                <div class=\"row\">");
                code.AppendLine("                    <section class=\"content-header\">");
                code.AppendLine("                        <div class=\"box box-default color-palette-box\">");
                code.AppendLine("                            <div class=\"box-body\">");
                code.AppendLine("                                <div class=\"row\">");
                code.AppendLine("                                    <form id=\"search-form\">");
                code.AppendLine("                                        <!-- Advance Search -->");
                code.AppendLine("                                        <div class=\"col-sm-12\">");
                code.AppendLine("                                            <div class=\"row\">");
                code.AppendLine("                                                <div class=\"col-sm-12\">");
                code.AppendLine("                                                    <div id=\"advance-search-form\" class=\"collapse\">");
                code.AppendLine("                                                        <a href=\"#\" data-toggle=\"collapse\" data-target=\"#advance-search-form\">@MyResources.Winplus.Resource.AdvanceSearch</a>");
                code.AppendLine("                                                        <div style=\"height: 3px;\"></div>");
                code.AppendLine("                                                        <div class=\"row\">");

                // Advance search
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (!col.iDataGenerator.IsPrimaryKey && !(col.iDataGenerator is TextGenerator) && !(col.iDataGenerator is ByteArrayGenerator))
                    {
                        code.Append(col.iDataGenerator.GenCode_SearchForm_Element());
                    }
                }

                code.AppendLine("                                                        </div>");
                code.AppendLine("                                                    </div>");
                code.AppendLine("                                                </div>");
                code.AppendLine("                                            </div>");
                code.AppendLine("                                        </div>");
                code.AppendLine("");
                code.AppendLine("                                        <!-- Basic search by text -->");
                code.AppendLine("                                        <div class=\"col-sm-12\">");
                code.AppendLine("                                        <div class=\"input-group\">");
                code.AppendLine("                                            <div class=\"form-group\">");
                code.AppendLine("                                                <input type=\"text\" id=\"SearchText\" class=\"form-control input-search\" placeholder=\"@MyResources.Winplus.Resource.TextToSearch\" />");
                code.AppendLine("                                            </div>");
                code.AppendLine("                                            <div class=\"input-group-btn\">");
                code.AppendLine("                                                <button type=\"button\" class=\"btn btn-default\" aria-label=\"Advance Search\" data-toggle=\"collapse\" data-target=\"#advance-search-form\">");
                code.AppendLine("                                                    <span class=\"glyphicon glyphicon-plus-sign\"></span>");
                code.AppendLine("                                                </button>");
                code.AppendLine("                                                <button type=\"button\" onclick=\"javascript:gotoSearch();\" class=\"btn btn-default\" aria-label=\"Search\">");
                code.AppendLine("                                                    <span class=\"glyphicon glyphicon-search\"></span>");
                code.AppendLine("                                                </button>");
                code.AppendLine("                                            </div>");
                code.AppendLine("                                        </div>");
                code.AppendLine("                                    </div>");
                code.AppendLine("                                    </form>");
                code.AppendLine("                                </div>");
                code.AppendLine("                            </div>");
                code.AppendLine("                            <div style=\"height: 3px;\"></div>");
                code.AppendLine("                            ");
                code.AppendLine("                            <!-- Grid view -->");
                code.AppendLine("                            <section class=\"content\" style=\"overflow-x:auto;\">");
                code.AppendLine("                                <table id=\"tbResult\" class=\"display nowrap hover table table-striped table-bordered\">");
                code.AppendLine("                                    <thead>");
                code.AppendLine("                                        <tr>");
                code.AppendLine("                                            <th>@MyResources.Winplus.Resource.ColumnIndex</th>");
                code.AppendLine("                                            <th><input type=\"checkbox\" class=\"checkall\" /></th>");
                code.AppendLine("                                            <th>@Html.DisplayNameFor(model => model.search."+tbInfo.TableName+"Name)</th>");

                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (!col.iDataGenerator.IsPrimaryKey 
                        && col.ColumnName != (tbInfo.TableName + "Name") 
                        && !(col.iDataGenerator is TextGenerator) 
                        && !(col.iDataGenerator is ByteArrayGenerator)
                        && col.ColumnName != "VersionNumber")
                    {
                        code.AppendLine("                                            <th>@Html.DisplayNameFor(model => model.search." + col.ColumnName + ")</th>");
                    }
                }

                code.AppendLine("                                        </tr>");
                code.AppendLine("                                    </thead>");
                code.AppendLine("                                    <tbody>");
                code.AppendLine("                                    </tbody>");
                code.AppendLine("                                </table>");
                code.AppendLine("                            </section>");
                code.AppendLine("                        </div>");
                code.AppendLine("                    </section>");
                code.AppendLine("                </div>");
                code.AppendLine("            </div>");
                code.AppendLine("        </div>");
                code.AppendLine("");
                code.AppendLine("    </div>");
                code.AppendLine("");
                code.AppendLine("    <div style=\"display: none\" class=\"grid-menu\">");
                code.AppendLine("        @if (Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Add\"))");
                code.AppendLine("        {");
                code.AppendLine("            <button class=\"btn btn-success btn-circle\" type=\"button\" onclick=\"gotoAdd();\" title=\"Add\"><i class=\"fa fa-plus\"></i></button>");
                code.AppendLine("            <button class=\"btn btn-success btn-circle\" type=\"button\" onclick=\"gotoCopy();\" title=\"Copy\"><i class=\"fa fa-copy\"></i></button>");
                code.AppendLine("        }");
                code.AppendLine("        @if (Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Edit\"))");
                code.AppendLine("        {");
                code.AppendLine("            <button class=\"btn btn-warning btn-circle\" type=\"button\" onclick=\"gotoEdit();\" title=\"Edit\"><i class=\"fa fa-edit\"></i></button>");
                if (tbInfo.AllColumnName.Contains("ActiveStatusId"))
                {
                    code.AppendLine("            <button class=\"btn btn-primary btn-circle\" type=\"button\" onclick=\"gotoActive(1);\" title=\"Active\"><i class=\"fa fa-check\" ></i></button>");
                    code.AppendLine("            <button class=\"btn btn-default btn-circle\" type=\"button\" onclick=\"gotoActive(2);\" title=\"Inactive\"><i class=\"fa fa-times\"></i></button>");
                }

                code.AppendLine("        }");
                code.AppendLine("        @if (Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Delete\"))");
                code.AppendLine("        {");
                code.AppendLine("            <button class=\"btn btn-danger btn-circle\" type=\"button\" onclick=\"gotoDelete();\" title=\"Delete\"><i class=\"fa fa-minus\"></i></button>");
                code.AppendLine("        }");
                code.AppendLine("        @if (Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Export\"))");
                code.AppendLine("        {");
                code.AppendLine("            <button class=\"btn btn-primary btn-circle\" type=\"button\" onclick=\"gotoExportExcel();\" title=\"Export Excel\"><i class=\"fa fa-table\"></i></button>");
                code.AppendLine("        }");
                code.AppendLine("        @if (Model.HasAuthorize(Model.loginUser, \"" + tbInfo.TableName + "\", \"Import\"))");
                code.AppendLine("        {");
                code.AppendLine("            <button class=\"btn btn-info btn-circle\" type=\"button\" data-toggle=\"modal\" data-target=\"#modal-import\" title=\"Import Excel\"><i class=\"fa fa-database\"></i></button>");
                code.AppendLine("        }");
                code.AppendLine("    </div>");
                code.AppendLine("");
                code.AppendLine("</div>");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("<script>");
                code.AppendLine("");
                code.AppendLine("    // After menu click and ajax loading completed...");
                code.AppendLine("    function singlePageLoad() {");
                code.AppendLine("        _initTbResult();");
                code.AppendLine("        bindingStandardField();");
                code.AppendLine("");
                code.AppendLine("        $('#search-section .input-search').unbind('keyup');");
                code.AppendLine("        $(\"#search-section .input-search\").keyup(function (event) {");
                code.AppendLine("            if (event.keyCode == 13) {");
                code.AppendLine("                gotoSearch();");
                code.AppendLine("            }");
                code.AppendLine("        });");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    var tbResult = null;");
                code.AppendLine("    function _initTbResult() {");
                code.AppendLine("        tbResult = $('#tbResult').DataTable({");
                code.AppendLine("            \"serverSide\": true,");
                code.AppendLine("            \"bFilter\": true,");
                code.AppendLine("            \"columnDefs\": [");
                code.AppendLine("                { \"targets\": [1], \"orderable\": false }");
                code.AppendLine("            ],");
                code.AppendLine("            \"order\": [[ 0, \"desc\" ]],");
                code.AppendLine("            \"ajax\": {");
                code.AppendLine("                \"url\": '@Url.Content(\"~/"+tbInfo.TableNameLower+"/search?\")'+ formParams(\"#search-form .input-search\"),");
                code.AppendLine("                \"type\": \"POST\"");
                code.AppendLine("            },");
                code.AppendLine("            \"columns\": [");
                code.AppendLine("                { \"name\": \"" + tbInfo.TableName + "Id\", \"width\": \"5px\", \"data\": \"RowNr\", \"render\": function (data, type, row, meta) { return FormatInt32(data, type, row, meta); } },");
                code.AppendLine("                { \"name\": \"\", \"width\": \"5px\", \"data\": \"" + tbInfo.TableName + "Id\", \"render\": function (data, type, row, meta) { return FormatCheckbox(data, type, row, meta, \"" + tbInfo.TableName + "Id\",\"selectItem\"); } },");
                code.AppendLine("                { \"name\": \"" + tbInfo.TableName + "Name\", \"data\": \"" + tbInfo.TableName + "Name\", \"render\": function (data, type, row, meta) { return FormatEditById(data, type, row, meta, \"" + tbInfo.TableName + "Id\"); } },");

                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (!col.iDataGenerator.IsPrimaryKey 
                        && col.ColumnName != (tbInfo.TableName + "Name") 
                        && !(col.iDataGenerator is TextGenerator) 
                        && !(col.iDataGenerator is ByteArrayGenerator)
                        && col.ColumnName != "VersionNumber")
                    {
                        if (col.iDataGenerator.IsForeignKeyColumn)
                        {
                            code.AppendLine("                { \"name\": \"" + col.ColumnName + "\", \"data\": \"" + col.ColumnName + col.iDataGenerator.FKTableInfo.TableName + "Name\", \"render\": function (data, type, row, meta) { return FormatString(data, type, row, meta); } },");
                        }
                        else
                        {
                            code.Append(col.iDataGenerator.GenCode_SearchForm_JSGridData());
                        }
                    }
                }

                
                code.AppendLine("            ],");
                code.AppendLine("            \"cache\": false,");
                code.AppendLine("            fnDrawCallback: function( settings, json ) {");
                code.AppendLine("                $('.i-checks').iCheck({");
                code.AppendLine("                    checkboxClass: 'icheckbox_square-green',");
                code.AppendLine("                    radioClass: 'iradio_square-green',");
                code.AppendLine("                });");
                code.AppendLine("");
                code.AppendLine("                $(\".checkall\").iCheck({");
                code.AppendLine("                    checkboxClass: 'icheckbox_square-green',");
                code.AppendLine("                    radioClass: 'iradio_square-green',");
                code.AppendLine("                }).on('ifChanged', function (e) {");
                code.AppendLine("                    // Get the field name");
                code.AppendLine("                    var isChecked = e.currentTarget.checked;");
                code.AppendLine("                    if (isChecked) {");
                code.AppendLine("                        $('.chk"+tbInfo.TableName+"Id').iCheck('check');");
                code.AppendLine("                    } else {");
                code.AppendLine("                        $('.chk" + tbInfo.TableName + "Id').iCheck('uncheck');");
                code.AppendLine("                    }");
                code.AppendLine("                });");

                code.AppendLine("            },");
                code.AppendLine("            initComplete: function(settings, json) {");
                code.AppendLine("                $(\"#tbResult_filter\").html($(\"#search-section .grid-menu\").html());");
                code.AppendLine("            }");
                code.AppendLine("        });");
                code.AppendLine("");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function gotoSearch() {");
                code.AppendLine("        $('.checkall').iCheck('uncheck');");
                code.AppendLine("        _showSearch(true);");
                code.AppendLine("        tbResult.ajax.url(\"@Url.Content(\"~/"+tbInfo.TableNameLower+"/search?\")\" + formParams(\"#search-form .input-search\")).load();");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function gotoAdd() {");
                code.AppendLine("        _showSearch(false);");
                code.AppendLine("        ajaxPartialLoad(\"@Url.Content(\"~/"+tbInfo.TableNameLower+"/find/0\")\", \"addedit-section\", _gotoAdd)");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    // Callback after ajax completed");
                code.AppendLine("    function _gotoAdd(html) {");
                code.AppendLine("        _initialAddEditForm();");
                code.AppendLine("");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function gotoCopy() {");
                code.AppendLine("        var id = getCheckedOneValue(\"selectItem\");");
                code.AppendLine("        if (id != 0) {");
                code.AppendLine("            _showSearch(false);");
                code.AppendLine("            ajaxPartialLoad(\"@Url.Content(\"~/" + tbInfo.TableNameLower + "/copy/\")\" + id, \"addedit-section\", _gotoCopy)");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    // Callback after ajax completed");
                code.AppendLine("    function _gotoCopy(html) {");
                code.AppendLine("        _initialAddEditForm();");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function gotoEdit() {");
                code.AppendLine("        var id = getCheckedOneValue(\"selectItem\");");
                code.AppendLine("        if (id != 0) {");
                code.AppendLine("            _showSearch(false);");
                code.AppendLine("            ajaxPartialLoad(\"@Url.Content(\"~/"+tbInfo.TableNameLower+"/find/\")\" + id, \"addedit-section\", _gotoEdit)");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    // Callback after ajax completed");
                code.AppendLine("    function _gotoEdit(html) {");
                code.AppendLine("        _initialAddEditForm();");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    function gotoEditById(id) {");
                code.AppendLine("        if (id != 0) {");
                code.AppendLine("            _showSearch(false);");
                code.AppendLine("            ajaxPartialLoad(\"@Url.Content(\"~/"+tbInfo.TableNameLower+"/find/\")\" + id, \"addedit-section\", _gotoEditById)");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    // Callback after ajax completed");
                code.AppendLine("    function _gotoEditById(html) {");
                code.AppendLine("        _initialAddEditForm();");
                code.AppendLine("    }");
                code.AppendLine("");
                if (tbInfo.AllColumnName.Contains("ActiveStatusId"))
                {
                    code.AppendLine("    function gotoActive(activeId) {");
                    code.AppendLine("        var ids = getCheckedMultiple(\"selectItem\",true);");
                    code.AppendLine("        if (ids != \"\") {");
                    code.AppendLine("            var activeMessage = activeId==1?'active':'Inactive';");
                    code.AppendLine("            if (confirm(\"Would you like to \" + activeMessage + \" data?\")) {");
                    code.AppendLine("                ajaxUrlRequest(\"@Url.Content(\"~/" + tbInfo.TableNameLower + "/active?\")id=\"+activeId+\"&ids=\" + ids, _gotoActive);");
                    code.AppendLine("            }");
                    code.AppendLine("        }");
                    code.AppendLine("    }");
                    code.AppendLine("");
                    code.AppendLine("    // Callback after ajax completed");
                    code.AppendLine("    function _gotoActive(data) {");
                    code.AppendLine("        if (data.StatusCode == \"0\")");
                    code.AppendLine("        {");
                    code.AppendLine("            gotoSearch();");
                    code.AppendLine("        }");
                    code.AppendLine("        else");
                    code.AppendLine("        {");
                    code.AppendLine("            alert(data.Message);");
                    code.AppendLine("        }");
                    code.AppendLine("    }");
                    code.AppendLine("");
                }
                
                code.AppendLine("    function gotoDelete() {");
                code.AppendLine("        var ids = getCheckedMultiple(\"selectItem\",true);");
                code.AppendLine("        if (ids != \"\") {");
                code.AppendLine("            if (confirm(\"Would you like to delete data?\")) {");
                code.AppendLine("                ajaxUrlRequest(\"@Url.Content(\"~/"+tbInfo.TableNameLower+"/delete?ids=\")\" + ids, _gotoDelete);");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("    // Callback after ajax completed");
                code.AppendLine("    function _gotoDelete(data) {");
                code.AppendLine("        if (data.StatusCode == \"0\")");
                code.AppendLine("        {");
                code.AppendLine("            gotoSearch();");
                code.AppendLine("        }");
                code.AppendLine("        else");
                code.AppendLine("        {");
                code.AppendLine("            alert(data.Message);");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");


                code.AppendLine("    function gotoExportExcel() {");
                code.AppendLine("        var ids = getCheckedMultiple(\"selectItem\",false);");
                code.AppendLine("        location.href = \"@Url.Content(\"~/"+tbInfo.TableNameLower+"/exportexcel?\")ids=\" + ids+\"&\" + formParams(\"#search-form .input-search\");");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("    function _showSearch(_showSearch) {");
                code.AppendLine("        if (_showSearch) {");
                code.AppendLine("            $(\"#search-section\").show();");
                code.AppendLine("            $(\"#addedit-section\").hide();");
                code.AppendLine("        } else {");
                code.AppendLine("            $(\"#search-section\").hide();");
                code.AppendLine("            $(\"#addedit-section\").show();");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("</script>");




                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), "search.cshtml", finalCode, IsOverride);
                Console.WriteLine(" - Generated SearchForm " + tbInfo.TableName);
            }
        }
    }
}
