﻿using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenDisplayIndex: ICodeGenerator
    {
        public CodeGenDisplayIndex(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            StringBuilder code = new StringBuilder();
            code.AppendLine("using System;");
            code.AppendLine("using System.Collections.Generic;");
            code.AppendLine("using System.Linq;");
            code.AppendLine("using System.Text;");
            code.AppendLine("using System.Threading.Tasks;");
            code.AppendLine("");
            code.AppendLine("namespace Library.Winplus.syslib.core.domain");
            code.AppendLine("{");
            code.AppendLine("    public class DisplayIndex");
            code.AppendLine("    {");
            code.AppendLine("        private static HashSet<string> Tables = new HashSet<string>();");
            code.AppendLine("        static DisplayIndex()");
            code.AppendLine("        {");
            code.AppendLine("            Tables.Clear();");

            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.ColumnName == "DisplayIndex")
                    {
                        code.AppendLine("            Tables.Add(\"" + tbInfo.TableName + "\");");
                    }
                }
            }

            code.AppendLine("        }");
            code.AppendLine("");
            code.AppendLine("        public static bool Has(string TableName)");
            code.AppendLine("        {");
            code.AppendLine("            return Tables.Contains(TableName);");
            code.AppendLine("        }");
            code.AppendLine("    }");
            code.AppendLine("}");

            string finalCode = code.ToString();
            GencodeHelper.WriteCode(FolderName, "DisplayIndex.cs", finalCode, IsOverride);
            Console.WriteLine(" - Generated " + "DisplayIndex.cs");
        }
    }
}
