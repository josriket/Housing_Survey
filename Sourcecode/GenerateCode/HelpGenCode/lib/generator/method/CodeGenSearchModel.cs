﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenSearchModel : ICodeGenerator
    {
        public CodeGenSearchModel(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("using System;");
                code.AppendLine("using System.Text;");
                code.AppendLine("using Library.Winplus.syslib.core.simple;");
                code.AppendLine("using Library.Winplus.syslib.core.util;");
                code.AppendLine("using Library.Winplus.syslib.core.connection;");
                code.AppendLine("using Library.Winplus.syslib.customize.loginuser.model;");
                code.AppendLine("using Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model;");

                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".search");
                code.AppendLine("{");
                code.AppendLine("    public partial class " + tbInfo.TableName + "Search : SimpleSearch<" + tbInfo.TableName + ">");
                code.AppendLine("    {");
                code.AppendLine("        public " + tbInfo.TableName + "Search()");
                code.AppendLine("        {");
                code.AppendLine("            search = new " + tbInfo.TableName + "();");
                code.AppendLine("        }");

                code.AppendLine("        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)");
                code.AppendLine("        {");
                code.AppendLine("            if (CustomSearch)");
                code.AppendLine("            {");
                code.AppendLine("                CustomPrepareSql(request, loginUser);");
                code.AppendLine("            }");
                code.AppendLine("            else");
                code.AppendLine("            {");

                code.AppendLine("                ParseRequest(request);");
                code.AppendLine("                DefaultSearch = \"" + tbInfo.TableName + "Id desc\";");
                code.AppendLine("");

                code.AppendLine("                StringBuilder sqlQuery = new StringBuilder(@\"");
                code.AppendLine("                Select " + tbInfo.TableName + ".*");
                String aliasSelected = AliasSelect(tbInfo.lstColumnInfo);
                if (!string.IsNullOrEmpty(aliasSelected))
                {
                    code.Append(aliasSelected);
                }
                code.AppendLine("                From [" + tbInfo.TableName + "] " + tbInfo.TableName);

                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.iDataGenerator.IsForeignKeyColumn)
                    {
                        code.Append(col.iDataGenerator.GenCode_SearchModel_SqlJoinTable());
                    }

                }

                code.AppendLine("                Where 1=1\");");
                code.AppendLine("");

                code.AppendLine("                String text = null;");
                int count = 0;
                code.AppendLine("                /* Default text search */");
                code.AppendLine("                if (StringUtil.IsContainsKeyValue(SearchKey, \"SearchText\", ref text))");
                code.AppendLine("                {");
                code.AppendLine("                    sqlQuery.AppendLine(\"and (" + genLikeParam(tbInfo.lstColumnInfo, ref count) + ")\");");
                code.AppendLine("                    param.Add(new DBParameter(\"SearchText\", StringUtil.ToString(text)));");
                code.AppendLine("                }");

                code.AppendLine("");
                code.AppendLine("                #region search condition");
                code.AppendLine("");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    code.Append(col.iDataGenerator.GenCode_SearchModel_WhereCondition());
                    code.AppendLine("");
                }
                code.AppendLine("");
                code.AppendLine("                if (!string.IsNullOrEmpty(Ids))");
                code.AppendLine("                {");
                code.AppendLine("                    sqlQuery.AppendLine(\"and " + tbInfo.TableName + "." + tbInfo.TableName + "Id in(\" + Ids + \")\");");
                code.AppendLine("                }");
                code.AppendLine("");
                code.AppendLine("                #endregion search condition");

                code.AppendLine("");
                code.AppendLine("                /* prepare temporary parameter */");
                code.AppendLine("                this.sqlQuery = sqlQuery.ToString();");
                code.AppendLine("");
                code.AppendLine("                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + \" \" + OrderDir) : DefaultSearch;");
                code.AppendLine("            }");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("}");


                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), tbInfo.TableName + "Search.cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated Model " + tbInfo.TableName);
            }
        }

        private string genLikeParam(List<ColumnInfo2> list, ref int count)
        {
            count = 0;
            List<String> append = new List<string>();

            foreach (ColumnInfo2 r in list)
            {
                if (r.DataType == "String")
                {
                    append.Add(r.iDataGenerator.TableName + ".[" + r.iDataGenerator.ColumnName + "] like '%'+@SearchText+'%'");
                    count++;
                }

                if (r.iDataGenerator.IsForeignKeyColumn)
                {
                    append.Add(r.iDataGenerator.FKTableInfo.TableName + "_" + r.ColumnName + ".[" + r.iDataGenerator.FKTableInfo.TableName + "Name] like '%'+@SearchText+'%'");
                    count++;
                }
            }
            count = append.Count();
            return string.Join(" or ", append);
        }


        private string AliasSelect(List<ColumnInfo2> list)
        {
            StringBuilder code = new StringBuilder();
            foreach (ColumnInfo2 r in list)
            {
                if (r.iDataGenerator.IsForeignKeyColumn)
                {
                    code.AppendLine("                 " + (", " + r.iDataGenerator.FKTableInfo.TableName + "_" + r.ColumnName + ".[" + r.iDataGenerator.FKTableInfo.TableName + "Name] as [" + r.ColumnName + r.iDataGenerator.FKTableInfo.TableName + "Name]"));
                }
            }
            return code.ToString();
        }
    }
}
