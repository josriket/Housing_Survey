﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenModel : ICodeGenerator
    {
        public CodeGenModel(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();
                code.AppendLine("using System;");
                code.AppendLine("using System.Web.Mvc;");
                code.AppendLine("using Library.Winplus.syslib.core.simple;");
                code.AppendLine("using Library.Winplus.syslib.core.util;");
                code.AppendLine("using Library.Winplus.syslib.core.attribute;");
                code.AppendLine("using Library.Winplus.syslib.standard.fileupload.model;");
                code.AppendLine("using System.Web;");
                code.AppendLine("");
                code.AppendLine("namespace Library.Winplus.syslib.standard." + tbInfo.TableNameLower + ".model");
                code.AppendLine("{");
                code.AppendLine("    [Serializable]");
                code.AppendLine("    public partial class " + tbInfo.TableName + " : SimpleModel");
                code.AppendLine("    {");

                code.AppendLine("        /* Table column */");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                        code.Append(col.iDataGenerator.GenCode_Model_DeclareVariable());
                }
                code.AppendLine("");
                code.AppendLine("        /* Foreign key display model */");
                foreach (ColumnInfo2 col in tbInfo.lstColumnInfo)
                {
                    if (col.iDataGenerator.IsForeignKeyColumn)
                    {
                        code.Append(col.iDataGenerator.GenCode_Model_DeclareVariableFK());
                    }
                }

                code.AppendLine("    }");
                code.AppendLine("}");




                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), tbInfo.TableName + ".cs", finalCode, IsOverride);
                Console.WriteLine(" - Generated Model " + tbInfo.TableName);
            }
        }
    }
}
