﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpGenCode.lib.generator.datamodel;
using HelpGenCode.lib.generator.datamodel.context;
using HelpGenCode.lib.generator.datamodel.context.model;
using HelpGenCode.lib.generator.method.interf;

namespace HelpGenCode.lib.generator.method
{
    public class CodeGenImportForm : ICodeGenerator
    {
        public CodeGenImportForm(string FolderName, bool IsOverride)
            : base(FolderName, IsOverride)
        {
        }

        public override void Generate()
        {
            foreach (KeyValuePair<String, TableInfo2> tbInfoPair in GeneratorContext.TableInfo)
            {
                TableInfo2 tbInfo = tbInfoPair.Value;

                StringBuilder code = new StringBuilder();

                code.AppendLine("@model Library.Winplus.syslib.standard."+tbInfo.TableNameLower+".search."+tbInfo.TableName+"Search");
                code.AppendLine("");
                code.AppendLine("<div class=\"modal inmodal\" id=\"modal-import\" tabindex=\"-1\" role=\"dialog\"  aria-hidden=\"true\">");
                code.AppendLine("    <div class=\"modal-dialog\">");
                code.AppendLine("        <div class=\"modal-content animated fadeIn\">");
                code.AppendLine("            <div class=\"modal-header\">");
                code.AppendLine("                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">");
                code.AppendLine("                    <span aria-hidden=\"true\">&times;</span>");
                code.AppendLine("                    <span class=\"sr-only\">Close</span>");
                code.AppendLine("                </button>");
                code.AppendLine("                <i class=\"fa fa-file-excel-o modal-icon\" style=\"color:#1F6D42;\"></i>");
                code.AppendLine("                <h4 class=\"modal-title\">Import excel file</h4>");
                code.AppendLine("            </div>");
                code.AppendLine("            <form id=\"importform-1\" method=\"post\" class=\"form-horizontal\" action=\"@Url.Content(\"~/" + tbInfo.TableNameLower + "/import\")\" onsubmit=\"ajaxSubmitForm(event,this,'btn-import',_gotoImport);\" enctype=\"multipart/form-data\">");
                code.AppendLine("                <div class=\"modal-body\">");
                code.AppendLine("                    <div class=\"row\">");
                code.AppendLine("                        <div class=\"form-group col-sm-12\">");
                code.AppendLine("		                    <label class=\"col-sm-4 control-label\">");
                code.AppendLine("                               @MyResources.Winplus.Resource.ExcelFileUpload");
                code.AppendLine("		                    </label>");
                code.AppendLine("		                    <div class=\"col-sm-8\">");
                code.AppendLine("			                    <input type=\"file\" id=\"FileDataUpload\" class=\"form-control\" name=\"FileDataUpload\" accept=\".xlsx\" />");
                code.AppendLine("		                    </div>");
                code.AppendLine("	                    </div>");
                code.AppendLine("                    </div>");
                code.AppendLine("");
                code.AppendLine("                </div>");
                code.AppendLine("                <div class=\"modal-footer\">");
                code.AppendLine("                    <button type=\"button\" id=\"btn-import-close\" class=\"btn btn-white\" data-dismiss=\"modal\">Close</button>");
                code.AppendLine("                    <button type=\"submit\" id=\"btn-import\" class=\"btn ladda-button btn-primary\" data-style=\"expand-left\">Import</button>");
                code.AppendLine("                </div>");
                code.AppendLine("            </form>");
                code.AppendLine("        </div>");
                code.AppendLine("    </div>");
                code.AppendLine("</div>");
                code.AppendLine("<script>");
                code.AppendLine("");
                code.AppendLine("    function _gotoImport(data, status) {");
                code.AppendLine("        if (data.StatusCode == \"0\") {");
                code.AppendLine("            $(\"#FileDataUpload\").val('');");
                code.AppendLine("            alert(\"@MyResources.Winplus.Resource.ImportSuccess\");");
                code.AppendLine("            $('#modal-import').modal('hide');");
                code.AppendLine("            gotoSearch();");
                code.AppendLine("        } else {");
                code.AppendLine("            alert(data.Message);");
                code.AppendLine("        }");
                code.AppendLine("    }");
                code.AppendLine("");
                code.AppendLine("</script>");

                string finalCode = code.ToString();
                GencodeHelper.WriteCode(FolderName.Replace("<<TableLowerName>>", tbInfo.TableNameLower), "import.cshtml", finalCode, IsOverride);
                Console.WriteLine(" - Generated Import Form " + tbInfo.TableName);
            }
        }
    }
}
