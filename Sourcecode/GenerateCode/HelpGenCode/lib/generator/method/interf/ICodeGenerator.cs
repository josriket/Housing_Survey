﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpGenCode.lib.generator.method.interf
{
    public abstract class ICodeGenerator
    {
        protected string FolderName;
        protected bool IsOverride;
        public ICodeGenerator(string FolderName, bool IsOverride)
        {
            this.FolderName = FolderName;
            this.IsOverride = IsOverride;
        }

        public abstract void Generate();
    }
}
