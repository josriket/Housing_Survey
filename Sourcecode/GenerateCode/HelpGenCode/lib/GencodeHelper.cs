﻿using HelpGenCode.lib.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibrary.syslib.core.connection;
using SystemLibrary.syslib.core.util;
using System.IO;
using System.Data.SqlClient;
using HelpGenCode.lib.generator.datamodel.context.util;
using System.Threading;

namespace HelpGenCode.lib
{
    public class GencodeHelper
    {

        public static HashSet<string> LstStandardColumn = new HashSet<string>() { "DisplayIndex", "CreatedOn", "CreatedBy", "ModifiedOn", "ModifiedBy", "Version" };

        public static List<RelateTableInfo> TableInfo = new List<RelateTableInfo>();

        public static TableInfo GetTableInfo(DBConnection conn, string TableName)
        {
            TableInfo prop = new TableInfo(TableName);
            string sql = "SELECT column_name,is_nullable FROM information_schema.columns WHERE table_schema='public' AND table_name='" + TableName + "'";
            DataSet ds = conn.executeDataSet(sql);
            Dictionary<String, bool> ColumnNullable = new Dictionary<string, bool>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ColumnNullable.Add(row["column_name"].ToString(), row["is_nullable"].ToString().Equals("YES"));
            }


            sql = "select * From " + QueryUtil.PreFixBegin + TableName + QueryUtil.PreFixEnd;
            SqlDataReader reader = conn.executeDataReader(sql, null);

            DataTable tableInfo = reader.GetSchemaTable();

            foreach (DataRow row in tableInfo.Rows)
            {
                string ColumnName = row["ColumnName"].ToString();

                if (ColumnName == "StartDate")
                {

                }

                string DataType = row["DataType"].ToString().Replace("System.", "");
                bool AllowDBNull = ColumnNullable[ColumnName];
                int ColumnSize = Convert.ToInt32(row["ColumnSize"]);
                if (DataType == "DateTime" && ColumnSize == 4)
                {
                    DataType = "Date";
                }

                ColumnInfo colInfo = new ColumnInfo(ColumnName, DataType, AllowDBNull, ColumnSize);

                prop.lstColumnInfo.Add(colInfo);

                if (!GencodeHelper.LstStandardColumn.Contains(ColumnName))
                {
                    if (!prop.AllDataType.Contains(colInfo.DataType))
                    {
                        prop.AllDataType.Add(colInfo.DataType);
                    }
                }
            }

            return prop;
        }

        public string ReadCode(string Path)
        {
            StringBuilder code = new StringBuilder();
            string[] lines = System.IO.File.ReadAllLines(Path);
            foreach (string line in lines)
            {
                code.AppendLine("code.AppendLine(\"" + line.Replace("\"", "\\\"") + "\");");
            }

            return code.ToString();
        }


        public static void WriteCode(string DestFolder, string FileName, string Code, bool isOverride)
        {
            Exception error = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    Directory.CreateDirectory(DestFolder);
                    string FilePath = DestFolder + "\\" + FileName;
                    if (File.Exists(FilePath))
                    {
                        if (!isOverride) return;

                        File.Delete(FilePath);
                    }

                    using (StreamWriter sw = File.CreateText(FilePath))
                    {
                        sw.Write(Code);
                    }

                    error = null;
                    break;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(500);
                    error = ex;
                }
            }

            if (error != null) throw error;
        }

        public static Dictionary<String, int> _ALIAS_NAME = new Dictionary<string, int>();

        private static String checkDupAliasName(String aliasName)
        {
            if (aliasName == "is") aliasName = "iz";

            if (!_ALIAS_NAME.ContainsKey(aliasName))
            {
                _ALIAS_NAME.Add(aliasName, 1);
                return aliasName;
            }
            else
            {
                _ALIAS_NAME[aliasName] = _ALIAS_NAME[aliasName] + 1;
                return aliasName + "" + _ALIAS_NAME[aliasName];
            }
        }


        private static DataSet GetTableSchema(DBConnection conn)
        {
            return conn.executeDataSet(@"SELECT table_name
                                              FROM information_schema.tables
                                             WHERE table_schema='public'
                                               AND table_type='BASE TABLE';");
        }

        public static void PrepareTableInfo(DBConnection conn)
        {
            DataSet ds = GetTableSchema(conn);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                String _TableName = row[0].ToString();
                String undupAliasName = checkDupAliasName(StringUtil.getAliasName(_TableName));

                TableInfo.Add(new RelateTableInfo(_TableName + "Id", _TableName, undupAliasName));
            }
        }

        public string GenerateMenuInHtml(DBConnection conn)
        {
            StringBuilder html = new StringBuilder();
            List<String> LstTableName = GeneratorUtil.GetListOfTableName(conn);

            foreach (String _TableName in LstTableName)
            {
                html.Append("	                        <li><a href=\"javascript:navigateTo('@Url.Content(\"~/"+_TableName+"\")')\">" + StringUtil.addSpaceWhenUpper(_TableName) + "</a></li>\n");
            }

            return html.ToString();
        }
   
    }
}
