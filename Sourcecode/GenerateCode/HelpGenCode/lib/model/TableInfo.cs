﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.model
{
    public class TableInfo
    {
        public List<ColumnInfo> lstColumnInfo { get; set; }
        public List<ColumnInfo> lstUnRelateColumnInfo { get; set; }
        public HashSet<String> AllDataType { get; set; }
        public String AliasName { get; set; }
        public String TableName { get; set; }
        public String TableNameLower { get; set; }
        public String TableNameFirstLower { get; set; }
        private List<RelateTableInfo> lstRelateTable { get; set; }

        public String SavePath(String DestFolder)
        {
            return DestFolder.Replace("<<TableLowerName>>", TableNameLower);
        }

        public TableInfo(String TableName)
        {
            lstColumnInfo = new List<ColumnInfo>();
            AllDataType = new HashSet<string>();

            this.TableName = TableName;
            this.TableNameLower = TableName.ToLower();
            this.TableNameFirstLower = StringUtil.firsStringLower(TableName);
            this.AliasName = GetTableInfo(TableName).AlisName;
        }

        public List<RelateTableInfo> GetRelateTable()
        {
            if (lstRelateTable == null)
            {
                lstRelateTable = new List<RelateTableInfo>();
                lstUnRelateColumnInfo = new List<ColumnInfo>();
                foreach (ColumnInfo col in lstColumnInfo)
                {
                    if (!col.ColumnName.Equals(TableName + "Name") && !col.ColumnName.Equals(TableName + "Id"))
                    {
                        RelateTableInfo r = IsContainRelateColumn(col.ColumnName);
                        if (r != null)
                        {
                            lstRelateTable.Add(r);
                        }
                        else
                        {
                            lstUnRelateColumnInfo.Add(col);
                        }
                    }
                }
            }

            return lstRelateTable;
        }

        public static RelateTableInfo IsContainRelateColumn(String columnName)
        {
            foreach (RelateTableInfo r in GencodeHelper.TableInfo)
            {
                if (r.ColumnName.Equals(columnName))
                {
                    return r;
                }
            }
            return null;
        }

        private RelateTableInfo GetTableInfo(String TableName)
        {
            foreach (RelateTableInfo r in GencodeHelper.TableInfo)
            {
                if (r.TableName.Equals(TableName))
                {
                    return r;
                }
            }
            return null;
        }
    }
}
