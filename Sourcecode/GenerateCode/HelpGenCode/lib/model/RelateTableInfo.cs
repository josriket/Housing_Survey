﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpGenCode.lib.model
{
    public class RelateTableInfo
    {
        public String ColumnName { get; set; }
        public String TableName { get; set; }
        public String AlisName { get; set; }

        public RelateTableInfo(String ColumnName, String TableName, String AlisName)
        {
            this.ColumnName = ColumnName;
            this.TableName = TableName;
            this.AlisName = AlisName;
        }
    }
}
