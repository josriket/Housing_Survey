﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemLibrary.syslib.core.util;

namespace HelpGenCode.lib.model
{
    public class ColumnInfo
    {
        public static Dictionary<string, string> MapDataType = new Dictionary<string, string>();
        public static Dictionary<string, string> MapInputType = new Dictionary<string, string>();
        //public static Dictionary<string, string> MapRenderType = new Dictionary<string, string>();

        static ColumnInfo()
        {
            MapDataType.Add("Int64", "Long");
            MapDataType.Add("String", "String");
            MapDataType.Add("DateTime", "Timestamp");
            MapDataType.Add("Date", "Date");
            MapDataType.Add("Guid", "UUID");
            MapDataType.Add("Int32", "Integer");
            MapDataType.Add("Decimal", "Double");
            MapDataType.Add("Byte[]", "byte[]");
            MapDataType.Add("Boolean", "Boolean");
            MapDataType.Add("TimeSpan", "Time");



            MapInputType.Add("Int64", "number");
            MapInputType.Add("String", "text");
            MapInputType.Add("DateTime", "text");
            MapInputType.Add("Date", "text");
            MapInputType.Add("Guid", "text");
            MapInputType.Add("Int32", "number");
            MapInputType.Add("Decimal", "number");
            MapInputType.Add("Byte[]", "text");
            MapInputType.Add("Boolean", "text");
            MapInputType.Add("TimeSpan", "text");
        }

        public string ColumnName { get; set; }
        public string ColumnNameFirstLower { get; set; }
        public string DataType { get; set; }
        public bool AllowDBNull { get; set; }
        public int ColumnSize { get; set; }
        public string SearchOperator { get; set; }
        public string InputType { get; set; }
        //public string RenderType { get; set; }

        public ColumnInfo(string ColumnName, string DataType, bool AllowDBNull, int ColumnSize)
        {
            
            this.ColumnName = ColumnName;
            this.ColumnNameFirstLower = StringUtil.firsStringLower(ColumnName);
            this.DataType = MapDataType[DataType];
            //this.RenderType = MapRenderType[DataType];
            this.AllowDBNull = AllowDBNull;
            this.ColumnSize = ColumnSize;
            this.SearchOperator = (this.DataType == "String")? "like":"=";
            this.InputType = MapInputType[DataType];
        }

    }
}
