﻿// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

/*
Gherkin mode - http://www.cukes.info/
Report bugs/issues here: https://github.com/codemirror/CodeMirror/issues
*/

// Following Objs from Brackets implementation: https://github.com/tregusti/brackets-gherkin/blob/master/main.js
//var Quotes = {
//  SINGLE: 1,
//  DOUBLE: 2
//};

//var regex = {
//  keywords: /(Feature| {2}(Scenario|In order to|As|I)| {4}(Given|When|Then|And))/
//};

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode("gherkin", function () {
  return {
    startState: function () {
      return {
        lineNumber: 0,
        tableHeaderLine: false,
        allowFeature: true,
        allowBackground: false,
        allowScenario: false,
        allowSteps: false,
        allowPlaceholders: false,
        allowMultilineArgument: false,
        inMultilineString: false,
        inMultilineTable: false,
        inKeywordLine: false
      };
    },
    token: function (stream, state) {
      if (stream.sol()) {
        state.lineNumber++;
        state.inKeywordLine = false;
        if (state.inMultilineTable) {
            state.tableHeaderLine = false;
            if (!stream.match(/\s*\|/, false)) {
              state.allowMultilineArgument = false;
              state.inMultilineTable = false;
            }
        }
      }

      stream.eatSpace();

      if (state.allowMultilineArgument) {

        // STRING
        if (state.inMultilineString) {
          if (stream.match('"""')) {
            state.inMultilineString = false;
            state.allowMultilineArgument = false;
          } else {
            stream.match(/.*/);
          }
          return "string";
        }

        // TABLE
        if (state.inMultilineTable) {
          if (stream.match(/\|\s*/)) {
            return "bracket";
          } else {
            stream.match(/[^\|]*/);
            return state.tableHeaderLine ? "header" : "string";
          }
        }

        // DETECT START
        if (stream.match('"""')) {
          // String
          state.inMultilineString = true;
          return "string";
        } else if (stream.match("|")) {
          // Table
          state.inMultilineTable = true;
          state.tableHeaderLine = true;
          return "bracket";
        }

      }

      // LINE COMMENT
      if (stream.match(/#.*/)) {
        return "comment";

      // TAG
      } else if (!state.inKeywordLine && stream.match(/@\S+/)) {
        return "tag";

      // FEATURE
      } else if (!state.inKeywordLine && state.allowFeature && stream.match(/(ๆฉ่ฝ|ๅ่ฝ|ใใฃใผใใฃ|๊ธฐ๋ฅ|เนเธเธฃเธเธซเธฅเธฑเธ|เธเธงเธฒเธกเธชเธฒเธกเธฒเธฃเธ|เธเธงเธฒเธกเธเนเธญเธเธเธฒเธฃเธเธฒเธเธเธธเธฃเธเธดเธ|เฒนเณเฒเณเฒเฒณ|เฐเฑเฐฃเฐฎเฑ|เจฎเฉเจนเจพเจเจฆเจฐเจพ|เจจเจเจถ เจจเฉเจนเจพเจฐ|เจเจพเจธเฉเจเจค|เคฐเฅเคช เคฒเฅเค|ูู฻ฺฺฏ฻|ุฎุงุตูุฉ|ืชืืื ื|ะคัะฝะบััะพะฝะฐะป|ะคัะฝะบัะธั|ะคัะฝะบัะธะพะฝะฐะปะฝะพัั|ะคัะฝะบัะธะพะฝะฐะป|าฎะทะตะฝัำะปะตะบะปะตะปะตะบ|ะกะฒะพะนััะฒะพ|ะัะพะฑะธะฝะฐ|ะำฉะผะบะธะฝะปะตะบ|ะะพะณััะฝะพัั|ฮฮตฮนฯฮฟฯฯฮณฮฏฮฑ|ฮฯฮฝฮฑฯฯฯฮทฯฮฑ|Wลaลciwoลฤ|Vlastnosลฅ|Trajto|Tรญnh nฤng|Savybฤ|Pretty much|Poลพiadavka|Poลพadavek|Potrzeba biznesowa|รzellik|Osobina|Ominaisuus|Omadus|OH HAI|Moguฤnost|Mogucnost|Jellemzล|Hwรฆt|Hwaet|Funzionalitร |Funktionalitรฉit|Funktionalitรคt|Funkcja|Funkcionalnost|Funkcionalitฤte|Funkcia|Fungsi|Functionaliteit|Funcศionalitate|Funcลฃionalitate|Functionalitate|Funcionalitat|Funcionalidade|Fonctionnalitรฉ|Fitur|Fฤซฤa|Feature|Eiginleiki|Egenskap|Egenskab|Caracterรญstica|Caracteristica|Business Need|Aspekt|Arwedd|Ahoy matey!|Ability):/)) {
        state.allowScenario = true;
        state.allowBackground = true;
        state.allowPlaceholders = false;
        state.allowSteps = false;
        state.allowMultilineArgument = false;
        state.inKeywordLine = true;
        return "keyword";

      // BACKGROUND
      } else if (!state.inKeywordLine && state.allowBackground && stream.match(/(่ๆฏ|๋ฐฐ๊ฒฝ|เนเธเธงเธเธดเธ|เฒนเฒฟเฒจเณเฒจเณเฒฒเณ|เฐจเฑเฐชเฐฅเฑเฐฏเฐ|เจชเจฟเจเฉเจเฉ|เคชเฅเคทเฅเค เคญเฅเคฎเคฟ|ุฒู฻ูู|ุงูุฎูููุฉ|ืจืงืข|ะขะฐัะธั|ะัะตะดัััะพัะธั|ะัะตะดะธััะพัะธั|ะะพะทะฐะดะธะฝะฐ|ะะตัะตะดัะผะพะฒะฐ|ะัะฝะพะฒะฐ|ะะพะฝัะตะบัั|ะะตัะตั|ฮฅฯฯฮฒฮฑฮธฯฮฟ|Zaลoลผenia|Yo\-ho\-ho|Tausta|Taust|Situฤcija|Rerefons|Pozadina|Pozadie|Pozadรญ|Osnova|Latar Belakang|Kontext|Konteksts|Kontekstas|Kontekst|Hรกttรฉr|Hannergrond|Grundlage|Geรงmiล|Fundo|Fono|First off|Dis is what went down|Dasar|Contexto|Contexte|Context|Contesto|Cenรกrio de Fundo|Cenario de Fundo|Cefndir|Bแปi cแบฃnh|Bakgrunnur|Bakgrunn|Bakgrund|Baggrund|Background|B4|Antecedents|Antecedentes|รr|Aer|Achtergrond):/)) {
        state.allowPlaceholders = false;
        state.allowSteps = true;
        state.allowBackground = false;
        state.allowMultilineArgument = false;
        state.inKeywordLine = true;
        return "keyword";

      // SCENARIO OUTLINE
      } else if (!state.inKeywordLine && state.allowScenario && stream.match(/(ๅ ดๆฏๅคง็ถฑ|ๅบๆฏๅคง็บฒ|ๅๆฌๅคง็ถฑ|ๅงๆฌๅคง็บฒ|ใใณใใฌ|ใทใใชใชใใณใใฌใผใ|ใทใใชใชใใณใใฌ|ใทใใชใชใขใฆใใฉใคใณ|์๋๋ฆฌ์ค ๊ฐ์|เธชเธฃเธธเธเนเธซเธเธธเธเธฒเธฃเธเน|เนเธเธฃเธเธชเธฃเนเธฒเธเธเธญเธเนเธซเธเธธเธเธฒเธฃเธเน|เฒตเฒฟเฒตเฒฐเฒฃเณ|เฐเฐฅเฐจเฐ|เจชเจเจเจฅเจพ เจฐเฉเจช เจฐเฉเจเจพ|เจชเจเจเจฅเจพ เจขเจพเจเจเจพ|เคชเคฐเคฟเคฆเฅเคถเฅเคฏ เคฐเฅเคชเคฐเฅเคเคพ|ุณููุงุฑูู ูุฎุทุท|ุงูฺฏู฻ ุณูุงุฑ฻ู|ืชืื ืืช ืชืจืืืฉ|ะกัะตะฝะฐัะธะนะฝัาฃ ัำฉะทะตะปะตัะต|ะกัะตะฝะฐัะธะน ััััะบัััะฐัะธ|ะกัััะบัััะฐ ััะตะฝะฐััั|ะกัััะบัััะฐ ััะตะฝะฐัะธั|ะกัััะบัััะฐ ััะตะฝะฐัะธัะฐ|ะกะบะธัะฐ|ะ ะฐะผะบะฐ ะฝะฐ ััะตะฝะฐัะธะน|ะะพะฝัะตะฟั|ฮ ฮตฯฮนฮณฯฮฑฯฮฎ ฮฃฮตฮฝฮฑฯฮฏฮฟฯ|Wharrimean is|Template Situai|Template Senario|Template Keadaan|Tapausaihio|Szenariogrundriss|Szablon scenariusza|Swa hwรฆr swa|Swa hwaer swa|Struktura scenarija|Structurฤ scenariu|Structura scenariu|Skica|Skenario konsep|Shiver me timbers|Senaryo taslaฤฤฑ|Schema dello scenario|Scenariomall|Scenariomal|Scenario Template|Scenario Outline|Scenario Amlinellol|Scenฤrijs pฤc parauga|Scenarijaus ลกablonas|Reckon it's like|Raamstsenaarium|Plang vum Szenario|Plan du Scรฉnario|Plan du scรฉnario|Osnova scรฉnรกลe|Osnova Scenรกra|Nรกฤrt Scenรกru|Nรกฤrt Scรฉnรกลe|Nรกฤrt Scenรกra|MISHUN SRSLY|Menggariskan Senario|Lรฝsing Dรฆma|Lรฝsing Atburรฐarรกsar|Konturo de la scenaro|Koncept|Khung tรฌnh huแปng|Khung kแปch bแบฃn|Forgatรณkรถnyv vรกzlat|Esquema do Cenรกrio|Esquema do Cenario|Esquema del escenario|Esquema de l'escenari|Esbozo do escenario|Delineaรงรฃo do Cenรกrio|Delineacao do Cenario|All y'all|Abstrakt Scenario|Abstract Scenario):/)) {
        state.allowPlaceholders = true;
        state.allowSteps = true;
        state.allowMultilineArgument = false;
        state.inKeywordLine = true;
        return "keyword";

      // EXAMPLES
      } else if (state.allowScenario && stream.match(/(ไพๅญ|ไพ|ใตใณใใซ|์|เธเธธเธเธเธญเธเนเธซเธเธธเธเธฒเธฃเธเน|เธเธธเธเธเธญเธเธเธฑเธงเธญเธขเนเธฒเธ|เฒเฒฆเฒพเฒนเฒฐเฒฃเณเฒเฒณเณ|เฐเฐฆเฐพเฐนเฐฐเฐฃเฐฒเฑ|เจเจฆเจพเจนเจฐเจจเจพเจ|เคเคฆเคพเคนเคฐเคฃ|ููููู ูุง|ุงูุซูุฉ|ืืืืืืืช|าฎัะฝำะบะปำั|ะกัะตะฝะฐัะธัะธ|ะัะธะผะตัั|ะัะธะผะตัะธ|ะัะธะบะปะฐะดะธ|ะะธัะพะปะปะฐั|ะะธัะฐะปะปะฐั|ฮฃฮตฮฝฮฌฯฮนฮฑ|ฮ ฮฑฯฮฑฮดฮตฮฏฮณฮผฮฑฯฮฑ|You'll wanna|Voorbeelden|Variantai|Tapaukset|Se รพe|Se the|Se รฐe|Scenarios|Scenariji|Scenarijai|Przykลady|Primjeri|Primeri|Pลรญklady|Prรญklady|Piemฤri|Pรฉldรกk|Pavyzdลพiai|Paraugs|รrnekler|Juhtumid|Exemplos|Exemples|Exemple|Exempel|EXAMPLZ|Examples|Esempi|Enghreifftiau|Ekzemploj|Eksempler|Ejemplos|Dแปฏ liแปu|Dead men tell no tales|Dรฆmi|Contoh|Cenรกrios|Cenarios|Beispiller|Beispiele|Atburรฐarรกsir):/)) {
        state.allowPlaceholders = false;
        state.allowSteps = true;
        state.allowBackground = false;
        state.allowMultilineArgument = true;
        return "keyword";

      // SCENARIO
      } else if (!state.inKeywordLine && state.allowScenario && stream.match(/(ๅ ดๆฏ|ๅบๆฏ|ๅๆฌ|ๅงๆฌ|ใทใใชใช|์๋๋ฆฌ์ค|เนเธซเธเธธเธเธฒเธฃเธเน|เฒเฒฅเฒพเฒธเฒพเฒฐเฒพเฒเฒถ|เฐธเฐจเฑเฐจเฐฟเฐตเฑเฐถเฐ|เจชเจเจเจฅเจพ|เคชเคฐเคฟเคฆเฅเคถเฅเคฏ|ุณููุงุฑูู|ุณูุงุฑ฻ู|ืชืจืืืฉ|ะกัะตะฝะฐััะน|ะกัะตะฝะฐัะธะพ|ะกัะตะฝะฐัะธะน|ะัะธะผะตั|ฮฃฮตฮฝฮฌฯฮนฮฟ|Tรฌnh huแปng|The thing of it is|Tapaus|Szenario|Swa|Stsenaarium|Skenario|Situai|Senaryo|Senario|Scenaro|Scenariusz|Scenariu|Scรฉnario|Scenario|Scenarijus|Scenฤrijs|Scenarij|Scenarie|Scรฉnรกล|Scenรกr|Primer|MISHUN|Kแปch bแบฃn|Keadaan|Heave to|Forgatรณkรถnyv|Escenario|Escenari|Cenรกrio|Cenario|Awww, look mate|Atburรฐarรกs):/)) {
        state.allowPlaceholders = false;
        state.allowSteps = true;
        state.allowBackground = false;
        state.allowMultilineArgument = false;
        state.inKeywordLine = true;
        return "keyword";

      // STEPS
      } else if (!state.inKeywordLine && state.allowSteps && stream.match(/(้ฃ้บผ|้ฃไน|่ไธ|็ถ|ๅฝ|ๅนถไธ|ๅๆ|ๅๆถ|ๅๆ|ๅ่ฎพ|ๅ่จญ|ๅๅฎ|ๅๅฆ|ไฝๆฏ|ไฝใ|ไธฆไธ|ใใ|ใชใใฐ|ใใ ใ|ใใใ|ใใค|ํ์ง๋ง|์กฐ๊ฑด|๋จผ์ |๋ง์ผ|๋ง์ฝ|๋จ|๊ทธ๋ฆฌ๊ณ |๊ทธ๋ฌ๋ฉด|เนเธฅเธฐ |เนเธกเธทเนเธญ |เนเธเน |เธเธฑเธเธเธฑเนเธ |เธเธณเธซเธเธเนเธซเน |เฒธเณเฒฅเฒฟเฒคเฒฟเฒฏเฒจเณเฒจเณ |เฒฎเฒคเณเฒคเณ |เฒจเฒฟเณเฒกเฒฟเฒฆ |เฒจเฒเฒคเฒฐ |เฒเฒฆเฒฐเณ |เฐฎเฐฐเฐฟเฐฏเฑ |เฐเฑเฐชเฑเฐชเฐฌเฐกเฐฟเฐจเฐฆเฐฟ |เฐเฐพเฐจเฐฟ |เฐ เฐชเฐฐเฐฟเฐธเฑเฐฅเฐฟเฐคเฐฟเฐฒเฑ |เฐเฐชเฑเฐชเฑเฐกเฑ |เจชเจฐ |เจคเจฆ |เจเฉเจเจฐ |เจเจฟเจตเฉเจ เจเจฟ |เจเจฆเฉเจ |เจเจคเฉ |เคฏเคฆเคฟ |เคชเคฐเคจเฅเคคเฅ |เคชเคฐ |เคคเคฌ |เคคเคฆเคพ |เคคเคฅเคพ |เคเคฌ |เคเฅเคเคเคฟ |เคเคฟเคจเฅเคคเฅ |เคเคฆเคพ |เคเคฐ |เคเคเคฐ |ู |ููฺฏุงู฻ |ูุชู |ููู |ุนูุฏูุง |ุซู |ุจูุฑุถ |ุจุง ูุฑุถ |ุงูุง |ุงุฐุงู |ุขูฺฏุงู |ืืืฉืจ |ืืื |ืืืื ืชื |ืืื |ืื |ืืื |ะฏะบัะพ |าบำะผ |ะฃะฝะดะฐ |ะขะพะดั |ะขะพะณะดะฐ |ะขะพ |ะขะฐะบะถะต |ะขะฐ |ะัััั |ะัะธะฟัััะธะผะพ, ัะพ |ะัะธะฟัััะธะผะพ |ะะฝะดะฐ |ะะพ |ะะตัะฐะน |ะำัะธาำะดำ |ะะตะบะธะฝ |ะำะบะธะฝ |ะะพะปะธ |ะะพะณะดะฐ |ะะพะณะฐัะพ |ะะฐะดะฐ |ะะฐะด |ะ ัะพะผั ะถะต |ะ |ะ |ะะฐะดะฐัะพ |ะะฐะดะฐัะธ |ะะฐะดะฐัะต |ะัะปะธ |ะะพะฟัััะธะผ |ะะฐะฝะพ |ะะฐะดะตะฝะพ |ะำ |ะะฐ |ะะธัะพะบ |ำะผะผะฐ |ำะนัะธะบ |ำะณำั |ะะผะผะพ |ะะปะธ |ะะปะต |ะะณะฐั |ะ ัะฐะบะพะถ |ะ |ฮคฯฯฮต |ฮฯฮฑฮฝ |ฮฮฑฮน |ฮฮตฮดฮฟฮผฮญฮฝฮฟฯ |ฮฮปฮปฮฌ |รurh |รegar |รa รพe |รรก |รa |Zatati |Zakลadajฤc |Zadato |Zadate |Zadano |Zadani |Zadan |Za pลedpokladu |Za predpokladu |Youse know when youse got |Youse know like when |Yna |Yeah nah |Y'know |Y |Wun |Wtedy |When y'all |When |Wenn |WEN |wann |Ve |Vร  |Und |Un |ugeholl |Too right |Thurh |Thรฌ |Then y'all |Then |Tha the |Tha |Tetapi |Tapi |Tak |Tada |Tad |Stel |Soit |Siis |ศi |ลi |Si |Sed |Se |Sรฅ |Quando |Quand |Quan |Pryd |Potom |Pokud |Pokiaฤพ |Perรฒ |Pero |Pak |Oraz |Onda |Ond |Oletetaan |Og |Och |O zaman |Niin |Nhฦฐng |Nรคr |Nรฅr |Mutta |Men |Mas |Maka |Majd |Majฤc |Mais |Maar |mรค |Ma |Lorsque |Lorsqu'|Logo |Let go and haul |Kun |Kuid |Kui |Kiedy |Khi |Ketika |Kemudian |Keฤ |Kdyลพ |Kaj |Kai |Kada |Kad |Jeลผeli |Jeลli |Ja |It's just unbelievable |Ir |I CAN HAZ |I |Ha |Givun |Givet |Given y'all |Given |Gitt |Gegeven |Gegeben seien |Gegeben sei |Gdy |Gangway! |Fakat |รtant donnรฉs |Etant donnรฉs |รtant donnรฉes |Etant donnรฉes |รtant donnรฉe |Etant donnรฉe |รtant donnรฉ |Etant donnรฉ |Et |รs |Entonces |Entรณn |Entรฃo |Entao |En |Eฤer ki |Ef |Eeldades |E |รurh |Duota |Dun |Donitaฤตo |Donat |Donada |Do |Diyelim ki |Diberi |Dengan |Den youse gotta |DEN |De |Dato |Daศi fiind |Daลฃi fiind |Dati fiind |Dati |Date fiind |Date |Data |Dat fiind |Dar |Dann |dann |Dan |Dados |Dado |Dadas |Dada |รa รฐe |รa |Cuando |Cho |Cando |Cรขnd |Cand |Cal |But y'all |But at the end of the day I reckon |BUT |But |Buh |Blimey! |Biแบฟt |Bet |Bagi |Aye |awer |Avast! |Atunci |Atesa |Atรจs |Apabila |Anrhegedig a |Angenommen |And y'all |And |AN |An |an |Amikor |Amennyiben |Ama |Als |Alors |Allora |Ali |Aleshores |Ale |Akkor |Ak |Adott |Ac |Aber |A zรกroveล |A tieลพ |A taktieลพ |A takรฉ |A |a |7 |\* )/)) {
        state.inStep = true;
        state.allowPlaceholders = true;
        state.allowMultilineArgument = true;
        state.inKeywordLine = true;
        return "keyword";

      // INLINE STRING
      } else if (stream.match(/"[^"]*"?/)) {
        return "string";

      // PLACEHOLDER
      } else if (state.allowPlaceholders && stream.match(/<[^>]*>?/)) {
        return "variable";

      // Fall through
      } else {
        stream.next();
        stream.eatWhile(/[^@"<#]/);
        return null;
      }
    }
  };
});

CodeMirror.defineMIME("text/x-feature", "gherkin");

});
