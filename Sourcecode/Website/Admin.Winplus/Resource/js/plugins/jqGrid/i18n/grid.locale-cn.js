﻿;(function($){
/**
 * jqGrid Chinese Translation
 * ๅๅกๅ yanhonglei@gmail.com
 * http://www.kafeitu.me 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
    defaults : {
        recordtext: "{0} - {1}\u3000ๅฑ {2} ๆก", // ๅฑๅญๅๆฏๅจ่ง็ฉบๆ ผ
        emptyrecords: "ๆ ๆฐๆฎๆพ็คบ",
        loadtext: "่ฏปๅไธญ...",
        pgtext : " {0} ๅฑ {1} ้กต"
    },
    search : {
        caption: "ๆ็ดข...",
        Find: "ๆฅๆพ",
        Reset: "้็ฝฎ",
        odata: [{ oper:'eq', text:'็ญไบ\u3000\u3000'},{ oper:'ne', text:'ไธ็ญ\u3000\u3000'},{ oper:'lt', text:'ๅฐไบ\u3000\u3000'},{ oper:'le', text:'ๅฐไบ็ญไบ'},{ oper:'gt', text:'ๅคงไบ\u3000\u3000'},{ oper:'ge', text:'ๅคงไบ็ญไบ'},{ oper:'bw', text:'ๅผๅงไบ'},{ oper:'bn', text:'ไธๅผๅงไบ'},{ oper:'in', text:'ๅฑไบ\u3000\u3000'},{ oper:'ni', text:'ไธๅฑไบ'},{ oper:'ew', text:'็ปๆไบ'},{ oper:'en', text:'ไธ็ปๆไบ'},{ oper:'cn', text:'ๅๅซ\u3000\u3000'},{ oper:'nc', text:'ไธๅๅซ'},{ oper:'nu', text:'ไธๅญๅจ'},{ oper:'nn', text:'ๅญๅจ'}],
        groupOps: [ { op: "AND", text: "ๆๆ" },    { op: "OR",  text: "ไปปไธ" } ],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
    },
    edit : {
        addCaption: "ๆทปๅ ่ฎฐๅฝ",
        editCaption: "็ผ่พ่ฎฐๅฝ",
        bSubmit: "ๆไบค",
        bCancel: "ๅๆถ",
        bClose: "ๅณ้ญ",
        saveData: "ๆฐๆฎๅทฒๆนๅ๏ผๆฏๅฆไฟๅญ๏ผ",
        bYes : "ๆฏ",
        bNo : "ๅฆ",
        bExit : "ๅๆถ",
        msg: {
            required:"ๆญคๅญๆฎตๅฟ้",
            number:"่ฏท่พๅฅๆๆๆฐๅญ",
            minValue:"่พๅผๅฟ้กปๅคงไบ็ญไบ ",
            maxValue:"่พๅผๅฟ้กปๅฐไบ็ญไบ ",
            email: "่ฟไธๆฏๆๆ็e-mailๅฐๅ",
            integer: "่ฏท่พๅฅๆๆๆดๆฐ",
            date: "่ฏท่พๅฅๆๆๆถ้ด",
            url: "ๆ ๆ็ฝๅใๅ็ผๅฟ้กปไธบ ('http://' ๆ 'https://')",
            nodefined : " ๆชๅฎไน๏ผ",
            novalue : " ้่ฆ่ฟๅๅผ๏ผ",
            customarray : "่ชๅฎไนๅฝๆฐ้่ฆ่ฟๅๆฐ็ป๏ผ",
            customfcheck : "ๅฟ้กปๆ่ชๅฎไนๅฝๆฐ!"
        }
    },
    view : {
        caption: "ๆฅ็่ฎฐๅฝ",
        bClose: "ๅณ้ญ"
    },
    del : {
        caption: "ๅ ้ค",
        msg: "ๅ ้คๆ้่ฎฐๅฝ๏ผ",
        bSubmit: "ๅ ้ค",
        bCancel: "ๅๆถ"
    },
    nav : {
        edittext: "",
        edittitle: "็ผ่พๆ้่ฎฐๅฝ",
        addtext:"",
        addtitle: "ๆทปๅ ๆฐ่ฎฐๅฝ",
        deltext: "",
        deltitle: "ๅ ้คๆ้่ฎฐๅฝ",
        searchtext: "",
        searchtitle: "ๆฅๆพ",
        refreshtext: "",
        refreshtitle: "ๅทๆฐ่กจๆ ผ",
        alertcap: "ๆณจๆ",
        alerttext: "่ฏท้ๆฉ่ฎฐๅฝ",
        viewtext: "",
        viewtitle: "ๆฅ็ๆ้่ฎฐๅฝ"
    },
    col : {
        caption: "้ๆฉๅ",
        bSubmit: "็กฎๅฎ",
        bCancel: "ๅๆถ"
    },
    errors : {
        errcap : "้่ฏฏ",
        nourl : "ๆฒกๆ่ฎพ็ฝฎurl",
        norecords: "ๆฒกๆ่ฆๅค็็่ฎฐๅฝ",
        model : "colNames ๅ colModel ้ฟๅบฆไธ็ญ๏ผ"
    },
    formatter : {
        integer : {thousandsSeparator: ",", defaultValue: '0'},
        number : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'},
        currency : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
        date : {
            dayNames:   [
                "ๆฅ", "ไธ", "ไบ", "ไธ", "ๅ", "ไบ", "ๅญ",
                "ๆๆๆฅ", "ๆๆไธ", "ๆๆไบ", "ๆๆไธ", "ๆๆๅ", "ๆๆไบ", "ๆๆๅญ",
            ],
            monthNames: [
                "ไธ", "ไบ", "ไธ", "ๅ", "ไบ", "ๅญ", "ไธ", "ๅซ", "ไน", "ๅ", "ๅไธ", "ๅไบ",
                "ไธๆ", "ไบๆ", "ไธๆ", "ๅๆ", "ไบๆ", "ๅญๆ", "ไธๆ", "ๅซๆ", "ไนๆ", "ๅๆ", "ๅไธๆ", "ๅไบๆ"
            ],
            AmPm : ["am","pm","ไธๅ","ไธๅ"],
            S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th';},
            srcformat: 'Y-m-d',
            newformat: 'Y-m-d',
            parseRe : /[#%\\\/:_;.,\t\s-]/,
            masks : {
                // see http://php.net/manual/en/function.date.php for PHP format used in jqGrid
                // and see http://docs.jquery.com/UI/Datepicker/formatDate
                // and https://github.com/jquery/globalize#dates for alternative formats used frequently
                // one can find on https://github.com/jquery/globalize/tree/master/lib/cultures many
                // information about date, time, numbers and currency formats used in different countries
                // one should just convert the information in PHP format
                ISO8601Long:"Y-m-d H:i:s",
                ISO8601Short:"Y-m-d",
                // short date:
                //    n - Numeric representation of a month, without leading zeros
                //    j - Day of the month without leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                // example: 3/1/2012 which means 1 March 2012
                ShortDate: "n/j/Y", // in jQuery UI Datepicker: "M/d/yyyy"
                // long date:
                //    l - A full textual representation of the day of the week
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                LongDate: "l, F d, Y", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy"
                // long date with long time:
                //    l - A full textual representation of the day of the week
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    s - Seconds, with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                FullDateTime: "l, F d, Y g:i:s A", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy h:mm:ss tt"
                // month day:
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                MonthDay: "F d", // in jQuery UI Datepicker: "MMMM dd"
                // short time (without seconds)
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                ShortTime: "g:i A", // in jQuery UI Datepicker: "h:mm tt"
                // long time (with seconds)
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    s - Seconds, with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                LongTime: "g:i:s A", // in jQuery UI Datepicker: "h:mm:ss tt"
                SortableDateTime: "Y-m-d\\TH:i:s",
                UniversalSortableDateTime: "Y-m-d H:i:sO",
                // month with year
                //    Y - A full numeric representation of a year, 4 digits
                //    F - A full textual representation of a month
                YearMonth: "F, Y" // in jQuery UI Datepicker: "MMMM, yyyy"
            },
            reformatAfterEdit : false
        },
        baseLinkUrl: '',
        showAction: '',
        target: '',
        checkbox : {disabled:true},
        idName : 'id'
    }
});
})(jQuery);
