﻿;(function($){
/**
 * jqGrid Vietnamese Translation
 * Lรช ฤรฌnh Dลฉng dungtdc@gmail.com
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "View {0} - {1} of {2}",
		emptyrecords: "Khรดng cรณ dแปฏ liแปu",
		loadtext: "ฤang nแบกp dแปฏ liแปu...",
		pgtext : "Trang {0} trong tแปng sแป {1}"
	},
	search : {
		caption: "Tรฌm kiแบฟm...",
		Find: "Tรฌm",
		Reset: "Khแปi tแบกo lแบกi",
		odata: [{ oper:'eq', text:"bแบฑng"},{ oper:'ne', text:"khรดng bแบฑng"},{ oper:'lt', text:"bรฉ hฦกn"},{ oper:'le', text:"bรฉ hฦกn hoแบทc bแบฑng"},{ oper:'gt', text:"lแปn hฦกn"},{ oper:'ge', text:"lแปn hฦกn hoแบทc bแบฑng"},{ oper:'bw', text:"bแบฏt ฤแบงu vแปi"},{ oper:'bn', text:"khรดng bแบฏt ฤแบงu vแปi"},{ oper:'in', text:"trong"},{ oper:'ni', text:"khรดng nแบฑm trong"},{ oper:'ew', text:"kแบฟt thรบc vแปi"},{ oper:'en', text:"khรดng kแบฟt thรบc vแปi"},{ oper:'cn', text:"chแปฉa"},{ oper:'nc', text:"khรดng chแปฉa"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "Vร", text: "tแบฅt cแบฃ" },	{ op: "HOแบถC",  text: "bแบฅt kแปณ" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "Thรชm bแบฃn ghi",
		editCaption: "Sแปญa bแบฃn ghi",
		bSubmit: "Gแปญi",
		bCancel: "Hแปงy bแป",
		bClose: "ฤรณng",
		saveData: "Dแปฏ liแปu ฤรฃ thay ฤแปi! Cรณ lฦฐu thay ฤแปi khรดng?",
		bYes : "Cรณ",
		bNo : "Khรดng",
		bExit : "Hแปงy bแป",
		msg: {
			required:"Trฦฐแปng dแปฏ liแปu bแบฏt buแปc cรณ",
			number:"Hรฃy ฤiแปn ฤรบng sแป",
			minValue:"giรก trแป phแบฃi lแปn hฦกn hoแบทc bแบฑng vแปi ",
			maxValue:"giรก trแป phแบฃi bรฉ hฦกn hoแบทc bแบฑng",
			email: "khรดng phแบฃi lร  mแปt email ฤรบng",
			integer: "Hรฃy ฤiแปn ฤรบng sแป nguyรชn",
			date: "Hรฃy ฤiแปn ฤรบng ngร y thรกng",
			url: "khรดng phแบฃi lร  URL. Khแปi ฤแบงu bแบฏt buแปc lร  ('http://' hoแบทc 'https://')",
			nodefined : " chฦฐa ฤฦฐแปฃc ฤแปnh nghฤฉa!",
			novalue : " giรก trแป trแบฃ vแป bแบฏt buแปc phแบฃi cรณ!",
			customarray : "Hร m nรชn trแบฃ vแป mแปt mแบฃng!",
			customfcheck : "Custom function should be present in case of custom checking!"
			
		}
	},
	view : {
		caption: "Xem bแบฃn ghi",
		bClose: "ฤรณng"
	},
	del : {
		caption: "Xรณa",
		msg: "Xรณa bแบฃn ghi ฤรฃ chแปn?",
		bSubmit: "Xรณa",
		bCancel: "Hแปงy bแป"
	},
	nav : {
		edittext: "",
		edittitle: "Sแปญa dรฒng ฤรฃ chแปn",
		addtext:"",
		addtitle: "Thรชm mแปi 1 dรฒng",
		deltext: "",
		deltitle: "Xรณa dรฒng ฤรฃ chแปn",
		searchtext: "",
		searchtitle: "Tรฌm bแบฃn ghi",
		refreshtext: "",
		refreshtitle: "Nแบกp lแบกi lฦฐแปi",
		alertcap: "Cแบฃnh bรกo",
		alerttext: "Hรฃy chแปn mแปt dรฒng",
		viewtext: "",
		viewtitle: "Xem dรฒng ฤรฃ chแปn"
	},
	col : {
		caption: "Chแปn cแปt",
		bSubmit: "OK",
		bCancel: "Hแปงy bแป"
	},
	errors : {
		errcap : "Lแปi",
		nourl : "khรดng url ฤฦฐแปฃc ฤแบทt",
		norecords: "Khรดng cรณ bแบฃn ghi ฤแป xแปญ lรฝ",
		model : "Chiแปu dร i cแปงa colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: ".", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, defaultValue: '0'},
		currency : {decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0'},
		date : {
			dayNames:   [
				"CN", "T2", "T3", "T4", "T5", "T6", "T7",
				"Chแปง nhแบญt", "Thแปฉ hai", "Thแปฉ ba", "Thแปฉ tฦฐ", "Thแปฉ nฤm", "Thแปฉ sรกu", "Thแปฉ bแบฃy"
			],
			monthNames: [
				"Th1", "Th2", "Th3", "Th4", "Th5", "Th6", "Th7", "Th8", "Th9", "Th10", "Th11", "Th12",
				"Thรกng mแปt", "Thรกng hai", "Thรกng ba", "Thรกng tฦฐ", "Thรกng nฤm", "Thรกng sรกu", "Thรกng bแบฃy", "Thรกng tรกm", "Thรกng chรญn", "Thรกng mฦฐแปi", "Thรกng mฦฐแปi mแปt", "Thรกng mฦฐแปi hai"
			],
			AmPm : ["sรกng","chiแปu","SรNG","CHIแปU"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th';},
			srcformat: 'Y-m-d',
			newformat: 'n/j/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				// see http://php.net/manual/en/function.date.php for PHP format used in jqGrid
				// and see http://docs.jquery.com/UI/Datepicker/formatDate
				// and https://github.com/jquery/globalize#dates for alternative formats used frequently
				// one can find on https://github.com/jquery/globalize/tree/master/lib/cultures many
				// information about date, time, numbers and currency formats used in different countries
				// one should just convert the information in PHP format
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				// short date:
				//    n - Numeric representation of a month, without leading zeros
				//    j - Day of the month without leading zeros
				//    Y - A full numeric representation of a year, 4 digits
				// example: 3/1/2012 which means 1 March 2012
				ShortDate: "n/j/Y", // in jQuery UI Datepicker: "M/d/yyyy"
				// long date:
				//    l - A full textual representation of the day of the week
				//    F - A full textual representation of a month
				//    d - Day of the month, 2 digits with leading zeros
				//    Y - A full numeric representation of a year, 4 digits
				LongDate: "l, F d, Y", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy"
				// long date with long time:
				//    l - A full textual representation of the day of the week
				//    F - A full textual representation of a month
				//    d - Day of the month, 2 digits with leading zeros
				//    Y - A full numeric representation of a year, 4 digits
				//    g - 12-hour format of an hour without leading zeros
				//    i - Minutes with leading zeros
				//    s - Seconds, with leading zeros
				//    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
				FullDateTime: "l, F d, Y g:i:s A", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy h:mm:ss tt"
				// month day:
				//    F - A full textual representation of a month
				//    d - Day of the month, 2 digits with leading zeros
				MonthDay: "F d", // in jQuery UI Datepicker: "MMMM dd"
				// short time (without seconds)
				//    g - 12-hour format of an hour without leading zeros
				//    i - Minutes with leading zeros
				//    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
				ShortTime: "g:i A", // in jQuery UI Datepicker: "h:mm tt"
				// long time (with seconds)
				//    g - 12-hour format of an hour without leading zeros
				//    i - Minutes with leading zeros
				//    s - Seconds, with leading zeros
				//    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
				LongTime: "g:i:s A", // in jQuery UI Datepicker: "h:mm:ss tt"
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				// month with year
				//    Y - A full numeric representation of a year, 4 digits
				//    F - A full textual representation of a month
				YearMonth: "F, Y" // in jQuery UI Datepicker: "MMMM, yyyy"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
