﻿;(function($){
/**
 * jqGrid Bulgarian Translation 
 * Tony Tomov tony@trirand.com
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "{0} - {1} ะพั {2}",
		emptyrecords: "ะัะผะฐ ะทะฐะฟะธั(ะธ)",
		loadtext: "ะะฐัะตะถะดะฐะผ...",
		pgtext : "ะกัั. {0} ะพั {1}"
	},
	search : {
		caption: "ะขัััะตะฝะต...",
		Find: "ะะฐะผะตัะธ",
		Reset: "ะะทัะธััะธ",
		odata: [{ oper:'eq', text:"ัะฐะฒะฝะพ"},{ oper:'ne', text:"ัะฐะทะปะธัะฝะพ"},{ oper:'lt', text:"ะฟะพ-ะผะฐะปะบะพ"},{ oper:'le', text:"ะฟะพ-ะผะฐะปะบะพ ะธะปะธ="},{ oper:'gt', text:"ะฟะพ-ะณะพะปัะผะพ"},{ oper:'ge', text:"ะฟะพ-ะณะพะปัะผะพ ะธะปะธ ="},{ oper:'bw', text:"ะทะฐะฟะพัะฒะฐ ั"},{ oper:'bn', text:"ะฝะต ะทะฐะฟะพัะฒะฐ ั"},{ oper:'in', text:"ัะต ะฝะฐะผะธัะฐ ะฒ"},{ oper:'ni', text:"ะฝะต ัะต ะฝะฐะผะธัะฐ ะฒ"},{ oper:'ew', text:"ะทะฐะฒัััะฒะฐ ั"},{ oper:'en', text:"ะฝะต ะทะฐะฒัััะฐะฒะฐ ั"},{ oper:'cn', text:"ััะดััะถะฐ"},{ oper:'nc', text:"ะฝะต ััะดััะถะฐ"},{ oper:'nu', text:'ะต NULL'},{ oper:'nn', text:'ะฝะต ะต NULL'}],
	    groupOps: [	{ op: "AND", text: "&nbsp;ะ " },	{ op: "OR",  text: "ะะะ" }	],
		operandTitle : "ะะฐัะธัะฝะธ ะทะฐ ะธะทะฑะพั ะฝะฐ ะพะฟะตัะฐะฝะด.",
		resetTitle : "ะะทัะธััะธ ััะพะนะฝะพัััะฐ"
	},
	edit : {
		addCaption: "ะะพะฒ ะะฐะฟะธั",
		editCaption: "ะ ะตะดะฐะบัะธั ะะฐะฟะธั",
		bSubmit: "ะะฐะฟะธัะธ",
		bCancel: "ะะทัะพะด",
		bClose: "ะะฐัะฒะพัะธ",
		saveData: "ะะฐะฝะฝะธัะต ัะฐ ะฟัะพะผะตะฝะตะฝะธ! ะะฐ ััััะฐะฝั ะปะธ ะฟัะพะผะตะฝะธัะต?",
		bYes : "ะะฐ",
		bNo : "ะะต",
		bExit : "ะัะบะฐะท",
		msg: {
			required:"ะะพะปะตัะพ ะต ะทะฐะดัะปะถะธัะตะปะฝะพ",
			number:"ะัะฒะตะดะตัะต ะฒะฐะปะธะดะฝะพ ัะธัะปะพ!",
			minValue:"ััะพะนะฝะพัััะฐ ัััะฑะฒะฐ ะดะฐ ะต ะฟะพ-ะณะพะปัะผะฐ ะธะปะธ ัะฐะฒะฝะฐ ะพั",
			maxValue:"ััะพะนะฝะพัััะฐ ัััะฑะฒะฐ ะดะฐ ะต ะฟะพ-ะผะฐะปะบะฐ ะธะปะธ ัะฐะฒะฝะฐ ะพั",
			email: "ะฝะต ะต ะฒะฐะปะธะดะตะฝ ะตะป. ะฐะดัะตั",
			integer: "ะัะฒะตะดะตัะต ะฒะฐะปะธะดะฝะพ ััะปะพ ัะธัะปะพ",
			date: "ะัะฒะตะดะตัะต ะฒะฐะปะธะดะฝะฐ ะดะฐัะฐ",
			url: "e ะฝะตะฒะฐะปะธะดะตะฝ URL. ะะทะธัะบะฐะฒะฐ ัะต ะฟัะตัะธะบั('http://' ะธะปะธ 'https://')",
			nodefined : " ะต ะฝะตะดะตัะธะฝะธัะฐะฝะฐ!",
			novalue : " ะธะทะธัะบะฒะฐ ะฒัััะฐะฝะต ะฝะฐ ััะพะนะฝะพัั!",
			customarray : "ะะพััะตะฑ. ะคัะฝะบัะธั ัััะฑะฒะฐ ะดะฐ ะฒััะฝะต ะผะฐัะธะฒ!",
			customfcheck : "ะะพััะตะฑะธัะตะปัะบะฐ ััะฝะบัะธั ะต ะทะฐะดัะปะถะธัะตะปะฝะฐ ะฟัะธ ัะพะทะธ ัะธะฟ ะตะปะตะผะตะฝั!"
		}
	},
	view : {
		caption: "ะัะตะณะปะตะด ะทะฐะฟะธั",
		bClose: "ะะฐัะฒะพัะธ"
	},
	del : {
		caption: "ะะทััะธะฒะฐะฝะต",
		msg: "ะะฐ ะธะทััะธั ะปะธ ะธะทะฑัะฐะฝะธัั ะทะฐะฟะธั?",
		bSubmit: "ะะทััะธะน",
		bCancel: "ะัะบะฐะท"
	},
	nav : {
		edittext: " ",
		edittitle: "ะ ะตะดะฐะบัะธั ะธะทะฑัะฐะฝ ะทะฐะฟะธั",
		addtext:" ",
		addtitle: "ะะพะฑะฐะฒัะฝะต ะฝะพะฒ ะทะฐะฟะธั",
		deltext: " ",
		deltitle: "ะะทััะธะฒะฐะฝะต ะธะทะฑัะฐะฝ ะทะฐะฟะธั",
		searchtext: " ",
		searchtitle: "ะขัััะตะฝะต ะทะฐะฟะธั(ะธ)",
		refreshtext: "",
		refreshtitle: "ะะฑะฝะพะฒะธ ัะฐะฑะปะธัะฐ",
		alertcap: "ะัะตะดัะฟัะตะถะดะตะฝะธะต",
		alerttext: "ะะพะปั, ะธะทะฑะตัะตัะต ะทะฐะฟะธั",
		viewtext: "",
		viewtitle: "ะัะตะณะปะตะด ะธะทะฑัะฐะฝ ะทะฐะฟะธั"
	},
	col : {
		caption: "ะะทะฑะตัะธ ะบะพะปะพะฝะธ",
		bSubmit: "ะะบ",
		bCancel: "ะะทัะพะด"	
	},
	errors : {
		errcap : "ะัะตัะบะฐ",
		nourl : "ะัะผะฐ ะฟะพัะพัะตะฝ url ะฐะดัะตั",
		norecords: "ะัะผะฐ ะทะฐะฟะธั ะทะฐ ะพะฑัะฐะฑะพัะบะฐ",
		model : "ะะพะดะตะปะฐ ะฝะต ััะพัะฒะตัััะฒะฐ ะฝะฐ ะธะผะตะฝะฐัะฐ!"	
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:" ะปะฒ.", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"ะะตะด", "ะะพะฝ", "ะั", "ะกั", "ะงะตั", "ะะตั", "ะกัะฑ",
				"ะะตะดะตะปั", "ะะพะฝะตะดะตะปะฝะธะบ", "ะัะพัะฝะธะบ", "ะกััะดะฐ", "ะงะตัะฒััััะบ", "ะะตััะบ", "ะกัะฑะพัะฐ"
			],
			monthNames: [
				"ะฏะฝั", "ะคะตะฒ", "ะะฐั", "ะะฟั", "ะะฐะน", "ะฎะฝะธ", "ะฎะปะธ", "ะะฒะณ", "ะกะตะฟ", "ะะบั", "ะะพะฒ", "ะะตะบ",
				"ะฏะฝัะฐัะธ", "ะคะตะฒััะฐัะธ", "ะะฐัั", "ะะฟัะธะป", "ะะฐะน", "ะฎะฝะธ", "ะฎะปะธ", "ะะฒะณััั", "ะกะตะฟัะตะผะฒัะธ", "ะะบัะพะผะฒัะธ", "ะะพะตะผะฒัะธ", "ะะตะบะตะผะฒัะธ"
			],
			AmPm : ["","","",""],
			S: function (j) {
				if(j==7 || j==8 || j== 27 || j== 28) {
					return 'ะผะธ';
				}
				return ['ะฒะธ', 'ัะธ', 'ัะธ'][Math.min((j - 1) % 10, 2)];
			},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n/j/Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
