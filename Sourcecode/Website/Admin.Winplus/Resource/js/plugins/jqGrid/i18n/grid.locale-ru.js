﻿;(function($){
/**
 * jqGrid Russian Translation v1.0 02.07.2009 (based on translation by Alexey Kanaev v1.1 21.01.2009, http://softcore.com.ru)
 * Sergey Dyagovchenko
 * http://d.sumy.ua
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "ะัะพัะผะพัั {0} - {1} ะธะท {2}",
		emptyrecords: "ะะตั ะทะฐะฟะธัะตะน ะดะปั ะฟัะพัะผะพััะฐ",
		loadtext: "ะะฐะณััะทะบะฐ...",
		pgtext : "ะกัั. {0} ะธะท {1}"
	},
	search : {
		caption: "ะะพะธัะบ...",
		Find: "ะะฐะนัะธ",
		Reset: "ะกะฑัะพั",
		odata: [{ oper:'eq', text:"ัะฐะฒะฝะพ"},{ oper:'ne', text:"ะฝะต ัะฐะฒะฝะพ"},{ oper:'lt', text:"ะผะตะฝััะต"},{ oper:'le', text:"ะผะตะฝััะต ะธะปะธ ัะฐะฒะฝะพ"},{ oper:'gt', text:"ะฑะพะปััะต"},{ oper:'ge', text:"ะฑะพะปััะต ะธะปะธ ัะฐะฒะฝะพ"},{ oper:'bw', text:"ะฝะฐัะธะฝะฐะตััั ั"},{ oper:'bn', text:"ะฝะต ะฝะฐัะธะฝะฐะตััั ั"},{ oper:'in', text:"ะฝะฐัะพะดะธััั ะฒ"},{ oper:'ni', text:"ะฝะต ะฝะฐัะพะดะธััั ะฒ"},{ oper:'ew', text:"ะทะฐะบะฐะฝัะธะฒะฐะตััั ะฝะฐ"},{ oper:'en', text:"ะฝะต ะทะฐะบะฐะฝัะธะฒะฐะตััั ะฝะฐ"},{ oper:'cn', text:"ัะพะดะตัะถะธั"},{ oper:'nc', text:"ะฝะต ัะพะดะตัะถะธั"},{ oper:'nu', text:"ัะฐะฒะฝะพ NULL"},{ oper:'nn', text:"ะฝะต ัะฐะฒะฝะพ NULL"}],
		groupOps: [	{ op: "AND", text: "ะฒัะต" }, { op: "OR", text: "ะปัะฑะพะน" }],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "ะะพะฑะฐะฒะธัั ะทะฐะฟะธัั",
		editCaption: "ะ ะตะดะฐะบัะธัะพะฒะฐัั ะทะฐะฟะธัั",
		bSubmit: "ะกะพััะฐะฝะธัั",
		bCancel: "ะัะผะตะฝะฐ",
		bClose: "ะะฐะบัััั",
		saveData: "ะะฐะฝะฝัะต ะฑัะปะธ ะธะทะผะตะฝะตะฝะฝั! ะกะพััะฐะฝะธัั ะธะทะผะตะฝะตะฝะธั?",
		bYes : "ะะฐ",
		bNo : "ะะตั",
		bExit : "ะัะผะตะฝะฐ",
		msg: {
			required:"ะะพะปะต ัะฒะปัะตััั ะพะฑัะทะฐัะตะปัะฝัะผ",
			number:"ะะพะถะฐะปัะนััะฐ, ะฒะฒะตะดะธัะต ะฟัะฐะฒะธะปัะฝะพะต ัะธัะปะพ",
			minValue:"ะทะฝะฐัะตะฝะธะต ะดะพะปะถะฝะพ ะฑััั ะฑะพะปััะต ะปะธะฑะพ ัะฐะฒะฝะพ",
			maxValue:"ะทะฝะฐัะตะฝะธะต ะดะพะปะถะฝะพ ะฑััั ะผะตะฝััะต ะปะธะฑะพ ัะฐะฒะฝะพ",
			email: "ะฝะตะบะพััะตะบัะฝะพะต ะทะฝะฐัะตะฝะธะต e-mail",
			integer: "ะะพะถะฐะปัะนััะฐ, ะฒะฒะตะดะธัะต ัะตะปะพะต ัะธัะปะพ",
			date: "ะะพะถะฐะปัะนััะฐ, ะฒะฒะตะดะธัะต ะฟัะฐะฒะธะปัะฝัั ะดะฐัั",
			url: "ะฝะตะฒะตัะฝะฐั ัััะปะบะฐ. ะะตะพะฑัะพะดะธะผะพ ะฒะฒะตััะธ ะฟัะตัะธะบั ('http://' ะธะปะธ 'https://')",
			nodefined : " ะฝะต ะพะฟัะตะดะตะปะตะฝะพ!",
			novalue : " ะฒะพะทะฒัะฐัะฐะตะผะพะต ะทะฝะฐัะตะฝะธะต ะพะฑัะทะฐัะตะปัะฝะพ!",
			customarray : "ะะพะปัะทะพะฒะฐัะตะปััะบะฐั ััะฝะบัะธั ะดะพะปะถะฝะฐ ะฒะพะทะฒัะฐัะฐัั ะผะฐััะธะฒ!",
			customfcheck : "ะะพะปัะทะพะฒะฐัะตะปััะบะฐั ััะฝะบัะธั ะดะพะปะถะฝะฐ ะฟัะธัััััะฒะพะฒะฐัั ะฒ ัะปััะฐะธ ะฟะพะปัะทะพะฒะฐัะตะปััะบะพะน ะฟัะพะฒะตัะบะธ!"
		}
	},
	view : {
		caption: "ะัะพัะผะพัั ะทะฐะฟะธัะธ",
		bClose: "ะะฐะบัััั"
	},
	del : {
		caption: "ะฃะดะฐะปะธัั",
		msg: "ะฃะดะฐะปะธัั ะฒัะฑัะฐะฝะฝัั ะทะฐะฟะธัั(ะธ)?",
		bSubmit: "ะฃะดะฐะปะธัั",
		bCancel: "ะัะผะตะฝะฐ"
	},
	nav : {
		edittext: " ",
		edittitle: "ะ ะตะดะฐะบัะธัะพะฒะฐัั ะฒัะฑัะฐะฝะฝัั ะทะฐะฟะธัั",
		addtext:" ",
		addtitle: "ะะพะฑะฐะฒะธัั ะฝะพะฒัั ะทะฐะฟะธัั",
		deltext: " ",
		deltitle: "ะฃะดะฐะปะธัั ะฒัะฑัะฐะฝะฝัั ะทะฐะฟะธัั",
		searchtext: " ",
		searchtitle: "ะะฐะนัะธ ะทะฐะฟะธัะธ",
		refreshtext: "",
		refreshtitle: "ะะฑะฝะพะฒะธัั ัะฐะฑะปะธัั",
		alertcap: "ะะฝะธะผะฐะฝะธะต",
		alerttext: "ะะพะถะฐะปัะนััะฐ, ะฒัะฑะตัะธัะต ะทะฐะฟะธัั",
		viewtext: "",
		viewtitle: "ะัะพัะผะพััะตัั ะฒัะฑัะฐะฝะฝัั ะทะฐะฟะธัั"
	},
	col : {
		caption: "ะะพะบะฐะทะฐัั/ัะบัััั ััะพะปะฑัั",
		bSubmit: "ะกะพััะฐะฝะธัั",
		bCancel: "ะัะผะตะฝะฐ"	
	},
	errors : {
		errcap : "ะัะธะฑะบะฐ",
		nourl : "URL ะฝะต ัััะฐะฝะพะฒะปะตะฝ",
		norecords: "ะะตั ะทะฐะฟะธัะตะน ะดะปั ะพะฑัะฐะฑะพัะบะธ",
		model : "ะงะธัะปะพ ะฟะพะปะตะน ะฝะต ัะพะพัะฒะตัััะฒัะตั ัะธัะปั ััะพะปะฑัะพะฒ ัะฐะฑะปะธัั!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0,00'},
		currency : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0,00'},
		date : {
			dayNames:   [
				"ะั", "ะะฝ", "ะั", "ะกั", "ะงั", "ะั", "ะกะฑ",
				"ะะพัะบัะตัะตะฝะธะต", "ะะพะฝะตะดะตะปัะฝะธะบ", "ะัะพัะฝะธะบ", "ะกัะตะดะฐ", "ะงะตัะฒะตัะณ", "ะััะฝะธัะฐ", "ะกัะฑะฑะพัะฐ"
			],
			monthNames: [
				"ะฏะฝะฒ", "ะคะตะฒ", "ะะฐั", "ะะฟั", "ะะฐะน", "ะัะฝ", "ะัะป", "ะะฒะณ", "ะกะตะฝ", "ะะบั", "ะะพั", "ะะตะบ",
				"ะฏะฝะฒะฐัั", "ะคะตะฒัะฐะปั", "ะะฐัั", "ะะฟัะตะปั", "ะะฐะน", "ะัะฝั", "ะัะปั", "ะะฒะณััั", "ะกะตะฝััะฑัั", "ะะบััะฑัั", "ะะพัะฑัั", "ะะตะบะฐะฑัั"
			],
			AmPm : ["am","pm","AM","PM"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th';},
			srcformat: 'Y-m-d',
			newformat: 'd.m.Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n.j.Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y G:i:s",
				MonthDay: "F d",
				ShortTime: "G:i",
				LongTime: "G:i:s",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
