﻿;(function($){
/**
 * jqGrid English Translation
 * Tony Tomov tony@trirand.com
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "๋ณด๊ธฐ {0} - {1} / {2}",
		emptyrecords: "ํ์ํ  ํ์ด ์์ต๋๋ค",
		loadtext: "์กฐํ์ค...",
		pgtext : "ํ์ด์ง {0} / {1}"
	},
	search : {
		caption: "๊ฒ์...",
		Find: "์ฐพ๊ธฐ",
		Reset: "์ด๊ธฐํ",
		odata: [{ oper:'eq', text:"๊ฐ๋ค"},{ oper:'ne', text:"๊ฐ์ง ์๋ค"},{ oper:'lt', text:"์๋ค"},{ oper:'le', text:"์๊ฑฐ๋ ๊ฐ๋ค"},{ oper:'gt', text:"ํฌ๋ค"},{ oper:'ge', text:"ํฌ๊ฑฐ๋ ๊ฐ๋ค"},{ oper:'bw', text:"๋ก ์์ํ๋ค"},{ oper:'bn', text:"๋ก ์์ํ์ง ์๋๋ค"},{ oper:'in', text:"๋ด์ ์๋ค"},{ oper:'ni', text:"๋ด์ ์์ง ์๋ค"},{ oper:'ew', text:"๋ก ๋๋๋ค"},{ oper:'en', text:"๋ก ๋๋์ง ์๋๋ค"},{ oper:'cn', text:"๋ด์ ์กด์ฌํ๋ค"},{ oper:'nc', text:"๋ด์ ์กด์ฌํ์ง ์๋๋ค"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "์ ๋ถ" },	{ op: "OR",  text: "์์" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "ํ ์ถ๊ฐ",
		editCaption: "ํ ์์ ",
		bSubmit: "์ ์ก",
		bCancel: "์ทจ์",
		bClose: "๋ซ๊ธฐ",
		saveData: "์๋ฃ๊ฐ ๋ณ๊ฒฝ๋์์ต๋๋ค! ์ ์ฅํ์๊ฒ ์ต๋๊น?",
		bYes : "์",
		bNo : "์๋์ค",
		bExit : "์ทจ์",
		msg: {
			required:"ํ์ํญ๋ชฉ์๋๋ค",
			number:"์ ํจํ ๋ฒํธ๋ฅผ ์๋ ฅํด ์ฃผ์ธ์",
			minValue:"์๋ ฅ๊ฐ์ ํฌ๊ฑฐ๋ ๊ฐ์์ผ ํฉ๋๋ค",
			maxValue:"์๋ ฅ๊ฐ์ ์๊ฑฐ๋ ๊ฐ์์ผ ํฉ๋๋ค",
			email: "์ ํจํ์ง ์์ ์ด๋ฉ์ผ์ฃผ์์๋๋ค",
			integer: "์ ํจํ ์ซ์๋ฅผ ์๋ ฅํ์ธ์",
			date: "์ ํจํ ๋ ์ง๋ฅผ ์๋ ฅํ์ธ์",
			url: "์ ์ ํจํ์ง ์์ URL์๋๋ค. ๋ฌธ์ฅ์์ ๋ค์๋จ์ด๊ฐ ํ์ํฉ๋๋ค('http://' or 'https://')",
			nodefined : " ์ ์ ์๋์ง ์์์ต๋๋ค!",
			novalue : " ๋ฐํ๊ฐ์ด ํ์ํฉ๋๋ค!",
			customarray : "์ฌ์ฉ์์ ์ ํจ์๋ ๋ฐฐ์ด์ ๋ฐํํด์ผ ํฉ๋๋ค!",
			customfcheck : "Custom function should be present in case of custom checking!"
			
		}
	},
	view : {
		caption: "ํ ์กฐํ",
		bClose: "๋ซ๊ธฐ"
	},
	del : {
		caption: "์ญ์ ",
		msg: "์ ํ๋ ํ์ ์ญ์ ํ์๊ฒ ์ต๋๊น?",
		bSubmit: "์ญ์ ",
		bCancel: "์ทจ์"
	},
	nav : {
		edittext: "",
		edittitle: "์ ํ๋ ํ ํธ์ง",
		addtext:"",
		addtitle: "ํ ์ฝ์",
		deltext: "",
		deltitle: "์ ํ๋ ํ ์ญ์ ",
		searchtext: "",
		searchtitle: "ํ ์ฐพ๊ธฐ",
		refreshtext: "",
		refreshtitle: "๊ทธ๋ฆฌ๋ ๊ฐฑ์ ",
		alertcap: "๊ฒฝ๊ณ ",
		alerttext: "ํ์ ์ ํํ์ธ์",
		viewtext: "",
		viewtitle: "์ ํ๋ ํ ์กฐํ"
	},
	col : {
		caption: "์ด์ ์ ํํ์ธ์",
		bSubmit: "ํ์ธ",
		bCancel: "์ทจ์"
	},
	errors : {
		errcap : "์ค๋ฅ",
		nourl : "์ค์ ๋ url์ด ์์ต๋๋ค",
		norecords: "์ฒ๋ฆฌํ  ํ์ด ์์ต๋๋ค",
		model : "colNames์ ๊ธธ์ด๊ฐ colModel๊ณผ ์ผ์นํ์ง ์์ต๋๋ค!"
	},
	formatter : {
		integer : {thousandsSeparator: ",", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat",
				"์ผ", "์", "ํ", "์", "๋ชฉ", "๊ธ", "ํ "
			],
			monthNames: [
				"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
				"1์", "2์", "3์", "4์", "5์", "6์", "7์", "8์", "9์", "10์", "11์", "12์"
			],
			AmPm : ["am","pm","AM","PM"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'm-d-Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "Y/j/n",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
