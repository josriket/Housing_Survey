﻿;(function($){
/**
 * jqGrid Romanian Translation
 * Alexandru Emil Lupu contact@alecslupu.ro
 * http://www.alecslupu.ro/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Vizualizare {0} - {1} din {2}",
		emptyrecords: "Nu existฤ รฎnregistrฤri de vizualizat",
		loadtext: "รncฤrcare...",
		pgtext : "Pagina {0} din {1}"
	},
	search : {
		caption: "Cautฤ...",
		Find: "Cautฤ",
		Reset: "Resetare",
		odata: [{ oper:'eq', text:"egal"},{ oper:'ne', text:"diferit"},{ oper:'lt', text:"mai mic"},{ oper:'le', text:"mai mic sau egal"},{ oper:'gt', text:"mai mare"},{ oper:'ge', text:"mai mare sau egal"},{ oper:'bw', text:"รฎncepe cu"},{ oper:'bn', text:"nu รฎncepe cu"},{ oper:'in', text:"se gฤseศte รฎn"},{ oper:'ni', text:"nu se gฤseศte รฎn"},{ oper:'ew', text:"se terminฤ cu"},{ oper:'en', text:"nu se terminฤ cu"},{ oper:'cn', text:"conศine"},{ oper:'nc', text:""},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "toate" },	{ op: "OR",  text: "oricare" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "Adฤugare รฎnregistrare",
		editCaption: "Modificare รฎnregistrare",
		bSubmit: "Salveazฤ",
		bCancel: "Anulare",
		bClose: "รnchide",
		saveData: "Informaศiile au fost modificate! Salvaศi modificฤrile?",
		bYes : "Da",
		bNo : "Nu",
		bExit : "Anulare",
		msg: {
			required:"Cรขmpul este obligatoriu",
			number:"Vฤ rugฤm introduceศi un numฤr valid",
			minValue:"valoarea trebuie sa fie mai mare sau egalฤ cu",
			maxValue:"valoarea trebuie sa fie mai micฤ sau egalฤ cu",
			email: "nu este o adresฤ de e-mail validฤ",
			integer: "Vฤ rugฤm introduceศi un numฤr valid",
			date: "Vฤ rugฤm sฤ introduceศi o datฤ validฤ",
			url: "Nu este un URL valid. Prefixul  este necesar('http://' or 'https://')",
			nodefined : " is not defined!",
			novalue : " return value is required!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
		}
	},
	view : {
		caption: "Vizualizare รฎnregistrare",
		bClose: "รnchidere"
	},
	del : {
		caption: "ศtegere",
		msg: "ศtergeศi รฎnregistrarea (รฎnregistrฤrile) selectate?",
		bSubmit: "ศterge",
		bCancel: "Anulare"
	},
	nav : {
		edittext: "",
		edittitle: "Modificฤ rรขndul selectat",
		addtext:"",
		addtitle: "Adaugฤ rรขnd nou",
		deltext: "",
		deltitle: "ศterge rรขndul selectat",
		searchtext: "",
		searchtitle: "Cฤutare รฎnregistrฤri",
		refreshtext: "",
		refreshtitle: "Reรฎncarcare Grid",
		alertcap: "Avertisment",
		alerttext: "Vฤ rugฤm sฤ selectaศi un rรขnd",
		viewtext: "",
		viewtitle: "Vizualizeazฤ rรขndul selectat"
	},
	col : {
		caption: "Aratฤ/Ascunde coloanele",
		bSubmit: "Salveazฤ",
		bCancel: "Anulare"
	},
	errors : {
		errcap : "Eroare",
		nourl : "Niciun url nu este setat",
		norecords: "Nu sunt รฎnregistrฤri de procesat",
		model : "Lungimea colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0,00'},
		currency : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0,00'},
		date : {
			dayNames:   [
				"Dum", "Lun", "Mar", "Mie", "Joi", "Vin", "Sรขm",
				"Duminicฤ", "Luni", "Marศi", "Miercuri", "Joi", "Vineri", "Sรขmbฤtฤ"
			],
			monthNames: [
				"Ian", "Feb", "Mar", "Apr", "Mai", "Iun", "Iul", "Aug", "Sep", "Oct", "Noi", "Dec",
				"Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"
			],
			AmPm : ["am","pm","AM","PM"],
			/*
			 Here is a problem in romanian: 
					M	/	F
			 1st = primul / prima
			 2nd = Al doilea / A doua
			 3rd = Al treilea / A treia 
			 4th = Al patrulea/ A patra
			 5th = Al cincilea / A cincea 
			 6th = Al ศaselea / A ศasea
			 7th = Al ศaptelea / A ศaptea
			 .... 
			 */
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n/j/Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
