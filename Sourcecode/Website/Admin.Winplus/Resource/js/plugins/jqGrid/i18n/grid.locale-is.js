﻿;(function($){
/**
 * jqGrid Icelandic Translation
 * jtm@hi.is Univercity of Iceland
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Skoรฐa {0} - {1} af {2}",
	    emptyrecords: "Engar fรฆrslur",
		loadtext: "Hleรฐur...",
		pgtext : "Sรญรฐa {0} af {1}"
	},
	search : {
	    caption: "Leita...",
	    Find: "Leita",
	    Reset: "Endursetja",
	    odata: [{ oper:'eq', text:"sama og"},{ oper:'ne', text:"ekki sama og"},{ oper:'lt', text:"minna en"},{ oper:'le', text:"minna eรฐa jafnt og"},{ oper:'gt', text:"stรฆrra en"},{ oper:'ge', text:"stรฆrra eรฐa jafnt og"},{ oper:'bw', text:"byrjar รก"},{ oper:'bn', text:"byrjar ekki รก"},{ oper:'in', text:"er รญ"},{ oper:'ni', text:"er ekki รญ"},{ oper:'ew', text:"endar รก"},{ oper:'en', text:"endar ekki รก"},{ oper:'cn', text:"inniheldur"},{ oper:'nc', text:"inniheldur ekki"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
	    groupOps: [	{ op: "AND", text: "allt" },	{ op: "OR",  text: "eรฐa" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
	    addCaption: "Bรฆta viรฐ fรฆrslu",
	    editCaption: "Breyta fรฆrslu",
	    bSubmit: "Vista",
	    bCancel: "Hรฆtta viรฐ",
		bClose: "Loka",
		saveData: "Gรถgn hafa breyst! Vista breytingar?",
		bYes : "Jรก",
		bNo : "Nei",
		bExit : "Hรฆtta viรฐ",
	    msg: {
	        required:"Reitur er nauรฐsynlegur",
	        number:"Vinsamlega settu inn tรถlu",
	        minValue:"gildi verรฐur aรฐ vera meira en eรฐa jafnt og ",
	        maxValue:"gildi verรฐur aรฐ vera minna en eรฐa jafnt og ",
	        email: "er ekki lรถglegt email",
	        integer: "Vinsamlega settu inn tรถlu",
			date: "Vinsamlega setti inn dagsetningu",
			url: "er ekki lรถglegt URL. Vantar ('http://' eรฐa 'https://')",
			nodefined : " er ekki skilgreint!",
			novalue : " skilagildi nauรฐsynlegt!",
			customarray : "Fall skal skila fylki!",
			customfcheck : "Fall skal vera skilgreint!"
		}
	},
	view : {
	    caption: "Skoรฐa fรฆrslu",
	    bClose: "Loka"
	},
	del : {
	    caption: "Eyรฐa",
	    msg: "Eyรฐa vรถldum fรฆrslum ?",
	    bSubmit: "Eyรฐa",
	    bCancel: "Hรฆtta viรฐ"
	},
	nav : {
		edittext: " ",
	    edittitle: "Breyta fรฆrslu",
		addtext:" ",
	    addtitle: "Nรฝ fรฆrsla",
	    deltext: " ",
	    deltitle: "Eyรฐa fรฆrslu",
	    searchtext: " ",
	    searchtitle: "Leita",
	    refreshtext: "",
	    refreshtitle: "Endurhlaรฐa",
	    alertcap: "Viรฐvรถrun",
	    alerttext: "Vinsamlega veldu fรฆrslu",
		viewtext: "",
		viewtitle: "Skoรฐa valda fรฆrslu"
	},
	col : {
	    caption: "Sรฝna / fela dรกlka",
	    bSubmit: "Vista",
	    bCancel: "Hรฆtta viรฐ"	
	},
	errors : {
		errcap : "Villa",
		nourl : "Vantar slรณรฐ",
		norecords: "Engar fรฆrslur valdar",
	    model : "Lengd colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"Sun", "Mรกn", "รri", "Miรฐ", "Fim", "Fรถs", "Lau",
				"Sunnudagur", "Mรกnudagur", "รriรฐjudagur", "Miรฐvikudagur", "Fimmtudagur", "Fรถstudagur", "Laugardagur"
			],
			monthNames: [
				"Jan", "Feb", "Mar", "Apr", "Maรญ", "Jรบn", "Jรบl", "รgรบ", "Sep", "Oct", "Nรณv", "Des",
				"Janรบar", "Febrรบar", "Mars", "Aprรญl", "Maรญ", "Jรบnรฝ", "Jรบlรฝ", "รgรบst", "September", "Oktรณber", "Nรณvember", "Desember"
			],
			AmPm : ["am","pm","AM","PM"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
	            ISO8601Long:"Y-m-d H:i:s",
	            ISO8601Short:"Y-m-d",
	            ShortDate: "n/j/Y",
	            LongDate: "l, F d, Y",
	            FullDateTime: "l, F d, Y g:i:s A",
	            MonthDay: "F d",
	            ShortTime: "g:i A",
	            LongTime: "g:i:s A",
	            SortableDateTime: "Y-m-d\\TH:i:s",
	            UniversalSortableDateTime: "Y-m-d H:i:sO",
	            YearMonth: "F, Y"
	        },
	        reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
