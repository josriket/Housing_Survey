
    /* Table display format data method */
    function FormatString(data, type, row, meta) {
        return (data == null) ? "" : data;
    }

    function FormatDate(data, type, row, meta) {
        return (data == null) ? "" : new Date(data).format("dd/mm/yyyy", true);
    }

    function FormatDateTime(data, type, row, meta) {
        return (data == null) ? "" : new Date(data).format("dd/mm/yyyy HH:MM:ss", true);
    }

    function FormatTimeSpan(data, type, row, meta) {
        return (data == null) ? "" : data.substring(0,5);
    }

    function FormatDecimal(data, type, row, meta) {
        return (data == null) ? "" : addCommas(parseFloat(data).toFixed(2));
    }

    function FormatInt32(data, type, row, meta) {
        return (data == null) ? "" : data;
    }

    function FormatInt64(data, type, row, meta) {
        return (data == null) ? "" : data;
    }

    function FormatBoolean(data, type, row, meta) {
        return (data == null) ? "" : data;
    }

    function FormatGuid(data, type, row, meta) {
        return (data == null) ? "" : data;
    }
    
    function FormatCheckbox(data, type, row, meta, pkName, chkName) {
        return "<input type=\"CheckBox\" class=\"i-checks chk" + pkName + "\" name=\"" + chkName + "\" value=" + row[pkName] + " />";
    }

    function FormatEditById(data, type, row, meta, pkName) {
        return "<a href='javascript:gotoEditById(" + row[pkName] + ");'>" + data + "</a>";
    }

    function FormatChooseLink(data, type, row, meta) {
        var fulldata = data;
        if (data.length > 70) {
            data = data.substring(0, 70)+"...";
        }
        return "<a title=\"" + fulldata + "\" href='javascript:gotoChooseItem(" + row["id"] + ",\"" + fulldata + "\");'>" + data + "</a>";
    }

    function FormatHyperLink(data, type, row, meta, pkName) {
        return "<a href='javascript:DisplayImageByUrl(\"" + data + "\")'>" + data + "</a>";
    }

    /* Util method */
    function getCheckedOneValue(elName) {
        var checkedId = "";
        var els = document.getElementsByName(elName);
        var totalFound = 0;
        for (i = 0; i < els.length; i++) {
            if (els[i].checked) {
                checkedId = els[i].value;
                totalFound++;
            }
        }
        if (totalFound == 1) {
            return checkedId;
        } else if (totalFound == 0) {
            alert("Please select item");
        } else if (totalFound > 1) {
            alert("Please select one item");
        }
        return 0;
    }

    function getCheckedMultiple(elName, failAlert) {
        if (failAlert == null) failAlert = true;
        var checkedId = "";
        var els = document.getElementsByName(elName);
        var totalFound = 0;
        for (i = 0; i < els.length; i++) {
            if (els[i].checked) {
                if (checkedId != "") checkedId += ",";
                checkedId += els[i].value;
                totalFound++;
            }
        }
        if (totalFound == 0) {
            if (failAlert) {
                alert("Please select item");
            }
        } else {
            return checkedId
        }
        return "";
    }


    function formParams(formFilter) {
        var params = "";
        $(formFilter).each(function (index) {
            if ($(this).val() != "" && $(this).val()!=null) {
                if (params != "") params += "&";
                params += ($(this).attr("id") + "=" + $(this).val());
            }
        });
        return params;
    }

    function bindingStandardField() {
        $('.input-group.date').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        }).on('changeDate', function (event) {
            $(this).datepicker('hide');
            try{
                var fnName = $(this).find("input").attr("onchangedate");
                if (IsNotEmpty(fnName)) {
                    var fn = new Function(fnName);
                    //setTimeout(fn, 10)
                    fn();
                }
            } catch (e) {
                alert("Error onchangedate->" + e);
            }
            //console.info(event.date);
            //console.info($(this).find("input").attr("id"));
        }).css({ "z-index": 2000 });

        $('.input-daterange').datepicker({
            format: 'dd/mm/yyyy',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $('.clockpicker').clockpicker().css({ "z-index": 2000 });

        $('.wysiwyg-editor').summernote({
            toolbar: [
                ["font", ["bold", "italic", "underline", "clear"]],
                ["fontname", ["fontname"]],
                ["color", ["color"]],
            ]
        });


        $('.full-wysiwyg-editor').summernote();

        //$('.wysiwyg-editor-readonly').summernote('disable');

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    }


    function addRequired(inputId,attr) {
        $(inputId).attr("data-val", attr);
        if (attr == "true") {
            $(inputId).attr("required","")
        } else {
            $(inputId).removeAttr("required")
        }
    }

    function ajaxQuery(sql,callBack) {
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "text/plain; charset=utf-8",
            url: "/AjaxQuery",
            cache: false,
            data: sql,
            success: function (result) {
                callBack(result);
            },
        });
    }

    function BindSelectItem(jqueryId, dataSource, firstValue) {
        var element = $(jqueryId);

        element.empty();
        if (firstValue != null) {
            element.append("<option value=\"\">" + firstValue + "</option>");
        }

        if (IsNotEmpty(dataSource)) {
            for (i = 0; i < dataSource.length; i++) {
                var item = dataSource[i];
                element.append("<option value=\"" + item.Id + "\">" + item.Name + "</option>");
            }
        }
    }

    //      0123456789
    //Input dd/MM/yyyy , Output yyyy-MM-dd
    function ParseDBDate(dateTxt) {
        if (IsNotEmpty(dateTxt)) {
            return dateTxt.substring(6, 10) + '-' + dateTxt.substring(3, 5) + '-' + dateTxt.substring(0, 2);
        }
        return "";
    }

    function IsNotEmpty(val) {
        return !(val === undefined || val === null || val == '');
    }
    function IsEmpty(val) {
        return (val === undefined || val === null || val == '');
    }
    function ParseIntWithZero(val) {
        if (IsNotEmpty(val)) {
            return val;
        }
        return 0;
    }

    function addCommas(nStr) {
        nStr = nStr.replace(/,/g, "");
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function HideDataTableTools(tableId, filter, paginate, info,length) {
        if (filter) {
            $("#" + tableId + "_filter").html("");
        }
        if (paginate) {
            $("#" + tableId + "_paginate").html("");
        }
        if (info) {
            $("#" + tableId + "_info").html("");
        }
        if (length) {
            $("#" + tableId + "_length").html("");
        }
    }

    function CheckInputLength(el, length) {
        var jEl = $(el);
        if (jEl.val().length>0 && jEl.val().length < length) {
            jEl.val("");
            alert("\u0e42\u0e1b\u0e23\u0e14\u0e01\u0e23\u0e2d\u0e01\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25\u0e43\u0e2b\u0e49\u0e04\u0e23\u0e1a " + length + " \u0e2b\u0e25\u0e31\u0e01");
            jEl.focus();
        }
    }