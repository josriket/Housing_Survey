using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.choosedialog.model;
using Library.Winplus.syslib.standard.choosedialog.repository;
using Library.Winplus.syslib.standard.choosedialog.search;
using Library.Winplus.syslib.system.common.model;
using Library.Winplus.syslib.standard.systemmenu.repository;
namespace Library.Winplus.syslib.standard.choosedialog.controller
{
    public partial class ChooseDialogController : SimpleController<ChooseDialog, ChooseDialogSearch>
    {
        public override ActionResult _Index(DBConnection conn, LoginUser loginUser, ChooseDialogSearch model)
        {
            return View("~/Views/standard/choosedialog/search.cshtml", model);
        }

        public override ActionResult _Search(DBConnection conn, LoginUser loginUser, ChooseDialogSearch model)
        {
            model.PrepareSql(Request, loginUser);

            SearchResultModel result = new ChooseDialogRepository().AdvanceSearch(conn, model);
            
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }

        public override ActionResult _Find(DBConnection conn, LoginUser loginUser, Int64? id)
        {
            ChooseDialog model = new ChooseDialog();

            if (id.HasValue && id.Value != 0)
            {
                model = new ChooseDialogRepository().FindById(conn, id.Value);
            }

            return View("~/Views/standard/choosedialog/addedit.cshtml", model);
        }

        public override ActionResult _Copy(DBConnection conn, LoginUser loginUser, Int64? id)
        {
            ChooseDialog model = new ChooseDialog();

            if (id.HasValue && id.Value != 0)
            {
                model = new ChooseDialogRepository().FindById(conn, id.Value);
            }

            model.ChooseDialogId = null;

            return View("~/Views/standard/choosedialog/addedit.cshtml", model);
        }

        public override ActionResult _Save(DBConnection conn, LoginUser loginUser, ChooseDialog model)
        {
            try
            {
                conn.beginTrans();


                if (model.ChooseDialogId.HasValue && model.ChooseDialogId.Value != 0)
                {
                    // Update
                    new ChooseDialogRepository().Update(conn, model);
                }
                else
                {
                    // Create
                    new ChooseDialogRepository().Create(conn, model);
                }

                conn.commitTrans();
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _Active(DBConnection conn, LoginUser loginUser,Int64 activeStatusId, string ids)
        {
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    conn.beginTrans();

                    new ChooseDialogRepository().Active(conn, activeStatusId ,ids.Split(','));

                    conn.commitTrans();
                }
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _Delete(DBConnection conn, LoginUser loginUser, string ids)
        {
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    conn.beginTrans();

                    new ChooseDialogRepository().Delete(conn, ids.Split(','));

                    conn.commitTrans();
                }
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _ExportExcel(DBConnection conn, LoginUser loginUser, ChooseDialogSearch model)
        {
            return null;
        }

        public override ActionResult _Import(DBConnection conn, LoginUser loginUser, List<ChooseDialog> lstImport)
        {
            try
            {
                conn.beginTrans();

                new ChooseDialogRepository().ImportExcel(conn, loginUser, lstImport);

                conn.commitTrans();

                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                if (conn != null) conn.rollbackTrans();
                ViewBag.ErrorMessage = ex.ToString();

                return ResponseJsonError(ex.ToString());
            }
        }

        // ~/ChooseDialogC/GetEntityName?EntityName=
        public ActionResult GetEntityName(string EntityName)
        {
            string entityName = string.Empty;
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                try
                {
                    entityName = new SystemMenuRepository().GetSystemMenuName(conn, EntityName);
                }
                catch (Exception ex)
                {
                    return ResponseJsonError(ex.ToString());
                }
            }
            return ResponseJsonSuccess(entityName);
        }
    }
}
