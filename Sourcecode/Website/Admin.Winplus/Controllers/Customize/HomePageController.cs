﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.homepage.model;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.standard.systemrole.model;
using Library.Winplus.syslib.standard.systemrole.repository;

namespace Library.Winplus.syslib.customize.homepage
{
    public class HomePageController : Controller
    {
        private readonly String NotAuthorizeView = "~/Views/_shared/notauthorize.cshtml";

        [NonAction]
        protected LoginUser GetLogin()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                return new LoginUserRepository().FindByTokenId(conn, Session.SessionID);
            }
        }

        public ActionResult Hash()
        {
            HomePage model = new HomePage();
            LoginUser loginUser = GetLogin();
            try
            {
                if (loginUser != null)
                {
                    model.loginUser = loginUser;
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        model.lstDisplaySystemParentMenu = new LoginUserRepository().LoadDisplayMenu(conn, loginUser);
                        return View("~/Views/customize/homepage/homepage.cshtml", model);
                    }

                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
            }
            return View(NotAuthorizeView);
        }

        public ActionResult Index()
        {
            HomePage model = new HomePage();
            LoginUser loginUser = GetLogin();
            try
            {
                if (loginUser != null)
                {
                    model.loginUser = loginUser;
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        model.lstDisplaySystemParentMenu = new LoginUserRepository().LoadDisplayMenu(conn, loginUser);

                        model.DefaultMenuUrl = model.loginUser.User.DefaultMenuUrl;
                        if (string.IsNullOrEmpty(model.DefaultMenuUrl))
                        {
                            SystemRole role = new SystemRoleRepository().FindById(conn, model.loginUser.User.SystemRoleId.Value);
                            model.DefaultMenuUrl = role.DefaultMenuUrl;
                        }

                        return View("~/Views/customize/homepage/homepage.cshtml", model);
                    }

                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
            }
            return View(NotAuthorizeView);
        }

    }
}
