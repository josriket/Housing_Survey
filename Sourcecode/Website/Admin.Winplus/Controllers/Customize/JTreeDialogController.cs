﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.choosetree.model;
using Library.Winplus.syslib.customize.choosetree.repository;
using Library.Winplus.syslib.standard.systemmenu.model;
using Library.Winplus.syslib.standard.systemmenu.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin.Winplus.Controllers.Customize
{
    public class JTreeDialogController : BaseController
    {
        public ActionResult Index()
        {
            try
            {
                string selectedid = Request.QueryString["selectedid"];
                string selectedids = Request.QueryString["selectedids"];
                string entity = Request.QueryString["entity"];
                string filter = Request.QueryString["filter"];
                string onclick = Request.QueryString["onclick"];
                if (string.IsNullOrEmpty(onclick))
                {
                    if (entity == "IncidentCase")
                    {
                        onclick = "chooseJTreeItemIncidentCase";
                    }
                    else
                    {
                        onclick = "chooseJTreeItem";
                    }
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    filter = StringUtil.DecodeBase64StringToString(filter);
                    if (filter != null)
                    {
                        filter = HttpUtility.UrlDecode(filter);
                    }
                }

                List<string> SelectedIds = new List<string>();
                if (!string.IsNullOrEmpty(selectedid))
                {
                    SelectedIds.Add(selectedid);
                } 
                
                if (!string.IsNullOrEmpty(selectedids))
                {
                    foreach (string id in selectedids.Split(','))
                    {
                        SelectedIds.Add(id);
                    }
                }


                string entityName = string.Empty;
                List<JTree> lstJTree = new List<JTree>();
                using (DBConnection conn = DBConnection.DefaultConnection)
                {

                    lstJTree = new JTreeRepository(entity, SelectedIds).FindChild(conn, null, null, -1, onclick, false, filter);
                    if (lstJTree != null && lstJTree.Count == 1)
                    {
                        lstJTree[0].state.opened = true;
                    }

                    entityName = new SystemMenuRepository().GetSystemMenuName(conn, entity);
                }

                return ResponseJsonSuccess(entityName, lstJTree);
            }
            catch (Exception ex)
            {
                return ResponseJsonError(ex.ToString());
            }
        }
    }
}
