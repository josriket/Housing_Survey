﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;

namespace Admin.Winplus.Controllers.Customize
{
    public class AjaxQueryController : Controller
    {
        //
        // GET: /AjaxQuery/

        public ActionResult Index()
        {
            try
            {
                Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
                string sql = new StreamReader(Request.InputStream).ReadToEnd();
                if (!string.IsNullOrEmpty(sql))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        DataSet ds = conn.executeDataSet(sql);


                        return Content(JSONUtil.Serialize(ds.Tables[0]), "application/json");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }

    }
}
