﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.standard.systemuser.repository;
using Library.Winplus.syslib.standard.userlogin.model;
using Library.Winplus.syslib.standard.userlogin.repository;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.standard.project.repository;

namespace Library.Winplus.syslib.customize.loginuser
{
    public class LoginUserController : BaseController
    {
        public ActionResult Index()
        {
            AttempLogin model = new AttempLogin();


            return View("~/Views/customize/login/login.cshtml",model);
        }

        public ActionResult Checklogin(AttempLogin model)
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    SystemUserRepository repo = new SystemUserRepository();

                    List<SystemUser> lstUser = repo.FindsByUserName(conn, model.UserName);
                    if (lstUser == null || lstUser.Count != 1)
                    {
                        return ResponseJsonError(MyResources.Winplus.Resource.LoginFail, $"Usernotfound for: [{model.UserName}]");
                    }
                    else
                    {
                        SystemUser user = lstUser[0];
                        string encPass = EncriptionUtil.EncrypOneWay(model.UserName.ToUpper() + "_" + model.Password);
                        if (user.Password == encPass)
                        {
                            //Prepare variable
                            int? SessionTimeout = SystemConfigService.GetIntConfig(SystemConfigKey.SessionTimeout,30);
                            UserLoginRepository userLoginRepo = new UserLoginRepository();

                            // Temporary LoginUser for field (CreateBy,ModifyBy)
                            LoginUser loginUser = new LoginUser();
                            loginUser.SystemUserId = user.SystemUserId;

                            // Create login user (Prepare insert)
                            UserLogin userLogin = new UserLogin();
                            userLogin.UserLoginName = user.SystemUserName;
                            userLogin.SystemUserId = user.SystemUserId;
                            userLogin.TokenId = Session.SessionID;
                            userLogin.ActionDate = DateTime.Now;
                            userLogin.ExpireDate = DateTime.Now.AddMinutes(SessionTimeout.Value);
                            userLogin.CreatedOn = DateTime.Now;
                            userLogin.CreatedBy = user.SystemUserId;
                            userLogin.ProjectIds = new ProjectRepository().GetProjectIds(conn, userLogin.SystemUserId);
                            // Kill another user in this machine
                            //userLoginRepo.DeleteByTokenId(conn, Session.SessionID);

                            // Kill this user in anoter session
                            //userLoginRepo.DeleteBySystemUserId(conn, user.SystemUserId.Value);

                            // Insert UserLogin
                            userLoginRepo.Create(conn, userLogin);

                            Session["UserLogin"] = userLogin;

                            return ResponseJsonSuccess();

                        }
                        else
                        {
                            return ResponseJsonError(MyResources.Winplus.Resource.LoginFail, $"Password not match for user: [{model.UserName}] ,Password:{model.Password}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ResponseJsonError(ex.ToString());
            }
        }

        public ActionResult Logout()
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    LoginUser loginUser = GetLogin();
                    new LoginUserRepository().Logout(conn, Session.SessionID);
                }
            }
            catch
            {
            }
            return RedirectToAction("Index", "LoginUser");
        }
    }
}
