using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey.model;
using Library.Winplus.syslib.standard.survey.search;
using Library.Winplus.syslib.system.common.model;
using System.Data;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.project.repository;
using Library.Winplus.syslib.standard.survey.repository;
using Library.Winplus.syslib.customize.logger.service;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.core.util;

namespace Library.Winplus.syslib.standard.survey.controller
{
    public partial class SurveyController
    {
        public ActionResult Map(Int64? id)
        {
            LoginUser loginUser = GetLogin();
            if (loginUser.SystemUserId > 1)
            {
                if (!loginUser.ProjectIds.Split(',').Contains(id.Value.ToString()))
                {
                    return View("~/Views/_shared/NotAuthorize.cshtml", null);
                }
            }
            SurveySearch search = new SurveySearch();
            search.search.ProjectId = id;
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                search.Project = new ProjectRepository().FindById(conn, id.Value);
            }
            return View("~/Views/standard/survey/surveymap.cshtml", search);
        }

        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, SurveySearch model)
        {

        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, SurveySearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, SurveySearch model, SearchResultModel result)
        {
            //model.SearchMode = "Map";
            //model.Start = 0;
            //model.Length = 3000;
            //SearchResultModel resultMap = new SurveyRepository().AdvanceSearch(conn, model);
            //result.mapData = resultMap;
        }

        public ActionResult SearchMap()
        {
            LoginUser loginUser = GetLogin();

            SearchResultModel result = new SearchResultModel();
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                SurveySearch model = new SurveySearch();
                model.SearchMode = "Map";
                model.Start = 0;
                model.Length = 3000;
                model.PrepareSql(Request, loginUser);

                result = new SurveyRepository().AdvanceSearch(conn, model);

            }

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(result)); ;
        }

        public ActionResult FindSurvey(Int64? ProjectId, Int64? id)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue && HasAuthorize("Survey", loginUser, "View"))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        Survey model = new Survey();
                        SurveyRepository repo = new SurveyRepository();
                        repo.SqlFindAll = repo.SqlFindAll.Replace("From [Survey]", $"From [Survey_{ProjectId}]");

                        _FindHandleBefore(conn, loginUser, id);

                        if (id.HasValue && id.Value != 0)
                        {
                            model = repo.FindById(conn, id.Value);
                        }
                        model.loginUser = loginUser;

                        _FindHandleAfter(conn, loginUser, id, model);

                        return View("~/Views/standard/survey/dlg_addedit_form.cshtml", model);
                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Find", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, Survey model)
        {
            model.ImageBesideFullPath = model.ImageBeside;
            model.ImageFrontFullPath = model.ImageFront;
            model.ImageReferFullPath = model.ImageRefer;
        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, Survey model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, Survey model)
        {

        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, Survey model)
        {

        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<Survey> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<Survey> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, SurveySearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, SurveySearch model, DataSet ds)
        {

        }

        public ActionResult ExportSurvey()
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue)
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        SurveySearch model = new SurveySearch();
                        model.SearchMode = "CSV";
                        model.PrepareSql(Request, loginUser);

                        DataSet ds = new SurveyRepository().ExportCSV(conn, model);
                        
                        CSVOutputUtil exporter = new CSVOutputUtil();
                        string csvName = exporter.ExportData(model.ProjectId, ds.Tables[0]);

                        return Redirect($"~/Resource/csv/export/{csvName}");
                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "ExportExcel", ex.ToString());
                return View(InternalErrorView);
            }
        }
    }
}
