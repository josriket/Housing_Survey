using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemrolemenu.model;
using Library.Winplus.syslib.standard.systemrolemenu.search;
using Library.Winplus.syslib.system.common.model;
using Library.Winplus.syslib.standard.systemrole.model;
using Library.Winplus.syslib.standard.systemmenu.model;
using Library.Winplus.syslib.standard.systemrole.repository;
using Library.Winplus.syslib.standard.systemmenu.repository;
using System.Data;

namespace Library.Winplus.syslib.standard.systemrolemenu.controller
{
    public partial class SystemRoleMenuController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleMenuSearch model)
        {
        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleMenuSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, SystemRoleMenuSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, SystemRoleMenu model)
        {

        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, SystemRoleMenu model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleMenu model)
        {
            SystemRole role = new SystemRoleRepository().FindById(conn, model.SystemRoleId.Value);
            SystemMenu menu = new SystemMenuRepository().FindById(conn, model.SystemMenuId.Value);
            model.SystemRoleMenuName = string.Format("{0} - {1}", role.SystemRoleName, menu.SystemMenuName);
        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, SystemRoleMenu model)
        {

        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<SystemRoleMenu> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<SystemRoleMenu> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleMenuSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, SystemRoleMenuSearch model, DataSet ds)
        {

        }
    }
}
