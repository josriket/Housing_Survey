using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.roadlane.model;
using Library.Winplus.syslib.standard.roadlane.repository;
using Library.Winplus.syslib.standard.roadlane.search;
using Library.Winplus.syslib.standard.mvcsetting.service;
using Library.Winplus.syslib.system.common.model;
using System.Data;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.standard.fileupload.repository;

namespace Library.Winplus.syslib.standard.roadlane.controller
{
    public partial class RoadLaneController : SimpleController<RoadLane, RoadLaneSearch>
    {
        public override ActionResult _Index(DBConnection conn, LoginUser loginUser, RoadLaneSearch model)
        {
            model.loginUser = loginUser;

            _IndexHandleBefore(conn, loginUser, model);

            model.PrepareSql(Request, loginUser);

            return View("~/Views/standard/roadlane/" + MVCSettingService.CustomView("RoadLane", "search") + ".cshtml", model);
        }

        public override ActionResult _Search(DBConnection conn, LoginUser loginUser, RoadLaneSearch model)
        {
            model.loginUser = loginUser;

            _SearchHandleBefore(conn, loginUser, model);

            model.PrepareSql(Request, loginUser);

            SearchResultModel result = new RoadLaneRepository().AdvanceSearch(conn, model);

            _SearchHandleAfter(conn, loginUser, model, result); 

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }

        public override ActionResult _Find(DBConnection conn, LoginUser loginUser, Int64? id)
        {
            RoadLane model = new RoadLane();

            _FindHandleBefore(conn, loginUser, id);

            if (id.HasValue && id.Value != 0)
            {
                model = new RoadLaneRepository().FindById(conn, id.Value);
            }
            model.loginUser = loginUser;

            _FindHandleAfter(conn, loginUser, id, model);

            return View("~/Views/standard/roadlane/" + MVCSettingService.CustomView("RoadLane", "addedit") + ".cshtml", model);
        }

        public override ActionResult _Copy(DBConnection conn, LoginUser loginUser, Int64? id)
        {
            RoadLane model = new RoadLane();
            _CopyHandleBefore(conn, loginUser, id);

            if (id.HasValue && id.Value != 0)
            {
                model = new RoadLaneRepository().FindById(conn, id.Value);
            }

            model.RoadLaneId = null;
            model.CreatedBy = null;
            model.CreatedOn = null;
            model.ModifiedBy = null;
            model.ModifiedOn = null;
            model.loginUser = loginUser;

            _CopyHandleAfter(conn, loginUser, id, model);

            return View("~/Views/standard/roadlane/" + MVCSettingService.CustomView("RoadLane", "addedit") + ".cshtml", model);
        }

        public override ActionResult _Save(DBConnection conn, LoginUser loginUser, RoadLane model)
        {
            try
            {
                model.loginUser = loginUser;
                conn.beginTrans();


                _SaveHandleBefore(conn, loginUser, model);

                if (model.RoadLaneId.HasValue && model.RoadLaneId.Value != 0)
                {
                    // Update
                    model.ModifiedOn = DateTimeUtil.CurrentDate();
                    model.ModifiedBy = loginUser.SystemUserId;
                    new RoadLaneRepository().Update(conn, model);
                }
                else
                {
                    // Create
                    model.CreatedOn = DateTimeUtil.CurrentDate();
                    model.CreatedBy = loginUser.SystemUserId;
                    model.RoadLaneId = new RoadLaneRepository().Create(conn, model);
                }

                _SaveHandleAfter(conn, loginUser, model);

                conn.commitTrans();
                return ResponseJsonSuccess(null, model);
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _Active(DBConnection conn, LoginUser loginUser,Int64 activeStatusId, string ids)
        {
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    conn.beginTrans();

                    _ActiveHandleBefore(conn, loginUser, activeStatusId, ids);

                    new RoadLaneRepository().Active(conn, activeStatusId ,ids.Split(','));

                    _ActiveHandleAfter(conn, loginUser, activeStatusId, ids);

                    conn.commitTrans();
                }
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _Delete(DBConnection conn, LoginUser loginUser, string ids)
        {
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    conn.beginTrans();

                    _DeleteHandleBefore(conn, loginUser, ids);

                    new RoadLaneRepository().Delete(conn, ids.Split(','));

                    _DeleteHandleAfter(conn, loginUser, ids);

                    conn.commitTrans();
                }
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _ExportExcel(DBConnection conn, LoginUser loginUser, RoadLaneSearch model)
        {
            _ExportExcelHandleBefore(conn, loginUser, model);

            DataSet ds = new RoadLaneRepository().ExportExcel(conn, model);

            _ExportExcelHandleAfter(conn, loginUser, model, ds);

            ExcelOutputUtil exporter = new ExcelOutputUtil(rm.GetString("RoadLane"), Server.MapPath("~/Resource/XLSTemplate.xlsx"));
            exporter.ToWorkBook(ds.Tables[0]);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            exporter.ResponseMemory(stream);

            return File(stream.ToArray(), "application/ms-excel", "RoadLane_" + DateTimeUtil.CurrentDate().Value.ToString("yyyyMMddHHmmss") + ".xlsx");
        }

        public override ActionResult _Import(DBConnection conn, LoginUser loginUser, List<RoadLane> lstImport)
        {
            try
            {
                DateTime? CurrentDate = DateTimeUtil.CurrentDate();
                foreach (RoadLane import in lstImport)
                {
                    import.CreatedOn = CurrentDate;
                    import.CreatedBy = loginUser.SystemUserId;
                    import.ModifiedBy = null;
                    import.ModifiedOn = null;
                }

                conn.beginTrans();

                _ImportHandleBefore(conn, loginUser, lstImport);

                new RoadLaneRepository().ImportExcel(conn, loginUser, lstImport);

                _ImportHandleAfter(conn, loginUser, lstImport);

                conn.commitTrans();

                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                if (conn != null) conn.rollbackTrans();
                ViewBag.ErrorMessage = ex.ToString();

                return ResponseJsonError(ex.ToString());
            }
        }
    }
}
