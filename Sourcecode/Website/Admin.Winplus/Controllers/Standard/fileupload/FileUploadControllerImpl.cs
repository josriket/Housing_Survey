using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.fileupload.model;
using Library.Winplus.syslib.standard.fileupload.search;
using Library.Winplus.syslib.system.common.model;
using System.Data;
using Library.Winplus.syslib.standard.fileupload.repository;

namespace Library.Winplus.syslib.standard.fileupload.controller
{
    public partial class FileUploadController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, FileUploadSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, FileUploadSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, FileUploadSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, FileUpload model)
        {

        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, FileUpload model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, FileUpload model)
        {

        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, FileUpload model)
        {

        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<FileUpload> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<FileUpload> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, FileUploadSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, FileUploadSearch model, DataSet ds)
        {

        }

        public ActionResult Upload(FileUpload model)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                if (model.UploadFile != null && model.UploadFile.Length > 0)
                {
                    model.ActiveStatusId = 1;
                    model.FileUploadName = model.UploadFileFileName;
                    model.CreatedBy = LoginUser.DefaultUser.SystemUserId;

                    model.FileUploadId = new FileUploadRepository().Create(conn, model);
                    model.UploadFile = null;

                }
                return Json(model);
            }
        }

        public ActionResult Download(Int64? id)
        {
            if (id.HasValue)
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    FileUpload fileUpload = new FileUploadRepository().FindByFileUploadId(conn, id.Value);
                    return File(fileUpload.UploadFile, fileUpload.UploadFileContentType,fileUpload.UploadFileFileName);
                }
            }
            else
            {
                return null;
            }
        }

    }
}
