using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.ownership.model;
using Library.Winplus.syslib.standard.ownership.repository;
using Library.Winplus.syslib.standard.ownership.search;
using Library.Winplus.syslib.standard.mvcsetting.service;
using Library.Winplus.syslib.system.common.model;
using System.Data;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.standard.fileupload.repository;

namespace Library.Winplus.syslib.standard.ownership.controller
{
    public partial class OwnershipController : SimpleController<Ownership, OwnershipSearch>
    {
        public override ActionResult _Index(DBConnection conn, LoginUser loginUser, OwnershipSearch model)
        {
            model.loginUser = loginUser;

            _IndexHandleBefore(conn, loginUser, model);

            model.PrepareSql(Request, loginUser);

            return View("~/Views/standard/ownership/" + MVCSettingService.CustomView("Ownership", "search") + ".cshtml", model);
        }

        public override ActionResult _Search(DBConnection conn, LoginUser loginUser, OwnershipSearch model)
        {
            model.loginUser = loginUser;

            _SearchHandleBefore(conn, loginUser, model);

            model.PrepareSql(Request, loginUser);

            SearchResultModel result = new OwnershipRepository().AdvanceSearch(conn, model);

            _SearchHandleAfter(conn, loginUser, model, result); 

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }

        public override ActionResult _Find(DBConnection conn, LoginUser loginUser, Int64? id)
        {
            Ownership model = new Ownership();

            _FindHandleBefore(conn, loginUser, id);

            if (id.HasValue && id.Value != 0)
            {
                model = new OwnershipRepository().FindById(conn, id.Value);
            }
            model.loginUser = loginUser;

            _FindHandleAfter(conn, loginUser, id, model);

            return View("~/Views/standard/ownership/" + MVCSettingService.CustomView("Ownership", "addedit") + ".cshtml", model);
        }

        public override ActionResult _Copy(DBConnection conn, LoginUser loginUser, Int64? id)
        {
            Ownership model = new Ownership();
            _CopyHandleBefore(conn, loginUser, id);

            if (id.HasValue && id.Value != 0)
            {
                model = new OwnershipRepository().FindById(conn, id.Value);
            }

            model.OwnershipId = null;
            model.CreatedBy = null;
            model.CreatedOn = null;
            model.ModifiedBy = null;
            model.ModifiedOn = null;
            model.loginUser = loginUser;

            _CopyHandleAfter(conn, loginUser, id, model);

            return View("~/Views/standard/ownership/" + MVCSettingService.CustomView("Ownership", "addedit") + ".cshtml", model);
        }

        public override ActionResult _Save(DBConnection conn, LoginUser loginUser, Ownership model)
        {
            try
            {
                model.loginUser = loginUser;
                conn.beginTrans();


                _SaveHandleBefore(conn, loginUser, model);

                if (model.OwnershipId.HasValue && model.OwnershipId.Value != 0)
                {
                    // Update
                    model.ModifiedOn = DateTimeUtil.CurrentDate();
                    model.ModifiedBy = loginUser.SystemUserId;
                    new OwnershipRepository().Update(conn, model);
                }
                else
                {
                    // Create
                    model.CreatedOn = DateTimeUtil.CurrentDate();
                    model.CreatedBy = loginUser.SystemUserId;
                    model.OwnershipId = new OwnershipRepository().Create(conn, model);
                }

                _SaveHandleAfter(conn, loginUser, model);

                conn.commitTrans();
                return ResponseJsonSuccess(null, model);
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _Active(DBConnection conn, LoginUser loginUser,Int64 activeStatusId, string ids)
        {
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    conn.beginTrans();

                    _ActiveHandleBefore(conn, loginUser, activeStatusId, ids);

                    new OwnershipRepository().Active(conn, activeStatusId ,ids.Split(','));

                    _ActiveHandleAfter(conn, loginUser, activeStatusId, ids);

                    conn.commitTrans();
                }
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _Delete(DBConnection conn, LoginUser loginUser, string ids)
        {
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    conn.beginTrans();

                    _DeleteHandleBefore(conn, loginUser, ids);

                    new OwnershipRepository().Delete(conn, ids.Split(','));

                    _DeleteHandleAfter(conn, loginUser, ids);

                    conn.commitTrans();
                }
                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                conn.rollbackTrans();
                throw ex;
            }
        }

        public override ActionResult _ExportExcel(DBConnection conn, LoginUser loginUser, OwnershipSearch model)
        {
            _ExportExcelHandleBefore(conn, loginUser, model);

            DataSet ds = new OwnershipRepository().ExportExcel(conn, model);

            _ExportExcelHandleAfter(conn, loginUser, model, ds);

            ExcelOutputUtil exporter = new ExcelOutputUtil(rm.GetString("Ownership"), Server.MapPath("~/Resource/XLSTemplate.xlsx"));
            exporter.ToWorkBook(ds.Tables[0]);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            exporter.ResponseMemory(stream);

            return File(stream.ToArray(), "application/ms-excel", "Ownership_" + DateTimeUtil.CurrentDate().Value.ToString("yyyyMMddHHmmss") + ".xlsx");
        }

        public override ActionResult _Import(DBConnection conn, LoginUser loginUser, List<Ownership> lstImport)
        {
            try
            {
                DateTime? CurrentDate = DateTimeUtil.CurrentDate();
                foreach (Ownership import in lstImport)
                {
                    import.CreatedOn = CurrentDate;
                    import.CreatedBy = loginUser.SystemUserId;
                    import.ModifiedBy = null;
                    import.ModifiedOn = null;
                }

                conn.beginTrans();

                _ImportHandleBefore(conn, loginUser, lstImport);

                new OwnershipRepository().ImportExcel(conn, loginUser, lstImport);

                _ImportHandleAfter(conn, loginUser, lstImport);

                conn.commitTrans();

                return ResponseJsonSuccess();
            }
            catch (Exception ex)
            {
                if (conn != null) conn.rollbackTrans();
                ViewBag.ErrorMessage = ex.ToString();

                return ResponseJsonError(ex.ToString());
            }
        }
    }
}
