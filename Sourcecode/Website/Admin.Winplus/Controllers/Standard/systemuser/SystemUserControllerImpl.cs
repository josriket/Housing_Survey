using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.standard.systemuser.search;
using Library.Winplus.syslib.system.common.model;
using Library.Winplus.syslib.standard.systemuser.repository;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.core.util;
using System.Data;

namespace Library.Winplus.syslib.standard.systemuser.controller
{
    public partial class SystemUserController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, SystemUserSearch model)
        {
        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, SystemUserSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, SystemUserSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, SystemUser model)
        {

        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, SystemUser model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, SystemUser model)
        {
            model.SystemUserName = model.UserName;

            if (string.IsNullOrEmpty(model.Password))
            {
                if (model.SystemUserId.HasValue)
                {
                    SystemUser oldUser = new SystemUserRepository().FindById(conn, model.SystemUserId.Value);
                    model.Password = oldUser.Password;
                }
                else
                {
                    throw new InvalidDataException("Please enter password!");
                }
            }
            else
            {
                int? MinimumOfPasswordLength = SystemConfigService.GetIntConfig("MinimumOfPasswordLength",4);
                if (model.Password.Length < MinimumOfPasswordLength)
                {
                    throw new InvalidDataException(MyResources.Winplus.Resource.PleaseEnterPasswordMore8Digit);
                }
                model.Password = EncriptionUtil.EncrypOneWay(model.UserName.ToUpper() + "_" + model.Password);
            }
        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, SystemUser model)
        {
        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<SystemUser> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<SystemUser> lstImport)
        {

        }

        public ActionResult InitialUpdateProfile()
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    LoginUser loginUser = GetLogin();

                    SystemUser model = new SystemUserRepository().FindById(conn, loginUser.SystemUserId.Value);

                    return View("~/Views/customize/homepage/_include/_changepassword_form.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorMessage = ex.ToString();
            }
            return null;
        }

        public ActionResult UpdateProfile(SystemUser model)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null)
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        new SystemUserRepository().UpdateProfile(conn, loginUser, model);

                        return ResponseJsonSuccess();
                    }
                }
                else
                {
                    return ResponseJsonNotAuthorize();
                }
            }
            catch (Exception ex)
            {
                return ResponseJsonError(ex.ToString());
            }
        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, SystemUserSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, SystemUserSearch model, DataSet ds)
        {

        }
    }
}