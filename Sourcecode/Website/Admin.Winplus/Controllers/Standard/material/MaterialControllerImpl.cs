using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.material.model;
using Library.Winplus.syslib.standard.material.search;
using Library.Winplus.syslib.system.common.model;
using System.Data;

namespace Library.Winplus.syslib.standard.material.controller
{
    public partial class MaterialController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, MaterialSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, MaterialSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, MaterialSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, Material model)
        {

        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, Material model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, Material model)
        {

        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, Material model)
        {

        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<Material> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<Material> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, MaterialSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, MaterialSearch model, DataSet ds)
        {

        }
    }
}
