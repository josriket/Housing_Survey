using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemrole.model;
using Library.Winplus.syslib.standard.systemrole.search;
using Library.Winplus.syslib.system.common.model;
using System.Data;

namespace Library.Winplus.syslib.standard.systemrole.controller
{
    public partial class SystemRoleController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, SystemRoleSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, SystemRole model)
        {

        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, SystemRole model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, SystemRole model)
        {

        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, SystemRole model)
        {

        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<SystemRole> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<SystemRole> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, SystemRoleSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, SystemRoleSearch model, DataSet ds)
        {

        }
    }
}
