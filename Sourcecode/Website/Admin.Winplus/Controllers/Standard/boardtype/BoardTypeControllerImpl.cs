using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.boardtype.model;
using Library.Winplus.syslib.standard.boardtype.search;
using Library.Winplus.syslib.system.common.model;
using System.Data;

namespace Library.Winplus.syslib.standard.boardtype.controller
{
    public partial class BoardTypeController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, BoardTypeSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, BoardTypeSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, BoardTypeSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, BoardType model)
        {

        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, BoardType model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, BoardType model)
        {

        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, BoardType model)
        {

        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<BoardType> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<BoardType> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, BoardTypeSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, BoardTypeSearch model, DataSet ds)
        {

        }
    }
}
