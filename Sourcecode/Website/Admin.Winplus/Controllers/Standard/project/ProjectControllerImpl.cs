using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.project.search;
using Library.Winplus.syslib.system.common.model;
using System.Data;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.taskschedule.service;
using Library.Winplus.syslib.standard.survey.repository;
using Library.Winplus.syslib.standard.project.repository;

namespace Library.Winplus.syslib.standard.project.controller
{
    public partial class ProjectController
    {
        [NonAction]
        public void _IndexHandleBefore(DBConnection conn, LoginUser loginUser, ProjectSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleBefore(DBConnection conn, LoginUser loginUser, ProjectSearch model)
        {

        }

        [NonAction]
        public void _SearchHandleAfter(DBConnection conn, LoginUser loginUser, ProjectSearch model, SearchResultModel result)
        {

        }

        [NonAction]
        public void _FindHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _FindHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, Project model)
        {
            if (model.ProjectId.HasValue)
            {
                new SurveyRepository().SetTotalSurvey(conn, loginUser, model);
            }
        }

        [NonAction]
        public void _CopyHandleBefore(DBConnection conn, LoginUser loginUser, Int64? id)
        {

        }

        [NonAction]
        public void _CopyHandleAfter(DBConnection conn, LoginUser loginUser, Int64? id, Project model)
        {

        }

        [NonAction]
        public void _SaveHandleBefore(DBConnection conn, LoginUser loginUser, Project model)
        {

        }

        [NonAction]
        public void _SaveHandleAfter(DBConnection conn, LoginUser loginUser, Project model)
        {
            if(model.UploadFile!=null && model.UploadFile.Length>1)
            {
                string resourcePath = SystemConfigService.GetStringConfig("ResourcePath", @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Resource");
                string joeyPath = @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Resource";
                if (System.IO.Directory.Exists(joeyPath)) resourcePath = joeyPath;
                string fileName = $@"{resourcePath}\csv\import\ProjectSurvey_{model.ProjectId}_{loginUser.SystemUserId}_{StringUtil.NewGuid()}.csv";

                FileUtil.WriteFile(fileName, model.UploadFile);
                new ImportProjectSurveyService(model, fileName).PerformTask();
            }
            else
            {
                new ImportProjectSurveyService(model, null).CreateIfNotExists(conn,model.ProjectId.Value);
            }
        }

        [NonAction]
        public void _ActiveHandleBefore(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _ActiveHandleAfter(DBConnection conn, LoginUser loginUser, Int64 activeStatusId, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleBefore(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _DeleteHandleAfter(DBConnection conn, LoginUser loginUser, string ids)
        {

        }

        [NonAction]
        public void _ImportHandleBefore(DBConnection conn, LoginUser loginUser, List<Project> lstImport)
        {

        }

        [NonAction]
        public void _ImportHandleAfter(DBConnection conn, LoginUser loginUser, List<Project> lstImport)
        {

        }

        [NonAction]
        public void _ExportExcelHandleBefore(DBConnection conn, LoginUser loginUser, ProjectSearch model)
        {

        }

        [NonAction]
        public void _ExportExcelHandleAfter(DBConnection conn, LoginUser loginUser, ProjectSearch model, DataSet ds)
        {

        }
    }
}
