﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Library.Winplus.syslib.customize.taskschedule.service;
using Library.Winplus.syslib.customize.taskschedule.service.interf;
using Library.Winplus.syslib.standard.systemuser.repository;
using Library.Winplus.syslib.customize.helper.service;
using Library.Winplus.syslib.core.logger;

namespace Admin.Winplus
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private List<BasicTask> LstTask = new List<BasicTask>();
        protected void Application_Start()
        {
            //LoggerService.LogDebug("MvcApplication", "Application_Start", "Starting...");
            CriticalLogger.WriteLog("MvcApplication", "Application_Start..");

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            try
            {
                new HelpInitialActiveStatus().Initial();
                new HelpInitialSystemAdmin().Initial();
                new HelpInitialSystemMenu().Initial();
                new HelpInitialMasterData().Initial();
               
                // Task-Schedule
                LstTask.Add(new ClearTempService());
                foreach (BasicTask task in LstTask)
                {
                    task.StartThread();
                }

                new SystemUserRepository().EncrypAll();
            }
            catch (Exception ex)
            {
                CriticalLogger.WriteLog("MvcApplication", "Application_Error.."+ex.ToString());
                //LoggerService.LogError("MvcApplication", "Application_Start", ex.ToString());
                throw ex;
            }
        }

        protected void Application_End()
        {
            foreach (BasicTask task in LstTask)
            {
                task.StopThread();
            }
        }
    }
}