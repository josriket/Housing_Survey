﻿using ImageDownload.Winplus.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImageDownload.Winplus
{
    public class DownloadThread
    {
        public static bool Deleted = false;
        public NetworkCredential Credential { get; set; }
        public long ProjectId { get; set; }

        List<DownloadInfo> lstDownload = new List<DownloadInfo>();

        public DownloadThread(NetworkCredential credential, long projectId)
        {
            this.Credential = credential;
            this.ProjectId = projectId;
        }

        public void Add(string fileName, string ftpFile)
        {
            lstDownload.Add(new DownloadInfo() { FileName=fileName,FtpFile=ftpFile});
        }

        Thread thr = null;
        public void Start()
        {
            thr = new Thread(DownloadStart);
            thr.Start();
        }

        private void DownloadStart()
        {
            foreach(DownloadInfo d in lstDownload)
            {
                DownloadFile(Credential, ProjectId, d.FileName, d.FtpFile);
            }
        }

        private static object logger = new object();
        private void DownloadFile(NetworkCredential credential, long projectId, string fileName, string ftpFile)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpFile);
            request.Credentials = credential;
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (Stream ftpStream = request.GetResponse().GetResponseStream())
            {
                using (Stream fileStream = File.Create($"Image\\{projectId}\\{fileName}"))
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileStream.Write(buffer, 0, read);
                    }
                }
            }
            lock (logger)
            {
                File.AppendAllText($"DownloadedLog_{projectId}.txt", $"{fileName}{Environment.NewLine}");
            }
            Console.WriteLine($"#Downloaded->{fileName}");

            if (Deleted)
            {
                DeleteFile(credential, projectId, fileName, ftpFile);
            }
        }

        private string DeleteFile(NetworkCredential credential, long projectId, string fileName, string ftpFile)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpFile);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = credential;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                Console.WriteLine($"#Deleted->{fileName}");
                return response.StatusDescription;
            }
        }

        public void Join()
        {
            thr.Join();
        }
    }
}
