﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageDownload.Winplus.Lib
{
    public class DownloadInfo
    {
        public string FileName { get; set; }
        public string FtpFile { get; set; }
    }
}
