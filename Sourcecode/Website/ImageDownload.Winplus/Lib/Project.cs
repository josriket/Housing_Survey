﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageDownload.Winplus.Lib
{
    public class Project
    {
        public string ProjectName { get; set; }
        public long ProjectId { get; set; }
        public int Index { get; set; }
    }
}
