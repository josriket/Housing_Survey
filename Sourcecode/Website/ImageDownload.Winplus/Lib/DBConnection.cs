﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace ImageDownload.Winplus.Lib
{
    public class DBConnection : IDisposable
    {
        public static string DBType { get; set; }
        public SqlConnection conn;
        public SqlTransaction tran;

        public static DBConnection CustomConnection(string connections)
        {
            return new DBConnection(connections);
        }

        // New connection for Choose Database Connection "CRM","GW"
        // Example : DBConnection conn = new DBConnection(DBConnection.CRM);
        private DBConnection(string connectionstring)
        {
            openCon(connectionstring);
        }


        private void openCon(string connectionstring)
        {
            conn = new SqlConnection(connectionstring);
            conn.Open();
        }
        
        public DataSet executeDataSet(string query, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        public object executeScalar(string query, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            return cmd.ExecuteScalar();
        }

        public object executeScalar(string query, List<DBParameter> list)
        {
            return executeScalar(query, list.ToArray());
        }

        public SqlDataReader executeDataReader(string query, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            return cmd.ExecuteReader();
        }

    
        public int executeUpdate(string query, params DBParameter[] param)
        {
            Int64 generateKey = 0;
            return executeUpdate(query, ref generateKey, param);
        }

        public int executeUpdate(string query, ref Int64 generateKey, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            // SQL SERVER
            cmd.CommandText = "SELECT @@IDENTITY";
            Object pkId = cmd.ExecuteScalar();
            if (pkId != DBNull.Value)
            {
                generateKey = Convert.ToInt64(pkId);
            }

            // Sql SQL
            //generateKey =cmd.LastInsertedId;

            return result;

        }

        public void beginTrans()
        {
            if (conn == null) throw new Exception("Can't begin tranaction, Please initial connection before use.");
            tran = conn.BeginTransaction();
        }

        public void commitTrans()
        {
            if (conn == null && tran == null) throw new Exception("Can't commit tranaction, Please begin transaction before use.");
            if (tran != null)
            {
                tran.Commit();
                tran = null;
            }
        }

        public void rollbackTrans()
        {
            if (tran != null)
            {
                tran.Rollback();
                tran = null;
            }
        }

        public void closeCon()
        {
            if (tran != null)
            {
                rollbackTrans();
            }
            if (conn != null) conn.Close();
            conn = null;
        }

        public static void Close(DBConnection conn)
        {
            if (conn != null) conn.closeCon();
        }

        public void Dispose()
        {
            DBConnection.Close(this);
        }

        public bool IsExistsData(string sql, params DBParameter[] param)
        {
            SqlCommand command = new SqlCommand(FormatSql(sql), conn);
            if (tran != null)
            {
                command.Transaction = tran;
            }

            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    command.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return Convert.ToInt32(command.ExecuteScalar()) > 0;
        }


        private string FormatSql(string sql)
        {
            if (DBType == "Sql")
            {
                // Sql
                return Regex.Replace(sql.Replace("[", "`").Replace("]", "`"), "ISNULL", "COALESCE", RegexOptions.IgnoreCase);
            }
            else
            {
                // SQLSERVER
                return sql;
            }

        }
    }
}
