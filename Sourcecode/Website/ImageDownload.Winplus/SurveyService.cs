﻿using ImageDownload.Winplus;
using ImageDownload.Winplus.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Reflection;

namespace Library.Winplus.syslib.standard.survey.service
{
    public class SurveyService
    {
        public void Download()
        {
            Console.WriteLine("โปรดเลือกโครงการ");
            List<Project> lstProject = new List<Project>();
            using (DBConnection conn = DBConnection.CustomConnection("Data Source=zxyur.com;Initial Catalog=HousingSurveyDB;User ID=HousingSurveyDBAdmin;Password=7%t2Bk8f"))
            {
                DataSet ds = conn.executeDataSet("Select ProjectId,ProjectName From Project Order By ProjectId");
                int i = 0;
                foreach(DataRow row in ds.Tables[0].Rows)
                {
                    lstProject.Add(new Project() {
                        ProjectName = row["ProjectName"].ToString(),
                        ProjectId = (Int64)row["ProjectId"],
                        Index = i++
                    });
                }

                foreach(Project project in lstProject)
                {
                    Console.WriteLine($" {project.Index + 1}. {project.ProjectName} [{project.ProjectId}]");
                }
            }
            Console.Write(">");
            Int32 selectIndex = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"คุณต้องการลบไฟล์ต้นฉบับใช่หรือไม่ [Y,N]?");
            string confirmDeleted = Console.ReadLine().ToUpper();

            Console.WriteLine($@"คุณต้องการดาวน์โหลดรูปภาพโครงการ ""{lstProject[selectIndex-1].ProjectName}""{((confirmDeleted=="Y")?"(ลบรูปภาพ)": "(ไม่ลบรูปภาพ)")} ใช่หรือไม่ [Y,N]");

            Console.Write(">");
            string confirmFlag = Console.ReadLine();

            if (confirmFlag.ToUpper() == "Y")
            {
                DownloadThread.Deleted = (confirmDeleted == "Y");
                BeginDownload(lstProject[selectIndex - 1].ProjectId);
            }
            Console.WriteLine($"Download completed, Please enter to exit..");
            Console.ReadLine();
        }

        private void BeginDownload(Int64 ProjectId)
        {
            if (!Directory.Exists("Image")) Directory.CreateDirectory("Image");
            if (!Directory.Exists($"Image\\{ProjectId}")) Directory.CreateDirectory($"Image\\{ProjectId}");

            HashSet<string> Downloaded = GetDownloaded(ProjectId);
            NetworkCredential credential = new NetworkCredential("josriket@thaimaptech.com", "P@ssw0rd");

            string ftpRoot = $@"ftp://thaimaptech.com/public_html/fileserver/image/{ProjectId}";
            string[] files = ListFiles(credential, ftpRoot);
            if (files.Length > 0)
            {
                int totalThread = 25;
                List<DownloadThread> lstDownload = new List<DownloadThread>();
                for(int i = 0; i < totalThread; i++)
                {
                    lstDownload.Add(new DownloadThread(credential, ProjectId));
                }
                int index = 0;
                foreach (string file in files)
                {
                    if (file.Length > 4 && !Downloaded.Contains(file))
                    {
                        lstDownload[index % totalThread].Add(file, $"{ftpRoot}/{file}");
                    }
                    index++;
                }
                Downloaded.Clear();

                for (int i = 0; i < totalThread; i++)
                {
                    lstDownload[i].Start();
                }
                for (int i = 0; i < totalThread; i++)
                {
                    lstDownload[i].Join();
                }
            }
            else
            {
                Console.WriteLine($"Image not found in directory");
            }
        }

        private HashSet<string> GetDownloaded(long projectId)
        {
            HashSet<string> hash = new HashSet<string>();

            string file = $"DownloadedLog_{projectId}.txt";
            if (File.Exists(file))
            {
                string[] lines = File.ReadAllLines(file);
                foreach (string line in lines)
                {
                    if (!string.IsNullOrEmpty(line) && !hash.Contains(line))
                    {
                        hash.Add(line);
                    }
                }
            }
            return hash;
        }

        private string[] ListFiles(NetworkCredential credential,string ftpRoot)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpRoot);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = credential;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                return names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
