﻿using Library.Winplus.syslib.standard.survey.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageDownload.Winplus
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Console.WriteLine($"Survey image download start...");
                new SurveyService().Download();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
