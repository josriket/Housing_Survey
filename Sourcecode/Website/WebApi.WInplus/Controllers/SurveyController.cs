﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.fileupload.repository;
using Library.Winplus.syslib.standard.survey.model;
using Library.Winplus.syslib.standard.survey.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.WInplus.Controllers
{
    public class SurveyController : BaseApiController
    {
        [HttpPost]
        [Route("api/Survey/Search")]
        public IHttpActionResult Search([FromBody]SurveyCriteria model)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                PrepareLog(this, "Search", loginUser, model);

                List<SurveyResult> lstResult = null;
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    model.MaxDistance = SystemConfigService.GetIntConfig("MaxDistance", 2000);
                    lstResult = new SurveyRepository().MobileSearch(conn, loginUser, model);
                }

                return Ok(lstResult);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            finally
            {
                SaveLog();
            }
        }

        [Route("api/Survey/{ProjectId}/{id}")]
        public IHttpActionResult Get(Int64? ProjectId, Int64? id)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                PrepareLog(this, "Get", loginUser, id);

                Survey model = null;
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    model = new SurveyRepository().FindBySurveyId(conn, ProjectId.Value, id.Value);

                    model.ImageBesideFullPath = model.ImageBeside;
                    model.ImageFrontFullPath = model.ImageFront;
                    model.ImageReferFullPath = model.ImageRefer;
                }
                return Ok(model);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            finally
            {
                SaveLog();
            }
        }

        public static Object Locker = new Object();
        [HttpPost]
        [Route("api/Survey/Save")]
        public IHttpActionResult Save([FromBody]Survey model)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                PrepareLog(this, "Save", loginUser, model);

                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    model.UpdateFlag = true;
                    model.OwnerSystemUserId = loginUser.SystemUserId;
                    model.KeyInOn = DateTime.Now;
                    model.ModifiedOn = DateTime.Now;
                    model.ModifiedBy = loginUser.SystemUserId;

                    //string ImageUploadFolder = SystemConfigService.GetStringConfig("ImageUploadFolder", @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\WebApi.WInplus\ImageUpload");
                    //model.ImageBeside = new FileUploadRepository().TransferFile(model, "Beside", model.ImageBesideBase64);
                    //model.ImageFront = new FileUploadRepository().TransferFile(model, "Front", model.ImageFrontBase64);
                   // model.ImageRefer = new FileUploadRepository().TransferFile(model, "Refer", model.ImageReferBase64);

                    model.ImageBesideBase64 = null;
                    model.ImageFrontBase64 = null;
                    model.ImageReferBase64 = null;

                    SurveyRepository repo = new SurveyRepository();
                    repo.ModifyScript(model.ProjectId.Value);

                    if (model.SurveyId.HasValue)
                    {
                        repo.SqlUpdate = @"Update [Survey] Set [SurveyName]=@SurveyName
                                            , [SurveyCode]=@SurveyCode
                                            , [ProjectId]=@ProjectId
                                            , [Latitude]=@Latitude
                                            , [Longitude]=@Longitude
                                            , [BuildingName]=@BuildingName
                                            , [BuildingTypeId]=@BuildingTypeId
                                            , [BuildingTypeOther]=@BuildingTypeOther
                                            , [FloorNoId]=@FloorNoId
                                            , [FloorNoOther]=@FloorNoOther
                                            , [MaterialId]=@MaterialId
                                            , [MaterialOther]=@MaterialOther
                                            , [Width]=@Width, [Long]=@Long
                                            , [Height]=@Height
                                            , [FarFromRefer]=@FarFromRefer
                                            , [UnitNo]=@UnitNo
                                            , [OwnershipId]=@OwnershipId
                                            , [UpdateFlag]=@UpdateFlag
                                            , [DeleteFlag]=@DeleteFlag
                                            , [OwnerSystemUserId]=@OwnerSystemUserId
                                            , [KeyInOn]=@KeyInOn
                                            , [ModifiedOn]=getdate()
                                            , [ModifiedBy]=@ModifiedBy
                                            , [AdvantageId]=@AdvantageId
                                            , [Advantage]=@Advantage
                                            , [Remark]=@Remark 
                                            , [LocationTypeId]=@LocationTypeId
                                            , [PlaceOwner]=@PlaceOwner
                                            , [PlaceUserBenefic]=@PlaceUserBenefic
                                            , [HomeNo]=@HomeNo
                                            , [BoardTypeId]=@BoardTypeId
                                            , [BoardLocationId]=@BoardLocationId
                                            , [BoardStyleId]=@BoardStyleId
                                            , [BoardSide]=@BoardSide
                                            , [BoardText]=@BoardText
                                            , [RoadTypeId]=@RoadTypeId
                                            , [RoadMaterialId]=@RoadMaterialId
                                            , [BridgeTypeId]=@BridgeTypeId
                                            , [BridgeMaterialId]=@BridgeMaterialId
                                            , [PoleTypeId]=@PoleTypeId
                                            , [PoleMaterialId]=@PoleMaterialId
                                        Where [SurveyId]=@SurveyId";
                        // Update
                        repo.Update(conn, model);
                    }
                    else
                    {
                        lock (Locker)
                        {
                            model.CreatedOn = DateTime.Now;
                            model.CreatedBy = loginUser.SystemUserId;
                            model.NewPoint = true;
                            model.SurveyCode = repo.NewSurveyCode(conn, model.ProjectId.Value);
                            model.SurveyName = null;
                            // Insert
                            model.SurveyId = repo.Create(conn, model);
                        }
                    }
                }

                return Ok(model);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            finally
            {
                SaveLog();
            }
        }

        [HttpPost]
        [Route("api/Survey/ImageUpload")]
        public IHttpActionResult ImageUpload([FromBody]SurveyImageUpload model)
        {
            string dirId = "1iDAa0_2xH3TpdqDMclcGUszbwt3SxHlf";
            try
            {
                LoginUser loginUser = GetLogin();
                PrepareLog(this, "ImageUpload", loginUser, model);
                SurveyRepository repo = new SurveyRepository();
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    Survey survey = repo.FindBySurveyId(conn, model.ProjectId.Value, model.SurveyId.Value);
                    //Beside,Front,Refer

                    string fileName = $"{model.ProjectId.Value.ToString("000")}_{Convert.ToInt64(survey.SurveyCode).ToString("000000")}_{model.ImageType}.jpg";
                    

                    string GDriveCredentials = SystemConfigService.GetStringConfig("GDriveCredentials", @"D:\Workspace\P_Joe\housing_survey\Sourcecode\Website\Test\bin\Debug\credentials.json");
                    string GDriveTokenJson = SystemConfigService.GetStringConfig("GDriveTokenJson", @"D:\Workspace\P_Joe\housing_survey\Sourcecode\Website\Test\bin\Debug\token.json");
                    //GDriveCredentials = @"C:\Program Files (x86)\IIS Express\client_secret_880659806377-fdfce40so4uuncgnl9b9ued2pbc8151j.apps.googleusercontent.com.json";
                    //GDriveTokenJson = @"C:\Program Files (x86)\IIS Express\token2.json";
                    GoogleDriveUtil gDrive = new GoogleDriveUtil(GDriveCredentials, GDriveTokenJson);
                    model.ImageUploadName = gDrive.UploadFile(Convert.FromBase64String(model.ImageBase64), fileName, "image/jpg", dirId);


                    //string ImageUploadFolder = SystemConfigService.GetStringConfig("ImageUploadFolder", @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\WebApi.WInplus\ImageUpload");
                    //model.ImageUploadName = new FileUploadRepository().SaveLocalFile(ImageUploadFolder, survey, model.ImageType, model.ImageBase64);
                    
                    //model.ImageUploadName = new FileUploadRepository().TransferFile(survey, model.ImageType, model.ImageBase64);
                    
                    model.ImageBase64 = null;
                    repo.UpdateImage(conn, model);
                }

                return Ok(model);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex, $"dir:{dirId} -> {ex}");
            }
            finally
            {
                logger.Request = null;
                SaveLog();
            }
        }
    }
}
