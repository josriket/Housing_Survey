﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.standard.systemuser.repository;
using Library.Winplus.syslib.standard.userlogin.model;
using Library.Winplus.syslib.standard.userlogin.repository;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace WebApi.WInplus.Controllers
{
    public class LoginController : BaseApiController
    {
        public IHttpActionResult Post([FromBody]LoginRequest model)
        {
            try
            {
                PrepareLog(this, "Post", null, model);
                IHttpActionResult result = null;
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    SystemUserRepository repo = new SystemUserRepository();

                    SystemUser user = repo.FindByUserName(conn, model.UserName);
                    if (user==null)
                    {
                        result = NotFound();
                    }
                    else
                    {
                        bool passwordCorrect = false;
                        if (!string.IsNullOrEmpty(model.Password))
                        {
                            string encPass = EncriptionUtil.EncrypOneWay(model.UserName.ToUpper() + "_" + model.Password);
                            if (user.Password == encPass)
                            {
                                passwordCorrect = true;
                            }
                        }
                        else
                        {
                            passwordCorrect = true;
                        }

                        if (passwordCorrect)
                        {
                            //Prepare variable
                            int? SessionTimeout = SystemConfigService.GetIntConfig(SystemConfigKey.SessionTimeout, 60*24*365);
                            UserLoginRepository userLoginRepo = new UserLoginRepository();

                            // Temporary LoginUser for field (CreateBy,ModifyBy)
                            LoginUser loginUser = new LoginUser();
                            loginUser.SystemUserId = user.SystemUserId;

                            // Create login user (Prepare insert)
                            UserLogin userLogin = new UserLogin();
                            userLogin.UserLoginName = user.SystemUserName;
                            userLogin.SystemUserId = user.SystemUserId;
                            userLogin.TokenId = StringUtil.NewGuid();
                            userLogin.ActionDate = DateTime.Now;
                            userLogin.ExpireDate = DateTime.Now.AddMinutes(SessionTimeout.Value);
                            userLogin.CreatedOn = DateTime.Now;
                            userLogin.CreatedBy = user.SystemUserId;

                            // Kill another user in this machine
                            //userLoginRepo.DeleteByTokenId(conn, userLogin.TokenId);

                            // Kill this user in anoter session
                            //userLoginRepo.DeleteBySystemUserId(conn, user.SystemUserId.Value);

                            // Insert UserLogin
                            userLoginRepo.Create(conn, userLogin);
                            user.Password = null;
                            user.token = userLogin.TokenId;

                            result = Ok(user);
                        }
                        else
                        {
                            result = NotFound();
                        }
                    }
                }
                return result;
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
            finally
            {
                SaveLog();
            }
        }
    }
}
