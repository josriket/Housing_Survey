﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.logger;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.standard.logger.model;
using Library.Winplus.syslib.standard.logger.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace WebApi.WInplus.Controllers
{
    public class BaseApiController : ApiController
    {
        protected Logger logger = new Logger();

        protected LoginUser GetLogin()
        {
            LoginUser loginUser = null;
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                loginUser = new LoginUserRepository().FindByTokenId(conn, Request.Headers.GetValues("token").FirstOrDefault());
            }
            return loginUser;
        }

        protected override OkNegotiatedContentResult<T> Ok<T>(T content)
        {
            logger.Response = JSONUtil.Serialize(content);
            return base.Ok<T>(content);
        }

        protected void PrepareLog(ApiController controller,string method, LoginUser loginUser, object request)
        {
            if (request != null)
            {
                logger.Request = JSONUtil.Serialize(request);
            }
            logger.RequestUrl = Request.RequestUri.AbsolutePath;
            logger.LogClass = controller.GetType().Name;
            logger.LogMethod = method;
            logger.LogLevel = "Info";
            if (loginUser != null)
            {
                logger.TokenId = loginUser.TokenId;
                logger.UserName = loginUser.User.UserName;
            }
        }

        protected void SaveLog()
        {
            try
            {
                logger.LoggerName = logger.LogClass + "-" + logger.LogMethod;
                logger.CreatedBy = LoginUser.DefaultUser.SystemUserId;

                if (logger.LogLevel == "Error")
                {
                    CriticalLogger.WriteLog("BaseApi", JSONUtil.Serialize(logger));
                }
                else
                {
                    CriticalLogger.WriteLog("BaseApi", JSONUtil.Serialize(logger));
                }
            }
            catch(Exception ex)
            {
                // Need write log to file
            }
        }

        protected override ExceptionResult InternalServerError(Exception exception)
        {
            logger.LogLevel = "Error";
            logger.ErrorMessage = exception.ToString();
            return base.InternalServerError(exception);
        }

        protected ExceptionResult InternalServerError(Exception exception, string remark)
        {
            logger.LogLevel = "Error";
            logger.ErrorMessage = remark;
            return base.InternalServerError(exception);
        }

    }
}
