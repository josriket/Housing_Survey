﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.project.repository;
using Library.Winplus.syslib.standard.userproject.model;
using Library.Winplus.syslib.standard.userproject.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.WInplus.Controllers
{
    public class MasterDataController : BaseApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                LoginUser loginUser = GetLogin();
                PrepareLog(this, "Get", loginUser, null);
                if (loginUser != null)
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        result.Add("BuildingType", QueryUtil.GetSelectedItems(conn, "BuildingType", null, null, null, null));
                        result.Add("FloorNo", QueryUtil.GetSelectedItems(conn, "FloorNo", null, null, null, null));
                        result.Add("Material", QueryUtil.GetSelectedItems(conn, "Material", null, null, null, null));
                        result.Add("Ownership", QueryUtil.GetSelectedItems(conn, "Ownership", null, null, null, null));
                        result.Add("Advantage", QueryUtil.GetSelectedItems(conn, "Advantage", null, null, null, null));
                        result.Add("LocationType", QueryUtil.GetSelectedItems(conn, "LocationType", null, null, null, null));
                        result.Add("BoardType", QueryUtil.GetSelectedItems(conn, "BoardType", null, null, null, null));
                        result.Add("BoardLocation", QueryUtil.GetSelectedItems(conn, "BoardLocation", null, null, null, null));
                        result.Add("BoardStyle", QueryUtil.GetSelectedItems(conn, "BoardStyle", null, null, null, null));
                        result.Add("RoadType", QueryUtil.GetSelectedItems(conn, "RoadType", null, null, null, null));
                        result.Add("RoadMaterial", QueryUtil.GetSelectedItems(conn, "RoadMaterial", null, null, null, null));
                        result.Add("BridgeType", QueryUtil.GetSelectedItems(conn, "BridgeType", null, null, null, null));
                        result.Add("BridgeMaterial", QueryUtil.GetSelectedItems(conn, "BridgeMaterial", null, null, null, null));
                        result.Add("PoleType", QueryUtil.GetSelectedItems(conn, "PoleType", null, null, null, null));
                        result.Add("PoleMaterial", QueryUtil.GetSelectedItems(conn, "PoleMaterial", null, null, null, null));
                        result.Add("RoadLane", QueryUtil.GetSelectedItems(conn, "RoadLane", null, null, null, null));
                        result.Add("LandParcelType", QueryUtil.GetSelectedItems(conn, "LandParcelType", null, null, null, null));

                        UserProject uProject = new UserProjectRepository().FindBySystemUserId(conn, loginUser.SystemUserId.Value);
                        if (uProject != null)
                        {
                            Project project = new ProjectRepository().FindByProjectId(conn, uProject.ProjectId.Value);
                            result.Add("CurrentProject", project);
                        }
                    }
                    return Ok(result);
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            finally
            {
                SaveLog();
            }
        }

    }
}
