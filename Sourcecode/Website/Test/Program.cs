﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Library.Winplus.syslib.core.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Google.Apis.Drive.v3.FilesResource;

namespace Test
{
    class Program
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/drive-dotnet-quickstart.json
        static string[] Scopes = { DriveService.Scope.Drive };
        static string ApplicationName = "wecollect";

        static void Main(string[] args)
        {
            GoogleDriveUtil gDrive = new GoogleDriveUtil();
            string id = gDrive.GetFolderId("wecollect");
            byte[] file = System.IO.File.ReadAllBytes("D://3-Singha-119104.jpg");
            string sharedLink = gDrive.UploadFile(file, "6-5-2020 3-43-24 PM.png","image/png", "1hSkTzYDuxzqiYw2q9ithDYY7bPA2Fkqg");
            //string dir = gDrive.CreateDirectory("1hSkTzYDuxzqiYw2q9ithDYY7bPA2Fkqg", "Test001");
        }

    }
}
