﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.data.Interf
{
    public abstract class IJsonResponse
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public object JsonData { get; set; }
        public string SelectTab { get; set; }
        public string SelectElement { get; set; }
    }
}
