﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.service.interf;

namespace Library.Winplus.syslib.service
{
    public class CommonService : IService
    {
        public static List<T> SearchByStored<T>(DBConnection conn, string StoredName,T template,Dictionary<string, object> searchKey)
        {
            return QueryUtil.FindList<T>(conn, StoredName, searchKey);
        }

        public static DataSet SearchByStored(DBConnection conn, string StoredName, Dictionary<string, object> searchKey)
        {
            return conn.executeStoredProcedure(StoredName, searchKey);
        }

        public static T FindByStored<T>(DBConnection conn, string StoredName, T template, Dictionary<string, object> searchKey)
        {
            List<T> Lst = QueryUtil.FindList<T>(conn, StoredName, searchKey);
            if (Lst.Count != 1)
            {
                throw new InvalidDataException(MyResources.Winplus.Resource.ErrorNotfoundData);
            }
            return Lst[0];
        }

        public static bool IsDuplicate(DBConnection conn, string TableName, string PkName, Int64? PkValue, string ColumnName, string ColumnValue)
        {
            string sql = null;
            if (PkValue.HasValue)
            {
                sql = string.Format("select * from {0} where {1}<>'{2}' and {3}='{4}'", TableName, PkName, PkValue, ColumnName, ColumnValue);
            }
            else
            {
                sql = string.Format("select * from {0} where {1}='{2}'", TableName, ColumnName, ColumnValue);
            }

            return DataSetUtil.IsNotEmpty(conn.executeDataSet(sql));
        }

    }
}
