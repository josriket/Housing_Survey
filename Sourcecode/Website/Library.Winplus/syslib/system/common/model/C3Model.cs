﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.system.common.model
{
    public class C3Result
    {
        public string bindto { get; set; }
        public C3Data data { get; set; }
        public C3Axis axis { get; set; }

        public C3Result()
        {
            axis = new C3Axis();
            data = new C3Data();
        }

    }

    public class C3PieResult
    {
        public string bindto { get; set; }
        public C3Data data { get; set; }

        public C3PieResult()
        {
            data = new C3Data();
        }
    }

    public class C3GaugeResult
    {
        public string bindto { get; set; }
        public C3GaugeData data { get; set; }

        public C3GaugeResult()
        {
            data = new C3GaugeData();
        }
    }

    /*
        {
            bindto: '#stocked',
            data: {
                columns: [
                    ['data1', 30, 20, 80, 20, 70, 50],
                    ['data2', 50, 30, 10, 40, 15, 25],
                    ['data3', 20, 50, 10, 40, 15, 25]
                ],
                colors: {
                    data1: 'green',
                    data2: 'yellow',
                    data3: 'red'
                },
                type: 'bar',
                groups: [
                    ['data1', 'data2', , 'data3']
                ],
                names: {
                    data1: 'A-B',
                    data2: 'C-D',
                    data3: 'E-I'
                }
            },
            axis: {
                x: {
                    label: 'X Label',
                    type: 'category',
                    categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6']
                },
                y: {
                    label: 'Y Label',
                    max:98
                }
            }
        }
     */
}
