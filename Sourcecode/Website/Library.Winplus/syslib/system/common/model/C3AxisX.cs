﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.system.common.model
{
    public class C3AxisX
    {
        public string label { get; set; }
        public string type { get; set; }
        public List<string> categories { get; set; }

        public C3AxisX()
        {
            categories = new List<string>();
        }
    }

    //label: 'X Label',
    //type: 'category',
    //categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6']
}