﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.system.common.model
{
    public class C3AxisY
    {
        public string label { get; set; }
        public int max { get; set; }
        public int min { get; set; }
    }
        //label: 'Y Label',
        //max:98
}
