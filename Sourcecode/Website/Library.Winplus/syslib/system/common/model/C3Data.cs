﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.system.common.model
{
    public class C3Data
    {
        public List<List<object>> columns { get; set; }
        public Dictionary<string,string> colors { get; set; }
        public string type { get; set; }
        public List<List<string>> groups {get;set;}
        public Dictionary<string, string> names { get; set; }

        public C3Data()
        {
            columns = new List<List<object>>();
            colors = new Dictionary<string, string>();
            groups = new List<List<string>>();
            names = new Dictionary<string, string>();
        }
    }

    public class C3GaugeData
    {
        public List<List<object>> columns { get; set; }
        public Dictionary<string,List<string>> colors { get; set; }
        public string type { get; set; }

        public C3GaugeData()
        {
            columns = new List<List<object>>();
            colors = new Dictionary<string, List<string>>();
        }
    }
    /*
     columns: [
            ['data1', 30, 20, 80, 20, 70, 50],
            ['data2', 50, 30, 10, 40, 15, 25],
            ['data3', 20, 50, 10, 40, 15, 25]
        ],
        colors: {
            data1: 'green',
            data2: 'yellow',
            data3: 'red'
        },
        type: 'bar',
        groups: [
            ['data1', 'data2', , 'data3']
        ],
        names: {
            data1: 'A-B',
            data2: 'C-D',
            data3: 'E-I'
        }
     */
}
