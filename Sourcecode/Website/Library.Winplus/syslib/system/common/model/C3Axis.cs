﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.system.common.model
{
    public class C3Axis
    {
        public C3AxisX x { get; set; }
        public C3AxisY y { get; set; }

        public C3Axis()
        {
            x = new C3AxisX();
            y = new C3AxisY();
        }
    }

    /*
         x: {
            label: 'X Label',
            type: 'category',
            categories: ['cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6']
        },
        y: {
            label: 'Y Label',
            max:98
        }
     */
}
