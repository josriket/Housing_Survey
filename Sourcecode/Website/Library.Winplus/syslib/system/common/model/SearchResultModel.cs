﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Library.Winplus.syslib.system.common.model
{
    [Serializable]
    public class SearchResultModel
    {
        //public List<List<string>> data { get; set; }
        public DataTable data { get; set; }

        public string[] options { get; set; }
        public string[] files { get; set; }
        public int draw { get; set; }
        public string recordsTotal { get; set; }
        public string recordsFiltered { get; set; }

        public object mapData { get; set; }

        public C3Result c3Result { get; set; }
        public C3Result c3Result2 { get; set; }
        public Dictionary<string, C3PieResult> pieResult { get; set; }
        public Dictionary<string, C3Result> lineResult { get; set; }
        public Dictionary<string, C3GaugeResult> gaugeResult { get; set; }

    }
}