﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.data.Interf;

namespace Library.Winplus.syslib.data.Response
{
    public class JsonArrayResponse:IJsonResponse
    {
        public object Items { get; set; }
    }
}
