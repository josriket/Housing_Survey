using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.poletype.model;

namespace Library.Winplus.syslib.standard.poletype.repository
{
    public partial class PoleTypeRepository : SimpleRepository<PoleType>
    {
        #region General Query

        public PoleTypeRepository()
        {
            SqlInsert = @"Insert Into [PoleType]([PoleTypeName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@PoleTypeName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [PoleType] Set [PoleTypeName]=@PoleTypeName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [PoleTypeId]=@PoleTypeId";
            SqlDelete = @"Delete From [PoleType] Where [PoleTypeId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [PoleType] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<PoleType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (PoleType model in lstImport)
                {
                    if (model.PoleTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public PoleType FindByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[PoleTypeId]=@PoleTypeId " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("PoleTypeId", PoleTypeId));
        }

        public List<PoleType> FindsByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[PoleTypeId]=@PoleTypeId " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("PoleTypeId", PoleTypeId));
        }

        public int DeleteByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [PoleTypeId]=@PoleTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleTypeId", PoleTypeId));
        }

        public PoleType FindByPoleTypeName(DBConnection conn, String PoleTypeName, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[PoleTypeName]=@PoleTypeName " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("PoleTypeName", PoleTypeName));
        }

        public List<PoleType> FindsByPoleTypeName(DBConnection conn, String PoleTypeName, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[PoleTypeName]=@PoleTypeName " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("PoleTypeName", PoleTypeName));
        }

        public int DeleteByPoleTypeName(DBConnection conn, String PoleTypeName, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [PoleTypeName]=@PoleTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleTypeName", PoleTypeName));
        }

        public PoleType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<PoleType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public PoleType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<PoleType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public PoleType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<PoleType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public PoleType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<PoleType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public PoleType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 PoleType.* From [PoleType] PoleType Where PoleType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<PoleType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<PoleType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select PoleType.* From [PoleType] PoleType Where PoleType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<PoleType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [PoleType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
