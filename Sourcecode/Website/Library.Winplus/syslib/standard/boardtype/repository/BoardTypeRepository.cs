using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.boardtype.model;

namespace Library.Winplus.syslib.standard.boardtype.repository
{
    public partial class BoardTypeRepository : SimpleRepository<BoardType>
    {
        #region General Query

        public BoardTypeRepository()
        {
            SqlInsert = @"Insert Into [BoardType]([BoardTypeName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@BoardTypeName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BoardType] Set [BoardTypeName]=@BoardTypeName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BoardTypeId]=@BoardTypeId";
            SqlDelete = @"Delete From [BoardType] Where [BoardTypeId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BoardType] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BoardType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BoardType model in lstImport)
                {
                    if (model.BoardTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BoardType FindByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[BoardTypeId]=@BoardTypeId " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("BoardTypeId", BoardTypeId));
        }

        public List<BoardType> FindsByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[BoardTypeId]=@BoardTypeId " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("BoardTypeId", BoardTypeId));
        }

        public int DeleteByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [BoardTypeId]=@BoardTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardTypeId", BoardTypeId));
        }

        public BoardType FindByBoardTypeName(DBConnection conn, String BoardTypeName, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[BoardTypeName]=@BoardTypeName " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("BoardTypeName", BoardTypeName));
        }

        public List<BoardType> FindsByBoardTypeName(DBConnection conn, String BoardTypeName, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[BoardTypeName]=@BoardTypeName " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("BoardTypeName", BoardTypeName));
        }

        public int DeleteByBoardTypeName(DBConnection conn, String BoardTypeName, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [BoardTypeName]=@BoardTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardTypeName", BoardTypeName));
        }

        public BoardType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<BoardType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public BoardType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BoardType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BoardType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BoardType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BoardType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BoardType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BoardType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BoardType.* From [BoardType] BoardType Where BoardType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BoardType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BoardType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BoardType.* From [BoardType] BoardType Where BoardType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BoardType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BoardType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
