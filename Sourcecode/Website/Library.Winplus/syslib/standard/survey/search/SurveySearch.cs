using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey.model;

namespace Library.Winplus.syslib.standard.survey.search
{
    public partial class SurveySearch : SimpleSearch<Survey>
    {
        public SurveySearch()
        {
            search = new Survey();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SurveyId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select Survey.*
                 , Project_ProjectId.[ProjectName] as [ProjectIdProjectName]
                 , BuildingType_BuildingTypeId.[BuildingTypeName] as [BuildingTypeIdBuildingTypeName]
                 , FloorNo_FloorNoId.[FloorNoName] as [FloorNoIdFloorNoName]
                 , Material_MaterialId.[MaterialName] as [MaterialIdMaterialName]
                 , Ownership_OwnershipId.[OwnershipName] as [OwnershipIdOwnershipName]
                 , SystemUser_OwnerSystemUserId.[SystemUserName] as [OwnerSystemUserIdSystemUserName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                 , Advantage_AdvantageId.[AdvantageName] as [AdvantageIdAdvantageName]
                 , LocationType_LocationTypeId.[LocationTypeName] as [LocationTypeIdLocationTypeName]
                 , BoardType_BoardTypeId.[BoardTypeName] as [BoardTypeIdBoardTypeName]
                 , BoardLocation_BoardLocationId.[BoardLocationName] as [BoardLocationIdBoardLocationName]
                 , BoardStyle_BoardStyleId.[BoardStyleName] as [BoardStyleIdBoardStyleName]
                 , RoadType_RoadTypeId.[RoadTypeName] as [RoadTypeIdRoadTypeName]
                 , RoadMaterial_RoadMaterialId.[RoadMaterialName] as [RoadMaterialIdRoadMaterialName]
                 , RoadLane_RoadLaneId.[RoadLaneName] as [RoadLaneIdRoadLaneName]
                 , BridgeType_BridgeTypeId.[BridgeTypeName] as [BridgeTypeIdBridgeTypeName]
                 , BridgeMaterial_BridgeMaterialId.[BridgeMaterialName] as [BridgeMaterialIdBridgeMaterialName]
                 , PoleType_PoleTypeId.[PoleTypeName] as [PoleTypeIdPoleTypeName]
                 , PoleMaterial_PoleMaterialId.[PoleMaterialName] as [PoleMaterialIdPoleMaterialName]
                From [Survey] Survey
                Left Join [Project] Project_ProjectId on Project_ProjectId.[ProjectId]=Survey.[ProjectId]
                Left Join [BuildingType] BuildingType_BuildingTypeId on BuildingType_BuildingTypeId.[BuildingTypeId]=Survey.[BuildingTypeId]
                Left Join [FloorNo] FloorNo_FloorNoId on FloorNo_FloorNoId.[FloorNoId]=Survey.[FloorNoId]
                Left Join [Material] Material_MaterialId on Material_MaterialId.[MaterialId]=Survey.[MaterialId]
                Left Join [Ownership] Ownership_OwnershipId on Ownership_OwnershipId.[OwnershipId]=Survey.[OwnershipId]
                Left Join [SystemUser] SystemUser_OwnerSystemUserId on SystemUser_OwnerSystemUserId.[SystemUserId]=Survey.[OwnerSystemUserId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=Survey.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=Survey.[ModifiedBy]
                Left Join [Advantage] Advantage_AdvantageId on Advantage_AdvantageId.[AdvantageId]=Survey.[AdvantageId]
                Left Join [LocationType] LocationType_LocationTypeId on LocationType_LocationTypeId.[LocationTypeId]=Survey.[LocationTypeId]
                Left Join [BoardType] BoardType_BoardTypeId on BoardType_BoardTypeId.[BoardTypeId]=Survey.[BoardTypeId]
                Left Join [BoardLocation] BoardLocation_BoardLocationId on BoardLocation_BoardLocationId.[BoardLocationId]=Survey.[BoardLocationId]
                Left Join [BoardStyle] BoardStyle_BoardStyleId on BoardStyle_BoardStyleId.[BoardStyleId]=Survey.[BoardStyleId]
                Left Join [RoadType] RoadType_RoadTypeId on RoadType_RoadTypeId.[RoadTypeId]=Survey.[RoadTypeId]
                Left Join [RoadMaterial] RoadMaterial_RoadMaterialId on RoadMaterial_RoadMaterialId.[RoadMaterialId]=Survey.[RoadMaterialId]
                Left Join [RoadLane] RoadLane_RoadLaneId on RoadLane_RoadLaneId.[RoadLaneId]=Survey.[RoadLaneId]
                Left Join [BridgeType] BridgeType_BridgeTypeId on BridgeType_BridgeTypeId.[BridgeTypeId]=Survey.[BridgeTypeId]
                Left Join [BridgeMaterial] BridgeMaterial_BridgeMaterialId on BridgeMaterial_BridgeMaterialId.[BridgeMaterialId]=Survey.[BridgeMaterialId]
                Left Join [PoleType] PoleType_PoleTypeId on PoleType_PoleTypeId.[PoleTypeId]=Survey.[PoleTypeId]
                Left Join [PoleMaterial] PoleMaterial_PoleMaterialId on PoleMaterial_PoleMaterialId.[PoleMaterialId]=Survey.[PoleMaterialId]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (Survey.[SurveyName] like '%'+@SearchText+'%' or Survey.[SurveyCode] like '%'+@SearchText+'%' or Project_ProjectId.[ProjectName] like '%'+@SearchText+'%' or Survey.[BuildingName] like '%'+@SearchText+'%' or BuildingType_BuildingTypeId.[BuildingTypeName] like '%'+@SearchText+'%' or Survey.[BuildingTypeOther] like '%'+@SearchText+'%' or FloorNo_FloorNoId.[FloorNoName] like '%'+@SearchText+'%' or Survey.[FloorNoOther] like '%'+@SearchText+'%' or Material_MaterialId.[MaterialName] like '%'+@SearchText+'%' or Survey.[MaterialOther] like '%'+@SearchText+'%' or Survey.[ImageFront] like '%'+@SearchText+'%' or Survey.[ImageBeside] like '%'+@SearchText+'%' or Survey.[ImageRefer] like '%'+@SearchText+'%' or Ownership_OwnershipId.[OwnershipName] like '%'+@SearchText+'%' or SystemUser_OwnerSystemUserId.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%' or Advantage_AdvantageId.[AdvantageName] like '%'+@SearchText+'%' or Survey.[Advantage] like '%'+@SearchText+'%' or Survey.[Remark] like '%'+@SearchText+'%' or LocationType_LocationTypeId.[LocationTypeName] like '%'+@SearchText+'%' or Survey.[PlaceOwner] like '%'+@SearchText+'%' or Survey.[PlaceUserBenefic] like '%'+@SearchText+'%' or Survey.[HomeNo] like '%'+@SearchText+'%' or Survey.[Moo] like '%'+@SearchText+'%' or BoardType_BoardTypeId.[BoardTypeName] like '%'+@SearchText+'%' or BoardLocation_BoardLocationId.[BoardLocationName] like '%'+@SearchText+'%' or BoardStyle_BoardStyleId.[BoardStyleName] like '%'+@SearchText+'%' or Survey.[BoardText] like '%'+@SearchText+'%' or RoadType_RoadTypeId.[RoadTypeName] like '%'+@SearchText+'%' or RoadMaterial_RoadMaterialId.[RoadMaterialName] like '%'+@SearchText+'%' or RoadLane_RoadLaneId.[RoadLaneName] like '%'+@SearchText+'%' or BridgeType_BridgeTypeId.[BridgeTypeName] like '%'+@SearchText+'%' or BridgeMaterial_BridgeMaterialId.[BridgeMaterialName] like '%'+@SearchText+'%' or PoleType_PoleTypeId.[PoleTypeName] like '%'+@SearchText+'%' or PoleMaterial_PoleMaterialId.[PoleMaterialName] like '%'+@SearchText+'%' or Survey.[LandParcelNo] like '%'+@SearchText+'%' or Survey.[LandNo] like '%'+@SearchText+'%' or Survey.[LandUsed] like '%'+@SearchText+'%' or Survey.[LandParcelTypeIdSelected] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SurveyId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[SurveyId] = @SurveyId");
                    param.Add(new DBParameter("SurveyId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SurveyName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyName", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[SurveyName] like '%'+@SurveyName+'%'");
                    param.Add(new DBParameter("SurveyName", StringUtil.ToString(text)));
                }

                /* Advance Search by SurveyCode */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyCode", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[SurveyCode] like '%'+@SurveyCode+'%'");
                    param.Add(new DBParameter("SurveyCode", StringUtil.ToString(text)));
                }

                /* Advance Search by ProjectId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ProjectId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[ProjectId] = @ProjectId");
                    param.Add(new DBParameter("ProjectId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by Latitude */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Latitude", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Latitude] = @Latitude");
                    param.Add(new DBParameter("Latitude", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Longitude */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Longitude", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Longitude] = @Longitude");
                    param.Add(new DBParameter("Longitude", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by BuildingName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingName", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BuildingName] like '%'+@BuildingName+'%'");
                    param.Add(new DBParameter("BuildingName", StringUtil.ToString(text)));
                }

                /* Advance Search by BuildingTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BuildingTypeId] = @BuildingTypeId");
                    param.Add(new DBParameter("BuildingTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BuildingTypeOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BuildingTypeOther] like '%'+@BuildingTypeOther+'%'");
                    param.Add(new DBParameter("BuildingTypeOther", StringUtil.ToString(text)));
                }

                /* Advance Search by FloorNoId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[FloorNoId] = @FloorNoId");
                    param.Add(new DBParameter("FloorNoId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by FloorNoOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[FloorNoOther] like '%'+@FloorNoOther+'%'");
                    param.Add(new DBParameter("FloorNoOther", StringUtil.ToString(text)));
                }

                /* Advance Search by MaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[MaterialId] = @MaterialId");
                    param.Add(new DBParameter("MaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by MaterialOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[MaterialOther] like '%'+@MaterialOther+'%'");
                    param.Add(new DBParameter("MaterialOther", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageFront */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageFront", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[ImageFront] like '%'+@ImageFront+'%'");
                    param.Add(new DBParameter("ImageFront", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageBeside */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageBeside", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[ImageBeside] like '%'+@ImageBeside+'%'");
                    param.Add(new DBParameter("ImageBeside", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageRefer */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageRefer", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[ImageRefer] like '%'+@ImageRefer+'%'");
                    param.Add(new DBParameter("ImageRefer", StringUtil.ToString(text)));
                }

                /* Advance Search by Width */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Width", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Width] = @Width");
                    param.Add(new DBParameter("Width", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Long */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Long", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Long] = @Long");
                    param.Add(new DBParameter("Long", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Height */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Height", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Height] = @Height");
                    param.Add(new DBParameter("Height", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by FarFromRefer */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FarFromRefer", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[FarFromRefer] = @FarFromRefer");
                    param.Add(new DBParameter("FarFromRefer", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by UnitNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UnitNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[UnitNo] = @UnitNo");
                    param.Add(new DBParameter("UnitNo", StringUtil.ToInt32(text)));
                }

                /* Advance Search by OwnershipId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnershipId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[OwnershipId] = @OwnershipId");
                    param.Add(new DBParameter("OwnershipId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by UpdateFlag */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UpdateFlag", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[UpdateFlag] = @UpdateFlag");
                    param.Add(new DBParameter("UpdateFlag", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by DeleteFlag */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DeleteFlag", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[DeleteFlag] = @DeleteFlag");
                    param.Add(new DBParameter("DeleteFlag", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by OwnerSystemUserId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnerSystemUserId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[OwnerSystemUserId] = @OwnerSystemUserId");
                    param.Add(new DBParameter("OwnerSystemUserId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by KeyInOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey.`KeyInOn` >= @KeyInOn_From");
                    param.Add(new DBParameter("KeyInOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by KeyInOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey.`KeyInOn` < DATE_ADD(@KeyInOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("KeyInOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by AdvantageId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_AdvantageId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[AdvantageId] = @AdvantageId");
                    param.Add(new DBParameter("AdvantageId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by Advantage */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Advantage", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Advantage] like '%'+@Advantage+'%'");
                    param.Add(new DBParameter("Advantage", StringUtil.ToString(text)));
                }

                /* Advance Search by Remark */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Remark", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Remark] like '%'+@Remark+'%'");
                    param.Add(new DBParameter("Remark", StringUtil.ToString(text)));
                }

                /* Advance Search by NewPoint */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_NewPoint", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[NewPoint] = @NewPoint");
                    param.Add(new DBParameter("NewPoint", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by LocationTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LocationTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LocationTypeId] = @LocationTypeId");
                    param.Add(new DBParameter("LocationTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by PlaceOwner */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PlaceOwner", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[PlaceOwner] like '%'+@PlaceOwner+'%'");
                    param.Add(new DBParameter("PlaceOwner", StringUtil.ToString(text)));
                }

                /* Advance Search by PlaceUserBenefic */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PlaceUserBenefic", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[PlaceUserBenefic] like '%'+@PlaceUserBenefic+'%'");
                    param.Add(new DBParameter("PlaceUserBenefic", StringUtil.ToString(text)));
                }

                /* Advance Search by HomeNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_HomeNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[HomeNo] like '%'+@HomeNo+'%'");
                    param.Add(new DBParameter("HomeNo", StringUtil.ToString(text)));
                }

                /* Advance Search by Moo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Moo", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[Moo] like '%'+@Moo+'%'");
                    param.Add(new DBParameter("Moo", StringUtil.ToString(text)));
                }

                /* Advance Search by BoardTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BoardTypeId] = @BoardTypeId");
                    param.Add(new DBParameter("BoardTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BoardLocationId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardLocationId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BoardLocationId] = @BoardLocationId");
                    param.Add(new DBParameter("BoardLocationId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BoardStyleId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardStyleId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BoardStyleId] = @BoardStyleId");
                    param.Add(new DBParameter("BoardStyleId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BoardSide */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardSide", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BoardSide] = @BoardSide");
                    param.Add(new DBParameter("BoardSide", StringUtil.ToInt32(text)));
                }

                /* Advance Search by BoardText */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardText", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BoardText] like '%'+@BoardText+'%'");
                    param.Add(new DBParameter("BoardText", StringUtil.ToString(text)));
                }

                /* Advance Search by RoadTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_RoadTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[RoadTypeId] = @RoadTypeId");
                    param.Add(new DBParameter("RoadTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by RoadMaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_RoadMaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[RoadMaterialId] = @RoadMaterialId");
                    param.Add(new DBParameter("RoadMaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by RoadLaneId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_RoadLaneId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[RoadLaneId] = @RoadLaneId");
                    param.Add(new DBParameter("RoadLaneId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BridgeTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BridgeTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BridgeTypeId] = @BridgeTypeId");
                    param.Add(new DBParameter("BridgeTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BridgeMaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BridgeMaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[BridgeMaterialId] = @BridgeMaterialId");
                    param.Add(new DBParameter("BridgeMaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by PoleTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PoleTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[PoleTypeId] = @PoleTypeId");
                    param.Add(new DBParameter("PoleTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by PoleMaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PoleMaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[PoleMaterialId] = @PoleMaterialId");
                    param.Add(new DBParameter("PoleMaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by LandParcelNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandParcelNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandParcelNo] like '%'+@LandParcelNo+'%'");
                    param.Add(new DBParameter("LandParcelNo", StringUtil.ToString(text)));
                }

                /* Advance Search by LandNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandNo] like '%'+@LandNo+'%'");
                    param.Add(new DBParameter("LandNo", StringUtil.ToString(text)));
                }

                /* Advance Search by LandRai */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandRai", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandRai] = @LandRai");
                    param.Add(new DBParameter("LandRai", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandNgan */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandNgan", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandNgan] = @LandNgan");
                    param.Add(new DBParameter("LandNgan", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandWa */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandWa", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandWa] = @LandWa");
                    param.Add(new DBParameter("LandWa", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandSubWa */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandSubWa", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandSubWa] = @LandSubWa");
                    param.Add(new DBParameter("LandSubWa", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by LandUsed */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandUsed", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandUsed] like '%'+@LandUsed+'%'");
                    param.Add(new DBParameter("LandUsed", StringUtil.ToString(text)));
                }

                /* Advance Search by LandParcelTypeIdSelected */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandParcelTypeIdSelected", ref text))
                {
                    sqlQuery.AppendLine("and Survey.[LandParcelTypeIdSelected] like '%'+@LandParcelTypeIdSelected+'%'");
                    param.Add(new DBParameter("LandParcelTypeIdSelected", StringUtil.ToString(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and Survey.SurveyId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
