﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.survey.model;

namespace Library.Winplus.syslib.standard.survey.search
{
    public partial class SurveySearch
    {
        public bool CustomSearch = true;
        public string SearchMode { get; set; }
        public Int64 ProjectId { get; set; }

        public Project Project;

        public void CustomPrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            ParseRequest(request);
            DefaultSearch = "SurveyId desc";

            ProjectId = Convert.ToInt64(SearchKey["search_ProjectId"]);

            string sqlSelect = $@"Survey.*
                 , Project_ProjectId.[ProjectName] as [ProjectIdProjectName]
                 , BuildingType_BuildingTypeId.[BuildingTypeName] as [BuildingTypeIdBuildingTypeName]
				 , case when BuildingType_BuildingTypeId.[BuildingTypeName]= 'อื่นๆ' then Survey.BuildingTypeOther else BuildingType_BuildingTypeId.[BuildingTypeName] end BuildingTypeName
                 , FloorNo_FloorNoId.[FloorNoName] as [FloorNoIdFloorNoName]

                 , case when FloorNo_FloorNoId.[FloorNoName]= 'อื่นๆ' then Survey.FloorNoOther else FloorNo_FloorNoId.[FloorNoName] end FloorNoName
                 , Material_MaterialId.[MaterialName] as [MaterialIdMaterialName]
                 , Advantage_AdvantageId.[AdvantageName] as [AdvantageIdAdvantageName]
                 , case when Material_MaterialId.[MaterialName]= 'อื่นๆ' then Survey.MaterialOther else Material_MaterialId.[MaterialName] end MaterialName
                 , Ownership_OwnershipId.[OwnershipName] as [OwnershipIdOwnershipName]
                 , SystemUser_OwnerSystemUserId.[SystemUserName] as [OwnerSystemUserIdSystemUserName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
				 , case when Survey.UpdateFlag=1 then isnull(LocationType_LocationTypeId.EditedIcon,cd.ConfigValue) else isnull(LocationType_LocationTypeId.NewIcon,cn.ConfigValue) end  MapIcon";

            if (SearchMode == "CSV")
            {
                sqlSelect = $@"Survey.SurveyId
                            , Survey.[Longitude] [x]
                            , Survey.[Latitude] [y]
                            , Survey.[BuildingName] [BL_NAME]
                            , case when BuildingType_BuildingTypeId.[BuildingTypeName]='อื่นๆ' then Survey.BuildingTypeOther else BuildingType_BuildingTypeId.[BuildingTypeName] end [BL_TYPE]
                            , case when Material_MaterialId.[MaterialName]= 'อื่นๆ' then Survey.MaterialOther else Material_MaterialId.[MaterialName] end [BL_MATL]
                            , case when FloorNo_FloorNoId.[FloorNoName]= 'อื่นๆ' then Survey.FloorNoOther else FloorNo_FloorNoId.[FloorNoName] end [BL_NSTOREY]
                            , Survey.[UnitNo] [BL_NUNIT]
                            , Advantage_AdvantageId.[AdvantageName] as [BL_USE]
                            , Survey.[Advantage] [BL_ACT]
                            , Survey.[REMARK] [REMARK]
                            , Survey.[ImageFront] [PHOTO1]
                            , Survey.[ImageBeside] [PHOTO2]
                            , Survey.[ImageRefer] [PHOTO3]
                            , Survey.[Width] [BL_FRONTAGE]
                            , Survey.[Long] [BL_DEPTH]
                            , Survey.[Height] [BL_HEIGHT]
                            , Survey.[FarFromRefer] [BL_REF]
                            , SystemUser_OwnerSystemUserId.[SystemUserName] as [USER_NAME]
                            , Survey.[KeyInOn] [SURV_DATE]
                            , LocationType_LocationTypeId.LocationTypeName [TYPE]
                            , Survey.PlaceOwner [OWNER]
                            , Survey.PlaceUserBenefic [USER]
                            , Survey.HomeNo [BL_Add]
                            , BoardType_BoardTypeId.BoardTypeName [BB_TYPE]
                            , BoardLocation_BoardLocationId.BoardLocationName [BB_LOCATION]
                            , BoardStyle_BoardStyleId.BoardStyleName [BB_STYLE]
                            , Survey.BoardSide [BB_SIDE]
                            , Survey.BoardText [BB_TEXT]
                            , RoadType_RoadTypeId.RoadTypeName [RD_TYPE]
                            , RoadMaterial_RoadMaterialId.RoadMaterialName [RD_MATERIAL]
                            , BridgeType_BridgeTypeId.BridgeTypeName [BD_TYPE]
                            , BridgeMaterial_BridgeMaterialId.BridgeMaterialName [BD_MATERIAL]
                            , PoleType_PoleTypeId.PoleTypeName [PO_TYPE]
                            , PoleMaterial_PoleMaterialId.PoleMaterialName [PO_MATERIAL]
                            ";

                ColumnOrderByIndex.Clear();
            }

            StringBuilder sqlQuery = new StringBuilder($@"
                 Select {sqlSelect}
                From [Survey_{ProjectId}] Survey
                Left Join [Project] Project_ProjectId on Project_ProjectId.[ProjectId]=Survey.[ProjectId]
                Left Join [BuildingType] BuildingType_BuildingTypeId on BuildingType_BuildingTypeId.[BuildingTypeId]=Survey.[BuildingTypeId]
                Left Join [FloorNo] FloorNo_FloorNoId on FloorNo_FloorNoId.[FloorNoId]=Survey.[FloorNoId]
                Left Join [Material] Material_MaterialId on Material_MaterialId.[MaterialId]=Survey.[MaterialId]
                Left Join [Ownership] Ownership_OwnershipId on Ownership_OwnershipId.[OwnershipId]=Survey.[OwnershipId]
                Left Join [Advantage] Advantage_AdvantageId on Advantage_AdvantageId.[AdvantageId]=Survey.[AdvantageId]
                Left Join [SystemUser] SystemUser_OwnerSystemUserId on SystemUser_OwnerSystemUserId.[SystemUserId]=Survey.[OwnerSystemUserId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=Survey.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=Survey.[ModifiedBy]

                Left Join [LocationType] LocationType_LocationTypeId on LocationType_LocationTypeId.[LocationTypeId]=Survey.[LocationTypeId]
                Left Join [BoardType] BoardType_BoardTypeId on BoardType_BoardTypeId.[BoardTypeId]=Survey.[BoardTypeId]
                Left Join [BoardLocation] BoardLocation_BoardLocationId on BoardLocation_BoardLocationId.[BoardLocationId]=Survey.[BoardLocationId]
                Left Join [BoardStyle] BoardStyle_BoardStyleId on BoardStyle_BoardStyleId.[BoardStyleId]=Survey.[BoardStyleId]
                Left Join [RoadType] RoadType_RoadTypeId on RoadType_RoadTypeId.[RoadTypeId]=Survey.[RoadTypeId]
                Left Join [RoadMaterial] RoadMaterial_RoadMaterialId on RoadMaterial_RoadMaterialId.[RoadMaterialId]=Survey.[RoadMaterialId]
                Left Join [BridgeType] BridgeType_BridgeTypeId on BridgeType_BridgeTypeId.[BridgeTypeId]=Survey.[BridgeTypeId]
                Left Join [BridgeMaterial] BridgeMaterial_BridgeMaterialId on BridgeMaterial_BridgeMaterialId.[BridgeMaterialId]=Survey.[BridgeMaterialId]
                Left Join [PoleType] PoleType_PoleTypeId on PoleType_PoleTypeId.[PoleTypeId]=Survey.[PoleTypeId]
                Left Join [PoleMaterial] PoleMaterial_PoleMaterialId on PoleMaterial_PoleMaterialId.[PoleMaterialId]=Survey.[PoleMaterialId]
				Left join [SystemConfig] cn on cn.ConfigKey='MapIconNew'
				Left join [SystemConfig] cd on cd.ConfigKey='MapIconEdited'
                Where Survey.DeleteFlag=0");

            string text = null;
            /* Default text search */
            if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
            {
                sqlQuery.AppendLine("and (Survey.[SurveyName] like '%'+@SearchText+'%' or Survey.[SurveyCode] like '%'+@SearchText+'%' or Project_ProjectId.[ProjectName] like '%'+@SearchText+'%' or Survey.[BuildingName] like '%'+@SearchText+'%' or Survey.[Advantage] like '%'+@SearchText+'%' or BuildingType_BuildingTypeId.[BuildingTypeName] like '%'+@SearchText+'%' or Survey.[BuildingTypeOther] like '%'+@SearchText+'%' or FloorNo_FloorNoId.[FloorNoName] like '%'+@SearchText+'%' or Survey.[FloorNoOther] like '%'+@SearchText+'%' or Material_MaterialId.[MaterialName] like '%'+@SearchText+'%' or Survey.[MaterialOther] like '%'+@SearchText+'%' or Survey.[ImageFront] like '%'+@SearchText+'%' or Survey.[ImageBeside] like '%'+@SearchText+'%' or Survey.[ImageRefer] like '%'+@SearchText+'%' or Ownership_OwnershipId.[OwnershipName] like '%'+@SearchText+'%' or SystemUser_OwnerSystemUserId.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
            }

            decimal? StartingLatitude = null;
            decimal? StartingLongitude = null;
            decimal? MaxDistance = null;
            if (StringUtil.IsContainsKeyValue(SearchKey, "StartingLatitude", ref text))
            {
                StartingLatitude = Convert.ToDecimal(text);
            }
            if (StringUtil.IsContainsKeyValue(SearchKey, "StartingLongitude", ref text))
            {
                StartingLongitude = Convert.ToDecimal(text);
            }
            if (StringUtil.IsContainsKeyValue(SearchKey, "MaxDistance", ref text))
            {
                MaxDistance = Convert.ToDecimal(text);
            }

            if (StartingLatitude.HasValue && StartingLongitude.HasValue)
            {
                //and latitude-13.757397 < 0.003000
                //and longitude-100.613045 < 0.003000

                if (!MaxDistance.HasValue) MaxDistance = SystemConfigService.GetIntConfig("DefaultSearchRadius", 1000); //Kilomet
                sqlQuery.AppendLine(" And abs(Survey.Latitude-@StartingLatitude)<@MaxDistance");
                sqlQuery.AppendLine(" And abs(Survey.Longitude-@StartingLongitude)<@MaxDistance");
                //sqlQuery.AppendLine(" And dbo.GetDistanceBetween(@StartingLatitude, @StartingLongitude, Survey.latitude, Survey.longitude) <= @MaxDistance");

                param.Add(new DBParameter("StartingLatitude", StartingLatitude));
                param.Add(new DBParameter("StartingLongitude", StartingLongitude));
                param.Add(new DBParameter("MaxDistance", MaxDistance/1000000));
            }


            #region search condition

            /* Advance Search by SurveyId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyId", ref text))
            {
                sqlQuery.AppendLine("and Survey.[SurveyId] = @SurveyId");
                param.Add(new DBParameter("SurveyId", StringUtil.ToInt64(text)));
            }

            /* Advance Search by SurveyName */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyName", ref text))
            {
                sqlQuery.AppendLine("and Survey.[SurveyName] like '%'+@SurveyName+'%'");
                param.Add(new DBParameter("SurveyName", StringUtil.ToString(text)));
            }

            /* Advance Search by SurveyCode */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyCode", ref text))
            {
                if (SearchMode==null || SearchMode != "Map")
                {
                    sqlQuery.AppendLine("and Survey.[SurveyCode] = @SurveyCode");
                    param.Add(new DBParameter("SurveyCode", StringUtil.ToString(text)));
                }
            }
            

            /* Advance Search by BuildingName */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingName", ref text))
            {
                sqlQuery.AppendLine("and Survey.[BuildingName] like '%'+@BuildingName+'%'");
                param.Add(new DBParameter("BuildingName", StringUtil.ToString(text)));
            }

            /* Advance Search by Advantage */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_Advantage", ref text))
            {
                sqlQuery.AppendLine("and Survey.[Advantage] like '%'+@Advantage+'%'");
                param.Add(new DBParameter("Advantage", StringUtil.ToString(text)));
            }

            /* Advance Search by BuildingTypeId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_AdvantageId", ref text))
            {
                sqlQuery.AppendLine($"and Survey.[AdvantageId] IN({text})");
            }

            /* Advance Search by BuildingTypeId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeId", ref text))
            {
                sqlQuery.AppendLine($"and Survey.[BuildingTypeId] IN({text})");
            }

            /* Advance Search by BuildingTypeOther */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeOther", ref text))
            {
                sqlQuery.AppendLine("and Survey.[BuildingTypeOther] like '%'+@BuildingTypeOther+'%'");
                param.Add(new DBParameter("BuildingTypeOther", StringUtil.ToString(text)));
            }

            /* Advance Search by FloorNoId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoId", ref text))
            {
                sqlQuery.AppendLine($"and Survey.[FloorNoId] IN({text})");
            }

            /* Advance Search by FloorNoOther */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoOther", ref text))
            {
                sqlQuery.AppendLine("and Survey.[FloorNoOther] like '%'+@FloorNoOther+'%'");
                param.Add(new DBParameter("FloorNoOther", StringUtil.ToString(text)));
            }

            /* Advance Search by MaterialId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialId", ref text))
            {
                sqlQuery.AppendLine($"and Survey.[MaterialId] IN({text})");
            }

            /* Advance Search by MaterialOther */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialOther", ref text))
            {
                sqlQuery.AppendLine("and Survey.[MaterialOther] like '%'+@MaterialOther+'%'");
                param.Add(new DBParameter("MaterialOther", StringUtil.ToString(text)));
            }

            /* Advance Search by ImageFront */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageFront", ref text))
            {
                sqlQuery.AppendLine("and Survey.[ImageFront] like '%'+@ImageFront+'%'");
                param.Add(new DBParameter("ImageFront", StringUtil.ToString(text)));
            }

            /* Advance Search by ImageBeside */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageBeside", ref text))
            {
                sqlQuery.AppendLine("and Survey.[ImageBeside] like '%'+@ImageBeside+'%'");
                param.Add(new DBParameter("ImageBeside", StringUtil.ToString(text)));
            }

            /* Advance Search by ImageRefer */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageRefer", ref text))
            {
                sqlQuery.AppendLine("and Survey.[ImageRefer] like '%'+@ImageRefer+'%'");
                param.Add(new DBParameter("ImageRefer", StringUtil.ToString(text)));
            }

            /* Advance Search by Width */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_Width", ref text))
            {
                sqlQuery.AppendLine("and Survey.[Width] = @Width");
                param.Add(new DBParameter("Width", StringUtil.ToDecimal(text)));
            }

            /* Advance Search by Long */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_Long", ref text))
            {
                sqlQuery.AppendLine("and Survey.[Long] = @Long");
                param.Add(new DBParameter("Long", StringUtil.ToDecimal(text)));
            }

            /* Advance Search by Height */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_Height", ref text))
            {
                sqlQuery.AppendLine("and Survey.[Height] = @Height");
                param.Add(new DBParameter("Height", StringUtil.ToDecimal(text)));
            }

            /* Advance Search by FarFromRefer */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_FarFromRefer", ref text))
            {
                sqlQuery.AppendLine("and Survey.[FarFromRefer] = @FarFromRefer");
                param.Add(new DBParameter("FarFromRefer", StringUtil.ToDecimal(text)));
            }

            /* Advance Search by UnitNo */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_UnitNo", ref text))
            {
                sqlQuery.AppendLine("and Survey.[UnitNo] = @UnitNo");
                param.Add(new DBParameter("UnitNo", StringUtil.ToInt32(text)));
            }

            /* Advance Search by OwnershipId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnershipId", ref text))
            {
                sqlQuery.AppendLine("and Survey.[OwnershipId] = @OwnershipId");
                param.Add(new DBParameter("OwnershipId", StringUtil.ToInt64(text)));
            }

            /* Advance Search by UpdateFlag */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_UpdateFlag", ref text))
            {
                sqlQuery.AppendLine("and Survey.[UpdateFlag] = @UpdateFlag");
                param.Add(new DBParameter("UpdateFlag", StringUtil.ToBoolean(text)));
            }

            /* Advance Search by DeleteFlag */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_DeleteFlag", ref text))
            {
                sqlQuery.AppendLine("and Survey.[DeleteFlag] = @DeleteFlag");
                param.Add(new DBParameter("DeleteFlag", StringUtil.ToBoolean(text)));
            }

            /* Advance Search by DeleteFlag */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_NewPoint", ref text))
            {
                sqlQuery.AppendLine("and Survey.[NewPoint] = @NewPoint");
                param.Add(new DBParameter("NewPoint", StringUtil.ToBoolean(text)));
            }

            /* Advance Search by OwnerSystemUserId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnerSystemUserId", ref text))
            {
                if (text.Contains(","))
                {
                    sqlQuery.AppendLine($"and Survey.[OwnerSystemUserId] in({text})");
                }
                else
                {
                    sqlQuery.AppendLine("and Survey.[OwnerSystemUserId] = @OwnerSystemUserId");
                    param.Add(new DBParameter("OwnerSystemUserId", StringUtil.ToInt64(text)));
                }
            }

            /* Advance Search by KeyInOn From*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_From", ref text))
            {
                sqlQuery.AppendLine("and Survey.[KeyInOn] >= @KeyInOn_From");
                param.Add(new DBParameter("KeyInOn_From", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by KeyInOn To*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_To", ref text))
            {
                sqlQuery.AppendLine("and Survey.[KeyInOn] < dateadd(day,1,@KeyInOn_To)");
                param.Add(new DBParameter("KeyInOn_To", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by CreatedOn From*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
            {
                sqlQuery.AppendLine("and Survey.[CreatedOn] >= @CreatedOn_From");
                param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by CreatedOn To*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
            {
                sqlQuery.AppendLine("and Survey.[CreatedOn] < dateadd(day,1,@CreatedOn_To)");
                param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by CreatedBy */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
            {
                sqlQuery.AppendLine("and Survey.[CreatedBy] = @CreatedBy");
                param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
            }

            /* Advance Search by ModifiedOn From*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
            {
                sqlQuery.AppendLine("and Survey.[ModifiedOn] >= @ModifiedOn_From");
                param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by ModifiedOn To*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
            {
                sqlQuery.AppendLine("and Survey.[ModifiedOn] < dateadd(day,1,@ModifiedOn_To)");
                param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by ModifiedBy */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
            {
                sqlQuery.AppendLine("and Survey.[ModifiedBy] = @ModifiedBy");
                param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
            }


            if (!string.IsNullOrEmpty(Ids))
            {
                sqlQuery.AppendLine("and Survey.SurveyId in(" + Ids + ")");
            }

            #endregion search condition

            /* prepare temporary parameter */
            this.sqlQuery = sqlQuery.ToString();

            this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;

        }

    }
}
