using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.survey.model
{
    [Serializable]
    public partial class Survey : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("SurveyId", typeof(MyResources.Winplus.Resource))]
        public Int64? SurveyId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SurveyName", typeof(MyResources.Winplus.Resource))]
        public String SurveyName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SurveyCode", typeof(MyResources.Winplus.Resource))]
        public String SurveyCode { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ProjectId", typeof(MyResources.Winplus.Resource))]
        public Int64? ProjectId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Latitude", typeof(MyResources.Winplus.Resource))]
        public Decimal? Latitude { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Longitude", typeof(MyResources.Winplus.Resource))]
        public Decimal? Longitude { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BuildingName", typeof(MyResources.Winplus.Resource))]
        public String BuildingName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BuildingTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? BuildingTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BuildingTypeOther", typeof(MyResources.Winplus.Resource))]
        public String BuildingTypeOther { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FloorNoId", typeof(MyResources.Winplus.Resource))]
        public Int64? FloorNoId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FloorNoOther", typeof(MyResources.Winplus.Resource))]
        public String FloorNoOther { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MaterialId", typeof(MyResources.Winplus.Resource))]
        public Int64? MaterialId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MaterialOther", typeof(MyResources.Winplus.Resource))]
        public String MaterialOther { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ImageFront", typeof(MyResources.Winplus.Resource))]
        public String ImageFront { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ImageBeside", typeof(MyResources.Winplus.Resource))]
        public String ImageBeside { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ImageRefer", typeof(MyResources.Winplus.Resource))]
        public String ImageRefer { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Width", typeof(MyResources.Winplus.Resource))]
        public Decimal? Width { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Long", typeof(MyResources.Winplus.Resource))]
        public Decimal? Long { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Height", typeof(MyResources.Winplus.Resource))]
        public Decimal? Height { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FarFromRefer", typeof(MyResources.Winplus.Resource))]
        public Decimal? FarFromRefer { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UnitNo", typeof(MyResources.Winplus.Resource))]
        public Int32? UnitNo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("OwnershipId", typeof(MyResources.Winplus.Resource))]
        public Int64? OwnershipId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UpdateFlag", typeof(MyResources.Winplus.Resource))]
        public Boolean UpdateFlag { get; set; }

        [DataColumn]
        [LocalizedDisplayName("DeleteFlag", typeof(MyResources.Winplus.Resource))]
        public Boolean DeleteFlag { get; set; }

        [DataColumn]
        [LocalizedDisplayName("OwnerSystemUserId", typeof(MyResources.Winplus.Resource))]
        public Int64? OwnerSystemUserId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("KeyInOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? KeyInOn { get; set; }
        public string KeyInOnText
        {
            get { return StringUtil.FormatDate(KeyInOn); }
            set { KeyInOn = StringUtil.ToDate(value); }
        }
        public string KeyInOnTimeText
        {
            get { return StringUtil.FormatTime(KeyInOn); }
            set { KeyInOn = StringUtil.AddedTime(KeyInOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("AdvantageId", typeof(MyResources.Winplus.Resource))]
        public Int64? AdvantageId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Advantage", typeof(MyResources.Winplus.Resource))]
        public String Advantage { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Remark", typeof(MyResources.Winplus.Resource))]
        public String Remark { get; set; }

        [DataColumn]
        [LocalizedDisplayName("NewPoint", typeof(MyResources.Winplus.Resource))]
        public Boolean NewPoint { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LocationTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? LocationTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("PlaceOwner", typeof(MyResources.Winplus.Resource))]
        public String PlaceOwner { get; set; }

        [DataColumn]
        [LocalizedDisplayName("PlaceUserBenefic", typeof(MyResources.Winplus.Resource))]
        public String PlaceUserBenefic { get; set; }

        [DataColumn]
        [LocalizedDisplayName("HomeNo", typeof(MyResources.Winplus.Resource))]
        public String HomeNo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Moo", typeof(MyResources.Winplus.Resource))]
        public String Moo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BoardTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? BoardTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BoardLocationId", typeof(MyResources.Winplus.Resource))]
        public Int64? BoardLocationId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BoardStyleId", typeof(MyResources.Winplus.Resource))]
        public Int64? BoardStyleId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BoardSide", typeof(MyResources.Winplus.Resource))]
        public Int32? BoardSide { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BoardText", typeof(MyResources.Winplus.Resource))]
        public String BoardText { get; set; }

        [DataColumn]
        [LocalizedDisplayName("RoadTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? RoadTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("RoadMaterialId", typeof(MyResources.Winplus.Resource))]
        public Int64? RoadMaterialId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("RoadLaneId", typeof(MyResources.Winplus.Resource))]
        public Int64? RoadLaneId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BridgeTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? BridgeTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BridgeMaterialId", typeof(MyResources.Winplus.Resource))]
        public Int64? BridgeMaterialId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("PoleTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? PoleTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("PoleMaterialId", typeof(MyResources.Winplus.Resource))]
        public Int64? PoleMaterialId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandParcelNo", typeof(MyResources.Winplus.Resource))]
        public String LandParcelNo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandNo", typeof(MyResources.Winplus.Resource))]
        public String LandNo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandRai", typeof(MyResources.Winplus.Resource))]
        public Int32? LandRai { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandNgan", typeof(MyResources.Winplus.Resource))]
        public Int32? LandNgan { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandWa", typeof(MyResources.Winplus.Resource))]
        public Int32? LandWa { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandSubWa", typeof(MyResources.Winplus.Resource))]
        public Decimal? LandSubWa { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandUsed", typeof(MyResources.Winplus.Resource))]
        public String LandUsed { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LandParcelTypeIdSelected", typeof(MyResources.Winplus.Resource))]
        public String LandParcelTypeIdSelected { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("ProjectName", typeof(MyResources.Winplus.Resource))]
        public String ProjectIdProjectName { get; set; }

        [LocalizedDisplayName("BuildingTypeName", typeof(MyResources.Winplus.Resource))]
        public String BuildingTypeIdBuildingTypeName { get; set; }

        [LocalizedDisplayName("FloorNoName", typeof(MyResources.Winplus.Resource))]
        public String FloorNoIdFloorNoName { get; set; }

        [LocalizedDisplayName("MaterialName", typeof(MyResources.Winplus.Resource))]
        public String MaterialIdMaterialName { get; set; }

        [LocalizedDisplayName("OwnershipName", typeof(MyResources.Winplus.Resource))]
        public String OwnershipIdOwnershipName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String OwnerSystemUserIdSystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

        [LocalizedDisplayName("AdvantageName", typeof(MyResources.Winplus.Resource))]
        public String AdvantageIdAdvantageName { get; set; }

        [LocalizedDisplayName("LocationTypeName", typeof(MyResources.Winplus.Resource))]
        public String LocationTypeIdLocationTypeName { get; set; }

        [LocalizedDisplayName("BoardTypeName", typeof(MyResources.Winplus.Resource))]
        public String BoardTypeIdBoardTypeName { get; set; }

        [LocalizedDisplayName("BoardLocationName", typeof(MyResources.Winplus.Resource))]
        public String BoardLocationIdBoardLocationName { get; set; }

        [LocalizedDisplayName("BoardStyleName", typeof(MyResources.Winplus.Resource))]
        public String BoardStyleIdBoardStyleName { get; set; }

        [LocalizedDisplayName("RoadTypeName", typeof(MyResources.Winplus.Resource))]
        public String RoadTypeIdRoadTypeName { get; set; }

        [LocalizedDisplayName("RoadMaterialName", typeof(MyResources.Winplus.Resource))]
        public String RoadMaterialIdRoadMaterialName { get; set; }

        [LocalizedDisplayName("RoadLaneName", typeof(MyResources.Winplus.Resource))]
        public String RoadLaneIdRoadLaneName { get; set; }

        [LocalizedDisplayName("BridgeTypeName", typeof(MyResources.Winplus.Resource))]
        public String BridgeTypeIdBridgeTypeName { get; set; }

        [LocalizedDisplayName("BridgeMaterialName", typeof(MyResources.Winplus.Resource))]
        public String BridgeMaterialIdBridgeMaterialName { get; set; }

        [LocalizedDisplayName("PoleTypeName", typeof(MyResources.Winplus.Resource))]
        public String PoleTypeIdPoleTypeName { get; set; }

        [LocalizedDisplayName("PoleMaterialName", typeof(MyResources.Winplus.Resource))]
        public String PoleMaterialIdPoleMaterialName { get; set; }

    }
}
