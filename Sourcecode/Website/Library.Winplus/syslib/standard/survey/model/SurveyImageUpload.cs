﻿using System;

namespace Library.Winplus.syslib.standard.survey.model
{
    public class SurveyImageUpload
    {
        public Int64? ProjectId { get; set; }
        public Int64? SurveyId { get; set; }
        public string ImageType { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageUploadName { get; set; }
    }
}
