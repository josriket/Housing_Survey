using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;

namespace Library.Winplus.syslib.standard.survey.model
{
    public partial class Survey
    {
        public String ImageFrontBase64 { get; set; }
        public String ImageBesideBase64 { get; set; }
        public String ImageReferBase64 { get; set; }

        public String ImageFrontFullPath { get; set; }
        public String ImageBesideFullPath { get; set; }
        public String ImageReferFullPath { get; set; }
        public bool SurveyOverride { get; set; }

        public long[] LandParcelTypeIds {
            get {
                if (!string.IsNullOrEmpty(LandParcelTypeIdSelected))
                {
                    return StringUtil.SplitToLong(',', LandParcelTypeIdSelected);
                }
                return new long[] { };
            }

            set
            {
                if (value != null)
                {
                    this.LandParcelTypeIdSelected = string.Join(",", value);
                }
                else this.LandParcelTypeIdSelected = null;
            }
        }

        public bool SelectedLandParcelType(string id)
        {
            foreach(long idx in LandParcelTypeIds)
            {
                if (idx.ToString() == id) return true;
            }

            return false;
        }
    }
}
