﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.standard.survey.model
{
    public class SurveyCriteria
    {
        public long? ProjectId { get; set; }
        public string SurveyCode { get;set;}
        public decimal? StartingLatitude { get; set; }
        public decimal? StartingLongitude { get; set; }
        public int? MaxDistance { get; set; }
        public string LocationTypes { get; set; }
    }
}
