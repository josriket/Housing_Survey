﻿using System;

namespace Library.Winplus.syslib.standard.survey.model
{
    public class SurveyResult
    {
        public Int64? SurveyId { get; set; }

        public Int64? LocationTypeId { get; set; }

        public String SurveyName { get; set; }

        public String SurveyCode { get; set; }

        public Int64? ProjectId { get; set; }

        public Decimal? Latitude { get; set; }

        public Decimal? Longitude { get; set; }

        public String BuildingName { get; set; }
        
        public Boolean UpdateFlag { get; set; }
        
    }
}
