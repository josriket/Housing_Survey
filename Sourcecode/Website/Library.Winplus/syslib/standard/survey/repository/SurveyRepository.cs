using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey.model;

namespace Library.Winplus.syslib.standard.survey.repository
{
    public partial class SurveyRepository : SimpleRepository<Survey>
    {
        #region General Query

        public SurveyRepository()
        {
            SqlInsert = @"Insert Into [Survey]([SurveyName], [SurveyCode], [ProjectId], [Latitude], [Longitude], [BuildingName], [BuildingTypeId], [BuildingTypeOther], [FloorNoId], [FloorNoOther], [MaterialId], [MaterialOther], [ImageFront], [ImageBeside], [ImageRefer], [Width], [Long], [Height], [FarFromRefer], [UnitNo], [OwnershipId], [UpdateFlag], [DeleteFlag], [OwnerSystemUserId], [KeyInOn], [CreatedOn], [CreatedBy], [AdvantageId], [Advantage], [Remark], [NewPoint], [LocationTypeId], [PlaceOwner], [PlaceUserBenefic], [HomeNo], [Moo], [BoardTypeId], [BoardLocationId], [BoardStyleId], [BoardSide], [BoardText], [RoadTypeId], [RoadMaterialId], [RoadLaneId], [BridgeTypeId], [BridgeMaterialId], [PoleTypeId], [PoleMaterialId], [LandParcelNo], [LandNo], [LandRai], [LandNgan], [LandWa], [LandSubWa], [LandUsed], [LandParcelTypeIdSelected]) Values(@SurveyName, @SurveyCode, @ProjectId, @Latitude, @Longitude, @BuildingName, @BuildingTypeId, @BuildingTypeOther, @FloorNoId, @FloorNoOther, @MaterialId, @MaterialOther, @ImageFront, @ImageBeside, @ImageRefer, @Width, @Long, @Height, @FarFromRefer, @UnitNo, @OwnershipId, @UpdateFlag, @DeleteFlag, @OwnerSystemUserId, @KeyInOn, getdate(), @CreatedBy, @AdvantageId, @Advantage, @Remark, @NewPoint, @LocationTypeId, @PlaceOwner, @PlaceUserBenefic, @HomeNo, @Moo, @BoardTypeId, @BoardLocationId, @BoardStyleId, @BoardSide, @BoardText, @RoadTypeId, @RoadMaterialId, @RoadLaneId, @BridgeTypeId, @BridgeMaterialId, @PoleTypeId, @PoleMaterialId, @LandParcelNo, @LandNo, @LandRai, @LandNgan, @LandWa, @LandSubWa, @LandUsed, @LandParcelTypeIdSelected)";
            SqlUpdate = @"Update [Survey] Set [SurveyName]=@SurveyName, [SurveyCode]=@SurveyCode, [ProjectId]=@ProjectId, [Latitude]=@Latitude, [Longitude]=@Longitude, [BuildingName]=@BuildingName, [BuildingTypeId]=@BuildingTypeId, [BuildingTypeOther]=@BuildingTypeOther, [FloorNoId]=@FloorNoId, [FloorNoOther]=@FloorNoOther, [MaterialId]=@MaterialId, [MaterialOther]=@MaterialOther, [ImageFront]=@ImageFront, [ImageBeside]=@ImageBeside, [ImageRefer]=@ImageRefer, [Width]=@Width, [Long]=@Long, [Height]=@Height, [FarFromRefer]=@FarFromRefer, [UnitNo]=@UnitNo, [OwnershipId]=@OwnershipId, [UpdateFlag]=@UpdateFlag, [DeleteFlag]=@DeleteFlag, [OwnerSystemUserId]=@OwnerSystemUserId, [KeyInOn]=@KeyInOn, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy, [AdvantageId]=@AdvantageId, [Advantage]=@Advantage, [Remark]=@Remark, [NewPoint]=@NewPoint, [LocationTypeId]=@LocationTypeId, [PlaceOwner]=@PlaceOwner, [PlaceUserBenefic]=@PlaceUserBenefic, [HomeNo]=@HomeNo, [Moo]=@Moo, [BoardTypeId]=@BoardTypeId, [BoardLocationId]=@BoardLocationId, [BoardStyleId]=@BoardStyleId, [BoardSide]=@BoardSide, [BoardText]=@BoardText, [RoadTypeId]=@RoadTypeId, [RoadMaterialId]=@RoadMaterialId, [RoadLaneId]=@RoadLaneId, [BridgeTypeId]=@BridgeTypeId, [BridgeMaterialId]=@BridgeMaterialId, [PoleTypeId]=@PoleTypeId, [PoleMaterialId]=@PoleMaterialId, [LandParcelNo]=@LandParcelNo, [LandNo]=@LandNo, [LandRai]=@LandRai, [LandNgan]=@LandNgan, [LandWa]=@LandWa, [LandSubWa]=@LandSubWa, [LandUsed]=@LandUsed, [LandParcelTypeIdSelected]=@LandParcelTypeIdSelected Where [SurveyId]=@SurveyId";
            SqlDelete = @"Delete From [Survey] Where [SurveyId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Survey] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Survey> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Survey model in lstImport)
                {
                    if (model.SurveyId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Survey FindBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public List<Survey> FindsBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public int DeleteBySurveyId(DBConnection conn, Int64 SurveyId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [SurveyId]=@SurveyId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyId", SurveyId));
        }

        public Survey FindBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public List<Survey> FindsBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public int DeleteBySurveyName(DBConnection conn, String SurveyName, string whereText="")
        {
            String sql = "Delete From [Survey] Where [SurveyName]=@SurveyName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyName", SurveyName));
        }

        public Survey FindBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public List<Survey> FindsBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public int DeleteBySurveyCode(DBConnection conn, String SurveyCode, string whereText="")
        {
            String sql = "Delete From [Survey] Where [SurveyCode]=@SurveyCode " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyCode", SurveyCode));
        }

        public Survey FindByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public List<Survey> FindsByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public int DeleteByProjectId(DBConnection conn, Int64 ProjectId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [ProjectId]=@ProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectId", ProjectId));
        }

        public Survey FindByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public List<Survey> FindsByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public int DeleteByLatitude(DBConnection conn, Decimal Latitude, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Latitude]=@Latitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Latitude", Latitude));
        }

        public Survey FindByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public List<Survey> FindsByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public int DeleteByLongitude(DBConnection conn, Decimal Longitude, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Longitude]=@Longitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Longitude", Longitude));
        }

        public Survey FindByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public List<Survey> FindsByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public int DeleteByBuildingName(DBConnection conn, String BuildingName, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BuildingName]=@BuildingName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingName", BuildingName));
        }

        public Survey FindByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public List<Survey> FindsByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public int DeleteByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BuildingTypeId]=@BuildingTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public Survey FindByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public List<Survey> FindsByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public int DeleteByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BuildingTypeOther]=@BuildingTypeOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public Survey FindByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public List<Survey> FindsByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public int DeleteByFloorNoId(DBConnection conn, Int64 FloorNoId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [FloorNoId]=@FloorNoId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoId", FloorNoId));
        }

        public Survey FindByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public List<Survey> FindsByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public int DeleteByFloorNoOther(DBConnection conn, String FloorNoOther, string whereText="")
        {
            String sql = "Delete From [Survey] Where [FloorNoOther]=@FloorNoOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoOther", FloorNoOther));
        }

        public Survey FindByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public List<Survey> FindsByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public int DeleteByMaterialId(DBConnection conn, Int64 MaterialId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [MaterialId]=@MaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialId", MaterialId));
        }

        public Survey FindByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public List<Survey> FindsByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public int DeleteByMaterialOther(DBConnection conn, String MaterialOther, string whereText="")
        {
            String sql = "Delete From [Survey] Where [MaterialOther]=@MaterialOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialOther", MaterialOther));
        }

        public Survey FindByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public List<Survey> FindsByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public int DeleteByImageFront(DBConnection conn, String ImageFront, string whereText="")
        {
            String sql = "Delete From [Survey] Where [ImageFront]=@ImageFront " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageFront", ImageFront));
        }

        public Survey FindByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public List<Survey> FindsByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public int DeleteByImageBeside(DBConnection conn, String ImageBeside, string whereText="")
        {
            String sql = "Delete From [Survey] Where [ImageBeside]=@ImageBeside " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageBeside", ImageBeside));
        }

        public Survey FindByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public List<Survey> FindsByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public int DeleteByImageRefer(DBConnection conn, String ImageRefer, string whereText="")
        {
            String sql = "Delete From [Survey] Where [ImageRefer]=@ImageRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageRefer", ImageRefer));
        }

        public Survey FindByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Width]=@Width " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Width", Width));
        }

        public List<Survey> FindsByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Width]=@Width " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Width", Width));
        }

        public int DeleteByWidth(DBConnection conn, Decimal Width, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Width]=@Width " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Width", Width));
        }

        public Survey FindByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Long]=@Long " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Long", Long));
        }

        public List<Survey> FindsByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Long]=@Long " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Long", Long));
        }

        public int DeleteByLong(DBConnection conn, Decimal Long, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Long]=@Long " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Long", Long));
        }

        public Survey FindByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Height]=@Height " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Height", Height));
        }

        public List<Survey> FindsByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Height]=@Height " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Height", Height));
        }

        public int DeleteByHeight(DBConnection conn, Decimal Height, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Height]=@Height " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Height", Height));
        }

        public Survey FindByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public List<Survey> FindsByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public int DeleteByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string whereText="")
        {
            String sql = "Delete From [Survey] Where [FarFromRefer]=@FarFromRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FarFromRefer", FarFromRefer));
        }

        public Survey FindByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public List<Survey> FindsByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public int DeleteByUnitNo(DBConnection conn, Int32 UnitNo, string whereText="")
        {
            String sql = "Delete From [Survey] Where [UnitNo]=@UnitNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UnitNo", UnitNo));
        }

        public Survey FindByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public List<Survey> FindsByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public int DeleteByOwnershipId(DBConnection conn, Int64 OwnershipId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [OwnershipId]=@OwnershipId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnershipId", OwnershipId));
        }

        public Survey FindByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public List<Survey> FindsByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public int DeleteByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string whereText="")
        {
            String sql = "Delete From [Survey] Where [UpdateFlag]=@UpdateFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UpdateFlag", UpdateFlag));
        }

        public Survey FindByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public List<Survey> FindsByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public int DeleteByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string whereText="")
        {
            String sql = "Delete From [Survey] Where [DeleteFlag]=@DeleteFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DeleteFlag", DeleteFlag));
        }

        public Survey FindByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public List<Survey> FindsByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public int DeleteByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [OwnerSystemUserId]=@OwnerSystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public Survey FindByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public List<Survey> FindsByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public int DeleteByKeyInOn(DBConnection conn, DateTime KeyInOn, string whereText="")
        {
            String sql = "Delete From [Survey] Where [KeyInOn]=@KeyInOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("KeyInOn", KeyInOn));
        }

        public Survey FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Survey> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Survey] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Survey FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Survey> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Survey] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Survey FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Survey> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Survey] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Survey FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Survey> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Survey] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        public Survey FindByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public List<Survey> FindsByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public int DeleteByAdvantageId(DBConnection conn, Int64 AdvantageId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [AdvantageId]=@AdvantageId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("AdvantageId", AdvantageId));
        }

        public Survey FindByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public List<Survey> FindsByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public int DeleteByAdvantage(DBConnection conn, String Advantage, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Advantage]=@Advantage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Advantage", Advantage));
        }

        public Survey FindByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Remark]=@Remark " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Remark", Remark));
        }

        public List<Survey> FindsByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Remark]=@Remark " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Remark", Remark));
        }

        public int DeleteByRemark(DBConnection conn, String Remark, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Remark]=@Remark " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Remark", Remark));
        }

        public Survey FindByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public List<Survey> FindsByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public int DeleteByNewPoint(DBConnection conn, Boolean NewPoint, string whereText="")
        {
            String sql = "Delete From [Survey] Where [NewPoint]=@NewPoint " + whereText;
            return conn.executeUpdate(sql,new DBParameter("NewPoint", NewPoint));
        }

        public Survey FindByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LocationTypeId]=@LocationTypeId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LocationTypeId", LocationTypeId));
        }

        public List<Survey> FindsByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LocationTypeId]=@LocationTypeId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LocationTypeId", LocationTypeId));
        }

        public int DeleteByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LocationTypeId]=@LocationTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LocationTypeId", LocationTypeId));
        }

        public Survey FindByPlaceOwner(DBConnection conn, String PlaceOwner, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[PlaceOwner]=@PlaceOwner " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("PlaceOwner", PlaceOwner));
        }

        public List<Survey> FindsByPlaceOwner(DBConnection conn, String PlaceOwner, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[PlaceOwner]=@PlaceOwner " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("PlaceOwner", PlaceOwner));
        }

        public int DeleteByPlaceOwner(DBConnection conn, String PlaceOwner, string whereText="")
        {
            String sql = "Delete From [Survey] Where [PlaceOwner]=@PlaceOwner " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PlaceOwner", PlaceOwner));
        }

        public Survey FindByPlaceUserBenefic(DBConnection conn, String PlaceUserBenefic, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[PlaceUserBenefic]=@PlaceUserBenefic " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("PlaceUserBenefic", PlaceUserBenefic));
        }

        public List<Survey> FindsByPlaceUserBenefic(DBConnection conn, String PlaceUserBenefic, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[PlaceUserBenefic]=@PlaceUserBenefic " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("PlaceUserBenefic", PlaceUserBenefic));
        }

        public int DeleteByPlaceUserBenefic(DBConnection conn, String PlaceUserBenefic, string whereText="")
        {
            String sql = "Delete From [Survey] Where [PlaceUserBenefic]=@PlaceUserBenefic " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PlaceUserBenefic", PlaceUserBenefic));
        }

        public Survey FindByHomeNo(DBConnection conn, String HomeNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[HomeNo]=@HomeNo " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("HomeNo", HomeNo));
        }

        public List<Survey> FindsByHomeNo(DBConnection conn, String HomeNo, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[HomeNo]=@HomeNo " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("HomeNo", HomeNo));
        }

        public int DeleteByHomeNo(DBConnection conn, String HomeNo, string whereText="")
        {
            String sql = "Delete From [Survey] Where [HomeNo]=@HomeNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("HomeNo", HomeNo));
        }

        public Survey FindByMoo(DBConnection conn, String Moo, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[Moo]=@Moo " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("Moo", Moo));
        }

        public List<Survey> FindsByMoo(DBConnection conn, String Moo, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[Moo]=@Moo " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("Moo", Moo));
        }

        public int DeleteByMoo(DBConnection conn, String Moo, string whereText="")
        {
            String sql = "Delete From [Survey] Where [Moo]=@Moo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Moo", Moo));
        }

        public Survey FindByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BoardTypeId]=@BoardTypeId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BoardTypeId", BoardTypeId));
        }

        public List<Survey> FindsByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BoardTypeId]=@BoardTypeId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BoardTypeId", BoardTypeId));
        }

        public int DeleteByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BoardTypeId]=@BoardTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardTypeId", BoardTypeId));
        }

        public Survey FindByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BoardLocationId]=@BoardLocationId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BoardLocationId", BoardLocationId));
        }

        public List<Survey> FindsByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BoardLocationId]=@BoardLocationId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BoardLocationId", BoardLocationId));
        }

        public int DeleteByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BoardLocationId]=@BoardLocationId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardLocationId", BoardLocationId));
        }

        public Survey FindByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BoardStyleId]=@BoardStyleId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BoardStyleId", BoardStyleId));
        }

        public List<Survey> FindsByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BoardStyleId]=@BoardStyleId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BoardStyleId", BoardStyleId));
        }

        public int DeleteByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BoardStyleId]=@BoardStyleId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardStyleId", BoardStyleId));
        }

        public Survey FindByBoardSide(DBConnection conn, Int32 BoardSide, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BoardSide]=@BoardSide " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BoardSide", BoardSide));
        }

        public List<Survey> FindsByBoardSide(DBConnection conn, Int32 BoardSide, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BoardSide]=@BoardSide " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BoardSide", BoardSide));
        }

        public int DeleteByBoardSide(DBConnection conn, Int32 BoardSide, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BoardSide]=@BoardSide " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardSide", BoardSide));
        }

        public Survey FindByBoardText(DBConnection conn, String BoardText, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BoardText]=@BoardText " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BoardText", BoardText));
        }

        public List<Survey> FindsByBoardText(DBConnection conn, String BoardText, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BoardText]=@BoardText " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BoardText", BoardText));
        }

        public int DeleteByBoardText(DBConnection conn, String BoardText, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BoardText]=@BoardText " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardText", BoardText));
        }

        public Survey FindByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[RoadTypeId]=@RoadTypeId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("RoadTypeId", RoadTypeId));
        }

        public List<Survey> FindsByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[RoadTypeId]=@RoadTypeId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("RoadTypeId", RoadTypeId));
        }

        public int DeleteByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [RoadTypeId]=@RoadTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadTypeId", RoadTypeId));
        }

        public Survey FindByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[RoadMaterialId]=@RoadMaterialId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public List<Survey> FindsByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[RoadMaterialId]=@RoadMaterialId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public int DeleteByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [RoadMaterialId]=@RoadMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public Survey FindByRoadLaneId(DBConnection conn, Int64 RoadLaneId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[RoadLaneId]=@RoadLaneId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("RoadLaneId", RoadLaneId));
        }

        public List<Survey> FindsByRoadLaneId(DBConnection conn, Int64 RoadLaneId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[RoadLaneId]=@RoadLaneId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("RoadLaneId", RoadLaneId));
        }

        public int DeleteByRoadLaneId(DBConnection conn, Int64 RoadLaneId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [RoadLaneId]=@RoadLaneId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadLaneId", RoadLaneId));
        }

        public Survey FindByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BridgeTypeId]=@BridgeTypeId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public List<Survey> FindsByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BridgeTypeId]=@BridgeTypeId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public int DeleteByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BridgeTypeId]=@BridgeTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public Survey FindByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[BridgeMaterialId]=@BridgeMaterialId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public List<Survey> FindsByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[BridgeMaterialId]=@BridgeMaterialId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public int DeleteByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [BridgeMaterialId]=@BridgeMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public Survey FindByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[PoleTypeId]=@PoleTypeId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("PoleTypeId", PoleTypeId));
        }

        public List<Survey> FindsByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[PoleTypeId]=@PoleTypeId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("PoleTypeId", PoleTypeId));
        }

        public int DeleteByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [PoleTypeId]=@PoleTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleTypeId", PoleTypeId));
        }

        public Survey FindByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[PoleMaterialId]=@PoleMaterialId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public List<Survey> FindsByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[PoleMaterialId]=@PoleMaterialId " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public int DeleteByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string whereText="")
        {
            String sql = "Delete From [Survey] Where [PoleMaterialId]=@PoleMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public Survey FindByLandParcelNo(DBConnection conn, String LandParcelNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandParcelNo]=@LandParcelNo " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandParcelNo", LandParcelNo));
        }

        public List<Survey> FindsByLandParcelNo(DBConnection conn, String LandParcelNo, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandParcelNo]=@LandParcelNo " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandParcelNo", LandParcelNo));
        }

        public int DeleteByLandParcelNo(DBConnection conn, String LandParcelNo, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandParcelNo]=@LandParcelNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandParcelNo", LandParcelNo));
        }

        public Survey FindByLandNo(DBConnection conn, String LandNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandNo]=@LandNo " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandNo", LandNo));
        }

        public List<Survey> FindsByLandNo(DBConnection conn, String LandNo, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandNo]=@LandNo " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandNo", LandNo));
        }

        public int DeleteByLandNo(DBConnection conn, String LandNo, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandNo]=@LandNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandNo", LandNo));
        }

        public Survey FindByLandRai(DBConnection conn, Int32 LandRai, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandRai]=@LandRai " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandRai", LandRai));
        }

        public List<Survey> FindsByLandRai(DBConnection conn, Int32 LandRai, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandRai]=@LandRai " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandRai", LandRai));
        }

        public int DeleteByLandRai(DBConnection conn, Int32 LandRai, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandRai]=@LandRai " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandRai", LandRai));
        }

        public Survey FindByLandNgan(DBConnection conn, Int32 LandNgan, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandNgan]=@LandNgan " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandNgan", LandNgan));
        }

        public List<Survey> FindsByLandNgan(DBConnection conn, Int32 LandNgan, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandNgan]=@LandNgan " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandNgan", LandNgan));
        }

        public int DeleteByLandNgan(DBConnection conn, Int32 LandNgan, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandNgan]=@LandNgan " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandNgan", LandNgan));
        }

        public Survey FindByLandWa(DBConnection conn, Int32 LandWa, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandWa]=@LandWa " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandWa", LandWa));
        }

        public List<Survey> FindsByLandWa(DBConnection conn, Int32 LandWa, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandWa]=@LandWa " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandWa", LandWa));
        }

        public int DeleteByLandWa(DBConnection conn, Int32 LandWa, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandWa]=@LandWa " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandWa", LandWa));
        }

        public Survey FindByLandSubWa(DBConnection conn, Decimal LandSubWa, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandSubWa]=@LandSubWa " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandSubWa", LandSubWa));
        }

        public List<Survey> FindsByLandSubWa(DBConnection conn, Decimal LandSubWa, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandSubWa]=@LandSubWa " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandSubWa", LandSubWa));
        }

        public int DeleteByLandSubWa(DBConnection conn, Decimal LandSubWa, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandSubWa]=@LandSubWa " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandSubWa", LandSubWa));
        }

        public Survey FindByLandUsed(DBConnection conn, String LandUsed, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandUsed]=@LandUsed " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandUsed", LandUsed));
        }

        public List<Survey> FindsByLandUsed(DBConnection conn, String LandUsed, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandUsed]=@LandUsed " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandUsed", LandUsed));
        }

        public int DeleteByLandUsed(DBConnection conn, String LandUsed, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandUsed]=@LandUsed " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandUsed", LandUsed));
        }

        public Survey FindByLandParcelTypeIdSelected(DBConnection conn, String LandParcelTypeIdSelected, string orderBy="")
        {
            String sql = "Select Top 1 Survey.* From [Survey] Survey Where Survey.[LandParcelTypeIdSelected]=@LandParcelTypeIdSelected " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("LandParcelTypeIdSelected", LandParcelTypeIdSelected));
        }

        public List<Survey> FindsByLandParcelTypeIdSelected(DBConnection conn, String LandParcelTypeIdSelected, string orderBy="")
        {
            String sql = "Select Survey.* From [Survey] Survey Where Survey.[LandParcelTypeIdSelected]=@LandParcelTypeIdSelected " + orderBy;

            return QueryUtil.FindList<Survey>(conn, sql, new DBParameter("LandParcelTypeIdSelected", LandParcelTypeIdSelected));
        }

        public int DeleteByLandParcelTypeIdSelected(DBConnection conn, String LandParcelTypeIdSelected, string whereText="")
        {
            String sql = "Delete From [Survey] Where [LandParcelTypeIdSelected]=@LandParcelTypeIdSelected " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandParcelTypeIdSelected", LandParcelTypeIdSelected));
        }

        #endregion

    }
}
