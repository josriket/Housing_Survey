using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.survey.model;

namespace Library.Winplus.syslib.standard.survey.repository
{
    public partial class SurveyRepository
    {
        public string NewSurveyCode(DBConnection conn, long projectId)
        {
            string sql = $@"Select Max(cast(SurveyCode as bigint)) SurveyCode From Survey_{projectId} With(nolock)";
            object code = conn.executeScalar(sql);
            if (code == null || code == DBNull.Value)
            {
                return "1";
            }
            return (Convert.ToInt64(code) + 1).ToString();
        }

        public void UpdateImage(DBConnection conn, SurveyImageUpload model)
        {
            string sql = $@"Update Survey_{model.ProjectId} Set Image{model.ImageType}=@ImageUploadName Where SurveyId=@SurveyId";
            conn.executeUpdate(sql
                , new DBParameter("SurveyId", model.SurveyId)
                , new DBParameter("ImageUploadName", model.ImageUploadName)
                );
        }

        public override Int64? Create(DBConnection conn, Survey model)
        {
            SqlInsert = SqlInsert.Replace("[Survey]", "[Survey_{model.ProjectId}]");
            return QueryUtil.Create(conn, SqlInsert, model);
        }

        public override void Update(DBConnection conn, Survey model)
        {
            SqlUpdate = SqlUpdate.Replace("[Survey]", $"[Survey_{model.ProjectId}]");
            QueryUtil.Update(conn, SqlUpdate, model);
        }

        public void ModifyScript(Int64 ProjectId)
        {
            //SqlInsert = @"Insert Into [Survey]([SurveyName], [SurveyCode], [ProjectId], [Latitude], [Longitude], [BuildingName], [Advantage], [BuildingTypeId], [BuildingTypeOther], [FloorNoId], [FloorNoOther], [MaterialId], [MaterialOther], [ImageFront], [ImageBeside], [ImageRefer], [Width], [Long], [Height], [FarFromRefer], [UnitNo], [OwnershipId], [UpdateFlag], [DeleteFlag], [OwnerSystemUserId], [KeyInOn], [CreatedOn], [CreatedBy]) Values(@SurveyName, @SurveyCode, @ProjectId, @Latitude, @Longitude, @BuildingName, @Advantage, @BuildingTypeId, @BuildingTypeOther, @FloorNoId, @FloorNoOther, @MaterialId, @MaterialOther, @ImageFront, @ImageBeside, @ImageRefer, @Width, @Long, @Height, @FarFromRefer, @UnitNo, @OwnershipId, @UpdateFlag, @DeleteFlag, @OwnerSystemUserId, @KeyInOn, getdate(), @CreatedBy)";
            SqlInsert = SqlInsert.Replace("[Survey]", $"[Survey_{ProjectId}]");
            SqlUpdate = SqlUpdate.Replace("[Survey]", $"[Survey_{ProjectId}]");
        }

        public Survey FindBySurveyId(DBConnection conn, Int64 ProjectId, Int64 SurveyId, string orderBy = "")
        {
            String sql = $"Select Top 1 Survey.* From [Survey_{ProjectId}] Survey Where Survey.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.Find<Survey>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public void SetTotalSurvey(DBConnection conn, LoginUser loginUser, Project project)
        {
            string sql = $@"Select 
                             Count(1) TotalSurvey
                            ,Sum(Case When UpdateFlag=0 Then 1 Else 0 End) TotalInprocess
                            ,Sum(Case When UpdateFlag=0 Then 0 Else 1 End) TotalCompleted
                           From Survey_{project.ProjectId} 
                           Where ProjectId=@ProjectId
                           And DeleteFlag=0
                           Group By ProjectId";
            DataSet ds = conn.executeDataSet(sql, new DBParameter("ProjectId", project.ProjectId));
            if (DataSetUtil.IsNotEmpty(ds))
            {
                project.TotalSurvey = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalSurvey"]);
                project.TotalInprocess = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalInprocess"]);
                project.TotalCompleted = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalCompleted"]);
            }
        }

        public List<SurveyResult> MobileSearch(DBConnection conn, LoginUser loginUser, SurveyCriteria model)
        {
            List<DBParameter> lstParam = new List<DBParameter>();
            string sql = $@"Select Top 5000 Survey.* From  Survey_{model.ProjectId} Survey Where DeleteFlag=0";

            if (model.ProjectId.HasValue)
            {
                sql += " And ProjectId=@ProjectId";
                lstParam.Add(new DBParameter("ProjectId", model.ProjectId));
            }

            if (!string.IsNullOrEmpty(model.SurveyCode))
            {
                sql += " And SurveyCode=@SurveyCode";
                lstParam.Add(new DBParameter("SurveyCode", model.SurveyCode));
            }

            if(!string.IsNullOrEmpty(model.LocationTypes))
            {
                sql += $" And LocationTypeId in({model.LocationTypes})";
            }

            if(model.StartingLatitude.HasValue && model.StartingLongitude.HasValue)
            {
                if (!model.MaxDistance.HasValue) model.MaxDistance = SystemConfigService.GetIntConfig("DefaultSearchRadius", 1000); //Kilomet
                sql += " And abs(Survey.Latitude-@StartingLatitude)<@MaxDistance";
                sql += " And abs(Survey.Longitude-@StartingLongitude)<@MaxDistance";

                lstParam.Add(new DBParameter("StartingLatitude", model.StartingLatitude));
                lstParam.Add(new DBParameter("StartingLongitude", model.StartingLongitude));
                lstParam.Add(new DBParameter("MaxDistance", Convert.ToDecimal(model.MaxDistance) / 1000000));

                //if (!model.MaxDistance.HasValue) model.MaxDistance = SystemConfigService.GetIntConfig("DefaultSearchRadius",1000); //Kilomet
                //sql += " And dbo.GetDistanceBetween(@StartingLatitude, @StartingLongitude, latitude, longitude) <= @MaxDistance;";
                //lstParam.Add(new DBParameter("StartingLatitude", model.StartingLatitude));
                //lstParam.Add(new DBParameter("StartingLongitude", model.StartingLongitude));
                //lstParam.Add(new DBParameter("MaxDistance", model.MaxDistance));
            }

            return QueryUtil.FindList<SurveyResult>(conn, sql, lstParam.ToArray());
        }
    }
}
