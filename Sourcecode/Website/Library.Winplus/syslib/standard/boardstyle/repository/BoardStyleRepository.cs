using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.boardstyle.model;

namespace Library.Winplus.syslib.standard.boardstyle.repository
{
    public partial class BoardStyleRepository : SimpleRepository<BoardStyle>
    {
        #region General Query

        public BoardStyleRepository()
        {
            SqlInsert = @"Insert Into [BoardStyle]([BoardStyleName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@BoardStyleName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BoardStyle] Set [BoardStyleName]=@BoardStyleName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BoardStyleId]=@BoardStyleId";
            SqlDelete = @"Delete From [BoardStyle] Where [BoardStyleId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BoardStyle] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BoardStyle> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BoardStyle model in lstImport)
                {
                    if (model.BoardStyleId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BoardStyle FindByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[BoardStyleId]=@BoardStyleId " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("BoardStyleId", BoardStyleId));
        }

        public List<BoardStyle> FindsByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[BoardStyleId]=@BoardStyleId " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("BoardStyleId", BoardStyleId));
        }

        public int DeleteByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [BoardStyleId]=@BoardStyleId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardStyleId", BoardStyleId));
        }

        public BoardStyle FindByBoardStyleName(DBConnection conn, String BoardStyleName, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[BoardStyleName]=@BoardStyleName " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("BoardStyleName", BoardStyleName));
        }

        public List<BoardStyle> FindsByBoardStyleName(DBConnection conn, String BoardStyleName, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[BoardStyleName]=@BoardStyleName " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("BoardStyleName", BoardStyleName));
        }

        public int DeleteByBoardStyleName(DBConnection conn, String BoardStyleName, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [BoardStyleName]=@BoardStyleName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardStyleName", BoardStyleName));
        }

        public BoardStyle FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<BoardStyle> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public BoardStyle FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BoardStyle> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BoardStyle FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BoardStyle> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BoardStyle FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BoardStyle> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BoardStyle FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BoardStyle>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BoardStyle> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BoardStyle.* From [BoardStyle] BoardStyle Where BoardStyle.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BoardStyle>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BoardStyle] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
