using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.bridgetype.model;

namespace Library.Winplus.syslib.standard.bridgetype.repository
{
    public partial class BridgeTypeRepository : SimpleRepository<BridgeType>
    {
        #region General Query

        public BridgeTypeRepository()
        {
            SqlInsert = @"Insert Into [BridgeType]([BridgeTypeName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@BridgeTypeName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BridgeType] Set [BridgeTypeName]=@BridgeTypeName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BridgeTypeId]=@BridgeTypeId";
            SqlDelete = @"Delete From [BridgeType] Where [BridgeTypeId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BridgeType] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BridgeType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BridgeType model in lstImport)
                {
                    if (model.BridgeTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BridgeType FindByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[BridgeTypeId]=@BridgeTypeId " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public List<BridgeType> FindsByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[BridgeTypeId]=@BridgeTypeId " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public int DeleteByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [BridgeTypeId]=@BridgeTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public BridgeType FindByBridgeTypeName(DBConnection conn, String BridgeTypeName, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[BridgeTypeName]=@BridgeTypeName " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("BridgeTypeName", BridgeTypeName));
        }

        public List<BridgeType> FindsByBridgeTypeName(DBConnection conn, String BridgeTypeName, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[BridgeTypeName]=@BridgeTypeName " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("BridgeTypeName", BridgeTypeName));
        }

        public int DeleteByBridgeTypeName(DBConnection conn, String BridgeTypeName, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [BridgeTypeName]=@BridgeTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeTypeName", BridgeTypeName));
        }

        public BridgeType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<BridgeType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public BridgeType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BridgeType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BridgeType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BridgeType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BridgeType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BridgeType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BridgeType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BridgeType.* From [BridgeType] BridgeType Where BridgeType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BridgeType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BridgeType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BridgeType.* From [BridgeType] BridgeType Where BridgeType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BridgeType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BridgeType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
