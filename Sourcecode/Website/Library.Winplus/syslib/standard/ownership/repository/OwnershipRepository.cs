using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.ownership.model;

namespace Library.Winplus.syslib.standard.ownership.repository
{
    public partial class OwnershipRepository : SimpleRepository<Ownership>
    {
        #region General Query

        public OwnershipRepository()
        {
            SqlInsert = @"Insert Into [Ownership]([OwnershipName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@OwnershipName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [Ownership] Set [OwnershipName]=@OwnershipName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [OwnershipId]=@OwnershipId";
            SqlDelete = @"Delete From [Ownership] Where [OwnershipId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Ownership] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Ownership> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Ownership model in lstImport)
                {
                    if (model.OwnershipId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Ownership FindByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public List<Ownership> FindsByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public int DeleteByOwnershipId(DBConnection conn, Int64 OwnershipId, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [OwnershipId]=@OwnershipId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnershipId", OwnershipId));
        }

        public Ownership FindByOwnershipName(DBConnection conn, String OwnershipName, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[OwnershipName]=@OwnershipName " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("OwnershipName", OwnershipName));
        }

        public List<Ownership> FindsByOwnershipName(DBConnection conn, String OwnershipName, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[OwnershipName]=@OwnershipName " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("OwnershipName", OwnershipName));
        }

        public int DeleteByOwnershipName(DBConnection conn, String OwnershipName, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [OwnershipName]=@OwnershipName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnershipName", OwnershipName));
        }

        public Ownership FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<Ownership> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public Ownership FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Ownership> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Ownership FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Ownership> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Ownership FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Ownership> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Ownership FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Ownership.* From [Ownership] Ownership Where Ownership.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Ownership>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Ownership> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Ownership.* From [Ownership] Ownership Where Ownership.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Ownership>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Ownership] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
