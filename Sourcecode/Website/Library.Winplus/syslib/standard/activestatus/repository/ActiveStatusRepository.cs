using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.activestatus.model;

namespace Library.Winplus.syslib.standard.activestatus.repository
{
    public partial class ActiveStatusRepository : SimpleRepository<ActiveStatus>
    {
        #region General Query

        public ActiveStatusRepository()
        {
            SqlInsert = @"Insert Into [ActiveStatus]([ActiveStatusName], [CreatedOn], [CreatedBy]) Values(@ActiveStatusName, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [ActiveStatus] Set [ActiveStatusName]=@ActiveStatusName, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [ActiveStatusId]=@ActiveStatusId";
            SqlDelete = @"Delete From [ActiveStatus] Where [ActiveStatusId]=@Id";
            SqlActiveStatus = @"Update [ActiveStatus] Set [ActiveStatusId]=@ActiveStatusId Where [ActiveStatusId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [ActiveStatus] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<ActiveStatus> lstImport)
        {
            if (lstImport != null)
            {
                foreach (ActiveStatus model in lstImport)
                {
                    if (model.ActiveStatusId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public ActiveStatus FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<ActiveStatus>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<ActiveStatus> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<ActiveStatus>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [ActiveStatus] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public ActiveStatus FindByActiveStatusName(DBConnection conn, String ActiveStatusName, string orderBy="")
        {
            String sql = "Select Top 1 ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ActiveStatusName]=@ActiveStatusName " + orderBy;

            return QueryUtil.Find<ActiveStatus>(conn, sql, new DBParameter("ActiveStatusName", ActiveStatusName));
        }

        public List<ActiveStatus> FindsByActiveStatusName(DBConnection conn, String ActiveStatusName, string orderBy="")
        {
            String sql = "Select ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ActiveStatusName]=@ActiveStatusName " + orderBy;

            return QueryUtil.FindList<ActiveStatus>(conn, sql, new DBParameter("ActiveStatusName", ActiveStatusName));
        }

        public int DeleteByActiveStatusName(DBConnection conn, String ActiveStatusName, string whereText="")
        {
            String sql = "Delete From [ActiveStatus] Where [ActiveStatusName]=@ActiveStatusName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusName", ActiveStatusName));
        }

        public ActiveStatus FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<ActiveStatus>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<ActiveStatus> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<ActiveStatus>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [ActiveStatus] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public ActiveStatus FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<ActiveStatus>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<ActiveStatus> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<ActiveStatus>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [ActiveStatus] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public ActiveStatus FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<ActiveStatus>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<ActiveStatus> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<ActiveStatus>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [ActiveStatus] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public ActiveStatus FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<ActiveStatus>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<ActiveStatus> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select ActiveStatus.* From [ActiveStatus] ActiveStatus Where ActiveStatus.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<ActiveStatus>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [ActiveStatus] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
