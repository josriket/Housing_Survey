using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;

namespace Library.Winplus.syslib.standard.activestatus.model
{
    public partial class ActiveStatus
    {
        public static readonly Int64 Active = 1;
        public static readonly Int64 Inactive = 2;
    }
}
