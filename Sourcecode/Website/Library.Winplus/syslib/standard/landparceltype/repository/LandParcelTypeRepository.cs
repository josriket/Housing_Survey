using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.landparceltype.model;

namespace Library.Winplus.syslib.standard.landparceltype.repository
{
    public partial class LandParcelTypeRepository : SimpleRepository<LandParcelType>
    {
        #region General Query

        public LandParcelTypeRepository()
        {
            SqlInsert = @"Insert Into [LandParcelType]([LandParcelTypeName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@LandParcelTypeName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [LandParcelType] Set [LandParcelTypeName]=@LandParcelTypeName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [LandParcelTypeId]=@LandParcelTypeId";
            SqlDelete = @"Delete From [LandParcelType] Where [LandParcelTypeId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [LandParcelType] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<LandParcelType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (LandParcelType model in lstImport)
                {
                    if (model.LandParcelTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public LandParcelType FindByLandParcelTypeId(DBConnection conn, Int64 LandParcelTypeId, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[LandParcelTypeId]=@LandParcelTypeId " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("LandParcelTypeId", LandParcelTypeId));
        }

        public List<LandParcelType> FindsByLandParcelTypeId(DBConnection conn, Int64 LandParcelTypeId, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[LandParcelTypeId]=@LandParcelTypeId " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("LandParcelTypeId", LandParcelTypeId));
        }

        public int DeleteByLandParcelTypeId(DBConnection conn, Int64 LandParcelTypeId, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [LandParcelTypeId]=@LandParcelTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandParcelTypeId", LandParcelTypeId));
        }

        public LandParcelType FindByLandParcelTypeName(DBConnection conn, String LandParcelTypeName, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[LandParcelTypeName]=@LandParcelTypeName " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("LandParcelTypeName", LandParcelTypeName));
        }

        public List<LandParcelType> FindsByLandParcelTypeName(DBConnection conn, String LandParcelTypeName, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[LandParcelTypeName]=@LandParcelTypeName " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("LandParcelTypeName", LandParcelTypeName));
        }

        public int DeleteByLandParcelTypeName(DBConnection conn, String LandParcelTypeName, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [LandParcelTypeName]=@LandParcelTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandParcelTypeName", LandParcelTypeName));
        }

        public LandParcelType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<LandParcelType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public LandParcelType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<LandParcelType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public LandParcelType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<LandParcelType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public LandParcelType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<LandParcelType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public LandParcelType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<LandParcelType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<LandParcelType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select LandParcelType.* From [LandParcelType] LandParcelType Where LandParcelType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<LandParcelType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [LandParcelType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
