using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.roadmaterial.model;

namespace Library.Winplus.syslib.standard.roadmaterial.repository
{
    public partial class RoadMaterialRepository : SimpleRepository<RoadMaterial>
    {
        #region General Query

        public RoadMaterialRepository()
        {
            SqlInsert = @"Insert Into [RoadMaterial]([RoadMaterialName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@RoadMaterialName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [RoadMaterial] Set [RoadMaterialName]=@RoadMaterialName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [RoadMaterialId]=@RoadMaterialId";
            SqlDelete = @"Delete From [RoadMaterial] Where [RoadMaterialId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [RoadMaterial] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<RoadMaterial> lstImport)
        {
            if (lstImport != null)
            {
                foreach (RoadMaterial model in lstImport)
                {
                    if (model.RoadMaterialId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public RoadMaterial FindByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[RoadMaterialId]=@RoadMaterialId " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public List<RoadMaterial> FindsByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[RoadMaterialId]=@RoadMaterialId " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public int DeleteByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [RoadMaterialId]=@RoadMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public RoadMaterial FindByRoadMaterialName(DBConnection conn, String RoadMaterialName, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[RoadMaterialName]=@RoadMaterialName " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("RoadMaterialName", RoadMaterialName));
        }

        public List<RoadMaterial> FindsByRoadMaterialName(DBConnection conn, String RoadMaterialName, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[RoadMaterialName]=@RoadMaterialName " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("RoadMaterialName", RoadMaterialName));
        }

        public int DeleteByRoadMaterialName(DBConnection conn, String RoadMaterialName, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [RoadMaterialName]=@RoadMaterialName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadMaterialName", RoadMaterialName));
        }

        public RoadMaterial FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<RoadMaterial> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public RoadMaterial FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<RoadMaterial> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public RoadMaterial FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<RoadMaterial> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public RoadMaterial FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<RoadMaterial> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public RoadMaterial FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<RoadMaterial>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<RoadMaterial> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select RoadMaterial.* From [RoadMaterial] RoadMaterial Where RoadMaterial.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<RoadMaterial>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [RoadMaterial] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
