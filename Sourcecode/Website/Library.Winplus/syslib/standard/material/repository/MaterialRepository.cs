using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.material.model;

namespace Library.Winplus.syslib.standard.material.repository
{
    public partial class MaterialRepository : SimpleRepository<Material>
    {
        #region General Query

        public MaterialRepository()
        {
            SqlInsert = @"Insert Into [Material]([MaterialName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@MaterialName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [Material] Set [MaterialName]=@MaterialName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [MaterialId]=@MaterialId";
            SqlDelete = @"Delete From [Material] Where [MaterialId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Material] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Material> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Material model in lstImport)
                {
                    if (model.MaterialId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Material FindByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public List<Material> FindsByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public int DeleteByMaterialId(DBConnection conn, Int64 MaterialId, string whereText="")
        {
            String sql = "Delete From [Material] Where [MaterialId]=@MaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialId", MaterialId));
        }

        public Material FindByMaterialName(DBConnection conn, String MaterialName, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[MaterialName]=@MaterialName " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("MaterialName", MaterialName));
        }

        public List<Material> FindsByMaterialName(DBConnection conn, String MaterialName, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[MaterialName]=@MaterialName " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("MaterialName", MaterialName));
        }

        public int DeleteByMaterialName(DBConnection conn, String MaterialName, string whereText="")
        {
            String sql = "Delete From [Material] Where [MaterialName]=@MaterialName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialName", MaterialName));
        }

        public Material FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<Material> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [Material] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public Material FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Material> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Material] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Material FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Material> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Material] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Material FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Material> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Material] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Material FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Material.* From [Material] Material Where Material.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Material>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Material> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Material.* From [Material] Material Where Material.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Material>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Material] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
