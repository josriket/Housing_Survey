using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemuser.model;

namespace Library.Winplus.syslib.standard.systemuser.repository
{
    public partial class SystemUserRepository : SimpleRepository<SystemUser>
    {
        #region General Query

        public SystemUserRepository()
        {
            SqlInsert = @"Insert Into [SystemUser]([SystemUserName], [SystemRoleId], [UserName], [Password], [FirstName], [LastName], [Email], [PhoneNo], [DefaultMenuUrl], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@SystemUserName, @SystemRoleId, @UserName, @Password, @FirstName, @LastName, @Email, @PhoneNo, @DefaultMenuUrl, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SystemUser] Set [SystemUserName]=@SystemUserName, [SystemRoleId]=@SystemRoleId, [UserName]=@UserName, [Password]=@Password, [FirstName]=@FirstName, [LastName]=@LastName, [Email]=@Email, [PhoneNo]=@PhoneNo, [DefaultMenuUrl]=@DefaultMenuUrl, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SystemUserId]=@SystemUserId";
            SqlDelete = @"Delete From [SystemUser] Where [SystemUserId]=@Id";
            SqlActiveStatus = @"Update [SystemUser] Set [ActiveStatusId]=@ActiveStatusId Where [SystemUserId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SystemUser] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SystemUser> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SystemUser model in lstImport)
                {
                    if (model.SystemUserId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SystemUser FindBySystemUserId(DBConnection conn, Int64 SystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[SystemUserId]=@SystemUserId " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("SystemUserId", SystemUserId));
        }

        public List<SystemUser> FindsBySystemUserId(DBConnection conn, Int64 SystemUserId, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[SystemUserId]=@SystemUserId " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("SystemUserId", SystemUserId));
        }

        public int DeleteBySystemUserId(DBConnection conn, Int64 SystemUserId, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [SystemUserId]=@SystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemUserId", SystemUserId));
        }

        public SystemUser FindBySystemUserName(DBConnection conn, String SystemUserName, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[SystemUserName]=@SystemUserName " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("SystemUserName", SystemUserName));
        }

        public List<SystemUser> FindsBySystemUserName(DBConnection conn, String SystemUserName, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[SystemUserName]=@SystemUserName " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("SystemUserName", SystemUserName));
        }

        public int DeleteBySystemUserName(DBConnection conn, String SystemUserName, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [SystemUserName]=@SystemUserName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemUserName", SystemUserName));
        }

        public SystemUser FindBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[SystemRoleId]=@SystemRoleId " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("SystemRoleId", SystemRoleId));
        }

        public List<SystemUser> FindsBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[SystemRoleId]=@SystemRoleId " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("SystemRoleId", SystemRoleId));
        }

        public int DeleteBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [SystemRoleId]=@SystemRoleId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemRoleId", SystemRoleId));
        }

        public SystemUser FindByUserName(DBConnection conn, String UserName, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[UserName]=@UserName " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("UserName", UserName));
        }

        public List<SystemUser> FindsByUserName(DBConnection conn, String UserName, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[UserName]=@UserName " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("UserName", UserName));
        }

        public int DeleteByUserName(DBConnection conn, String UserName, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [UserName]=@UserName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UserName", UserName));
        }

        public SystemUser FindByPassword(DBConnection conn, String Password, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[Password]=@Password " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("Password", Password));
        }

        public List<SystemUser> FindsByPassword(DBConnection conn, String Password, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[Password]=@Password " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("Password", Password));
        }

        public int DeleteByPassword(DBConnection conn, String Password, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [Password]=@Password " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Password", Password));
        }

        public SystemUser FindByFirstName(DBConnection conn, String FirstName, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[FirstName]=@FirstName " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("FirstName", FirstName));
        }

        public List<SystemUser> FindsByFirstName(DBConnection conn, String FirstName, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[FirstName]=@FirstName " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("FirstName", FirstName));
        }

        public int DeleteByFirstName(DBConnection conn, String FirstName, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [FirstName]=@FirstName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FirstName", FirstName));
        }

        public SystemUser FindByLastName(DBConnection conn, String LastName, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[LastName]=@LastName " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("LastName", LastName));
        }

        public List<SystemUser> FindsByLastName(DBConnection conn, String LastName, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[LastName]=@LastName " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("LastName", LastName));
        }

        public int DeleteByLastName(DBConnection conn, String LastName, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [LastName]=@LastName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LastName", LastName));
        }

        public SystemUser FindByEmail(DBConnection conn, String Email, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[Email]=@Email " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("Email", Email));
        }

        public List<SystemUser> FindsByEmail(DBConnection conn, String Email, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[Email]=@Email " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("Email", Email));
        }

        public int DeleteByEmail(DBConnection conn, String Email, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [Email]=@Email " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Email", Email));
        }

        public SystemUser FindByPhoneNo(DBConnection conn, String PhoneNo, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[PhoneNo]=@PhoneNo " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("PhoneNo", PhoneNo));
        }

        public List<SystemUser> FindsByPhoneNo(DBConnection conn, String PhoneNo, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[PhoneNo]=@PhoneNo " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("PhoneNo", PhoneNo));
        }

        public int DeleteByPhoneNo(DBConnection conn, String PhoneNo, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [PhoneNo]=@PhoneNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PhoneNo", PhoneNo));
        }

        public SystemUser FindByDefaultMenuUrl(DBConnection conn, String DefaultMenuUrl, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[DefaultMenuUrl]=@DefaultMenuUrl " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("DefaultMenuUrl", DefaultMenuUrl));
        }

        public List<SystemUser> FindsByDefaultMenuUrl(DBConnection conn, String DefaultMenuUrl, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[DefaultMenuUrl]=@DefaultMenuUrl " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("DefaultMenuUrl", DefaultMenuUrl));
        }

        public int DeleteByDefaultMenuUrl(DBConnection conn, String DefaultMenuUrl, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [DefaultMenuUrl]=@DefaultMenuUrl " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DefaultMenuUrl", DefaultMenuUrl));
        }

        public SystemUser FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<SystemUser> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public SystemUser FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SystemUser> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SystemUser FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SystemUser> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SystemUser FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SystemUser> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SystemUser FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemUser.* From [SystemUser] SystemUser Where SystemUser.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SystemUser> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SystemUser.* From [SystemUser] SystemUser Where SystemUser.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SystemUser>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SystemUser] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
