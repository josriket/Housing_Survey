using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.core.exception;

namespace Library.Winplus.syslib.standard.systemuser.repository
{
    public partial class SystemUserRepository
    {
        public void EncrypAll()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                string sql = @"Select SystemUserId,Password,UserName from SystemUser Where Len(Password)<=10";
                List<SystemUser> lstSystemUser = QueryUtil.FindList<SystemUser>(conn, sql);


                sql = "Update SystemUser Set Password=@Password where SystemUserId=@SystemUserId";
                foreach (SystemUser user in lstSystemUser)
                {
                    string Password = EncriptionUtil.EncrypOneWay(user.UserName.ToUpper() + user.Password);
                    conn.executeUpdate(sql
                        , new DBParameter("Password", Password)
                        , new DBParameter("SystemUserId", user.SystemUserId)
                        );
                }
            }
        }

        public void UpdateProfile(DBConnection conn, LoginUser loginUser, SystemUser model)
        {
            SystemUser currentUser = new SystemUserRepository().FindById(conn, loginUser.SystemUserId.Value);

            if (!string.IsNullOrEmpty(model.Password))
            {
                int? MinimumOfPasswordLength=SystemConfigService.GetIntConfig("MinimumOfPasswordLength",4);

                if (model.Password.Length < MinimumOfPasswordLength)
                {
                    throw new InvalidDataException(MyResources.Winplus.Resource.PleaseEnterPasswordMore8Digit);
                }
                currentUser.Password = EncriptionUtil.EncrypOneWay(currentUser.UserName.ToUpper() + model.Password);
            }

            QueryUtil.Update(conn, SqlUpdate, currentUser);
        }
    }
}
