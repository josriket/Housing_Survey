using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemuser.model;

namespace Library.Winplus.syslib.standard.systemuser.search
{
    public partial class SystemUserSearch : SimpleSearch<SystemUser>
    {
        public SystemUserSearch()
        {
            search = new SystemUser();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SystemUserId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select SystemUser.*
                 , SystemRole_SystemRoleId.[SystemRoleName] as [SystemRoleIdSystemRoleName]
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [SystemUser] SystemUser
                Left Join [SystemRole] SystemRole_SystemRoleId on SystemRole_SystemRoleId.[SystemRoleId]=SystemUser.[SystemRoleId]
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=SystemUser.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=SystemUser.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=SystemUser.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (SystemUser.[SystemUserName] like '%'+@SearchText+'%' or SystemRole_SystemRoleId.[SystemRoleName] like '%'+@SearchText+'%' or SystemUser.[UserName] like '%'+@SearchText+'%' or SystemUser.[Password] like '%'+@SearchText+'%' or SystemUser.[FirstName] like '%'+@SearchText+'%' or SystemUser.[LastName] like '%'+@SearchText+'%' or SystemUser.[Email] like '%'+@SearchText+'%' or SystemUser.[PhoneNo] like '%'+@SearchText+'%' or SystemUser.[DefaultMenuUrl] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SystemUserId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemUserId", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[SystemUserId] = @SystemUserId");
                    param.Add(new DBParameter("SystemUserId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemUserName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemUserName", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[SystemUserName] like '%'+@SystemUserName+'%'");
                    param.Add(new DBParameter("SystemUserName", StringUtil.ToString(text)));
                }

                /* Advance Search by SystemRoleId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemRoleId", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[SystemRoleId] = @SystemRoleId");
                    param.Add(new DBParameter("SystemRoleId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by UserName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UserName", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[UserName] like '%'+@UserName+'%'");
                    param.Add(new DBParameter("UserName", StringUtil.ToString(text)));
                }

                /* Advance Search by Password */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Password", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[Password] like '%'+@Password+'%'");
                    param.Add(new DBParameter("Password", StringUtil.ToString(text)));
                }

                /* Advance Search by FirstName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FirstName", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[FirstName] like '%'+@FirstName+'%'");
                    param.Add(new DBParameter("FirstName", StringUtil.ToString(text)));
                }

                /* Advance Search by LastName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LastName", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[LastName] like '%'+@LastName+'%'");
                    param.Add(new DBParameter("LastName", StringUtil.ToString(text)));
                }

                /* Advance Search by Email */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Email", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[Email] like '%'+@Email+'%'");
                    param.Add(new DBParameter("Email", StringUtil.ToString(text)));
                }

                /* Advance Search by PhoneNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PhoneNo", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[PhoneNo] like '%'+@PhoneNo+'%'");
                    param.Add(new DBParameter("PhoneNo", StringUtil.ToString(text)));
                }

                /* Advance Search by DefaultMenuUrl */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DefaultMenuUrl", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[DefaultMenuUrl] like '%'+@DefaultMenuUrl+'%'");
                    param.Add(new DBParameter("DefaultMenuUrl", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemUser.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and SystemUser.SystemUserId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
