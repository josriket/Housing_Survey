using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;

namespace Library.Winplus.syslib.standard.systemuser.model
{
    public partial class SystemUser
    {
        public new bool IsHiddenName = true;

        public string token { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
