using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.systemuser.model
{
    [Serializable]
    public partial class SystemUser : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("SystemUserId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemUserId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String SystemUserName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemRoleId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemRoleId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UserName", typeof(MyResources.Winplus.Resource))]
        public String UserName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Password", typeof(MyResources.Winplus.Resource))]
        public String Password { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FirstName", typeof(MyResources.Winplus.Resource))]
        public String FirstName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LastName", typeof(MyResources.Winplus.Resource))]
        public String LastName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Email", typeof(MyResources.Winplus.Resource))]
        public String Email { get; set; }

        [DataColumn]
        [LocalizedDisplayName("PhoneNo", typeof(MyResources.Winplus.Resource))]
        public String PhoneNo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("DefaultMenuUrl", typeof(MyResources.Winplus.Resource))]
        public String DefaultMenuUrl { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ActiveStatusId", typeof(MyResources.Winplus.Resource))]
        public Int64? ActiveStatusId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemRoleName", typeof(MyResources.Winplus.Resource))]
        public String SystemRoleIdSystemRoleName { get; set; }

        [LocalizedDisplayName("ActiveStatusName", typeof(MyResources.Winplus.Resource))]
        public String ActiveStatusIdActiveStatusName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
