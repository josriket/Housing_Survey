using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey_7.model;

namespace Library.Winplus.syslib.standard.survey_7.repository
{
    public partial class Survey_7Repository : SimpleRepository<Survey_7>
    {
        #region General Query

        public Survey_7Repository()
        {
            SqlInsert = @"Insert Into [Survey_7]([SurveyId], [SurveyName], [SurveyCode], [ProjectId], [Latitude], [Longitude], [BuildingName], [BuildingTypeId], [BuildingTypeOther], [FloorNoId], [FloorNoOther], [MaterialId], [MaterialOther], [ImageFront], [ImageBeside], [ImageRefer], [Width], [Long], [Height], [FarFromRefer], [UnitNo], [OwnershipId], [UpdateFlag], [DeleteFlag], [OwnerSystemUserId], [KeyInOn], [CreatedOn], [CreatedBy], [AdvantageId], [Advantage], [Remark], [NewPoint]) Values(@SurveyId, @SurveyName, @SurveyCode, @ProjectId, @Latitude, @Longitude, @BuildingName, @BuildingTypeId, @BuildingTypeOther, @FloorNoId, @FloorNoOther, @MaterialId, @MaterialOther, @ImageFront, @ImageBeside, @ImageRefer, @Width, @Long, @Height, @FarFromRefer, @UnitNo, @OwnershipId, @UpdateFlag, @DeleteFlag, @OwnerSystemUserId, @KeyInOn, getdate(), @CreatedBy, @AdvantageId, @Advantage, @Remark, @NewPoint)";
            SqlUpdate = @"Update [Survey_7] Set [SurveyId]=@SurveyId, [SurveyName]=@SurveyName, [SurveyCode]=@SurveyCode, [ProjectId]=@ProjectId, [Latitude]=@Latitude, [Longitude]=@Longitude, [BuildingName]=@BuildingName, [BuildingTypeId]=@BuildingTypeId, [BuildingTypeOther]=@BuildingTypeOther, [FloorNoId]=@FloorNoId, [FloorNoOther]=@FloorNoOther, [MaterialId]=@MaterialId, [MaterialOther]=@MaterialOther, [ImageFront]=@ImageFront, [ImageBeside]=@ImageBeside, [ImageRefer]=@ImageRefer, [Width]=@Width, [Long]=@Long, [Height]=@Height, [FarFromRefer]=@FarFromRefer, [UnitNo]=@UnitNo, [OwnershipId]=@OwnershipId, [UpdateFlag]=@UpdateFlag, [DeleteFlag]=@DeleteFlag, [OwnerSystemUserId]=@OwnerSystemUserId, [KeyInOn]=@KeyInOn, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy, [AdvantageId]=@AdvantageId, [Advantage]=@Advantage, [Remark]=@Remark, [NewPoint]=@NewPoint Where [Survey_7Id]=@Survey_7Id";
            SqlDelete = @"Delete From [Survey_7] Where [Survey_7Id]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Survey_7] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Survey_7> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Survey_7 model in lstImport)
                {
                    if (model.Survey_7Id.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Survey_7 FindBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public List<Survey_7> FindsBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public int DeleteBySurveyId(DBConnection conn, Int64 SurveyId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [SurveyId]=@SurveyId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyId", SurveyId));
        }

        public Survey_7 FindBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public List<Survey_7> FindsBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public int DeleteBySurveyName(DBConnection conn, String SurveyName, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [SurveyName]=@SurveyName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyName", SurveyName));
        }

        public Survey_7 FindBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public List<Survey_7> FindsBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public int DeleteBySurveyCode(DBConnection conn, String SurveyCode, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [SurveyCode]=@SurveyCode " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyCode", SurveyCode));
        }

        public Survey_7 FindByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public List<Survey_7> FindsByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public int DeleteByProjectId(DBConnection conn, Int64 ProjectId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [ProjectId]=@ProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectId", ProjectId));
        }

        public Survey_7 FindByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public List<Survey_7> FindsByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public int DeleteByLatitude(DBConnection conn, Decimal Latitude, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Latitude]=@Latitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Latitude", Latitude));
        }

        public Survey_7 FindByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public List<Survey_7> FindsByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public int DeleteByLongitude(DBConnection conn, Decimal Longitude, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Longitude]=@Longitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Longitude", Longitude));
        }

        public Survey_7 FindByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public List<Survey_7> FindsByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public int DeleteByBuildingName(DBConnection conn, String BuildingName, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [BuildingName]=@BuildingName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingName", BuildingName));
        }

        public Survey_7 FindByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public List<Survey_7> FindsByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public int DeleteByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [BuildingTypeId]=@BuildingTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public Survey_7 FindByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public List<Survey_7> FindsByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public int DeleteByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [BuildingTypeOther]=@BuildingTypeOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public Survey_7 FindByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public List<Survey_7> FindsByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public int DeleteByFloorNoId(DBConnection conn, Int64 FloorNoId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [FloorNoId]=@FloorNoId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoId", FloorNoId));
        }

        public Survey_7 FindByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public List<Survey_7> FindsByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public int DeleteByFloorNoOther(DBConnection conn, String FloorNoOther, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [FloorNoOther]=@FloorNoOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoOther", FloorNoOther));
        }

        public Survey_7 FindByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public List<Survey_7> FindsByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public int DeleteByMaterialId(DBConnection conn, Int64 MaterialId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [MaterialId]=@MaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialId", MaterialId));
        }

        public Survey_7 FindByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public List<Survey_7> FindsByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public int DeleteByMaterialOther(DBConnection conn, String MaterialOther, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [MaterialOther]=@MaterialOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialOther", MaterialOther));
        }

        public Survey_7 FindByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public List<Survey_7> FindsByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public int DeleteByImageFront(DBConnection conn, String ImageFront, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [ImageFront]=@ImageFront " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageFront", ImageFront));
        }

        public Survey_7 FindByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public List<Survey_7> FindsByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public int DeleteByImageBeside(DBConnection conn, String ImageBeside, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [ImageBeside]=@ImageBeside " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageBeside", ImageBeside));
        }

        public Survey_7 FindByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public List<Survey_7> FindsByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public int DeleteByImageRefer(DBConnection conn, String ImageRefer, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [ImageRefer]=@ImageRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageRefer", ImageRefer));
        }

        public Survey_7 FindByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Width]=@Width " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Width", Width));
        }

        public List<Survey_7> FindsByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Width]=@Width " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Width", Width));
        }

        public int DeleteByWidth(DBConnection conn, Decimal Width, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Width]=@Width " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Width", Width));
        }

        public Survey_7 FindByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Long]=@Long " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Long", Long));
        }

        public List<Survey_7> FindsByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Long]=@Long " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Long", Long));
        }

        public int DeleteByLong(DBConnection conn, Decimal Long, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Long]=@Long " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Long", Long));
        }

        public Survey_7 FindByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Height]=@Height " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Height", Height));
        }

        public List<Survey_7> FindsByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Height]=@Height " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Height", Height));
        }

        public int DeleteByHeight(DBConnection conn, Decimal Height, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Height]=@Height " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Height", Height));
        }

        public Survey_7 FindByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public List<Survey_7> FindsByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public int DeleteByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [FarFromRefer]=@FarFromRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FarFromRefer", FarFromRefer));
        }

        public Survey_7 FindByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public List<Survey_7> FindsByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public int DeleteByUnitNo(DBConnection conn, Int32 UnitNo, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [UnitNo]=@UnitNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UnitNo", UnitNo));
        }

        public Survey_7 FindByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public List<Survey_7> FindsByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public int DeleteByOwnershipId(DBConnection conn, Int64 OwnershipId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [OwnershipId]=@OwnershipId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnershipId", OwnershipId));
        }

        public Survey_7 FindByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public List<Survey_7> FindsByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public int DeleteByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [UpdateFlag]=@UpdateFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UpdateFlag", UpdateFlag));
        }

        public Survey_7 FindByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public List<Survey_7> FindsByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public int DeleteByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [DeleteFlag]=@DeleteFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DeleteFlag", DeleteFlag));
        }

        public Survey_7 FindByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public List<Survey_7> FindsByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public int DeleteByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [OwnerSystemUserId]=@OwnerSystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public Survey_7 FindByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public List<Survey_7> FindsByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public int DeleteByKeyInOn(DBConnection conn, DateTime KeyInOn, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [KeyInOn]=@KeyInOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("KeyInOn", KeyInOn));
        }

        public Survey_7 FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Survey_7> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Survey_7 FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Survey_7> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Survey_7 FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Survey_7> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Survey_7 FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Survey_7> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        public Survey_7 FindByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public List<Survey_7> FindsByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public int DeleteByAdvantageId(DBConnection conn, Int64 AdvantageId, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [AdvantageId]=@AdvantageId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("AdvantageId", AdvantageId));
        }

        public Survey_7 FindByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public List<Survey_7> FindsByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public int DeleteByAdvantage(DBConnection conn, String Advantage, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Advantage]=@Advantage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Advantage", Advantage));
        }

        public Survey_7 FindByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Remark]=@Remark " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("Remark", Remark));
        }

        public List<Survey_7> FindsByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[Remark]=@Remark " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("Remark", Remark));
        }

        public int DeleteByRemark(DBConnection conn, String Remark, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [Remark]=@Remark " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Remark", Remark));
        }

        public Survey_7 FindByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Top 1 Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.Find<Survey_7>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public List<Survey_7> FindsByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Survey_7.* From [Survey_7] Survey_7 Where Survey_7.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.FindList<Survey_7>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public int DeleteByNewPoint(DBConnection conn, Boolean NewPoint, string whereText="")
        {
            String sql = "Delete From [Survey_7] Where [NewPoint]=@NewPoint " + whereText;
            return conn.executeUpdate(sql,new DBParameter("NewPoint", NewPoint));
        }

        #endregion

    }
}
