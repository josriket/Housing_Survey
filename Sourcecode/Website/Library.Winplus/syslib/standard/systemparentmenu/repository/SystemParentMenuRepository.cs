using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemparentmenu.model;

namespace Library.Winplus.syslib.standard.systemparentmenu.repository
{
    public partial class SystemParentMenuRepository : SimpleRepository<SystemParentMenu>
    {
        #region General Query

        public SystemParentMenuRepository()
        {
            SqlInsert = @"Insert Into [SystemParentMenu]([SystemParentMenuName], [MenuUrl], [IconName], [IsHidden], [ActiveStatusId], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@SystemParentMenuName, @MenuUrl, @IconName, @IsHidden, @ActiveStatusId, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SystemParentMenu] Set [SystemParentMenuName]=@SystemParentMenuName, [MenuUrl]=@MenuUrl, [IconName]=@IconName, [IsHidden]=@IsHidden, [ActiveStatusId]=@ActiveStatusId, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SystemParentMenuId]=@SystemParentMenuId";
            SqlDelete = @"Delete From [SystemParentMenu] Where [SystemParentMenuId]=@Id";
            SqlActiveStatus = @"Update [SystemParentMenu] Set [ActiveStatusId]=@ActiveStatusId Where [SystemParentMenuId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SystemParentMenu] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SystemParentMenu> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SystemParentMenu model in lstImport)
                {
                    if (model.SystemParentMenuId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SystemParentMenu FindBySystemParentMenuId(DBConnection conn, Int64 SystemParentMenuId, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[SystemParentMenuId]=@SystemParentMenuId " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("SystemParentMenuId", SystemParentMenuId));
        }

        public List<SystemParentMenu> FindsBySystemParentMenuId(DBConnection conn, Int64 SystemParentMenuId, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[SystemParentMenuId]=@SystemParentMenuId " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("SystemParentMenuId", SystemParentMenuId));
        }

        public int DeleteBySystemParentMenuId(DBConnection conn, Int64 SystemParentMenuId, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [SystemParentMenuId]=@SystemParentMenuId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemParentMenuId", SystemParentMenuId));
        }

        public SystemParentMenu FindBySystemParentMenuName(DBConnection conn, String SystemParentMenuName, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[SystemParentMenuName]=@SystemParentMenuName " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("SystemParentMenuName", SystemParentMenuName));
        }

        public List<SystemParentMenu> FindsBySystemParentMenuName(DBConnection conn, String SystemParentMenuName, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[SystemParentMenuName]=@SystemParentMenuName " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("SystemParentMenuName", SystemParentMenuName));
        }

        public int DeleteBySystemParentMenuName(DBConnection conn, String SystemParentMenuName, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [SystemParentMenuName]=@SystemParentMenuName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemParentMenuName", SystemParentMenuName));
        }

        public SystemParentMenu FindByMenuUrl(DBConnection conn, String MenuUrl, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[MenuUrl]=@MenuUrl " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("MenuUrl", MenuUrl));
        }

        public List<SystemParentMenu> FindsByMenuUrl(DBConnection conn, String MenuUrl, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[MenuUrl]=@MenuUrl " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("MenuUrl", MenuUrl));
        }

        public int DeleteByMenuUrl(DBConnection conn, String MenuUrl, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [MenuUrl]=@MenuUrl " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MenuUrl", MenuUrl));
        }

        public SystemParentMenu FindByIconName(DBConnection conn, String IconName, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[IconName]=@IconName " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("IconName", IconName));
        }

        public List<SystemParentMenu> FindsByIconName(DBConnection conn, String IconName, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[IconName]=@IconName " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("IconName", IconName));
        }

        public int DeleteByIconName(DBConnection conn, String IconName, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [IconName]=@IconName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("IconName", IconName));
        }

        public SystemParentMenu FindByIsHidden(DBConnection conn, Boolean IsHidden, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[IsHidden]=@IsHidden " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("IsHidden", IsHidden));
        }

        public List<SystemParentMenu> FindsByIsHidden(DBConnection conn, Boolean IsHidden, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[IsHidden]=@IsHidden " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("IsHidden", IsHidden));
        }

        public int DeleteByIsHidden(DBConnection conn, Boolean IsHidden, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [IsHidden]=@IsHidden " + whereText;
            return conn.executeUpdate(sql,new DBParameter("IsHidden", IsHidden));
        }

        public SystemParentMenu FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<SystemParentMenu> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public SystemParentMenu FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<SystemParentMenu> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public SystemParentMenu FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SystemParentMenu> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SystemParentMenu FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SystemParentMenu> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SystemParentMenu FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SystemParentMenu> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SystemParentMenu FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SystemParentMenu>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SystemParentMenu> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SystemParentMenu.* From [SystemParentMenu] SystemParentMenu Where SystemParentMenu.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SystemParentMenu>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SystemParentMenu] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
