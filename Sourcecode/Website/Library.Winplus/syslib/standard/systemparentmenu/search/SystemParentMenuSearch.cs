using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemparentmenu.model;

namespace Library.Winplus.syslib.standard.systemparentmenu.search
{
    public partial class SystemParentMenuSearch : SimpleSearch<SystemParentMenu>
    {
        public SystemParentMenuSearch()
        {
            search = new SystemParentMenu();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SystemParentMenuId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select SystemParentMenu.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [SystemParentMenu] SystemParentMenu
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=SystemParentMenu.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=SystemParentMenu.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=SystemParentMenu.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (SystemParentMenu.[SystemParentMenuName] like '%'+@SearchText+'%' or SystemParentMenu.[MenuUrl] like '%'+@SearchText+'%' or SystemParentMenu.[IconName] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SystemParentMenuId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemParentMenuId", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[SystemParentMenuId] = @SystemParentMenuId");
                    param.Add(new DBParameter("SystemParentMenuId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemParentMenuName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemParentMenuName", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[SystemParentMenuName] like '%'+@SystemParentMenuName+'%'");
                    param.Add(new DBParameter("SystemParentMenuName", StringUtil.ToString(text)));
                }

                /* Advance Search by MenuUrl */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MenuUrl", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[MenuUrl] like '%'+@MenuUrl+'%'");
                    param.Add(new DBParameter("MenuUrl", StringUtil.ToString(text)));
                }

                /* Advance Search by IconName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_IconName", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[IconName] like '%'+@IconName+'%'");
                    param.Add(new DBParameter("IconName", StringUtil.ToString(text)));
                }

                /* Advance Search by IsHidden */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_IsHidden", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[IsHidden] = @IsHidden");
                    param.Add(new DBParameter("IsHidden", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by DisplayIndex */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DisplayIndex", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[DisplayIndex] = @DisplayIndex");
                    param.Add(new DBParameter("DisplayIndex", StringUtil.ToInt32(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and SystemParentMenu.SystemParentMenuId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
