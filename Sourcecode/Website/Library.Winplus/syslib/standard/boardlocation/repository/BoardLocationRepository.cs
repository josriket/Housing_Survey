using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.boardlocation.model;

namespace Library.Winplus.syslib.standard.boardlocation.repository
{
    public partial class BoardLocationRepository : SimpleRepository<BoardLocation>
    {
        #region General Query

        public BoardLocationRepository()
        {
            SqlInsert = @"Insert Into [BoardLocation]([BoardLocationName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@BoardLocationName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BoardLocation] Set [BoardLocationName]=@BoardLocationName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BoardLocationId]=@BoardLocationId";
            SqlDelete = @"Delete From [BoardLocation] Where [BoardLocationId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BoardLocation] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BoardLocation> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BoardLocation model in lstImport)
                {
                    if (model.BoardLocationId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BoardLocation FindByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[BoardLocationId]=@BoardLocationId " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("BoardLocationId", BoardLocationId));
        }

        public List<BoardLocation> FindsByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[BoardLocationId]=@BoardLocationId " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("BoardLocationId", BoardLocationId));
        }

        public int DeleteByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [BoardLocationId]=@BoardLocationId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardLocationId", BoardLocationId));
        }

        public BoardLocation FindByBoardLocationName(DBConnection conn, String BoardLocationName, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[BoardLocationName]=@BoardLocationName " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("BoardLocationName", BoardLocationName));
        }

        public List<BoardLocation> FindsByBoardLocationName(DBConnection conn, String BoardLocationName, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[BoardLocationName]=@BoardLocationName " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("BoardLocationName", BoardLocationName));
        }

        public int DeleteByBoardLocationName(DBConnection conn, String BoardLocationName, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [BoardLocationName]=@BoardLocationName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardLocationName", BoardLocationName));
        }

        public BoardLocation FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<BoardLocation> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public BoardLocation FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BoardLocation> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BoardLocation FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BoardLocation> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BoardLocation FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BoardLocation> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BoardLocation FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BoardLocation>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BoardLocation> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BoardLocation.* From [BoardLocation] BoardLocation Where BoardLocation.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BoardLocation>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BoardLocation] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
