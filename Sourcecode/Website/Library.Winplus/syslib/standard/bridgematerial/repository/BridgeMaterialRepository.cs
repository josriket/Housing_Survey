using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.bridgematerial.model;

namespace Library.Winplus.syslib.standard.bridgematerial.repository
{
    public partial class BridgeMaterialRepository : SimpleRepository<BridgeMaterial>
    {
        #region General Query

        public BridgeMaterialRepository()
        {
            SqlInsert = @"Insert Into [BridgeMaterial]([BridgeMaterialName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@BridgeMaterialName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BridgeMaterial] Set [BridgeMaterialName]=@BridgeMaterialName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BridgeMaterialId]=@BridgeMaterialId";
            SqlDelete = @"Delete From [BridgeMaterial] Where [BridgeMaterialId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BridgeMaterial] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BridgeMaterial> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BridgeMaterial model in lstImport)
                {
                    if (model.BridgeMaterialId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BridgeMaterial FindByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[BridgeMaterialId]=@BridgeMaterialId " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public List<BridgeMaterial> FindsByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[BridgeMaterialId]=@BridgeMaterialId " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public int DeleteByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [BridgeMaterialId]=@BridgeMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public BridgeMaterial FindByBridgeMaterialName(DBConnection conn, String BridgeMaterialName, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[BridgeMaterialName]=@BridgeMaterialName " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("BridgeMaterialName", BridgeMaterialName));
        }

        public List<BridgeMaterial> FindsByBridgeMaterialName(DBConnection conn, String BridgeMaterialName, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[BridgeMaterialName]=@BridgeMaterialName " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("BridgeMaterialName", BridgeMaterialName));
        }

        public int DeleteByBridgeMaterialName(DBConnection conn, String BridgeMaterialName, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [BridgeMaterialName]=@BridgeMaterialName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeMaterialName", BridgeMaterialName));
        }

        public BridgeMaterial FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<BridgeMaterial> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public BridgeMaterial FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BridgeMaterial> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BridgeMaterial FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BridgeMaterial> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BridgeMaterial FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BridgeMaterial> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BridgeMaterial FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BridgeMaterial>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BridgeMaterial> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BridgeMaterial.* From [BridgeMaterial] BridgeMaterial Where BridgeMaterial.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BridgeMaterial>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BridgeMaterial] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
