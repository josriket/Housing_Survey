using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.roadlane.model;

namespace Library.Winplus.syslib.standard.roadlane.repository
{
    public partial class RoadLaneRepository : SimpleRepository<RoadLane>
    {
        #region General Query

        public RoadLaneRepository()
        {
            SqlInsert = @"Insert Into [RoadLane]([RoadLaneName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@RoadLaneName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [RoadLane] Set [RoadLaneName]=@RoadLaneName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [RoadLaneId]=@RoadLaneId";
            SqlDelete = @"Delete From [RoadLane] Where [RoadLaneId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [RoadLane] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<RoadLane> lstImport)
        {
            if (lstImport != null)
            {
                foreach (RoadLane model in lstImport)
                {
                    if (model.RoadLaneId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public RoadLane FindByRoadLaneId(DBConnection conn, Int64 RoadLaneId, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[RoadLaneId]=@RoadLaneId " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("RoadLaneId", RoadLaneId));
        }

        public List<RoadLane> FindsByRoadLaneId(DBConnection conn, Int64 RoadLaneId, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[RoadLaneId]=@RoadLaneId " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("RoadLaneId", RoadLaneId));
        }

        public int DeleteByRoadLaneId(DBConnection conn, Int64 RoadLaneId, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [RoadLaneId]=@RoadLaneId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadLaneId", RoadLaneId));
        }

        public RoadLane FindByRoadLaneName(DBConnection conn, String RoadLaneName, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[RoadLaneName]=@RoadLaneName " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("RoadLaneName", RoadLaneName));
        }

        public List<RoadLane> FindsByRoadLaneName(DBConnection conn, String RoadLaneName, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[RoadLaneName]=@RoadLaneName " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("RoadLaneName", RoadLaneName));
        }

        public int DeleteByRoadLaneName(DBConnection conn, String RoadLaneName, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [RoadLaneName]=@RoadLaneName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadLaneName", RoadLaneName));
        }

        public RoadLane FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<RoadLane> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public RoadLane FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<RoadLane> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public RoadLane FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<RoadLane> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public RoadLane FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<RoadLane> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public RoadLane FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 RoadLane.* From [RoadLane] RoadLane Where RoadLane.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<RoadLane>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<RoadLane> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select RoadLane.* From [RoadLane] RoadLane Where RoadLane.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<RoadLane>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [RoadLane] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
