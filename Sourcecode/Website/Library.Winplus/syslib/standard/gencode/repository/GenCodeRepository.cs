using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.gencode.model;

namespace Library.Winplus.syslib.standard.gencode.repository
{
    public partial class GenCodeRepository : SimpleRepository<GenCode>
    {
        #region General Query

        public GenCodeRepository()
        {
            SqlInsert = @"Insert Into [GenCode]([GenCodeName], [RunningNo], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@GenCodeName, @RunningNo, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [GenCode] Set [GenCodeName]=@GenCodeName, [RunningNo]=@RunningNo, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [GenCodeId]=@GenCodeId";
            SqlDelete = @"Delete From [GenCode] Where [GenCodeId]=@Id";
            SqlActiveStatus = @"Update [GenCode] Set [ActiveStatusId]=@ActiveStatusId Where [GenCodeId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [GenCode] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<GenCode> lstImport)
        {
            if (lstImport != null)
            {
                foreach (GenCode model in lstImport)
                {
                    if (model.GenCodeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public GenCode FindByGenCodeId(DBConnection conn, Int64 GenCodeId, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[GenCodeId]=@GenCodeId " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("GenCodeId", GenCodeId));
        }

        public List<GenCode> FindsByGenCodeId(DBConnection conn, Int64 GenCodeId, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[GenCodeId]=@GenCodeId " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("GenCodeId", GenCodeId));
        }

        public int DeleteByGenCodeId(DBConnection conn, Int64 GenCodeId, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [GenCodeId]=@GenCodeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("GenCodeId", GenCodeId));
        }

        public GenCode FindByGenCodeName(DBConnection conn, String GenCodeName, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[GenCodeName]=@GenCodeName " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("GenCodeName", GenCodeName));
        }

        public List<GenCode> FindsByGenCodeName(DBConnection conn, String GenCodeName, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[GenCodeName]=@GenCodeName " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("GenCodeName", GenCodeName));
        }

        public int DeleteByGenCodeName(DBConnection conn, String GenCodeName, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [GenCodeName]=@GenCodeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("GenCodeName", GenCodeName));
        }

        public GenCode FindByRunningNo(DBConnection conn, Int64 RunningNo, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[RunningNo]=@RunningNo " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("RunningNo", RunningNo));
        }

        public List<GenCode> FindsByRunningNo(DBConnection conn, Int64 RunningNo, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[RunningNo]=@RunningNo " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("RunningNo", RunningNo));
        }

        public int DeleteByRunningNo(DBConnection conn, Int64 RunningNo, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [RunningNo]=@RunningNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RunningNo", RunningNo));
        }

        public GenCode FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<GenCode> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public GenCode FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<GenCode> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public GenCode FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<GenCode> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public GenCode FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<GenCode> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public GenCode FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 GenCode.* From [GenCode] GenCode Where GenCode.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<GenCode>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<GenCode> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select GenCode.* From [GenCode] GenCode Where GenCode.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<GenCode>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [GenCode] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
