using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.locationtype.model;

namespace Library.Winplus.syslib.standard.locationtype.repository
{
    public partial class LocationTypeRepository : SimpleRepository<LocationType>
    {
        #region General Query

        public LocationTypeRepository()
        {
            SqlInsert = @"Insert Into [LocationType]([LocationTypeName], [NewIcon], [EditedIcon], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@LocationTypeName, @NewIcon, @EditedIcon, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [LocationType] Set [LocationTypeName]=@LocationTypeName, [NewIcon]=@NewIcon, [EditedIcon]=@EditedIcon, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [LocationTypeId]=@LocationTypeId";
            SqlDelete = @"Delete From [LocationType] Where [LocationTypeId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [LocationType] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<LocationType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (LocationType model in lstImport)
                {
                    if (model.LocationTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public LocationType FindByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[LocationTypeId]=@LocationTypeId " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("LocationTypeId", LocationTypeId));
        }

        public List<LocationType> FindsByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[LocationTypeId]=@LocationTypeId " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("LocationTypeId", LocationTypeId));
        }

        public int DeleteByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [LocationTypeId]=@LocationTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LocationTypeId", LocationTypeId));
        }

        public LocationType FindByLocationTypeName(DBConnection conn, String LocationTypeName, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[LocationTypeName]=@LocationTypeName " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("LocationTypeName", LocationTypeName));
        }

        public List<LocationType> FindsByLocationTypeName(DBConnection conn, String LocationTypeName, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[LocationTypeName]=@LocationTypeName " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("LocationTypeName", LocationTypeName));
        }

        public int DeleteByLocationTypeName(DBConnection conn, String LocationTypeName, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [LocationTypeName]=@LocationTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LocationTypeName", LocationTypeName));
        }

        public LocationType FindByNewIcon(DBConnection conn, String NewIcon, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[NewIcon]=@NewIcon " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("NewIcon", NewIcon));
        }

        public List<LocationType> FindsByNewIcon(DBConnection conn, String NewIcon, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[NewIcon]=@NewIcon " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("NewIcon", NewIcon));
        }

        public int DeleteByNewIcon(DBConnection conn, String NewIcon, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [NewIcon]=@NewIcon " + whereText;
            return conn.executeUpdate(sql,new DBParameter("NewIcon", NewIcon));
        }

        public LocationType FindByEditedIcon(DBConnection conn, String EditedIcon, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[EditedIcon]=@EditedIcon " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("EditedIcon", EditedIcon));
        }

        public List<LocationType> FindsByEditedIcon(DBConnection conn, String EditedIcon, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[EditedIcon]=@EditedIcon " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("EditedIcon", EditedIcon));
        }

        public int DeleteByEditedIcon(DBConnection conn, String EditedIcon, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [EditedIcon]=@EditedIcon " + whereText;
            return conn.executeUpdate(sql,new DBParameter("EditedIcon", EditedIcon));
        }

        public LocationType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<LocationType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public LocationType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<LocationType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public LocationType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<LocationType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public LocationType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<LocationType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public LocationType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 LocationType.* From [LocationType] LocationType Where LocationType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<LocationType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<LocationType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select LocationType.* From [LocationType] LocationType Where LocationType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<LocationType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [LocationType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
