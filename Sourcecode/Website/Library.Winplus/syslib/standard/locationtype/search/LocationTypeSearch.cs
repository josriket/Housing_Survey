using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.locationtype.model;

namespace Library.Winplus.syslib.standard.locationtype.search
{
    public partial class LocationTypeSearch : SimpleSearch<LocationType>
    {
        public LocationTypeSearch()
        {
            search = new LocationType();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "LocationTypeId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select LocationType.*
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [LocationType] LocationType
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=LocationType.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=LocationType.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (LocationType.[LocationTypeName] like '%'+@SearchText+'%' or LocationType.[NewIcon] like '%'+@SearchText+'%' or LocationType.[EditedIcon] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by LocationTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LocationTypeId", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[LocationTypeId] = @LocationTypeId");
                    param.Add(new DBParameter("LocationTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by LocationTypeName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LocationTypeName", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[LocationTypeName] like '%'+@LocationTypeName+'%'");
                    param.Add(new DBParameter("LocationTypeName", StringUtil.ToString(text)));
                }

                /* Advance Search by NewIcon */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_NewIcon", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[NewIcon] like '%'+@NewIcon+'%'");
                    param.Add(new DBParameter("NewIcon", StringUtil.ToString(text)));
                }

                /* Advance Search by EditedIcon */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_EditedIcon", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[EditedIcon] like '%'+@EditedIcon+'%'");
                    param.Add(new DBParameter("EditedIcon", StringUtil.ToString(text)));
                }

                /* Advance Search by DisplayIndex */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DisplayIndex", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[DisplayIndex] = @DisplayIndex");
                    param.Add(new DBParameter("DisplayIndex", StringUtil.ToInt32(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and LocationType.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and LocationType.LocationTypeId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
