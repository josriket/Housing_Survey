using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.fileupload.model;

namespace Library.Winplus.syslib.standard.fileupload.repository
{
    public partial class FileUploadRepository : SimpleRepository<FileUpload>
    {
        #region General Query

        public FileUploadRepository()
        {
            SqlInsert = @"Insert Into [FileUpload]([FileUploadName], [UploadFile], [UploadFileFileName], [UploadFileContentType], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@FileUploadName, @UploadFile, @UploadFileFileName, @UploadFileContentType, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [FileUpload] Set [FileUploadName]=@FileUploadName, [UploadFile]=@UploadFile, [UploadFileFileName]=@UploadFileFileName, [UploadFileContentType]=@UploadFileContentType, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [FileUploadId]=@FileUploadId";
            SqlDelete = @"Delete From [FileUpload] Where [FileUploadId]=@Id";
            SqlActiveStatus = @"Update [FileUpload] Set [ActiveStatusId]=@ActiveStatusId Where [FileUploadId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [FileUpload] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<FileUpload> lstImport)
        {
            if (lstImport != null)
            {
                foreach (FileUpload model in lstImport)
                {
                    if (model.FileUploadId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public FileUpload FindByFileUploadId(DBConnection conn, Int64 FileUploadId, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[FileUploadId]=@FileUploadId " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("FileUploadId", FileUploadId));
        }

        public List<FileUpload> FindsByFileUploadId(DBConnection conn, Int64 FileUploadId, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[FileUploadId]=@FileUploadId " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("FileUploadId", FileUploadId));
        }

        public int DeleteByFileUploadId(DBConnection conn, Int64 FileUploadId, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [FileUploadId]=@FileUploadId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FileUploadId", FileUploadId));
        }

        public FileUpload FindByFileUploadName(DBConnection conn, String FileUploadName, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[FileUploadName]=@FileUploadName " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("FileUploadName", FileUploadName));
        }

        public List<FileUpload> FindsByFileUploadName(DBConnection conn, String FileUploadName, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[FileUploadName]=@FileUploadName " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("FileUploadName", FileUploadName));
        }

        public int DeleteByFileUploadName(DBConnection conn, String FileUploadName, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [FileUploadName]=@FileUploadName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FileUploadName", FileUploadName));
        }

        public FileUpload FindByUploadFileFileName(DBConnection conn, String UploadFileFileName, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[UploadFileFileName]=@UploadFileFileName " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("UploadFileFileName", UploadFileFileName));
        }

        public List<FileUpload> FindsByUploadFileFileName(DBConnection conn, String UploadFileFileName, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[UploadFileFileName]=@UploadFileFileName " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("UploadFileFileName", UploadFileFileName));
        }

        public int DeleteByUploadFileFileName(DBConnection conn, String UploadFileFileName, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [UploadFileFileName]=@UploadFileFileName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UploadFileFileName", UploadFileFileName));
        }

        public FileUpload FindByUploadFileContentType(DBConnection conn, String UploadFileContentType, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[UploadFileContentType]=@UploadFileContentType " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("UploadFileContentType", UploadFileContentType));
        }

        public List<FileUpload> FindsByUploadFileContentType(DBConnection conn, String UploadFileContentType, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[UploadFileContentType]=@UploadFileContentType " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("UploadFileContentType", UploadFileContentType));
        }

        public int DeleteByUploadFileContentType(DBConnection conn, String UploadFileContentType, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [UploadFileContentType]=@UploadFileContentType " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UploadFileContentType", UploadFileContentType));
        }

        public FileUpload FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<FileUpload> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public FileUpload FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<FileUpload> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public FileUpload FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<FileUpload> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public FileUpload FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<FileUpload> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public FileUpload FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 FileUpload.* From [FileUpload] FileUpload Where FileUpload.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<FileUpload>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<FileUpload> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select FileUpload.* From [FileUpload] FileUpload Where FileUpload.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<FileUpload>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [FileUpload] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
