using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.fileupload.model;
using Library.Winplus.syslib.standard.activestatus.model;
using System.IO;
using System.Net.Http;
using Library.Winplus.syslib.standard.survey.model;
using Library.Winplus.syslib.customize.systemconfig.service;

namespace Library.Winplus.syslib.standard.fileupload.repository
{
    public partial class FileUploadRepository
    {
        public string SaveLocalFile(string directory, Survey model, string position, string imageBesideBase64)
        {
            if (string.IsNullOrEmpty(imageBesideBase64))
            {
                return null;
            }
            else
            {
                string fileName = $"{Convert.ToInt64(model.SurveyCode).ToString("000000")}_{position}.jpg";
                directory = $"{directory}/{model.ProjectId.ToString()}";
                if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
                File.WriteAllBytes($"{directory}/{fileName}", Convert.FromBase64String(imageBesideBase64));
                return fileName;
            }
        }

        public string TransferFile(Survey model, string position, string imageBesideBase64)
        {
            if (string.IsNullOrEmpty(imageBesideBase64))
            {
                return null;
            }
            else
            {
                string fileName = $"{Convert.ToInt64(model.SurveyCode).ToString("000000")}_{position}.jpg";
                HttpContent bytesContent = new ByteArrayContent(Convert.FromBase64String(imageBesideBase64));
                HttpContent stringContent = new StringContent(model.ProjectId.ToString());
                string uploadServerUrl = SystemConfigService.GetStringConfig("UploadServerUrl", "http://fileserver.thaimaptech.com/upload.php");
                using (HttpClient client = new HttpClient())
                {
                    using (MultipartFormDataContent formData = new MultipartFormDataContent())
                    {
                        formData.Add(bytesContent, "fileToUpload", fileName);
                        formData.Add(stringContent, "projectid");
                        HttpResponseMessage response = client.PostAsync(uploadServerUrl, formData).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string resultText = response.Content.ReadAsStringAsync().Result;
                            if (!resultText.StartsWith("Success"))
                            {
                                throw new Exception(resultText);
                            }
                        }
                    }
                }

                return fileName;
            }

        }

        public List<FileUpload> FindsByIds(DBConnection conn, string ids)
        {
            if (!string.IsNullOrEmpty(ids))
            {
                return new FileUploadRepository().FindsByActiveStatusId(conn, ActiveStatus.Active, string.Format("and FileUploadId in({0})", ids));
            }
            return null;
        }

    }
}
