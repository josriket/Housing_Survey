using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.fileupload.model;

namespace Library.Winplus.syslib.standard.fileupload.search
{
    public partial class FileUploadSearch : SimpleSearch<FileUpload>
    {
        public FileUploadSearch()
        {
            search = new FileUpload();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "FileUploadId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select FileUpload.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [FileUpload] FileUpload
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=FileUpload.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=FileUpload.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=FileUpload.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (FileUpload.[FileUploadName] like '%'+@SearchText+'%' or FileUpload.[UploadFileFileName] like '%'+@SearchText+'%' or FileUpload.[UploadFileContentType] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by FileUploadId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FileUploadId", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[FileUploadId] = @FileUploadId");
                    param.Add(new DBParameter("FileUploadId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by FileUploadName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FileUploadName", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[FileUploadName] like '%'+@FileUploadName+'%'");
                    param.Add(new DBParameter("FileUploadName", StringUtil.ToString(text)));
                }


                /* Advance Search by UploadFileFileName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UploadFileFileName", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[UploadFileFileName] like '%'+@UploadFileFileName+'%'");
                    param.Add(new DBParameter("UploadFileFileName", StringUtil.ToString(text)));
                }

                /* Advance Search by UploadFileContentType */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UploadFileContentType", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[UploadFileContentType] like '%'+@UploadFileContentType+'%'");
                    param.Add(new DBParameter("UploadFileContentType", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and FileUpload.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and FileUpload.FileUploadId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
