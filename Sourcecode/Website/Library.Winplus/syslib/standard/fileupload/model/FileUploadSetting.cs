﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.standard.fileupload.model
{
    public class FileUploadSetting
    {
        public List<FileUpload> LstFileUpload { get; set; }
        public string FieldName { get; set; }
        public int MaxFileSize { get; set; }

        public FileUploadSetting()
        {
            MaxFileSize = 2; // Default=2MB
        }
    }
}
