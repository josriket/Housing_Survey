using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.fileupload.model
{
    [Serializable]
    public partial class FileUpload : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("FileUploadId", typeof(MyResources.Winplus.Resource))]
        public Int64? FileUploadId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FileUploadName", typeof(MyResources.Winplus.Resource))]
        public String FileUploadName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UploadFile", typeof(MyResources.Winplus.Resource))]
        public Byte[] UploadFile { get; set; }

        /* Remark: This field must generate after ContentType & FileName */
        public HttpPostedFileBase UploadFileUpload
        {
            get {
                return null;
            }
            set
            {
                string contentType = string.Empty;
                string fileDataFileName = string.Empty;
                UploadFile = FileUtil.ReadHttpPostFile(value, ref contentType, ref fileDataFileName);
                UploadFileContentType = contentType;
                UploadFileFileName = fileDataFileName;
            }
        }

        [DataColumn]
        [LocalizedDisplayName("UploadFileFileName", typeof(MyResources.Winplus.Resource))]
        public String UploadFileFileName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UploadFileContentType", typeof(MyResources.Winplus.Resource))]
        public String UploadFileContentType { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ActiveStatusId", typeof(MyResources.Winplus.Resource))]
        public Int64? ActiveStatusId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("ActiveStatusName", typeof(MyResources.Winplus.Resource))]
        public String ActiveStatusIdActiveStatusName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
