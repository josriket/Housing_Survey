using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.mvcsetting.model
{
    [Serializable]
    public partial class MVCSetting : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("MVCSettingId", typeof(MyResources.Winplus.Resource))]
        public Int64? MVCSettingId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MVCSettingName", typeof(MyResources.Winplus.Resource))]
        public String MVCSettingName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ControllerName", typeof(MyResources.Winplus.Resource))]
        public String ControllerName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ActionName", typeof(MyResources.Winplus.Resource))]
        public String ActionName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CustomViewName", typeof(MyResources.Winplus.Resource))]
        public String CustomViewName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ActiveStatusId", typeof(MyResources.Winplus.Resource))]
        public Int64? ActiveStatusId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("ActiveStatusName", typeof(MyResources.Winplus.Resource))]
        public String ActiveStatusIdActiveStatusName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
