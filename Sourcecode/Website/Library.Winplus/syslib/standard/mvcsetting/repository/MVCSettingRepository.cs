using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.mvcsetting.model;

namespace Library.Winplus.syslib.standard.mvcsetting.repository
{
    public partial class MVCSettingRepository : SimpleRepository<MVCSetting>
    {
        #region General Query

        public MVCSettingRepository()
        {
            SqlInsert = @"Insert Into [MVCSetting]([MVCSettingName], [ControllerName], [ActionName], [CustomViewName], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@MVCSettingName, @ControllerName, @ActionName, @CustomViewName, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [MVCSetting] Set [MVCSettingName]=@MVCSettingName, [ControllerName]=@ControllerName, [ActionName]=@ActionName, [CustomViewName]=@CustomViewName, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [MVCSettingId]=@MVCSettingId";
            SqlDelete = @"Delete From [MVCSetting] Where [MVCSettingId]=@Id";
            SqlActiveStatus = @"Update [MVCSetting] Set [ActiveStatusId]=@ActiveStatusId Where [MVCSettingId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [MVCSetting] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<MVCSetting> lstImport)
        {
            if (lstImport != null)
            {
                foreach (MVCSetting model in lstImport)
                {
                    if (model.MVCSettingId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public MVCSetting FindByMVCSettingId(DBConnection conn, Int64 MVCSettingId, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[MVCSettingId]=@MVCSettingId " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("MVCSettingId", MVCSettingId));
        }

        public List<MVCSetting> FindsByMVCSettingId(DBConnection conn, Int64 MVCSettingId, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[MVCSettingId]=@MVCSettingId " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("MVCSettingId", MVCSettingId));
        }

        public int DeleteByMVCSettingId(DBConnection conn, Int64 MVCSettingId, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [MVCSettingId]=@MVCSettingId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MVCSettingId", MVCSettingId));
        }

        public MVCSetting FindByMVCSettingName(DBConnection conn, String MVCSettingName, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[MVCSettingName]=@MVCSettingName " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("MVCSettingName", MVCSettingName));
        }

        public List<MVCSetting> FindsByMVCSettingName(DBConnection conn, String MVCSettingName, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[MVCSettingName]=@MVCSettingName " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("MVCSettingName", MVCSettingName));
        }

        public int DeleteByMVCSettingName(DBConnection conn, String MVCSettingName, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [MVCSettingName]=@MVCSettingName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MVCSettingName", MVCSettingName));
        }

        public MVCSetting FindByControllerName(DBConnection conn, String ControllerName, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ControllerName]=@ControllerName " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("ControllerName", ControllerName));
        }

        public List<MVCSetting> FindsByControllerName(DBConnection conn, String ControllerName, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ControllerName]=@ControllerName " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("ControllerName", ControllerName));
        }

        public int DeleteByControllerName(DBConnection conn, String ControllerName, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [ControllerName]=@ControllerName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ControllerName", ControllerName));
        }

        public MVCSetting FindByActionName(DBConnection conn, String ActionName, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ActionName]=@ActionName " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("ActionName", ActionName));
        }

        public List<MVCSetting> FindsByActionName(DBConnection conn, String ActionName, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ActionName]=@ActionName " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("ActionName", ActionName));
        }

        public int DeleteByActionName(DBConnection conn, String ActionName, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [ActionName]=@ActionName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActionName", ActionName));
        }

        public MVCSetting FindByCustomViewName(DBConnection conn, String CustomViewName, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[CustomViewName]=@CustomViewName " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("CustomViewName", CustomViewName));
        }

        public List<MVCSetting> FindsByCustomViewName(DBConnection conn, String CustomViewName, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[CustomViewName]=@CustomViewName " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("CustomViewName", CustomViewName));
        }

        public int DeleteByCustomViewName(DBConnection conn, String CustomViewName, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [CustomViewName]=@CustomViewName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CustomViewName", CustomViewName));
        }

        public MVCSetting FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<MVCSetting> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public MVCSetting FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<MVCSetting> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public MVCSetting FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<MVCSetting> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public MVCSetting FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<MVCSetting> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public MVCSetting FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<MVCSetting>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<MVCSetting> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select MVCSetting.* From [MVCSetting] MVCSetting Where MVCSetting.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<MVCSetting>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [MVCSetting] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
