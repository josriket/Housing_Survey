using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.mvcsetting.model;

namespace Library.Winplus.syslib.standard.mvcsetting.search
{
    public partial class MVCSettingSearch : SimpleSearch<MVCSetting>
    {
        public MVCSettingSearch()
        {
            search = new MVCSetting();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "MVCSettingId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select MVCSetting.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [MVCSetting] MVCSetting
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=MVCSetting.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=MVCSetting.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=MVCSetting.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (MVCSetting.[MVCSettingName] like '%'+@SearchText+'%' or MVCSetting.[ControllerName] like '%'+@SearchText+'%' or MVCSetting.[ActionName] like '%'+@SearchText+'%' or MVCSetting.[CustomViewName] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by MVCSettingId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MVCSettingId", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[MVCSettingId] = @MVCSettingId");
                    param.Add(new DBParameter("MVCSettingId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by MVCSettingName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MVCSettingName", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[MVCSettingName] like '%'+@MVCSettingName+'%'");
                    param.Add(new DBParameter("MVCSettingName", StringUtil.ToString(text)));
                }

                /* Advance Search by ControllerName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ControllerName", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[ControllerName] like '%'+@ControllerName+'%'");
                    param.Add(new DBParameter("ControllerName", StringUtil.ToString(text)));
                }

                /* Advance Search by ActionName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActionName", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[ActionName] like '%'+@ActionName+'%'");
                    param.Add(new DBParameter("ActionName", StringUtil.ToString(text)));
                }

                /* Advance Search by CustomViewName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CustomViewName", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[CustomViewName] like '%'+@CustomViewName+'%'");
                    param.Add(new DBParameter("CustomViewName", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and MVCSetting.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and MVCSetting.MVCSettingId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
