﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.standard.mvcsetting.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.standard.mvcsetting.service
{
   public class MVCSettingService
    {
        public static string CustomView(string ControllerName, string ViewName)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                string sql = "select * from MVCSetting  where ControllerName=@ControllerName and ActionName=@ActionName and ActiveStatusId=1";
                MVCSetting model = QueryUtil.Find<MVCSetting>(conn, sql
                    , new DBParameter("ControllerName", ControllerName)
                    , new DBParameter("ActionName", ViewName)
                    );
                if (model != null)
                {
                    return model.CustomViewName;
                }
                return ViewName;
            }
        }
    }
}
