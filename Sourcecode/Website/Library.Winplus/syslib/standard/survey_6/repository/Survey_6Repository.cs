using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey_6.model;

namespace Library.Winplus.syslib.standard.survey_6.repository
{
    public partial class Survey_6Repository : SimpleRepository<Survey_6>
    {
        #region General Query

        public Survey_6Repository()
        {
            SqlInsert = @"Insert Into [Survey_6]([SurveyId], [SurveyName], [SurveyCode], [ProjectId], [Latitude], [Longitude], [BuildingName], [BuildingTypeId], [BuildingTypeOther], [FloorNoId], [FloorNoOther], [MaterialId], [MaterialOther], [ImageFront], [ImageBeside], [ImageRefer], [Width], [Long], [Height], [FarFromRefer], [UnitNo], [OwnershipId], [UpdateFlag], [DeleteFlag], [OwnerSystemUserId], [KeyInOn], [CreatedOn], [CreatedBy], [AdvantageId], [Advantage], [Remark], [NewPoint]) Values(@SurveyId, @SurveyName, @SurveyCode, @ProjectId, @Latitude, @Longitude, @BuildingName, @BuildingTypeId, @BuildingTypeOther, @FloorNoId, @FloorNoOther, @MaterialId, @MaterialOther, @ImageFront, @ImageBeside, @ImageRefer, @Width, @Long, @Height, @FarFromRefer, @UnitNo, @OwnershipId, @UpdateFlag, @DeleteFlag, @OwnerSystemUserId, @KeyInOn, getdate(), @CreatedBy, @AdvantageId, @Advantage, @Remark, @NewPoint)";
            SqlUpdate = @"Update [Survey_6] Set [SurveyId]=@SurveyId, [SurveyName]=@SurveyName, [SurveyCode]=@SurveyCode, [ProjectId]=@ProjectId, [Latitude]=@Latitude, [Longitude]=@Longitude, [BuildingName]=@BuildingName, [BuildingTypeId]=@BuildingTypeId, [BuildingTypeOther]=@BuildingTypeOther, [FloorNoId]=@FloorNoId, [FloorNoOther]=@FloorNoOther, [MaterialId]=@MaterialId, [MaterialOther]=@MaterialOther, [ImageFront]=@ImageFront, [ImageBeside]=@ImageBeside, [ImageRefer]=@ImageRefer, [Width]=@Width, [Long]=@Long, [Height]=@Height, [FarFromRefer]=@FarFromRefer, [UnitNo]=@UnitNo, [OwnershipId]=@OwnershipId, [UpdateFlag]=@UpdateFlag, [DeleteFlag]=@DeleteFlag, [OwnerSystemUserId]=@OwnerSystemUserId, [KeyInOn]=@KeyInOn, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy, [AdvantageId]=@AdvantageId, [Advantage]=@Advantage, [Remark]=@Remark, [NewPoint]=@NewPoint Where [Survey_6Id]=@Survey_6Id";
            SqlDelete = @"Delete From [Survey_6] Where [Survey_6Id]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Survey_6] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Survey_6> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Survey_6 model in lstImport)
                {
                    if (model.Survey_6Id.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Survey_6 FindBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public List<Survey_6> FindsBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public int DeleteBySurveyId(DBConnection conn, Int64 SurveyId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [SurveyId]=@SurveyId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyId", SurveyId));
        }

        public Survey_6 FindBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public List<Survey_6> FindsBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public int DeleteBySurveyName(DBConnection conn, String SurveyName, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [SurveyName]=@SurveyName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyName", SurveyName));
        }

        public Survey_6 FindBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public List<Survey_6> FindsBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public int DeleteBySurveyCode(DBConnection conn, String SurveyCode, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [SurveyCode]=@SurveyCode " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyCode", SurveyCode));
        }

        public Survey_6 FindByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public List<Survey_6> FindsByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public int DeleteByProjectId(DBConnection conn, Int64 ProjectId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [ProjectId]=@ProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectId", ProjectId));
        }

        public Survey_6 FindByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public List<Survey_6> FindsByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public int DeleteByLatitude(DBConnection conn, Decimal Latitude, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Latitude]=@Latitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Latitude", Latitude));
        }

        public Survey_6 FindByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public List<Survey_6> FindsByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public int DeleteByLongitude(DBConnection conn, Decimal Longitude, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Longitude]=@Longitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Longitude", Longitude));
        }

        public Survey_6 FindByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public List<Survey_6> FindsByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public int DeleteByBuildingName(DBConnection conn, String BuildingName, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [BuildingName]=@BuildingName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingName", BuildingName));
        }

        public Survey_6 FindByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public List<Survey_6> FindsByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public int DeleteByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [BuildingTypeId]=@BuildingTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public Survey_6 FindByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public List<Survey_6> FindsByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public int DeleteByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [BuildingTypeOther]=@BuildingTypeOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public Survey_6 FindByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public List<Survey_6> FindsByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public int DeleteByFloorNoId(DBConnection conn, Int64 FloorNoId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [FloorNoId]=@FloorNoId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoId", FloorNoId));
        }

        public Survey_6 FindByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public List<Survey_6> FindsByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public int DeleteByFloorNoOther(DBConnection conn, String FloorNoOther, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [FloorNoOther]=@FloorNoOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoOther", FloorNoOther));
        }

        public Survey_6 FindByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public List<Survey_6> FindsByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public int DeleteByMaterialId(DBConnection conn, Int64 MaterialId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [MaterialId]=@MaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialId", MaterialId));
        }

        public Survey_6 FindByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public List<Survey_6> FindsByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public int DeleteByMaterialOther(DBConnection conn, String MaterialOther, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [MaterialOther]=@MaterialOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialOther", MaterialOther));
        }

        public Survey_6 FindByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public List<Survey_6> FindsByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public int DeleteByImageFront(DBConnection conn, String ImageFront, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [ImageFront]=@ImageFront " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageFront", ImageFront));
        }

        public Survey_6 FindByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public List<Survey_6> FindsByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public int DeleteByImageBeside(DBConnection conn, String ImageBeside, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [ImageBeside]=@ImageBeside " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageBeside", ImageBeside));
        }

        public Survey_6 FindByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public List<Survey_6> FindsByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public int DeleteByImageRefer(DBConnection conn, String ImageRefer, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [ImageRefer]=@ImageRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageRefer", ImageRefer));
        }

        public Survey_6 FindByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Width]=@Width " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Width", Width));
        }

        public List<Survey_6> FindsByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Width]=@Width " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Width", Width));
        }

        public int DeleteByWidth(DBConnection conn, Decimal Width, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Width]=@Width " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Width", Width));
        }

        public Survey_6 FindByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Long]=@Long " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Long", Long));
        }

        public List<Survey_6> FindsByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Long]=@Long " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Long", Long));
        }

        public int DeleteByLong(DBConnection conn, Decimal Long, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Long]=@Long " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Long", Long));
        }

        public Survey_6 FindByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Height]=@Height " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Height", Height));
        }

        public List<Survey_6> FindsByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Height]=@Height " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Height", Height));
        }

        public int DeleteByHeight(DBConnection conn, Decimal Height, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Height]=@Height " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Height", Height));
        }

        public Survey_6 FindByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public List<Survey_6> FindsByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public int DeleteByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [FarFromRefer]=@FarFromRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FarFromRefer", FarFromRefer));
        }

        public Survey_6 FindByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public List<Survey_6> FindsByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public int DeleteByUnitNo(DBConnection conn, Int32 UnitNo, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [UnitNo]=@UnitNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UnitNo", UnitNo));
        }

        public Survey_6 FindByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public List<Survey_6> FindsByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public int DeleteByOwnershipId(DBConnection conn, Int64 OwnershipId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [OwnershipId]=@OwnershipId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnershipId", OwnershipId));
        }

        public Survey_6 FindByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public List<Survey_6> FindsByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public int DeleteByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [UpdateFlag]=@UpdateFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UpdateFlag", UpdateFlag));
        }

        public Survey_6 FindByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public List<Survey_6> FindsByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public int DeleteByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [DeleteFlag]=@DeleteFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DeleteFlag", DeleteFlag));
        }

        public Survey_6 FindByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public List<Survey_6> FindsByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public int DeleteByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [OwnerSystemUserId]=@OwnerSystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public Survey_6 FindByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public List<Survey_6> FindsByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public int DeleteByKeyInOn(DBConnection conn, DateTime KeyInOn, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [KeyInOn]=@KeyInOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("KeyInOn", KeyInOn));
        }

        public Survey_6 FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Survey_6> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Survey_6 FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Survey_6> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Survey_6 FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Survey_6> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Survey_6 FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Survey_6> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        public Survey_6 FindByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public List<Survey_6> FindsByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public int DeleteByAdvantageId(DBConnection conn, Int64 AdvantageId, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [AdvantageId]=@AdvantageId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("AdvantageId", AdvantageId));
        }

        public Survey_6 FindByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public List<Survey_6> FindsByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public int DeleteByAdvantage(DBConnection conn, String Advantage, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Advantage]=@Advantage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Advantage", Advantage));
        }

        public Survey_6 FindByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Remark]=@Remark " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("Remark", Remark));
        }

        public List<Survey_6> FindsByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[Remark]=@Remark " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("Remark", Remark));
        }

        public int DeleteByRemark(DBConnection conn, String Remark, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [Remark]=@Remark " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Remark", Remark));
        }

        public Survey_6 FindByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Top 1 Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.Find<Survey_6>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public List<Survey_6> FindsByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Survey_6.* From [Survey_6] Survey_6 Where Survey_6.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.FindList<Survey_6>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public int DeleteByNewPoint(DBConnection conn, Boolean NewPoint, string whereText="")
        {
            String sql = "Delete From [Survey_6] Where [NewPoint]=@NewPoint " + whereText;
            return conn.executeUpdate(sql,new DBParameter("NewPoint", NewPoint));
        }

        #endregion

    }
}
