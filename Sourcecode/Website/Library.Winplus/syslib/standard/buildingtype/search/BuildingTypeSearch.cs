using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.buildingtype.model;

namespace Library.Winplus.syslib.standard.buildingtype.search
{
    public partial class BuildingTypeSearch : SimpleSearch<BuildingType>
    {
        public BuildingTypeSearch()
        {
            search = new BuildingType();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "BuildingTypeId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select BuildingType.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [BuildingType] BuildingType
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=BuildingType.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=BuildingType.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=BuildingType.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (BuildingType.[BuildingTypeName] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by BuildingTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeId", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.[BuildingTypeId] = @BuildingTypeId");
                    param.Add(new DBParameter("BuildingTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BuildingTypeName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeName", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.[BuildingTypeName] like '%'+@BuildingTypeName+'%'");
                    param.Add(new DBParameter("BuildingTypeName", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by DisplayIndex */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DisplayIndex", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.[DisplayIndex] = @DisplayIndex");
                    param.Add(new DBParameter("DisplayIndex", StringUtil.ToInt32(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and BuildingType.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and BuildingType.BuildingTypeId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
