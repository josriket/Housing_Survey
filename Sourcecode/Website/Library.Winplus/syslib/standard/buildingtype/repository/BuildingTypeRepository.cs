using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.buildingtype.model;

namespace Library.Winplus.syslib.standard.buildingtype.repository
{
    public partial class BuildingTypeRepository : SimpleRepository<BuildingType>
    {
        #region General Query

        public BuildingTypeRepository()
        {
            SqlInsert = @"Insert Into [BuildingType]([BuildingTypeName], [ActiveStatusId], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@BuildingTypeName, @ActiveStatusId, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BuildingType] Set [BuildingTypeName]=@BuildingTypeName, [ActiveStatusId]=@ActiveStatusId, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BuildingTypeId]=@BuildingTypeId";
            SqlDelete = @"Delete From [BuildingType] Where [BuildingTypeId]=@Id";
            SqlActiveStatus = @"Update [BuildingType] Set [ActiveStatusId]=@ActiveStatusId Where [BuildingTypeId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BuildingType] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BuildingType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BuildingType model in lstImport)
                {
                    if (model.BuildingTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BuildingType FindByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public List<BuildingType> FindsByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public int DeleteByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [BuildingTypeId]=@BuildingTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public BuildingType FindByBuildingTypeName(DBConnection conn, String BuildingTypeName, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[BuildingTypeName]=@BuildingTypeName " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("BuildingTypeName", BuildingTypeName));
        }

        public List<BuildingType> FindsByBuildingTypeName(DBConnection conn, String BuildingTypeName, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[BuildingTypeName]=@BuildingTypeName " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("BuildingTypeName", BuildingTypeName));
        }

        public int DeleteByBuildingTypeName(DBConnection conn, String BuildingTypeName, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [BuildingTypeName]=@BuildingTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeName", BuildingTypeName));
        }

        public BuildingType FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<BuildingType> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public BuildingType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<BuildingType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public BuildingType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BuildingType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BuildingType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BuildingType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BuildingType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BuildingType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BuildingType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BuildingType.* From [BuildingType] BuildingType Where BuildingType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BuildingType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BuildingType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BuildingType.* From [BuildingType] BuildingType Where BuildingType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BuildingType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BuildingType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
