using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.project.model;

namespace Library.Winplus.syslib.standard.project.repository
{
    public partial class ProjectRepository : SimpleRepository<Project>
    {
        #region General Query

        public ProjectRepository()
        {
            SqlInsert = @"Insert Into [Project]([ProjectName], [ProjectDescription], [InitialLatitude], [InitialLongitude], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@ProjectName, @ProjectDescription, @InitialLatitude, @InitialLongitude, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [Project] Set [ProjectName]=@ProjectName, [ProjectDescription]=@ProjectDescription, [InitialLatitude]=@InitialLatitude, [InitialLongitude]=@InitialLongitude, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [ProjectId]=@ProjectId";
            SqlDelete = @"Delete From [Project] Where [ProjectId]=@Id";
            SqlActiveStatus = @"Update [Project] Set [ActiveStatusId]=@ActiveStatusId Where [ProjectId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Project] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Project> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Project model in lstImport)
                {
                    if (model.ProjectId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Project FindByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public List<Project> FindsByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public int DeleteByProjectId(DBConnection conn, Int64 ProjectId, string whereText="")
        {
            String sql = "Delete From [Project] Where [ProjectId]=@ProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectId", ProjectId));
        }

        public Project FindByProjectName(DBConnection conn, String ProjectName, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[ProjectName]=@ProjectName " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("ProjectName", ProjectName));
        }

        public List<Project> FindsByProjectName(DBConnection conn, String ProjectName, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[ProjectName]=@ProjectName " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("ProjectName", ProjectName));
        }

        public int DeleteByProjectName(DBConnection conn, String ProjectName, string whereText="")
        {
            String sql = "Delete From [Project] Where [ProjectName]=@ProjectName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectName", ProjectName));
        }

        public Project FindByProjectDescription(DBConnection conn, String ProjectDescription, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[ProjectDescription]=@ProjectDescription " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("ProjectDescription", ProjectDescription));
        }

        public List<Project> FindsByProjectDescription(DBConnection conn, String ProjectDescription, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[ProjectDescription]=@ProjectDescription " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("ProjectDescription", ProjectDescription));
        }

        public int DeleteByProjectDescription(DBConnection conn, String ProjectDescription, string whereText="")
        {
            String sql = "Delete From [Project] Where [ProjectDescription]=@ProjectDescription " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectDescription", ProjectDescription));
        }

        public Project FindByInitialLatitude(DBConnection conn, Decimal InitialLatitude, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[InitialLatitude]=@InitialLatitude " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("InitialLatitude", InitialLatitude));
        }

        public List<Project> FindsByInitialLatitude(DBConnection conn, Decimal InitialLatitude, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[InitialLatitude]=@InitialLatitude " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("InitialLatitude", InitialLatitude));
        }

        public int DeleteByInitialLatitude(DBConnection conn, Decimal InitialLatitude, string whereText="")
        {
            String sql = "Delete From [Project] Where [InitialLatitude]=@InitialLatitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("InitialLatitude", InitialLatitude));
        }

        public Project FindByInitialLongitude(DBConnection conn, Decimal InitialLongitude, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[InitialLongitude]=@InitialLongitude " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("InitialLongitude", InitialLongitude));
        }

        public List<Project> FindsByInitialLongitude(DBConnection conn, Decimal InitialLongitude, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[InitialLongitude]=@InitialLongitude " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("InitialLongitude", InitialLongitude));
        }

        public int DeleteByInitialLongitude(DBConnection conn, Decimal InitialLongitude, string whereText="")
        {
            String sql = "Delete From [Project] Where [InitialLongitude]=@InitialLongitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("InitialLongitude", InitialLongitude));
        }

        public Project FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<Project> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [Project] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public Project FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Project> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Project] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Project FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Project> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Project] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Project FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Project> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Project] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Project FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Project.* From [Project] Project Where Project.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Project>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Project> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Project.* From [Project] Project Where Project.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Project>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Project] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
