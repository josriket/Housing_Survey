using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.survey.model;
using Library.Winplus.syslib.standard.survey.repository;
using Library.Winplus.syslib.standard.userproject.model;

namespace Library.Winplus.syslib.standard.project.repository
{
    public partial class ProjectRepository
    {

        public string GetProjectIds(DBConnection conn, long? systemUserId)
        {
            List<UserProject> lst = QueryUtil.FindList<UserProject>(conn
                , "Select Distinct ProjectId from UserProject Where SystemUserId=@SystemUserId"
                , new DBParameter("SystemUserId", systemUserId));
            List<string> lstResult = new List<string>();
            foreach(UserProject p in lst)
            {
                lstResult.Add(p.ProjectId.Value.ToString());
            }

            return string.Join(",", lstResult);
        }

        public void Import(DBConnection conn, LoginUser loginUser, Project project, List<SurveyUpload> lstImport)
        {
            SurveyRepository repo = new SurveyRepository();
            Survey survey = null;
            foreach (SurveyUpload s in lstImport)
            {
                survey = new Survey();
                survey.SurveyName = s.SurveyCode;
                survey.SurveyCode = s.SurveyCode;
                survey.Latitude = s.Latitude;
                survey.Longitude = s.Longitude;
                survey.CreatedBy = loginUser.SystemUserId;
                survey.UpdateFlag = false;
                survey.DeleteFlag = false;
                survey.ProjectId = project.ProjectId;

                repo.Create(conn, survey);
            }
        }
    }
}
