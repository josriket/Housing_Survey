﻿using System;

namespace Library.Winplus.syslib.standard.project.model
{
    public class SurveyUpload
    {
        public String SurveyCode { get; set; }
        public Decimal? Latitude { get; set; }
        public Decimal? Longitude { get; set; }
    }
}
