using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;

namespace Library.Winplus.syslib.standard.project.model
{
    public partial class Project
    {
        public Byte[] UploadFile { get; set; }
        public String UploadFileFileName { get; set; }
        public String UploadFileContentType { get; set; }
        public int? TotalSurvey { get; set; }
        public int? TotalInprocess { get; set; }
        public int? TotalCompleted { get; set; }
        public bool OverrideData { get; set; }

        /* Remark: This field must generate after ContentType & FileName */
        public HttpPostedFileBase UploadFileUpload
        {
            get
            {
                return null;
            }
            set
            {
                string contentType = string.Empty;
                string fileDataFileName = string.Empty;
                UploadFile = FileUtil.ReadHttpPostFile(value, ref contentType, ref fileDataFileName);
                UploadFileContentType = contentType;
                UploadFileFileName = fileDataFileName;
            }
        }
    }
}
