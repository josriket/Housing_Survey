using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey_5.model;

namespace Library.Winplus.syslib.standard.survey_5.search
{
    public partial class Survey_5Search : SimpleSearch<Survey_5>
    {
        public Survey_5Search()
        {
            search = new Survey_5();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "Survey_5Id desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select Survey_5.*
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [Survey_5] Survey_5
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=Survey_5.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=Survey_5.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (Survey_5.[SurveyName] like '%'+@SearchText+'%' or Survey_5.[SurveyCode] like '%'+@SearchText+'%' or Survey_5.[BuildingName] like '%'+@SearchText+'%' or Survey_5.[BuildingTypeOther] like '%'+@SearchText+'%' or Survey_5.[FloorNoOther] like '%'+@SearchText+'%' or Survey_5.[MaterialOther] like '%'+@SearchText+'%' or Survey_5.[ImageFront] like '%'+@SearchText+'%' or Survey_5.[ImageBeside] like '%'+@SearchText+'%' or Survey_5.[ImageRefer] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%' or Survey_5.[Advantage] like '%'+@SearchText+'%' or Survey_5.[Remark] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SurveyId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[SurveyId] = @SurveyId");
                    param.Add(new DBParameter("SurveyId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SurveyName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyName", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[SurveyName] like '%'+@SurveyName+'%'");
                    param.Add(new DBParameter("SurveyName", StringUtil.ToString(text)));
                }

                /* Advance Search by SurveyCode */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyCode", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[SurveyCode] like '%'+@SurveyCode+'%'");
                    param.Add(new DBParameter("SurveyCode", StringUtil.ToString(text)));
                }

                /* Advance Search by ProjectId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ProjectId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[ProjectId] = @ProjectId");
                    param.Add(new DBParameter("ProjectId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by Latitude */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Latitude", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Latitude] = @Latitude");
                    param.Add(new DBParameter("Latitude", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Longitude */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Longitude", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Longitude] = @Longitude");
                    param.Add(new DBParameter("Longitude", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by BuildingName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingName", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[BuildingName] like '%'+@BuildingName+'%'");
                    param.Add(new DBParameter("BuildingName", StringUtil.ToString(text)));
                }

                /* Advance Search by BuildingTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[BuildingTypeId] = @BuildingTypeId");
                    param.Add(new DBParameter("BuildingTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BuildingTypeOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[BuildingTypeOther] like '%'+@BuildingTypeOther+'%'");
                    param.Add(new DBParameter("BuildingTypeOther", StringUtil.ToString(text)));
                }

                /* Advance Search by FloorNoId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[FloorNoId] = @FloorNoId");
                    param.Add(new DBParameter("FloorNoId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by FloorNoOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[FloorNoOther] like '%'+@FloorNoOther+'%'");
                    param.Add(new DBParameter("FloorNoOther", StringUtil.ToString(text)));
                }

                /* Advance Search by MaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[MaterialId] = @MaterialId");
                    param.Add(new DBParameter("MaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by MaterialOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[MaterialOther] like '%'+@MaterialOther+'%'");
                    param.Add(new DBParameter("MaterialOther", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageFront */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageFront", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[ImageFront] like '%'+@ImageFront+'%'");
                    param.Add(new DBParameter("ImageFront", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageBeside */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageBeside", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[ImageBeside] like '%'+@ImageBeside+'%'");
                    param.Add(new DBParameter("ImageBeside", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageRefer */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageRefer", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[ImageRefer] like '%'+@ImageRefer+'%'");
                    param.Add(new DBParameter("ImageRefer", StringUtil.ToString(text)));
                }

                /* Advance Search by Width */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Width", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Width] = @Width");
                    param.Add(new DBParameter("Width", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Long */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Long", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Long] = @Long");
                    param.Add(new DBParameter("Long", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Height */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Height", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Height] = @Height");
                    param.Add(new DBParameter("Height", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by FarFromRefer */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FarFromRefer", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[FarFromRefer] = @FarFromRefer");
                    param.Add(new DBParameter("FarFromRefer", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by UnitNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UnitNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[UnitNo] = @UnitNo");
                    param.Add(new DBParameter("UnitNo", StringUtil.ToInt32(text)));
                }

                /* Advance Search by OwnershipId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnershipId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[OwnershipId] = @OwnershipId");
                    param.Add(new DBParameter("OwnershipId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by UpdateFlag */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UpdateFlag", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[UpdateFlag] = @UpdateFlag");
                    param.Add(new DBParameter("UpdateFlag", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by DeleteFlag */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DeleteFlag", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[DeleteFlag] = @DeleteFlag");
                    param.Add(new DBParameter("DeleteFlag", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by OwnerSystemUserId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnerSystemUserId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[OwnerSystemUserId] = @OwnerSystemUserId");
                    param.Add(new DBParameter("OwnerSystemUserId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by KeyInOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.`KeyInOn` >= @KeyInOn_From");
                    param.Add(new DBParameter("KeyInOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by KeyInOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.`KeyInOn` < DATE_ADD(@KeyInOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("KeyInOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by AdvantageId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_AdvantageId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[AdvantageId] = @AdvantageId");
                    param.Add(new DBParameter("AdvantageId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by Advantage */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Advantage", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Advantage] like '%'+@Advantage+'%'");
                    param.Add(new DBParameter("Advantage", StringUtil.ToString(text)));
                }

                /* Advance Search by Remark */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Remark", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[Remark] like '%'+@Remark+'%'");
                    param.Add(new DBParameter("Remark", StringUtil.ToString(text)));
                }

                /* Advance Search by NewPoint */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_NewPoint", ref text))
                {
                    sqlQuery.AppendLine("and Survey_5.[NewPoint] = @NewPoint");
                    param.Add(new DBParameter("NewPoint", StringUtil.ToBoolean(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and Survey_5.Survey_5Id in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
