using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.survey_5.model
{
    [Serializable]
    public partial class Survey_5 : SimpleModel
    {
        /* Table column */
        [DataColumn]
        [LocalizedDisplayName("SurveyId", typeof(MyResources.Winplus.Resource))]
        public Int64? SurveyId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SurveyName", typeof(MyResources.Winplus.Resource))]
        public String SurveyName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SurveyCode", typeof(MyResources.Winplus.Resource))]
        public String SurveyCode { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ProjectId", typeof(MyResources.Winplus.Resource))]
        public Int64? ProjectId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Latitude", typeof(MyResources.Winplus.Resource))]
        public Decimal? Latitude { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Longitude", typeof(MyResources.Winplus.Resource))]
        public Decimal? Longitude { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BuildingName", typeof(MyResources.Winplus.Resource))]
        public String BuildingName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BuildingTypeId", typeof(MyResources.Winplus.Resource))]
        public Int64? BuildingTypeId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("BuildingTypeOther", typeof(MyResources.Winplus.Resource))]
        public String BuildingTypeOther { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FloorNoId", typeof(MyResources.Winplus.Resource))]
        public Int64? FloorNoId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FloorNoOther", typeof(MyResources.Winplus.Resource))]
        public String FloorNoOther { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MaterialId", typeof(MyResources.Winplus.Resource))]
        public Int64? MaterialId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MaterialOther", typeof(MyResources.Winplus.Resource))]
        public String MaterialOther { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ImageFront", typeof(MyResources.Winplus.Resource))]
        public String ImageFront { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ImageBeside", typeof(MyResources.Winplus.Resource))]
        public String ImageBeside { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ImageRefer", typeof(MyResources.Winplus.Resource))]
        public String ImageRefer { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Width", typeof(MyResources.Winplus.Resource))]
        public Decimal? Width { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Long", typeof(MyResources.Winplus.Resource))]
        public Decimal? Long { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Height", typeof(MyResources.Winplus.Resource))]
        public Decimal? Height { get; set; }

        [DataColumn]
        [LocalizedDisplayName("FarFromRefer", typeof(MyResources.Winplus.Resource))]
        public Decimal? FarFromRefer { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UnitNo", typeof(MyResources.Winplus.Resource))]
        public Int32? UnitNo { get; set; }

        [DataColumn]
        [LocalizedDisplayName("OwnershipId", typeof(MyResources.Winplus.Resource))]
        public Int64? OwnershipId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UpdateFlag", typeof(MyResources.Winplus.Resource))]
        public Boolean UpdateFlag { get; set; }

        [DataColumn]
        [LocalizedDisplayName("DeleteFlag", typeof(MyResources.Winplus.Resource))]
        public Boolean DeleteFlag { get; set; }

        [DataColumn]
        [LocalizedDisplayName("OwnerSystemUserId", typeof(MyResources.Winplus.Resource))]
        public Int64? OwnerSystemUserId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("KeyInOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? KeyInOn { get; set; }
        public string KeyInOnText
        {
            get { return StringUtil.FormatDate(KeyInOn); }
            set { KeyInOn = StringUtil.ToDate(value); }
        }
        public string KeyInOnTimeText
        {
            get { return StringUtil.FormatTime(KeyInOn); }
            set { KeyInOn = StringUtil.AddedTime(KeyInOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("AdvantageId", typeof(MyResources.Winplus.Resource))]
        public Int64? AdvantageId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Advantage", typeof(MyResources.Winplus.Resource))]
        public String Advantage { get; set; }

        [DataColumn]
        [LocalizedDisplayName("Remark", typeof(MyResources.Winplus.Resource))]
        public String Remark { get; set; }

        [DataColumn]
        [LocalizedDisplayName("NewPoint", typeof(MyResources.Winplus.Resource))]
        public Boolean NewPoint { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
