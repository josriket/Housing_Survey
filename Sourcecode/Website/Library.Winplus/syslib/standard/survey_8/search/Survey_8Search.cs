using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey_8.model;

namespace Library.Winplus.syslib.standard.survey_8.search
{
    public partial class Survey_8Search : SimpleSearch<Survey_8>
    {
        public Survey_8Search()
        {
            search = new Survey_8();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "Survey_8Id desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select Survey_8.*
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [Survey_8] Survey_8
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=Survey_8.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=Survey_8.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (Survey_8.[SurveyName] like '%'+@SearchText+'%' or Survey_8.[SurveyCode] like '%'+@SearchText+'%' or Survey_8.[BuildingName] like '%'+@SearchText+'%' or Survey_8.[BuildingTypeOther] like '%'+@SearchText+'%' or Survey_8.[FloorNoOther] like '%'+@SearchText+'%' or Survey_8.[MaterialOther] like '%'+@SearchText+'%' or Survey_8.[ImageFront] like '%'+@SearchText+'%' or Survey_8.[ImageBeside] like '%'+@SearchText+'%' or Survey_8.[ImageRefer] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%' or Survey_8.[Advantage] like '%'+@SearchText+'%' or Survey_8.[Remark] like '%'+@SearchText+'%' or Survey_8.[PlaceOwner] like '%'+@SearchText+'%' or Survey_8.[PlaceUserBenefic] like '%'+@SearchText+'%' or Survey_8.[HomeNo] like '%'+@SearchText+'%' or Survey_8.[Moo] like '%'+@SearchText+'%' or Survey_8.[BoardText] like '%'+@SearchText+'%' or Survey_8.[LandParcelNo] like '%'+@SearchText+'%' or Survey_8.[LandNo] like '%'+@SearchText+'%' or Survey_8.[LandUsed] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SurveyId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[SurveyId] = @SurveyId");
                    param.Add(new DBParameter("SurveyId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SurveyName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyName", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[SurveyName] like '%'+@SurveyName+'%'");
                    param.Add(new DBParameter("SurveyName", StringUtil.ToString(text)));
                }

                /* Advance Search by SurveyCode */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SurveyCode", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[SurveyCode] like '%'+@SurveyCode+'%'");
                    param.Add(new DBParameter("SurveyCode", StringUtil.ToString(text)));
                }

                /* Advance Search by ProjectId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ProjectId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[ProjectId] = @ProjectId");
                    param.Add(new DBParameter("ProjectId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by Latitude */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Latitude", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Latitude] = @Latitude");
                    param.Add(new DBParameter("Latitude", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Longitude */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Longitude", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Longitude] = @Longitude");
                    param.Add(new DBParameter("Longitude", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by BuildingName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingName", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BuildingName] like '%'+@BuildingName+'%'");
                    param.Add(new DBParameter("BuildingName", StringUtil.ToString(text)));
                }

                /* Advance Search by BuildingTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BuildingTypeId] = @BuildingTypeId");
                    param.Add(new DBParameter("BuildingTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BuildingTypeOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BuildingTypeOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BuildingTypeOther] like '%'+@BuildingTypeOther+'%'");
                    param.Add(new DBParameter("BuildingTypeOther", StringUtil.ToString(text)));
                }

                /* Advance Search by FloorNoId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[FloorNoId] = @FloorNoId");
                    param.Add(new DBParameter("FloorNoId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by FloorNoOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FloorNoOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[FloorNoOther] like '%'+@FloorNoOther+'%'");
                    param.Add(new DBParameter("FloorNoOther", StringUtil.ToString(text)));
                }

                /* Advance Search by MaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[MaterialId] = @MaterialId");
                    param.Add(new DBParameter("MaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by MaterialOther */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MaterialOther", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[MaterialOther] like '%'+@MaterialOther+'%'");
                    param.Add(new DBParameter("MaterialOther", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageFront */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageFront", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[ImageFront] like '%'+@ImageFront+'%'");
                    param.Add(new DBParameter("ImageFront", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageBeside */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageBeside", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[ImageBeside] like '%'+@ImageBeside+'%'");
                    param.Add(new DBParameter("ImageBeside", StringUtil.ToString(text)));
                }

                /* Advance Search by ImageRefer */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ImageRefer", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[ImageRefer] like '%'+@ImageRefer+'%'");
                    param.Add(new DBParameter("ImageRefer", StringUtil.ToString(text)));
                }

                /* Advance Search by Width */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Width", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Width] = @Width");
                    param.Add(new DBParameter("Width", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Long */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Long", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Long] = @Long");
                    param.Add(new DBParameter("Long", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by Height */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Height", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Height] = @Height");
                    param.Add(new DBParameter("Height", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by FarFromRefer */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_FarFromRefer", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[FarFromRefer] = @FarFromRefer");
                    param.Add(new DBParameter("FarFromRefer", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by UnitNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UnitNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[UnitNo] = @UnitNo");
                    param.Add(new DBParameter("UnitNo", StringUtil.ToInt32(text)));
                }

                /* Advance Search by OwnershipId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnershipId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[OwnershipId] = @OwnershipId");
                    param.Add(new DBParameter("OwnershipId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by UpdateFlag */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UpdateFlag", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[UpdateFlag] = @UpdateFlag");
                    param.Add(new DBParameter("UpdateFlag", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by DeleteFlag */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DeleteFlag", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[DeleteFlag] = @DeleteFlag");
                    param.Add(new DBParameter("DeleteFlag", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by OwnerSystemUserId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_OwnerSystemUserId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[OwnerSystemUserId] = @OwnerSystemUserId");
                    param.Add(new DBParameter("OwnerSystemUserId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by KeyInOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.`KeyInOn` >= @KeyInOn_From");
                    param.Add(new DBParameter("KeyInOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by KeyInOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_KeyInOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.`KeyInOn` < DATE_ADD(@KeyInOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("KeyInOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by AdvantageId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_AdvantageId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[AdvantageId] = @AdvantageId");
                    param.Add(new DBParameter("AdvantageId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by Advantage */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Advantage", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Advantage] like '%'+@Advantage+'%'");
                    param.Add(new DBParameter("Advantage", StringUtil.ToString(text)));
                }

                /* Advance Search by Remark */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Remark", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Remark] like '%'+@Remark+'%'");
                    param.Add(new DBParameter("Remark", StringUtil.ToString(text)));
                }

                /* Advance Search by NewPoint */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_NewPoint", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[NewPoint] = @NewPoint");
                    param.Add(new DBParameter("NewPoint", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by LocationTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LocationTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LocationTypeId] = @LocationTypeId");
                    param.Add(new DBParameter("LocationTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by PlaceOwner */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PlaceOwner", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[PlaceOwner] like '%'+@PlaceOwner+'%'");
                    param.Add(new DBParameter("PlaceOwner", StringUtil.ToString(text)));
                }

                /* Advance Search by PlaceUserBenefic */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PlaceUserBenefic", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[PlaceUserBenefic] like '%'+@PlaceUserBenefic+'%'");
                    param.Add(new DBParameter("PlaceUserBenefic", StringUtil.ToString(text)));
                }

                /* Advance Search by HomeNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_HomeNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[HomeNo] like '%'+@HomeNo+'%'");
                    param.Add(new DBParameter("HomeNo", StringUtil.ToString(text)));
                }

                /* Advance Search by Moo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Moo", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[Moo] like '%'+@Moo+'%'");
                    param.Add(new DBParameter("Moo", StringUtil.ToString(text)));
                }

                /* Advance Search by BoardTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BoardTypeId] = @BoardTypeId");
                    param.Add(new DBParameter("BoardTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BoardLocationId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardLocationId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BoardLocationId] = @BoardLocationId");
                    param.Add(new DBParameter("BoardLocationId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BoardStyleId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardStyleId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BoardStyleId] = @BoardStyleId");
                    param.Add(new DBParameter("BoardStyleId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BoardSide */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardSide", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BoardSide] = @BoardSide");
                    param.Add(new DBParameter("BoardSide", StringUtil.ToInt32(text)));
                }

                /* Advance Search by BoardText */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BoardText", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BoardText] like '%'+@BoardText+'%'");
                    param.Add(new DBParameter("BoardText", StringUtil.ToString(text)));
                }

                /* Advance Search by RoadTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_RoadTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[RoadTypeId] = @RoadTypeId");
                    param.Add(new DBParameter("RoadTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by RoadMaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_RoadMaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[RoadMaterialId] = @RoadMaterialId");
                    param.Add(new DBParameter("RoadMaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BridgeTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BridgeTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BridgeTypeId] = @BridgeTypeId");
                    param.Add(new DBParameter("BridgeTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BridgeMaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BridgeMaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[BridgeMaterialId] = @BridgeMaterialId");
                    param.Add(new DBParameter("BridgeMaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by PoleTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PoleTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[PoleTypeId] = @PoleTypeId");
                    param.Add(new DBParameter("PoleTypeId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by PoleMaterialId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_PoleMaterialId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[PoleMaterialId] = @PoleMaterialId");
                    param.Add(new DBParameter("PoleMaterialId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by LandParcelNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandParcelNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandParcelNo] like '%'+@LandParcelNo+'%'");
                    param.Add(new DBParameter("LandParcelNo", StringUtil.ToString(text)));
                }

                /* Advance Search by LandNo */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandNo", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandNo] like '%'+@LandNo+'%'");
                    param.Add(new DBParameter("LandNo", StringUtil.ToString(text)));
                }

                /* Advance Search by LandRai */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandRai", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandRai] = @LandRai");
                    param.Add(new DBParameter("LandRai", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandNgan */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandNgan", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandNgan] = @LandNgan");
                    param.Add(new DBParameter("LandNgan", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandWa */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandWa", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandWa] = @LandWa");
                    param.Add(new DBParameter("LandWa", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandSubWa */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandSubWa", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandSubWa] = @LandSubWa");
                    param.Add(new DBParameter("LandSubWa", StringUtil.ToInt32(text)));
                }

                /* Advance Search by LandUsed */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandUsed", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandUsed] like '%'+@LandUsed+'%'");
                    param.Add(new DBParameter("LandUsed", StringUtil.ToString(text)));
                }

                /* Advance Search by LandParcelTypeId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LandParcelTypeId", ref text))
                {
                    sqlQuery.AppendLine("and Survey_8.[LandParcelTypeId] = @LandParcelTypeId");
                    param.Add(new DBParameter("LandParcelTypeId", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and Survey_8.Survey_8Id in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
