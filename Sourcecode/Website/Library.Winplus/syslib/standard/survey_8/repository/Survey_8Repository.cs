using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.survey_8.model;

namespace Library.Winplus.syslib.standard.survey_8.repository
{
    public partial class Survey_8Repository : SimpleRepository<Survey_8>
    {
        #region General Query

        public Survey_8Repository()
        {
            SqlInsert = @"Insert Into [Survey_8]([SurveyId], [SurveyName], [SurveyCode], [ProjectId], [Latitude], [Longitude], [BuildingName], [BuildingTypeId], [BuildingTypeOther], [FloorNoId], [FloorNoOther], [MaterialId], [MaterialOther], [ImageFront], [ImageBeside], [ImageRefer], [Width], [Long], [Height], [FarFromRefer], [UnitNo], [OwnershipId], [UpdateFlag], [DeleteFlag], [OwnerSystemUserId], [KeyInOn], [CreatedOn], [CreatedBy], [AdvantageId], [Advantage], [Remark], [NewPoint], [LocationTypeId], [PlaceOwner], [PlaceUserBenefic], [HomeNo], [Moo], [BoardTypeId], [BoardLocationId], [BoardStyleId], [BoardSide], [BoardText], [RoadTypeId], [RoadMaterialId], [BridgeTypeId], [BridgeMaterialId], [PoleTypeId], [PoleMaterialId], [LandParcelNo], [LandNo], [LandRai], [LandNgan], [LandWa], [LandSubWa], [LandUsed], [LandParcelTypeId]) Values(@SurveyId, @SurveyName, @SurveyCode, @ProjectId, @Latitude, @Longitude, @BuildingName, @BuildingTypeId, @BuildingTypeOther, @FloorNoId, @FloorNoOther, @MaterialId, @MaterialOther, @ImageFront, @ImageBeside, @ImageRefer, @Width, @Long, @Height, @FarFromRefer, @UnitNo, @OwnershipId, @UpdateFlag, @DeleteFlag, @OwnerSystemUserId, @KeyInOn, getdate(), @CreatedBy, @AdvantageId, @Advantage, @Remark, @NewPoint, @LocationTypeId, @PlaceOwner, @PlaceUserBenefic, @HomeNo, @Moo, @BoardTypeId, @BoardLocationId, @BoardStyleId, @BoardSide, @BoardText, @RoadTypeId, @RoadMaterialId, @BridgeTypeId, @BridgeMaterialId, @PoleTypeId, @PoleMaterialId, @LandParcelNo, @LandNo, @LandRai, @LandNgan, @LandWa, @LandSubWa, @LandUsed, @LandParcelTypeId)";
            SqlUpdate = @"Update [Survey_8] Set [SurveyId]=@SurveyId, [SurveyName]=@SurveyName, [SurveyCode]=@SurveyCode, [ProjectId]=@ProjectId, [Latitude]=@Latitude, [Longitude]=@Longitude, [BuildingName]=@BuildingName, [BuildingTypeId]=@BuildingTypeId, [BuildingTypeOther]=@BuildingTypeOther, [FloorNoId]=@FloorNoId, [FloorNoOther]=@FloorNoOther, [MaterialId]=@MaterialId, [MaterialOther]=@MaterialOther, [ImageFront]=@ImageFront, [ImageBeside]=@ImageBeside, [ImageRefer]=@ImageRefer, [Width]=@Width, [Long]=@Long, [Height]=@Height, [FarFromRefer]=@FarFromRefer, [UnitNo]=@UnitNo, [OwnershipId]=@OwnershipId, [UpdateFlag]=@UpdateFlag, [DeleteFlag]=@DeleteFlag, [OwnerSystemUserId]=@OwnerSystemUserId, [KeyInOn]=@KeyInOn, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy, [AdvantageId]=@AdvantageId, [Advantage]=@Advantage, [Remark]=@Remark, [NewPoint]=@NewPoint, [LocationTypeId]=@LocationTypeId, [PlaceOwner]=@PlaceOwner, [PlaceUserBenefic]=@PlaceUserBenefic, [HomeNo]=@HomeNo, [Moo]=@Moo, [BoardTypeId]=@BoardTypeId, [BoardLocationId]=@BoardLocationId, [BoardStyleId]=@BoardStyleId, [BoardSide]=@BoardSide, [BoardText]=@BoardText, [RoadTypeId]=@RoadTypeId, [RoadMaterialId]=@RoadMaterialId, [BridgeTypeId]=@BridgeTypeId, [BridgeMaterialId]=@BridgeMaterialId, [PoleTypeId]=@PoleTypeId, [PoleMaterialId]=@PoleMaterialId, [LandParcelNo]=@LandParcelNo, [LandNo]=@LandNo, [LandRai]=@LandRai, [LandNgan]=@LandNgan, [LandWa]=@LandWa, [LandSubWa]=@LandSubWa, [LandUsed]=@LandUsed, [LandParcelTypeId]=@LandParcelTypeId Where [Survey_8Id]=@Survey_8Id";
            SqlDelete = @"Delete From [Survey_8] Where [Survey_8Id]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Survey_8] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Survey_8> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Survey_8 model in lstImport)
                {
                    if (model.Survey_8Id.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Survey_8 FindBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public List<Survey_8> FindsBySurveyId(DBConnection conn, Int64 SurveyId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[SurveyId]=@SurveyId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("SurveyId", SurveyId));
        }

        public int DeleteBySurveyId(DBConnection conn, Int64 SurveyId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [SurveyId]=@SurveyId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyId", SurveyId));
        }

        public Survey_8 FindBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public List<Survey_8> FindsBySurveyName(DBConnection conn, String SurveyName, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[SurveyName]=@SurveyName " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("SurveyName", SurveyName));
        }

        public int DeleteBySurveyName(DBConnection conn, String SurveyName, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [SurveyName]=@SurveyName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyName", SurveyName));
        }

        public Survey_8 FindBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public List<Survey_8> FindsBySurveyCode(DBConnection conn, String SurveyCode, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[SurveyCode]=@SurveyCode " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("SurveyCode", SurveyCode));
        }

        public int DeleteBySurveyCode(DBConnection conn, String SurveyCode, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [SurveyCode]=@SurveyCode " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SurveyCode", SurveyCode));
        }

        public Survey_8 FindByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public List<Survey_8> FindsByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public int DeleteByProjectId(DBConnection conn, Int64 ProjectId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [ProjectId]=@ProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectId", ProjectId));
        }

        public Survey_8 FindByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public List<Survey_8> FindsByLatitude(DBConnection conn, Decimal Latitude, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Latitude]=@Latitude " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Latitude", Latitude));
        }

        public int DeleteByLatitude(DBConnection conn, Decimal Latitude, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Latitude]=@Latitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Latitude", Latitude));
        }

        public Survey_8 FindByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public List<Survey_8> FindsByLongitude(DBConnection conn, Decimal Longitude, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Longitude]=@Longitude " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Longitude", Longitude));
        }

        public int DeleteByLongitude(DBConnection conn, Decimal Longitude, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Longitude]=@Longitude " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Longitude", Longitude));
        }

        public Survey_8 FindByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public List<Survey_8> FindsByBuildingName(DBConnection conn, String BuildingName, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BuildingName]=@BuildingName " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BuildingName", BuildingName));
        }

        public int DeleteByBuildingName(DBConnection conn, String BuildingName, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BuildingName]=@BuildingName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingName", BuildingName));
        }

        public Survey_8 FindByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public List<Survey_8> FindsByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BuildingTypeId]=@BuildingTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public int DeleteByBuildingTypeId(DBConnection conn, Int64 BuildingTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BuildingTypeId]=@BuildingTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeId", BuildingTypeId));
        }

        public Survey_8 FindByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public List<Survey_8> FindsByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BuildingTypeOther]=@BuildingTypeOther " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public int DeleteByBuildingTypeOther(DBConnection conn, String BuildingTypeOther, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BuildingTypeOther]=@BuildingTypeOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BuildingTypeOther", BuildingTypeOther));
        }

        public Survey_8 FindByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public List<Survey_8> FindsByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public int DeleteByFloorNoId(DBConnection conn, Int64 FloorNoId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [FloorNoId]=@FloorNoId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoId", FloorNoId));
        }

        public Survey_8 FindByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public List<Survey_8> FindsByFloorNoOther(DBConnection conn, String FloorNoOther, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[FloorNoOther]=@FloorNoOther " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("FloorNoOther", FloorNoOther));
        }

        public int DeleteByFloorNoOther(DBConnection conn, String FloorNoOther, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [FloorNoOther]=@FloorNoOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoOther", FloorNoOther));
        }

        public Survey_8 FindByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public List<Survey_8> FindsByMaterialId(DBConnection conn, Int64 MaterialId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[MaterialId]=@MaterialId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("MaterialId", MaterialId));
        }

        public int DeleteByMaterialId(DBConnection conn, Int64 MaterialId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [MaterialId]=@MaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialId", MaterialId));
        }

        public Survey_8 FindByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public List<Survey_8> FindsByMaterialOther(DBConnection conn, String MaterialOther, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[MaterialOther]=@MaterialOther " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("MaterialOther", MaterialOther));
        }

        public int DeleteByMaterialOther(DBConnection conn, String MaterialOther, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [MaterialOther]=@MaterialOther " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MaterialOther", MaterialOther));
        }

        public Survey_8 FindByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public List<Survey_8> FindsByImageFront(DBConnection conn, String ImageFront, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ImageFront]=@ImageFront " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("ImageFront", ImageFront));
        }

        public int DeleteByImageFront(DBConnection conn, String ImageFront, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [ImageFront]=@ImageFront " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageFront", ImageFront));
        }

        public Survey_8 FindByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public List<Survey_8> FindsByImageBeside(DBConnection conn, String ImageBeside, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ImageBeside]=@ImageBeside " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("ImageBeside", ImageBeside));
        }

        public int DeleteByImageBeside(DBConnection conn, String ImageBeside, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [ImageBeside]=@ImageBeside " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageBeside", ImageBeside));
        }

        public Survey_8 FindByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public List<Survey_8> FindsByImageRefer(DBConnection conn, String ImageRefer, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ImageRefer]=@ImageRefer " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("ImageRefer", ImageRefer));
        }

        public int DeleteByImageRefer(DBConnection conn, String ImageRefer, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [ImageRefer]=@ImageRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ImageRefer", ImageRefer));
        }

        public Survey_8 FindByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Width]=@Width " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Width", Width));
        }

        public List<Survey_8> FindsByWidth(DBConnection conn, Decimal Width, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Width]=@Width " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Width", Width));
        }

        public int DeleteByWidth(DBConnection conn, Decimal Width, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Width]=@Width " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Width", Width));
        }

        public Survey_8 FindByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Long]=@Long " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Long", Long));
        }

        public List<Survey_8> FindsByLong(DBConnection conn, Decimal Long, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Long]=@Long " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Long", Long));
        }

        public int DeleteByLong(DBConnection conn, Decimal Long, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Long]=@Long " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Long", Long));
        }

        public Survey_8 FindByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Height]=@Height " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Height", Height));
        }

        public List<Survey_8> FindsByHeight(DBConnection conn, Decimal Height, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Height]=@Height " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Height", Height));
        }

        public int DeleteByHeight(DBConnection conn, Decimal Height, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Height]=@Height " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Height", Height));
        }

        public Survey_8 FindByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public List<Survey_8> FindsByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[FarFromRefer]=@FarFromRefer " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("FarFromRefer", FarFromRefer));
        }

        public int DeleteByFarFromRefer(DBConnection conn, Decimal FarFromRefer, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [FarFromRefer]=@FarFromRefer " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FarFromRefer", FarFromRefer));
        }

        public Survey_8 FindByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public List<Survey_8> FindsByUnitNo(DBConnection conn, Int32 UnitNo, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[UnitNo]=@UnitNo " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("UnitNo", UnitNo));
        }

        public int DeleteByUnitNo(DBConnection conn, Int32 UnitNo, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [UnitNo]=@UnitNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UnitNo", UnitNo));
        }

        public Survey_8 FindByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public List<Survey_8> FindsByOwnershipId(DBConnection conn, Int64 OwnershipId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[OwnershipId]=@OwnershipId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("OwnershipId", OwnershipId));
        }

        public int DeleteByOwnershipId(DBConnection conn, Int64 OwnershipId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [OwnershipId]=@OwnershipId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnershipId", OwnershipId));
        }

        public Survey_8 FindByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public List<Survey_8> FindsByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[UpdateFlag]=@UpdateFlag " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("UpdateFlag", UpdateFlag));
        }

        public int DeleteByUpdateFlag(DBConnection conn, Boolean UpdateFlag, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [UpdateFlag]=@UpdateFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UpdateFlag", UpdateFlag));
        }

        public Survey_8 FindByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public List<Survey_8> FindsByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[DeleteFlag]=@DeleteFlag " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("DeleteFlag", DeleteFlag));
        }

        public int DeleteByDeleteFlag(DBConnection conn, Boolean DeleteFlag, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [DeleteFlag]=@DeleteFlag " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DeleteFlag", DeleteFlag));
        }

        public Survey_8 FindByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public List<Survey_8> FindsByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[OwnerSystemUserId]=@OwnerSystemUserId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public int DeleteByOwnerSystemUserId(DBConnection conn, Int64 OwnerSystemUserId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [OwnerSystemUserId]=@OwnerSystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("OwnerSystemUserId", OwnerSystemUserId));
        }

        public Survey_8 FindByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public List<Survey_8> FindsByKeyInOn(DBConnection conn, DateTime KeyInOn, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[KeyInOn]=@KeyInOn " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("KeyInOn", KeyInOn));
        }

        public int DeleteByKeyInOn(DBConnection conn, DateTime KeyInOn, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [KeyInOn]=@KeyInOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("KeyInOn", KeyInOn));
        }

        public Survey_8 FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Survey_8> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Survey_8 FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Survey_8> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Survey_8 FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Survey_8> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Survey_8 FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Survey_8> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        public Survey_8 FindByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public List<Survey_8> FindsByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public int DeleteByAdvantageId(DBConnection conn, Int64 AdvantageId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [AdvantageId]=@AdvantageId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("AdvantageId", AdvantageId));
        }

        public Survey_8 FindByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public List<Survey_8> FindsByAdvantage(DBConnection conn, String Advantage, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Advantage]=@Advantage " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Advantage", Advantage));
        }

        public int DeleteByAdvantage(DBConnection conn, String Advantage, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Advantage]=@Advantage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Advantage", Advantage));
        }

        public Survey_8 FindByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Remark]=@Remark " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Remark", Remark));
        }

        public List<Survey_8> FindsByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Remark]=@Remark " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Remark", Remark));
        }

        public int DeleteByRemark(DBConnection conn, String Remark, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Remark]=@Remark " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Remark", Remark));
        }

        public Survey_8 FindByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public List<Survey_8> FindsByNewPoint(DBConnection conn, Boolean NewPoint, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[NewPoint]=@NewPoint " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("NewPoint", NewPoint));
        }

        public int DeleteByNewPoint(DBConnection conn, Boolean NewPoint, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [NewPoint]=@NewPoint " + whereText;
            return conn.executeUpdate(sql,new DBParameter("NewPoint", NewPoint));
        }

        public Survey_8 FindByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LocationTypeId]=@LocationTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LocationTypeId", LocationTypeId));
        }

        public List<Survey_8> FindsByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LocationTypeId]=@LocationTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LocationTypeId", LocationTypeId));
        }

        public int DeleteByLocationTypeId(DBConnection conn, Int64 LocationTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LocationTypeId]=@LocationTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LocationTypeId", LocationTypeId));
        }

        public Survey_8 FindByPlaceOwner(DBConnection conn, String PlaceOwner, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PlaceOwner]=@PlaceOwner " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("PlaceOwner", PlaceOwner));
        }

        public List<Survey_8> FindsByPlaceOwner(DBConnection conn, String PlaceOwner, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PlaceOwner]=@PlaceOwner " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("PlaceOwner", PlaceOwner));
        }

        public int DeleteByPlaceOwner(DBConnection conn, String PlaceOwner, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [PlaceOwner]=@PlaceOwner " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PlaceOwner", PlaceOwner));
        }

        public Survey_8 FindByPlaceUserBenefic(DBConnection conn, String PlaceUserBenefic, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PlaceUserBenefic]=@PlaceUserBenefic " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("PlaceUserBenefic", PlaceUserBenefic));
        }

        public List<Survey_8> FindsByPlaceUserBenefic(DBConnection conn, String PlaceUserBenefic, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PlaceUserBenefic]=@PlaceUserBenefic " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("PlaceUserBenefic", PlaceUserBenefic));
        }

        public int DeleteByPlaceUserBenefic(DBConnection conn, String PlaceUserBenefic, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [PlaceUserBenefic]=@PlaceUserBenefic " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PlaceUserBenefic", PlaceUserBenefic));
        }

        public Survey_8 FindByHomeNo(DBConnection conn, String HomeNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[HomeNo]=@HomeNo " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("HomeNo", HomeNo));
        }

        public List<Survey_8> FindsByHomeNo(DBConnection conn, String HomeNo, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[HomeNo]=@HomeNo " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("HomeNo", HomeNo));
        }

        public int DeleteByHomeNo(DBConnection conn, String HomeNo, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [HomeNo]=@HomeNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("HomeNo", HomeNo));
        }

        public Survey_8 FindByMoo(DBConnection conn, String Moo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Moo]=@Moo " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("Moo", Moo));
        }

        public List<Survey_8> FindsByMoo(DBConnection conn, String Moo, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[Moo]=@Moo " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("Moo", Moo));
        }

        public int DeleteByMoo(DBConnection conn, String Moo, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [Moo]=@Moo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Moo", Moo));
        }

        public Survey_8 FindByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardTypeId]=@BoardTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BoardTypeId", BoardTypeId));
        }

        public List<Survey_8> FindsByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardTypeId]=@BoardTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BoardTypeId", BoardTypeId));
        }

        public int DeleteByBoardTypeId(DBConnection conn, Int64 BoardTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BoardTypeId]=@BoardTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardTypeId", BoardTypeId));
        }

        public Survey_8 FindByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardLocationId]=@BoardLocationId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BoardLocationId", BoardLocationId));
        }

        public List<Survey_8> FindsByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardLocationId]=@BoardLocationId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BoardLocationId", BoardLocationId));
        }

        public int DeleteByBoardLocationId(DBConnection conn, Int64 BoardLocationId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BoardLocationId]=@BoardLocationId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardLocationId", BoardLocationId));
        }

        public Survey_8 FindByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardStyleId]=@BoardStyleId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BoardStyleId", BoardStyleId));
        }

        public List<Survey_8> FindsByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardStyleId]=@BoardStyleId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BoardStyleId", BoardStyleId));
        }

        public int DeleteByBoardStyleId(DBConnection conn, Int64 BoardStyleId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BoardStyleId]=@BoardStyleId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardStyleId", BoardStyleId));
        }

        public Survey_8 FindByBoardSide(DBConnection conn, Int32 BoardSide, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardSide]=@BoardSide " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BoardSide", BoardSide));
        }

        public List<Survey_8> FindsByBoardSide(DBConnection conn, Int32 BoardSide, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardSide]=@BoardSide " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BoardSide", BoardSide));
        }

        public int DeleteByBoardSide(DBConnection conn, Int32 BoardSide, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BoardSide]=@BoardSide " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardSide", BoardSide));
        }

        public Survey_8 FindByBoardText(DBConnection conn, String BoardText, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardText]=@BoardText " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BoardText", BoardText));
        }

        public List<Survey_8> FindsByBoardText(DBConnection conn, String BoardText, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BoardText]=@BoardText " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BoardText", BoardText));
        }

        public int DeleteByBoardText(DBConnection conn, String BoardText, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BoardText]=@BoardText " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BoardText", BoardText));
        }

        public Survey_8 FindByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[RoadTypeId]=@RoadTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("RoadTypeId", RoadTypeId));
        }

        public List<Survey_8> FindsByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[RoadTypeId]=@RoadTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("RoadTypeId", RoadTypeId));
        }

        public int DeleteByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [RoadTypeId]=@RoadTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadTypeId", RoadTypeId));
        }

        public Survey_8 FindByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[RoadMaterialId]=@RoadMaterialId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public List<Survey_8> FindsByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[RoadMaterialId]=@RoadMaterialId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public int DeleteByRoadMaterialId(DBConnection conn, Int64 RoadMaterialId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [RoadMaterialId]=@RoadMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadMaterialId", RoadMaterialId));
        }

        public Survey_8 FindByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BridgeTypeId]=@BridgeTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public List<Survey_8> FindsByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BridgeTypeId]=@BridgeTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public int DeleteByBridgeTypeId(DBConnection conn, Int64 BridgeTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BridgeTypeId]=@BridgeTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeTypeId", BridgeTypeId));
        }

        public Survey_8 FindByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BridgeMaterialId]=@BridgeMaterialId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public List<Survey_8> FindsByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[BridgeMaterialId]=@BridgeMaterialId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public int DeleteByBridgeMaterialId(DBConnection conn, Int64 BridgeMaterialId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [BridgeMaterialId]=@BridgeMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BridgeMaterialId", BridgeMaterialId));
        }

        public Survey_8 FindByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PoleTypeId]=@PoleTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("PoleTypeId", PoleTypeId));
        }

        public List<Survey_8> FindsByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PoleTypeId]=@PoleTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("PoleTypeId", PoleTypeId));
        }

        public int DeleteByPoleTypeId(DBConnection conn, Int64 PoleTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [PoleTypeId]=@PoleTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleTypeId", PoleTypeId));
        }

        public Survey_8 FindByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PoleMaterialId]=@PoleMaterialId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public List<Survey_8> FindsByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[PoleMaterialId]=@PoleMaterialId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public int DeleteByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [PoleMaterialId]=@PoleMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public Survey_8 FindByLandParcelNo(DBConnection conn, String LandParcelNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandParcelNo]=@LandParcelNo " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandParcelNo", LandParcelNo));
        }

        public List<Survey_8> FindsByLandParcelNo(DBConnection conn, String LandParcelNo, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandParcelNo]=@LandParcelNo " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandParcelNo", LandParcelNo));
        }

        public int DeleteByLandParcelNo(DBConnection conn, String LandParcelNo, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandParcelNo]=@LandParcelNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandParcelNo", LandParcelNo));
        }

        public Survey_8 FindByLandNo(DBConnection conn, String LandNo, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandNo]=@LandNo " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandNo", LandNo));
        }

        public List<Survey_8> FindsByLandNo(DBConnection conn, String LandNo, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandNo]=@LandNo " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandNo", LandNo));
        }

        public int DeleteByLandNo(DBConnection conn, String LandNo, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandNo]=@LandNo " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandNo", LandNo));
        }

        public Survey_8 FindByLandRai(DBConnection conn, Int32 LandRai, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandRai]=@LandRai " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandRai", LandRai));
        }

        public List<Survey_8> FindsByLandRai(DBConnection conn, Int32 LandRai, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandRai]=@LandRai " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandRai", LandRai));
        }

        public int DeleteByLandRai(DBConnection conn, Int32 LandRai, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandRai]=@LandRai " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandRai", LandRai));
        }

        public Survey_8 FindByLandNgan(DBConnection conn, Int32 LandNgan, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandNgan]=@LandNgan " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandNgan", LandNgan));
        }

        public List<Survey_8> FindsByLandNgan(DBConnection conn, Int32 LandNgan, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandNgan]=@LandNgan " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandNgan", LandNgan));
        }

        public int DeleteByLandNgan(DBConnection conn, Int32 LandNgan, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandNgan]=@LandNgan " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandNgan", LandNgan));
        }

        public Survey_8 FindByLandWa(DBConnection conn, Int32 LandWa, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandWa]=@LandWa " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandWa", LandWa));
        }

        public List<Survey_8> FindsByLandWa(DBConnection conn, Int32 LandWa, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandWa]=@LandWa " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandWa", LandWa));
        }

        public int DeleteByLandWa(DBConnection conn, Int32 LandWa, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandWa]=@LandWa " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandWa", LandWa));
        }

        public Survey_8 FindByLandSubWa(DBConnection conn, Int32 LandSubWa, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandSubWa]=@LandSubWa " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandSubWa", LandSubWa));
        }

        public List<Survey_8> FindsByLandSubWa(DBConnection conn, Int32 LandSubWa, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandSubWa]=@LandSubWa " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandSubWa", LandSubWa));
        }

        public int DeleteByLandSubWa(DBConnection conn, Int32 LandSubWa, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandSubWa]=@LandSubWa " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandSubWa", LandSubWa));
        }

        public Survey_8 FindByLandUsed(DBConnection conn, String LandUsed, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandUsed]=@LandUsed " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandUsed", LandUsed));
        }

        public List<Survey_8> FindsByLandUsed(DBConnection conn, String LandUsed, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandUsed]=@LandUsed " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandUsed", LandUsed));
        }

        public int DeleteByLandUsed(DBConnection conn, String LandUsed, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandUsed]=@LandUsed " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandUsed", LandUsed));
        }

        public Survey_8 FindByLandParcelTypeId(DBConnection conn, Int64 LandParcelTypeId, string orderBy="")
        {
            String sql = "Select Top 1 Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandParcelTypeId]=@LandParcelTypeId " + orderBy;

            return QueryUtil.Find<Survey_8>(conn, sql, new DBParameter("LandParcelTypeId", LandParcelTypeId));
        }

        public List<Survey_8> FindsByLandParcelTypeId(DBConnection conn, Int64 LandParcelTypeId, string orderBy="")
        {
            String sql = "Select Survey_8.* From [Survey_8] Survey_8 Where Survey_8.[LandParcelTypeId]=@LandParcelTypeId " + orderBy;

            return QueryUtil.FindList<Survey_8>(conn, sql, new DBParameter("LandParcelTypeId", LandParcelTypeId));
        }

        public int DeleteByLandParcelTypeId(DBConnection conn, Int64 LandParcelTypeId, string whereText="")
        {
            String sql = "Delete From [Survey_8] Where [LandParcelTypeId]=@LandParcelTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LandParcelTypeId", LandParcelTypeId));
        }

        #endregion

    }
}
