using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.polematerial.model;

namespace Library.Winplus.syslib.standard.polematerial.repository
{
    public partial class PoleMaterialRepository : SimpleRepository<PoleMaterial>
    {
        #region General Query

        public PoleMaterialRepository()
        {
            SqlInsert = @"Insert Into [PoleMaterial]([PoleMaterialName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@PoleMaterialName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [PoleMaterial] Set [PoleMaterialName]=@PoleMaterialName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [PoleMaterialId]=@PoleMaterialId";
            SqlDelete = @"Delete From [PoleMaterial] Where [PoleMaterialId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [PoleMaterial] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<PoleMaterial> lstImport)
        {
            if (lstImport != null)
            {
                foreach (PoleMaterial model in lstImport)
                {
                    if (model.PoleMaterialId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public PoleMaterial FindByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[PoleMaterialId]=@PoleMaterialId " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public List<PoleMaterial> FindsByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[PoleMaterialId]=@PoleMaterialId " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public int DeleteByPoleMaterialId(DBConnection conn, Int64 PoleMaterialId, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [PoleMaterialId]=@PoleMaterialId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleMaterialId", PoleMaterialId));
        }

        public PoleMaterial FindByPoleMaterialName(DBConnection conn, String PoleMaterialName, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[PoleMaterialName]=@PoleMaterialName " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("PoleMaterialName", PoleMaterialName));
        }

        public List<PoleMaterial> FindsByPoleMaterialName(DBConnection conn, String PoleMaterialName, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[PoleMaterialName]=@PoleMaterialName " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("PoleMaterialName", PoleMaterialName));
        }

        public int DeleteByPoleMaterialName(DBConnection conn, String PoleMaterialName, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [PoleMaterialName]=@PoleMaterialName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("PoleMaterialName", PoleMaterialName));
        }

        public PoleMaterial FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<PoleMaterial> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public PoleMaterial FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<PoleMaterial> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public PoleMaterial FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<PoleMaterial> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public PoleMaterial FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<PoleMaterial> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public PoleMaterial FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<PoleMaterial>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<PoleMaterial> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select PoleMaterial.* From [PoleMaterial] PoleMaterial Where PoleMaterial.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<PoleMaterial>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [PoleMaterial] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
