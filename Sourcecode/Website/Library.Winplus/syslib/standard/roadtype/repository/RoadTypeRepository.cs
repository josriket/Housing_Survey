using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.roadtype.model;

namespace Library.Winplus.syslib.standard.roadtype.repository
{
    public partial class RoadTypeRepository : SimpleRepository<RoadType>
    {
        #region General Query

        public RoadTypeRepository()
        {
            SqlInsert = @"Insert Into [RoadType]([RoadTypeName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@RoadTypeName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [RoadType] Set [RoadTypeName]=@RoadTypeName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [RoadTypeId]=@RoadTypeId";
            SqlDelete = @"Delete From [RoadType] Where [RoadTypeId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [RoadType] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<RoadType> lstImport)
        {
            if (lstImport != null)
            {
                foreach (RoadType model in lstImport)
                {
                    if (model.RoadTypeId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public RoadType FindByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[RoadTypeId]=@RoadTypeId " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("RoadTypeId", RoadTypeId));
        }

        public List<RoadType> FindsByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[RoadTypeId]=@RoadTypeId " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("RoadTypeId", RoadTypeId));
        }

        public int DeleteByRoadTypeId(DBConnection conn, Int64 RoadTypeId, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [RoadTypeId]=@RoadTypeId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadTypeId", RoadTypeId));
        }

        public RoadType FindByRoadTypeName(DBConnection conn, String RoadTypeName, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[RoadTypeName]=@RoadTypeName " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("RoadTypeName", RoadTypeName));
        }

        public List<RoadType> FindsByRoadTypeName(DBConnection conn, String RoadTypeName, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[RoadTypeName]=@RoadTypeName " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("RoadTypeName", RoadTypeName));
        }

        public int DeleteByRoadTypeName(DBConnection conn, String RoadTypeName, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [RoadTypeName]=@RoadTypeName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RoadTypeName", RoadTypeName));
        }

        public RoadType FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<RoadType> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public RoadType FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<RoadType> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public RoadType FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<RoadType> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public RoadType FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<RoadType> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public RoadType FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 RoadType.* From [RoadType] RoadType Where RoadType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<RoadType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<RoadType> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select RoadType.* From [RoadType] RoadType Where RoadType.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<RoadType>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [RoadType] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
