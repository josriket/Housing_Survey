using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemrole.model;
using Library.Winplus.syslib.standard.systemmenu.model;
using Library.Winplus.syslib.standard.systemrolemenu.model;
using Library.Winplus.syslib.standard.systemrolemenu.repository;

namespace Library.Winplus.syslib.standard.systemrole.repository
{
    public partial class SystemRoleRepository
    {
        public void InitialBasicRow(DBConnection conn, LoginUser loginUser, SystemRole model)
        {
            string sql = "select * from SystemMenu where SystemName in('IncidentReport','IncidentReportAuditLog','IncidentReportChanel','IncidentReportCloseApprove','IncidentReportJCIModuleAnalyze','IncidentReportJCIMEModuleAnalyze','IncidentReportJCIModule')";
            List<SystemMenu> irMenu = QueryUtil.FindList<SystemMenu>(conn, sql);

            SystemRoleMenu roleMenu = null;
            SystemRoleMenuRepository repo = new SystemRoleMenuRepository();
            foreach (SystemMenu menu in irMenu)
            {
                sql = "select * from SystemRoleMenu where SystemRoleId=@SystemRoleId and SystemMenuId=@SystemMenuId";
                roleMenu = QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleId", model.SystemRoleId.Value), new DBParameter("SystemMenuId", menu.SystemMenuId.Value));
                if (roleMenu == null)
                {
                    roleMenu = new SystemRoleMenu();
                    roleMenu.SystemRoleId = model.SystemRoleId;
                    roleMenu.SystemMenuId = menu.SystemMenuId;
                    roleMenu.SystemRoleMenuName = model.SystemRoleName + " - " + menu.SystemMenuName;

                    roleMenu.CanSearch = true;
                    roleMenu.CanView = true;
                    roleMenu.CanAdd = true;
                    roleMenu.CanEdit = true;
                    roleMenu.CanDelete = true;
                    roleMenu.CanExport = true;
                    roleMenu.CanImport = true;
                    roleMenu.MenuVisible = false;

                    repo.Create(conn, roleMenu);
                }
            }
        }
    }
}