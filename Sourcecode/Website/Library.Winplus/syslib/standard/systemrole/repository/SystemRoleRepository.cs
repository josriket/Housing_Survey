using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemrole.model;

namespace Library.Winplus.syslib.standard.systemrole.repository
{
    public partial class SystemRoleRepository : SimpleRepository<SystemRole>
    {
        #region General Query

        public SystemRoleRepository()
        {
            SqlInsert = @"Insert Into [SystemRole]([SystemRoleName], [DefaultMenuUrl], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@SystemRoleName, @DefaultMenuUrl, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SystemRole] Set [SystemRoleName]=@SystemRoleName, [DefaultMenuUrl]=@DefaultMenuUrl, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SystemRoleId]=@SystemRoleId";
            SqlDelete = @"Delete From [SystemRole] Where [SystemRoleId]=@Id";
            SqlActiveStatus = @"Update [SystemRole] Set [ActiveStatusId]=@ActiveStatusId Where [SystemRoleId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SystemRole] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SystemRole> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SystemRole model in lstImport)
                {
                    if (model.SystemRoleId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SystemRole FindBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[SystemRoleId]=@SystemRoleId " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("SystemRoleId", SystemRoleId));
        }

        public List<SystemRole> FindsBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[SystemRoleId]=@SystemRoleId " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("SystemRoleId", SystemRoleId));
        }

        public int DeleteBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [SystemRoleId]=@SystemRoleId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemRoleId", SystemRoleId));
        }

        public SystemRole FindBySystemRoleName(DBConnection conn, String SystemRoleName, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[SystemRoleName]=@SystemRoleName " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("SystemRoleName", SystemRoleName));
        }

        public List<SystemRole> FindsBySystemRoleName(DBConnection conn, String SystemRoleName, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[SystemRoleName]=@SystemRoleName " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("SystemRoleName", SystemRoleName));
        }

        public int DeleteBySystemRoleName(DBConnection conn, String SystemRoleName, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [SystemRoleName]=@SystemRoleName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemRoleName", SystemRoleName));
        }

        public SystemRole FindByDefaultMenuUrl(DBConnection conn, String DefaultMenuUrl, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[DefaultMenuUrl]=@DefaultMenuUrl " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("DefaultMenuUrl", DefaultMenuUrl));
        }

        public List<SystemRole> FindsByDefaultMenuUrl(DBConnection conn, String DefaultMenuUrl, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[DefaultMenuUrl]=@DefaultMenuUrl " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("DefaultMenuUrl", DefaultMenuUrl));
        }

        public int DeleteByDefaultMenuUrl(DBConnection conn, String DefaultMenuUrl, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [DefaultMenuUrl]=@DefaultMenuUrl " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DefaultMenuUrl", DefaultMenuUrl));
        }

        public SystemRole FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<SystemRole> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public SystemRole FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SystemRole> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SystemRole FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SystemRole> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SystemRole FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SystemRole> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SystemRole FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemRole.* From [SystemRole] SystemRole Where SystemRole.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SystemRole>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SystemRole> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SystemRole.* From [SystemRole] SystemRole Where SystemRole.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SystemRole>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SystemRole] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
