using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.bglogger.model;

namespace Library.Winplus.syslib.standard.bglogger.repository
{
    public partial class BGLoggerRepository : SimpleRepository<BGLogger>
    {
        #region General Query

        public BGLoggerRepository()
        {
            SqlInsert = @"Insert Into [BGLogger]([BGLoggerName], [LogClass], [LogMethod], [LogLevel], [LogMessage], [LogError], [CreatedOn], [CreatedBy]) Values(@BGLoggerName, @LogClass, @LogMethod, @LogLevel, @LogMessage, @LogError, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [BGLogger] Set [BGLoggerName]=@BGLoggerName, [LogClass]=@LogClass, [LogMethod]=@LogMethod, [LogLevel]=@LogLevel, [LogMessage]=@LogMessage, [LogError]=@LogError, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [BGLoggerId]=@BGLoggerId";
            SqlDelete = @"Delete From [BGLogger] Where [BGLoggerId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [BGLogger] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<BGLogger> lstImport)
        {
            if (lstImport != null)
            {
                foreach (BGLogger model in lstImport)
                {
                    if (model.BGLoggerId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public BGLogger FindByBGLoggerId(DBConnection conn, Int64 BGLoggerId, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[BGLoggerId]=@BGLoggerId " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("BGLoggerId", BGLoggerId));
        }

        public List<BGLogger> FindsByBGLoggerId(DBConnection conn, Int64 BGLoggerId, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[BGLoggerId]=@BGLoggerId " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("BGLoggerId", BGLoggerId));
        }

        public int DeleteByBGLoggerId(DBConnection conn, Int64 BGLoggerId, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [BGLoggerId]=@BGLoggerId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BGLoggerId", BGLoggerId));
        }

        public BGLogger FindByBGLoggerName(DBConnection conn, String BGLoggerName, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[BGLoggerName]=@BGLoggerName " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("BGLoggerName", BGLoggerName));
        }

        public List<BGLogger> FindsByBGLoggerName(DBConnection conn, String BGLoggerName, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[BGLoggerName]=@BGLoggerName " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("BGLoggerName", BGLoggerName));
        }

        public int DeleteByBGLoggerName(DBConnection conn, String BGLoggerName, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [BGLoggerName]=@BGLoggerName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("BGLoggerName", BGLoggerName));
        }

        public BGLogger FindByLogClass(DBConnection conn, String LogClass, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogClass]=@LogClass " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("LogClass", LogClass));
        }

        public List<BGLogger> FindsByLogClass(DBConnection conn, String LogClass, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogClass]=@LogClass " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("LogClass", LogClass));
        }

        public int DeleteByLogClass(DBConnection conn, String LogClass, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [LogClass]=@LogClass " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogClass", LogClass));
        }

        public BGLogger FindByLogMethod(DBConnection conn, String LogMethod, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogMethod]=@LogMethod " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("LogMethod", LogMethod));
        }

        public List<BGLogger> FindsByLogMethod(DBConnection conn, String LogMethod, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogMethod]=@LogMethod " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("LogMethod", LogMethod));
        }

        public int DeleteByLogMethod(DBConnection conn, String LogMethod, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [LogMethod]=@LogMethod " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogMethod", LogMethod));
        }

        public BGLogger FindByLogLevel(DBConnection conn, String LogLevel, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogLevel]=@LogLevel " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("LogLevel", LogLevel));
        }

        public List<BGLogger> FindsByLogLevel(DBConnection conn, String LogLevel, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogLevel]=@LogLevel " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("LogLevel", LogLevel));
        }

        public int DeleteByLogLevel(DBConnection conn, String LogLevel, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [LogLevel]=@LogLevel " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogLevel", LogLevel));
        }

        public BGLogger FindByLogMessage(DBConnection conn, String LogMessage, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogMessage]=@LogMessage " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("LogMessage", LogMessage));
        }

        public List<BGLogger> FindsByLogMessage(DBConnection conn, String LogMessage, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogMessage]=@LogMessage " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("LogMessage", LogMessage));
        }

        public int DeleteByLogMessage(DBConnection conn, String LogMessage, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [LogMessage]=@LogMessage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogMessage", LogMessage));
        }

        public BGLogger FindByLogError(DBConnection conn, String LogError, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogError]=@LogError " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("LogError", LogError));
        }

        public List<BGLogger> FindsByLogError(DBConnection conn, String LogError, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[LogError]=@LogError " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("LogError", LogError));
        }

        public int DeleteByLogError(DBConnection conn, String LogError, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [LogError]=@LogError " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogError", LogError));
        }

        public BGLogger FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<BGLogger> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public BGLogger FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<BGLogger> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public BGLogger FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<BGLogger> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public BGLogger FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 BGLogger.* From [BGLogger] BGLogger Where BGLogger.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<BGLogger>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<BGLogger> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select BGLogger.* From [BGLogger] BGLogger Where BGLogger.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<BGLogger>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [BGLogger] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
