using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.bglogger.model;

namespace Library.Winplus.syslib.standard.bglogger.search
{
    public partial class BGLoggerSearch : SimpleSearch<BGLogger>
    {
        public BGLoggerSearch()
        {
            search = new BGLogger();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "BGLoggerId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select BGLogger.*
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [BGLogger] BGLogger
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=BGLogger.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=BGLogger.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (BGLogger.[BGLoggerName] like '%'+@SearchText+'%' or BGLogger.[LogClass] like '%'+@SearchText+'%' or BGLogger.[LogMethod] like '%'+@SearchText+'%' or BGLogger.[LogLevel] like '%'+@SearchText+'%' or BGLogger.[LogMessage] like '%'+@SearchText+'%' or BGLogger.[LogError] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by BGLoggerId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BGLoggerId", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[BGLoggerId] = @BGLoggerId");
                    param.Add(new DBParameter("BGLoggerId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by BGLoggerName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_BGLoggerName", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[BGLoggerName] like '%'+@BGLoggerName+'%'");
                    param.Add(new DBParameter("BGLoggerName", StringUtil.ToString(text)));
                }

                /* Advance Search by LogClass */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogClass", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[LogClass] like '%'+@LogClass+'%'");
                    param.Add(new DBParameter("LogClass", StringUtil.ToString(text)));
                }

                /* Advance Search by LogMethod */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogMethod", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[LogMethod] like '%'+@LogMethod+'%'");
                    param.Add(new DBParameter("LogMethod", StringUtil.ToString(text)));
                }

                /* Advance Search by LogLevel */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogLevel", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[LogLevel] like '%'+@LogLevel+'%'");
                    param.Add(new DBParameter("LogLevel", StringUtil.ToString(text)));
                }

                /* Advance Search by LogMessage */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogMessage", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[LogMessage] like '%'+@LogMessage+'%'");
                    param.Add(new DBParameter("LogMessage", StringUtil.ToString(text)));
                }

                /* Advance Search by LogError */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogError", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[LogError] like '%'+@LogError+'%'");
                    param.Add(new DBParameter("LogError", StringUtil.ToString(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and BGLogger.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and BGLogger.BGLoggerId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
