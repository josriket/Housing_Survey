using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.userlogin.model;

namespace Library.Winplus.syslib.standard.userlogin.repository
{
    public partial class UserLoginRepository : SimpleRepository<UserLogin>
    {
        #region General Query

        public UserLoginRepository()
        {
            SqlInsert = @"Insert Into [UserLogin]([UserLoginName], [SystemUserId], [TokenId], [ActionDate], [ExpireDate], [CreatedOn], [CreatedBy], [ProjectIds]) Values(@UserLoginName, @SystemUserId, @TokenId, @ActionDate, @ExpireDate, getdate(), @CreatedBy, @ProjectIds)";
            SqlUpdate = @"Update [UserLogin] Set [UserLoginName]=@UserLoginName, [SystemUserId]=@SystemUserId, [TokenId]=@TokenId, [ActionDate]=@ActionDate, [ExpireDate]=@ExpireDate, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy, [ProjectIds]=@ProjectIds Where [UserLoginId]=@UserLoginId";
            SqlDelete = @"Delete From [UserLogin] Where [UserLoginId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [UserLogin] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<UserLogin> lstImport)
        {
            if (lstImport != null)
            {
                foreach (UserLogin model in lstImport)
                {
                    if (model.UserLoginId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public UserLogin FindByUserLoginId(DBConnection conn, Int64 UserLoginId, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[UserLoginId]=@UserLoginId " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("UserLoginId", UserLoginId));
        }

        public List<UserLogin> FindsByUserLoginId(DBConnection conn, Int64 UserLoginId, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[UserLoginId]=@UserLoginId " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("UserLoginId", UserLoginId));
        }

        public int DeleteByUserLoginId(DBConnection conn, Int64 UserLoginId, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [UserLoginId]=@UserLoginId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UserLoginId", UserLoginId));
        }

        public UserLogin FindByUserLoginName(DBConnection conn, String UserLoginName, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[UserLoginName]=@UserLoginName " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("UserLoginName", UserLoginName));
        }

        public List<UserLogin> FindsByUserLoginName(DBConnection conn, String UserLoginName, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[UserLoginName]=@UserLoginName " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("UserLoginName", UserLoginName));
        }

        public int DeleteByUserLoginName(DBConnection conn, String UserLoginName, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [UserLoginName]=@UserLoginName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UserLoginName", UserLoginName));
        }

        public UserLogin FindBySystemUserId(DBConnection conn, Int64 SystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[SystemUserId]=@SystemUserId " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("SystemUserId", SystemUserId));
        }

        public List<UserLogin> FindsBySystemUserId(DBConnection conn, Int64 SystemUserId, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[SystemUserId]=@SystemUserId " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("SystemUserId", SystemUserId));
        }

        public int DeleteBySystemUserId(DBConnection conn, Int64 SystemUserId, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [SystemUserId]=@SystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemUserId", SystemUserId));
        }

        public UserLogin FindByTokenId(DBConnection conn, String TokenId, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[TokenId]=@TokenId " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("TokenId", TokenId));
        }

        public List<UserLogin> FindsByTokenId(DBConnection conn, String TokenId, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[TokenId]=@TokenId " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("TokenId", TokenId));
        }

        public int DeleteByTokenId(DBConnection conn, String TokenId, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [TokenId]=@TokenId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("TokenId", TokenId));
        }

        public UserLogin FindByActionDate(DBConnection conn, DateTime ActionDate, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ActionDate]=@ActionDate " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("ActionDate", ActionDate));
        }

        public List<UserLogin> FindsByActionDate(DBConnection conn, DateTime ActionDate, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ActionDate]=@ActionDate " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("ActionDate", ActionDate));
        }

        public int DeleteByActionDate(DBConnection conn, DateTime ActionDate, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [ActionDate]=@ActionDate " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActionDate", ActionDate));
        }

        public UserLogin FindByExpireDate(DBConnection conn, DateTime ExpireDate, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ExpireDate]=@ExpireDate " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("ExpireDate", ExpireDate));
        }

        public List<UserLogin> FindsByExpireDate(DBConnection conn, DateTime ExpireDate, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ExpireDate]=@ExpireDate " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("ExpireDate", ExpireDate));
        }

        public int DeleteByExpireDate(DBConnection conn, DateTime ExpireDate, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [ExpireDate]=@ExpireDate " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ExpireDate", ExpireDate));
        }

        public UserLogin FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<UserLogin> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public UserLogin FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<UserLogin> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public UserLogin FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<UserLogin> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public UserLogin FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<UserLogin> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        public UserLogin FindByProjectIds(DBConnection conn, String ProjectIds, string orderBy="")
        {
            String sql = "Select Top 1 UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ProjectIds]=@ProjectIds " + orderBy;

            return QueryUtil.Find<UserLogin>(conn, sql, new DBParameter("ProjectIds", ProjectIds));
        }

        public List<UserLogin> FindsByProjectIds(DBConnection conn, String ProjectIds, string orderBy="")
        {
            String sql = "Select UserLogin.* From [UserLogin] UserLogin Where UserLogin.[ProjectIds]=@ProjectIds " + orderBy;

            return QueryUtil.FindList<UserLogin>(conn, sql, new DBParameter("ProjectIds", ProjectIds));
        }

        public int DeleteByProjectIds(DBConnection conn, String ProjectIds, string whereText="")
        {
            String sql = "Delete From [UserLogin] Where [ProjectIds]=@ProjectIds " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectIds", ProjectIds));
        }

        #endregion

    }
}
