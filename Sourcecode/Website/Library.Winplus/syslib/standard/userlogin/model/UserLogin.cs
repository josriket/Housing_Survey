using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.userlogin.model
{
    [Serializable]
    public partial class UserLogin : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("UserLoginId", typeof(MyResources.Winplus.Resource))]
        public Int64? UserLoginId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UserLoginName", typeof(MyResources.Winplus.Resource))]
        public String UserLoginName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemUserId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemUserId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("TokenId", typeof(MyResources.Winplus.Resource))]
        public String TokenId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ActionDate", typeof(MyResources.Winplus.Resource))]
        public DateTime? ActionDate { get; set; }
        public string ActionDateText
        {
            get { return StringUtil.FormatDate(ActionDate); }
            set { ActionDate = StringUtil.ToDate(value); }
        }
        public string ActionDateTimeText
        {
            get { return StringUtil.FormatTime(ActionDate); }
            set { ActionDate = StringUtil.AddedTime(ActionDate, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ExpireDate", typeof(MyResources.Winplus.Resource))]
        public DateTime? ExpireDate { get; set; }
        public string ExpireDateText
        {
            get { return StringUtil.FormatDate(ExpireDate); }
            set { ExpireDate = StringUtil.ToDate(value); }
        }
        public string ExpireDateTimeText
        {
            get { return StringUtil.FormatTime(ExpireDate); }
            set { ExpireDate = StringUtil.AddedTime(ExpireDate, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ProjectIds", typeof(MyResources.Winplus.Resource))]
        public String ProjectIds { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String SystemUserIdSystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
