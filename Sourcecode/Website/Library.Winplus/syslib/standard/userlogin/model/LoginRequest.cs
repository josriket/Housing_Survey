﻿using System;

namespace Library.Winplus.syslib.standard.userlogin.model
{
    public class LoginRequest
    {
        public String UserName { get; set; }
        public String Password { get; set; }
    }
}
