using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.userlogin.model;

namespace Library.Winplus.syslib.standard.userlogin.search
{
    public partial class UserLoginSearch : SimpleSearch<UserLogin>
    {
        public UserLoginSearch()
        {
            search = new UserLogin();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "UserLoginId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select UserLogin.*
                 , SystemUser_SystemUserId.[SystemUserName] as [SystemUserIdSystemUserName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [UserLogin] UserLogin
                Left Join [SystemUser] SystemUser_SystemUserId on SystemUser_SystemUserId.[SystemUserId]=UserLogin.[SystemUserId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=UserLogin.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=UserLogin.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (UserLogin.[UserLoginName] like '%'+@SearchText+'%' or SystemUser_SystemUserId.[SystemUserName] like '%'+@SearchText+'%' or UserLogin.[TokenId] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%' or UserLogin.[ProjectIds] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by UserLoginId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UserLoginId", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[UserLoginId] = @UserLoginId");
                    param.Add(new DBParameter("UserLoginId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by UserLoginName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UserLoginName", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[UserLoginName] like '%'+@UserLoginName+'%'");
                    param.Add(new DBParameter("UserLoginName", StringUtil.ToString(text)));
                }

                /* Advance Search by SystemUserId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemUserId", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[SystemUserId] = @SystemUserId");
                    param.Add(new DBParameter("SystemUserId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by TokenId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_TokenId", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[TokenId] like '%'+@TokenId+'%'");
                    param.Add(new DBParameter("TokenId", StringUtil.ToString(text)));
                }

                /* Advance Search by ActionDate From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActionDate_From", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`ActionDate` >= @ActionDate_From");
                    param.Add(new DBParameter("ActionDate_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ActionDate To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActionDate_To", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`ActionDate` < DATE_ADD(@ActionDate_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ActionDate_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ExpireDate From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ExpireDate_From", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`ExpireDate` >= @ExpireDate_From");
                    param.Add(new DBParameter("ExpireDate_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ExpireDate To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ExpireDate_To", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`ExpireDate` < DATE_ADD(@ExpireDate_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ExpireDate_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ProjectIds */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ProjectIds", ref text))
                {
                    sqlQuery.AppendLine("and UserLogin.[ProjectIds] like '%'+@ProjectIds+'%'");
                    param.Add(new DBParameter("ProjectIds", StringUtil.ToString(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and UserLogin.UserLoginId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
