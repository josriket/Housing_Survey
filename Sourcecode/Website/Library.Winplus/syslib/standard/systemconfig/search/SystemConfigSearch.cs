using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemconfig.model;

namespace Library.Winplus.syslib.standard.systemconfig.search
{
    public partial class SystemConfigSearch : SimpleSearch<SystemConfig>
    {
        public SystemConfigSearch()
        {
            search = new SystemConfig();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SystemConfigId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select SystemConfig.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [SystemConfig] SystemConfig
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=SystemConfig.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=SystemConfig.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=SystemConfig.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (SystemConfig.[SystemConfigName] like '%'+@SearchText+'%' or SystemConfig.[ConfigKey] like '%'+@SearchText+'%' or SystemConfig.[ConfigValue] like '%'+@SearchText+'%' or SystemConfig.[Remark] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SystemConfigId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemConfigId", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[SystemConfigId] = @SystemConfigId");
                    param.Add(new DBParameter("SystemConfigId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemConfigName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemConfigName", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[SystemConfigName] like '%'+@SystemConfigName+'%'");
                    param.Add(new DBParameter("SystemConfigName", StringUtil.ToString(text)));
                }

                /* Advance Search by ConfigKey */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ConfigKey", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[ConfigKey] like '%'+@ConfigKey+'%'");
                    param.Add(new DBParameter("ConfigKey", StringUtil.ToString(text)));
                }

                /* Advance Search by ConfigValue */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ConfigValue", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[ConfigValue] like '%'+@ConfigValue+'%'");
                    param.Add(new DBParameter("ConfigValue", StringUtil.ToString(text)));
                }

                /* Advance Search by Remark */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Remark", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[Remark] like '%'+@Remark+'%'");
                    param.Add(new DBParameter("Remark", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemConfig.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and SystemConfig.SystemConfigId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
