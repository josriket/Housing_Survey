using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemconfig.model;

namespace Library.Winplus.syslib.standard.systemconfig.repository
{
    public partial class SystemConfigRepository : SimpleRepository<SystemConfig>
    {
        #region General Query

        public SystemConfigRepository()
        {
            SqlInsert = @"Insert Into [SystemConfig]([SystemConfigName], [ConfigKey], [ConfigValue], [Remark], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@SystemConfigName, @ConfigKey, @ConfigValue, @Remark, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SystemConfig] Set [SystemConfigName]=@SystemConfigName, [ConfigKey]=@ConfigKey, [ConfigValue]=@ConfigValue, [Remark]=@Remark, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SystemConfigId]=@SystemConfigId";
            SqlDelete = @"Delete From [SystemConfig] Where [SystemConfigId]=@Id";
            SqlActiveStatus = @"Update [SystemConfig] Set [ActiveStatusId]=@ActiveStatusId Where [SystemConfigId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SystemConfig] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SystemConfig> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SystemConfig model in lstImport)
                {
                    if (model.SystemConfigId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SystemConfig FindBySystemConfigId(DBConnection conn, Int64 SystemConfigId, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[SystemConfigId]=@SystemConfigId " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("SystemConfigId", SystemConfigId));
        }

        public List<SystemConfig> FindsBySystemConfigId(DBConnection conn, Int64 SystemConfigId, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[SystemConfigId]=@SystemConfigId " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("SystemConfigId", SystemConfigId));
        }

        public int DeleteBySystemConfigId(DBConnection conn, Int64 SystemConfigId, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [SystemConfigId]=@SystemConfigId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemConfigId", SystemConfigId));
        }

        public SystemConfig FindBySystemConfigName(DBConnection conn, String SystemConfigName, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[SystemConfigName]=@SystemConfigName " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("SystemConfigName", SystemConfigName));
        }

        public List<SystemConfig> FindsBySystemConfigName(DBConnection conn, String SystemConfigName, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[SystemConfigName]=@SystemConfigName " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("SystemConfigName", SystemConfigName));
        }

        public int DeleteBySystemConfigName(DBConnection conn, String SystemConfigName, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [SystemConfigName]=@SystemConfigName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemConfigName", SystemConfigName));
        }

        public SystemConfig FindByConfigKey(DBConnection conn, String ConfigKey, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ConfigKey]=@ConfigKey " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("ConfigKey", ConfigKey));
        }

        public List<SystemConfig> FindsByConfigKey(DBConnection conn, String ConfigKey, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ConfigKey]=@ConfigKey " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("ConfigKey", ConfigKey));
        }

        public int DeleteByConfigKey(DBConnection conn, String ConfigKey, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [ConfigKey]=@ConfigKey " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ConfigKey", ConfigKey));
        }

        public SystemConfig FindByConfigValue(DBConnection conn, String ConfigValue, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ConfigValue]=@ConfigValue " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("ConfigValue", ConfigValue));
        }

        public List<SystemConfig> FindsByConfigValue(DBConnection conn, String ConfigValue, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ConfigValue]=@ConfigValue " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("ConfigValue", ConfigValue));
        }

        public int DeleteByConfigValue(DBConnection conn, String ConfigValue, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [ConfigValue]=@ConfigValue " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ConfigValue", ConfigValue));
        }

        public SystemConfig FindByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[Remark]=@Remark " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("Remark", Remark));
        }

        public List<SystemConfig> FindsByRemark(DBConnection conn, String Remark, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[Remark]=@Remark " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("Remark", Remark));
        }

        public int DeleteByRemark(DBConnection conn, String Remark, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [Remark]=@Remark " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Remark", Remark));
        }

        public SystemConfig FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<SystemConfig> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public SystemConfig FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SystemConfig> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SystemConfig FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SystemConfig> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SystemConfig FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SystemConfig> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SystemConfig FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SystemConfig>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SystemConfig> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SystemConfig.* From [SystemConfig] SystemConfig Where SystemConfig.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SystemConfig>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SystemConfig] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
