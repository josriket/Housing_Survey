using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.systemrolemenu.model
{
    [Serializable]
    public partial class SystemRoleMenu : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("SystemRoleMenuId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemRoleMenuId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemRoleMenuName", typeof(MyResources.Winplus.Resource))]
        public String SystemRoleMenuName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemMenuId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemMenuId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemRoleId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemRoleId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanSearch", typeof(MyResources.Winplus.Resource))]
        public Boolean CanSearch { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanView", typeof(MyResources.Winplus.Resource))]
        public Boolean CanView { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanAdd", typeof(MyResources.Winplus.Resource))]
        public Boolean CanAdd { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanEdit", typeof(MyResources.Winplus.Resource))]
        public Boolean CanEdit { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanDelete", typeof(MyResources.Winplus.Resource))]
        public Boolean CanDelete { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanExport", typeof(MyResources.Winplus.Resource))]
        public Boolean CanExport { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CanImport", typeof(MyResources.Winplus.Resource))]
        public Boolean CanImport { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MenuVisible", typeof(MyResources.Winplus.Resource))]
        public Boolean MenuVisible { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemMenuName", typeof(MyResources.Winplus.Resource))]
        public String SystemMenuIdSystemMenuName { get; set; }

        [LocalizedDisplayName("SystemRoleName", typeof(MyResources.Winplus.Resource))]
        public String SystemRoleIdSystemRoleName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
