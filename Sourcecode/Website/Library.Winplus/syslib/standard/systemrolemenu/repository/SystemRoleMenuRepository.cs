using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemrolemenu.model;

namespace Library.Winplus.syslib.standard.systemrolemenu.repository
{
    public partial class SystemRoleMenuRepository : SimpleRepository<SystemRoleMenu>
    {
        #region General Query

        public SystemRoleMenuRepository()
        {
            SqlInsert = @"Insert Into [SystemRoleMenu]([SystemRoleMenuName], [SystemMenuId], [SystemRoleId], [CanSearch], [CanView], [CanAdd], [CanEdit], [CanDelete], [CanExport], [CanImport], [MenuVisible], [CreatedOn], [CreatedBy]) Values(@SystemRoleMenuName, @SystemMenuId, @SystemRoleId, @CanSearch, @CanView, @CanAdd, @CanEdit, @CanDelete, @CanExport, @CanImport, @MenuVisible, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SystemRoleMenu] Set [SystemRoleMenuName]=@SystemRoleMenuName, [SystemMenuId]=@SystemMenuId, [SystemRoleId]=@SystemRoleId, [CanSearch]=@CanSearch, [CanView]=@CanView, [CanAdd]=@CanAdd, [CanEdit]=@CanEdit, [CanDelete]=@CanDelete, [CanExport]=@CanExport, [CanImport]=@CanImport, [MenuVisible]=@MenuVisible, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SystemRoleMenuId]=@SystemRoleMenuId";
            SqlDelete = @"Delete From [SystemRoleMenu] Where [SystemRoleMenuId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SystemRoleMenu] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SystemRoleMenu> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SystemRoleMenu model in lstImport)
                {
                    if (model.SystemRoleMenuId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SystemRoleMenu FindBySystemRoleMenuId(DBConnection conn, Int64 SystemRoleMenuId, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemRoleMenuId]=@SystemRoleMenuId " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleMenuId", SystemRoleMenuId));
        }

        public List<SystemRoleMenu> FindsBySystemRoleMenuId(DBConnection conn, Int64 SystemRoleMenuId, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemRoleMenuId]=@SystemRoleMenuId " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleMenuId", SystemRoleMenuId));
        }

        public int DeleteBySystemRoleMenuId(DBConnection conn, Int64 SystemRoleMenuId, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [SystemRoleMenuId]=@SystemRoleMenuId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemRoleMenuId", SystemRoleMenuId));
        }

        public SystemRoleMenu FindBySystemRoleMenuName(DBConnection conn, String SystemRoleMenuName, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemRoleMenuName]=@SystemRoleMenuName " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleMenuName", SystemRoleMenuName));
        }

        public List<SystemRoleMenu> FindsBySystemRoleMenuName(DBConnection conn, String SystemRoleMenuName, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemRoleMenuName]=@SystemRoleMenuName " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleMenuName", SystemRoleMenuName));
        }

        public int DeleteBySystemRoleMenuName(DBConnection conn, String SystemRoleMenuName, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [SystemRoleMenuName]=@SystemRoleMenuName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemRoleMenuName", SystemRoleMenuName));
        }

        public SystemRoleMenu FindBySystemMenuId(DBConnection conn, Int64 SystemMenuId, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemMenuId]=@SystemMenuId " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("SystemMenuId", SystemMenuId));
        }

        public List<SystemRoleMenu> FindsBySystemMenuId(DBConnection conn, Int64 SystemMenuId, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemMenuId]=@SystemMenuId " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("SystemMenuId", SystemMenuId));
        }

        public int DeleteBySystemMenuId(DBConnection conn, Int64 SystemMenuId, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [SystemMenuId]=@SystemMenuId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemMenuId", SystemMenuId));
        }

        public SystemRoleMenu FindBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemRoleId]=@SystemRoleId " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleId", SystemRoleId));
        }

        public List<SystemRoleMenu> FindsBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[SystemRoleId]=@SystemRoleId " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("SystemRoleId", SystemRoleId));
        }

        public int DeleteBySystemRoleId(DBConnection conn, Int64 SystemRoleId, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [SystemRoleId]=@SystemRoleId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemRoleId", SystemRoleId));
        }

        public SystemRoleMenu FindByCanSearch(DBConnection conn, Boolean CanSearch, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanSearch]=@CanSearch " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanSearch", CanSearch));
        }

        public List<SystemRoleMenu> FindsByCanSearch(DBConnection conn, Boolean CanSearch, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanSearch]=@CanSearch " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanSearch", CanSearch));
        }

        public int DeleteByCanSearch(DBConnection conn, Boolean CanSearch, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanSearch]=@CanSearch " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanSearch", CanSearch));
        }

        public SystemRoleMenu FindByCanView(DBConnection conn, Boolean CanView, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanView]=@CanView " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanView", CanView));
        }

        public List<SystemRoleMenu> FindsByCanView(DBConnection conn, Boolean CanView, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanView]=@CanView " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanView", CanView));
        }

        public int DeleteByCanView(DBConnection conn, Boolean CanView, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanView]=@CanView " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanView", CanView));
        }

        public SystemRoleMenu FindByCanAdd(DBConnection conn, Boolean CanAdd, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanAdd]=@CanAdd " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanAdd", CanAdd));
        }

        public List<SystemRoleMenu> FindsByCanAdd(DBConnection conn, Boolean CanAdd, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanAdd]=@CanAdd " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanAdd", CanAdd));
        }

        public int DeleteByCanAdd(DBConnection conn, Boolean CanAdd, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanAdd]=@CanAdd " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanAdd", CanAdd));
        }

        public SystemRoleMenu FindByCanEdit(DBConnection conn, Boolean CanEdit, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanEdit]=@CanEdit " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanEdit", CanEdit));
        }

        public List<SystemRoleMenu> FindsByCanEdit(DBConnection conn, Boolean CanEdit, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanEdit]=@CanEdit " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanEdit", CanEdit));
        }

        public int DeleteByCanEdit(DBConnection conn, Boolean CanEdit, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanEdit]=@CanEdit " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanEdit", CanEdit));
        }

        public SystemRoleMenu FindByCanDelete(DBConnection conn, Boolean CanDelete, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanDelete]=@CanDelete " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanDelete", CanDelete));
        }

        public List<SystemRoleMenu> FindsByCanDelete(DBConnection conn, Boolean CanDelete, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanDelete]=@CanDelete " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanDelete", CanDelete));
        }

        public int DeleteByCanDelete(DBConnection conn, Boolean CanDelete, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanDelete]=@CanDelete " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanDelete", CanDelete));
        }

        public SystemRoleMenu FindByCanExport(DBConnection conn, Boolean CanExport, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanExport]=@CanExport " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanExport", CanExport));
        }

        public List<SystemRoleMenu> FindsByCanExport(DBConnection conn, Boolean CanExport, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanExport]=@CanExport " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanExport", CanExport));
        }

        public int DeleteByCanExport(DBConnection conn, Boolean CanExport, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanExport]=@CanExport " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanExport", CanExport));
        }

        public SystemRoleMenu FindByCanImport(DBConnection conn, Boolean CanImport, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanImport]=@CanImport " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CanImport", CanImport));
        }

        public List<SystemRoleMenu> FindsByCanImport(DBConnection conn, Boolean CanImport, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CanImport]=@CanImport " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CanImport", CanImport));
        }

        public int DeleteByCanImport(DBConnection conn, Boolean CanImport, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CanImport]=@CanImport " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CanImport", CanImport));
        }

        public SystemRoleMenu FindByMenuVisible(DBConnection conn, Boolean MenuVisible, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[MenuVisible]=@MenuVisible " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("MenuVisible", MenuVisible));
        }

        public List<SystemRoleMenu> FindsByMenuVisible(DBConnection conn, Boolean MenuVisible, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[MenuVisible]=@MenuVisible " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("MenuVisible", MenuVisible));
        }

        public int DeleteByMenuVisible(DBConnection conn, Boolean MenuVisible, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [MenuVisible]=@MenuVisible " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MenuVisible", MenuVisible));
        }

        public SystemRoleMenu FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SystemRoleMenu> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SystemRoleMenu FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SystemRoleMenu> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SystemRoleMenu FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SystemRoleMenu> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SystemRoleMenu FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SystemRoleMenu>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SystemRoleMenu> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SystemRoleMenu.* From [SystemRoleMenu] SystemRoleMenu Where SystemRoleMenu.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SystemRoleMenu>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SystemRoleMenu] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
