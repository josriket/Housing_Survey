using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemrolemenu.model;

namespace Library.Winplus.syslib.standard.systemrolemenu.search
{
    public partial class SystemRoleMenuSearch : SimpleSearch<SystemRoleMenu>
    {
        public SystemRoleMenuSearch()
        {
            search = new SystemRoleMenu();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SystemRoleMenuId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select SystemRoleMenu.*
                 , SystemMenu_SystemMenuId.[SystemMenuName] as [SystemMenuIdSystemMenuName]
                 , SystemRole_SystemRoleId.[SystemRoleName] as [SystemRoleIdSystemRoleName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [SystemRoleMenu] SystemRoleMenu
                Left Join [SystemMenu] SystemMenu_SystemMenuId on SystemMenu_SystemMenuId.[SystemMenuId]=SystemRoleMenu.[SystemMenuId]
                Left Join [SystemRole] SystemRole_SystemRoleId on SystemRole_SystemRoleId.[SystemRoleId]=SystemRoleMenu.[SystemRoleId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=SystemRoleMenu.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=SystemRoleMenu.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (SystemRoleMenu.[SystemRoleMenuName] like '%'+@SearchText+'%' or SystemMenu_SystemMenuId.[SystemMenuName] like '%'+@SearchText+'%' or SystemRole_SystemRoleId.[SystemRoleName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SystemRoleMenuId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemRoleMenuId", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[SystemRoleMenuId] = @SystemRoleMenuId");
                    param.Add(new DBParameter("SystemRoleMenuId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemRoleMenuName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemRoleMenuName", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[SystemRoleMenuName] like '%'+@SystemRoleMenuName+'%'");
                    param.Add(new DBParameter("SystemRoleMenuName", StringUtil.ToString(text)));
                }

                /* Advance Search by SystemMenuId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemMenuId", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[SystemMenuId] = @SystemMenuId");
                    param.Add(new DBParameter("SystemMenuId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemRoleId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemRoleId", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[SystemRoleId] = @SystemRoleId");
                    param.Add(new DBParameter("SystemRoleId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CanSearch */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanSearch", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanSearch] = @CanSearch");
                    param.Add(new DBParameter("CanSearch", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CanView */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanView", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanView] = @CanView");
                    param.Add(new DBParameter("CanView", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CanAdd */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanAdd", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanAdd] = @CanAdd");
                    param.Add(new DBParameter("CanAdd", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CanEdit */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanEdit", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanEdit] = @CanEdit");
                    param.Add(new DBParameter("CanEdit", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CanDelete */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanDelete", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanDelete] = @CanDelete");
                    param.Add(new DBParameter("CanDelete", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CanExport */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanExport", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanExport] = @CanExport");
                    param.Add(new DBParameter("CanExport", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CanImport */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CanImport", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CanImport] = @CanImport");
                    param.Add(new DBParameter("CanImport", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by MenuVisible */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MenuVisible", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[MenuVisible] = @MenuVisible");
                    param.Add(new DBParameter("MenuVisible", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and SystemRoleMenu.SystemRoleMenuId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
