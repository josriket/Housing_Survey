using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.userproject.model
{
    [Serializable]
    public partial class UserProject : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("UserProjectId", typeof(MyResources.Winplus.Resource))]
        public Int64? UserProjectId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UserProjectName", typeof(MyResources.Winplus.Resource))]
        public String UserProjectName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemUserId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemUserId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ProjectId", typeof(MyResources.Winplus.Resource))]
        public Int64? ProjectId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String SystemUserIdSystemUserName { get; set; }

        [LocalizedDisplayName("ProjectName", typeof(MyResources.Winplus.Resource))]
        public String ProjectIdProjectName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
