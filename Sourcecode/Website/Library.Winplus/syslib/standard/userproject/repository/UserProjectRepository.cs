using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.userproject.model;

namespace Library.Winplus.syslib.standard.userproject.repository
{
    public partial class UserProjectRepository : SimpleRepository<UserProject>
    {
        #region General Query

        public UserProjectRepository()
        {
            SqlInsert = @"Insert Into [UserProject]([UserProjectName], [SystemUserId], [ProjectId], [CreatedOn], [CreatedBy]) Values(@UserProjectName, @SystemUserId, @ProjectId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [UserProject] Set [UserProjectName]=@UserProjectName, [SystemUserId]=@SystemUserId, [ProjectId]=@ProjectId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [UserProjectId]=@UserProjectId";
            SqlDelete = @"Delete From [UserProject] Where [UserProjectId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [UserProject] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<UserProject> lstImport)
        {
            if (lstImport != null)
            {
                foreach (UserProject model in lstImport)
                {
                    if (model.UserProjectId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public UserProject FindByUserProjectId(DBConnection conn, Int64 UserProjectId, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[UserProjectId]=@UserProjectId " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("UserProjectId", UserProjectId));
        }

        public List<UserProject> FindsByUserProjectId(DBConnection conn, Int64 UserProjectId, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[UserProjectId]=@UserProjectId " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("UserProjectId", UserProjectId));
        }

        public int DeleteByUserProjectId(DBConnection conn, Int64 UserProjectId, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [UserProjectId]=@UserProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UserProjectId", UserProjectId));
        }

        public UserProject FindByUserProjectName(DBConnection conn, String UserProjectName, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[UserProjectName]=@UserProjectName " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("UserProjectName", UserProjectName));
        }

        public List<UserProject> FindsByUserProjectName(DBConnection conn, String UserProjectName, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[UserProjectName]=@UserProjectName " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("UserProjectName", UserProjectName));
        }

        public int DeleteByUserProjectName(DBConnection conn, String UserProjectName, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [UserProjectName]=@UserProjectName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UserProjectName", UserProjectName));
        }

        public UserProject FindBySystemUserId(DBConnection conn, Int64 SystemUserId, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[SystemUserId]=@SystemUserId " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("SystemUserId", SystemUserId));
        }

        public List<UserProject> FindsBySystemUserId(DBConnection conn, Int64 SystemUserId, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[SystemUserId]=@SystemUserId " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("SystemUserId", SystemUserId));
        }

        public int DeleteBySystemUserId(DBConnection conn, Int64 SystemUserId, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [SystemUserId]=@SystemUserId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemUserId", SystemUserId));
        }

        public UserProject FindByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public List<UserProject> FindsByProjectId(DBConnection conn, Int64 ProjectId, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[ProjectId]=@ProjectId " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("ProjectId", ProjectId));
        }

        public int DeleteByProjectId(DBConnection conn, Int64 ProjectId, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [ProjectId]=@ProjectId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProjectId", ProjectId));
        }

        public UserProject FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<UserProject> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public UserProject FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<UserProject> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public UserProject FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<UserProject> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public UserProject FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 UserProject.* From [UserProject] UserProject Where UserProject.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<UserProject>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<UserProject> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select UserProject.* From [UserProject] UserProject Where UserProject.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<UserProject>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [UserProject] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
