using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.userproject.model;

namespace Library.Winplus.syslib.standard.userproject.search
{
    public partial class UserProjectSearch
    {
        public bool CustomSearch = true;

        public void CustomPrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            ParseRequest(request);
            DefaultSearch = "UserProjectId desc";

            StringBuilder sqlQuery = new StringBuilder(@"
                Select UserProject.*
                 , SystemUser_SystemUserId.[SystemUserName] as [SystemUserIdSystemUserName]
                 , Project_ProjectId.[ProjectName] as [ProjectIdProjectName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [UserProject] UserProject
                Left Join [SystemUser] SystemUser_SystemUserId on SystemUser_SystemUserId.[SystemUserId]=UserProject.[SystemUserId]
                Left Join [Project] Project_ProjectId on Project_ProjectId.[ProjectId]=UserProject.[ProjectId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=UserProject.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=UserProject.[ModifiedBy]
                Where 1=1");

            long? SupervisorRoleId = SystemConfigService.GetLongConfig("SupervisorRoleId", 3);
            if (loginUser.SystemUserId > 1 && loginUser.User.SystemRoleId.Value == SupervisorRoleId)
            {
                sqlQuery.AppendLine($"And SystemUser_SystemUserId.CreatedBy={loginUser.SystemUserId}");
            }

            String text = null;
            /* Default text search */
            if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
            {
                sqlQuery.AppendLine("and (UserProject.[UserProjectName] like '%'+@SearchText+'%' or SystemUser_SystemUserId.[SystemUserName] like '%'+@SearchText+'%' or Project_ProjectId.[ProjectName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
            }

            #region search condition

            /* Advance Search by UserProjectId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_UserProjectId", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[UserProjectId] = @UserProjectId");
                param.Add(new DBParameter("UserProjectId", StringUtil.ToInt64(text)));
            }

            /* Advance Search by UserProjectName */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_UserProjectName", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[UserProjectName] like '%'+@UserProjectName+'%'");
                param.Add(new DBParameter("UserProjectName", StringUtil.ToString(text)));
            }

            /* Advance Search by SystemUserId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemUserId", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[SystemUserId] = @SystemUserId");
                param.Add(new DBParameter("SystemUserId", StringUtil.ToInt64(text)));
            }

            /* Advance Search by ProjectId */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ProjectId", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[ProjectId] = @ProjectId");
                param.Add(new DBParameter("ProjectId", StringUtil.ToInt64(text)));
            }

            /* Advance Search by CreatedOn From*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[CreatedOn] >= @CreatedOn_From");
                param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by CreatedOn To*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[CreatedOn] < dateadd(day,1,@CreatedOn_To)");
                param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by CreatedBy */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[CreatedBy] = @CreatedBy");
                param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
            }

            /* Advance Search by ModifiedOn From*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[ModifiedOn] >= @ModifiedOn_From");
                param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by ModifiedOn To*/
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[ModifiedOn] < dateadd(day,1,@ModifiedOn_To)");
                param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
            }

            /* Advance Search by ModifiedBy */
            if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
            {
                sqlQuery.AppendLine("and UserProject.[ModifiedBy] = @ModifiedBy");
                param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
            }


            if (!string.IsNullOrEmpty(Ids))
            {
                sqlQuery.AppendLine("and UserProject.UserProjectId in(" + Ids + ")");
            }

            #endregion search condition

            /* prepare temporary parameter */
            this.sqlQuery = sqlQuery.ToString();

            this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
        }

    }
}
