using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.emailtemplate.model;

namespace Library.Winplus.syslib.standard.emailtemplate.search
{
    public partial class EmailTemplateSearch : SimpleSearch<EmailTemplate>
    {
        public EmailTemplateSearch()
        {
            search = new EmailTemplate();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "EmailTemplateId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select EmailTemplate.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [EmailTemplate] EmailTemplate
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=EmailTemplate.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=EmailTemplate.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=EmailTemplate.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (EmailTemplate.[EmailTemplateName] like '%'+@SearchText+'%' or EmailTemplate.[TemplateCode] like '%'+@SearchText+'%' or EmailTemplate.[TemplateBody] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by EmailTemplateId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_EmailTemplateId", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[EmailTemplateId] = @EmailTemplateId");
                    param.Add(new DBParameter("EmailTemplateId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by EmailTemplateName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_EmailTemplateName", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[EmailTemplateName] like '%'+@EmailTemplateName+'%'");
                    param.Add(new DBParameter("EmailTemplateName", StringUtil.ToString(text)));
                }

                /* Advance Search by TemplateCode */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_TemplateCode", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[TemplateCode] like '%'+@TemplateCode+'%'");
                    param.Add(new DBParameter("TemplateCode", StringUtil.ToString(text)));
                }

                /* Advance Search by TemplateBody */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_TemplateBody", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[TemplateBody] like '%'+@TemplateBody+'%'");
                    param.Add(new DBParameter("TemplateBody", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and EmailTemplate.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and EmailTemplate.EmailTemplateId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
