using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.emailtemplate.model;

namespace Library.Winplus.syslib.standard.emailtemplate.repository
{
    public partial class EmailTemplateRepository : SimpleRepository<EmailTemplate>
    {
        #region General Query

        public EmailTemplateRepository()
        {
            SqlInsert = @"Insert Into [EmailTemplate]([EmailTemplateName], [TemplateCode], [TemplateBody], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@EmailTemplateName, @TemplateCode, @TemplateBody, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [EmailTemplate] Set [EmailTemplateName]=@EmailTemplateName, [TemplateCode]=@TemplateCode, [TemplateBody]=@TemplateBody, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [EmailTemplateId]=@EmailTemplateId";
            SqlDelete = @"Delete From [EmailTemplate] Where [EmailTemplateId]=@Id";
            SqlActiveStatus = @"Update [EmailTemplate] Set [ActiveStatusId]=@ActiveStatusId Where [EmailTemplateId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [EmailTemplate] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<EmailTemplate> lstImport)
        {
            if (lstImport != null)
            {
                foreach (EmailTemplate model in lstImport)
                {
                    if (model.EmailTemplateId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public EmailTemplate FindByEmailTemplateId(DBConnection conn, Int64 EmailTemplateId, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[EmailTemplateId]=@EmailTemplateId " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("EmailTemplateId", EmailTemplateId));
        }

        public List<EmailTemplate> FindsByEmailTemplateId(DBConnection conn, Int64 EmailTemplateId, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[EmailTemplateId]=@EmailTemplateId " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("EmailTemplateId", EmailTemplateId));
        }

        public int DeleteByEmailTemplateId(DBConnection conn, Int64 EmailTemplateId, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [EmailTemplateId]=@EmailTemplateId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("EmailTemplateId", EmailTemplateId));
        }

        public EmailTemplate FindByEmailTemplateName(DBConnection conn, String EmailTemplateName, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[EmailTemplateName]=@EmailTemplateName " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("EmailTemplateName", EmailTemplateName));
        }

        public List<EmailTemplate> FindsByEmailTemplateName(DBConnection conn, String EmailTemplateName, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[EmailTemplateName]=@EmailTemplateName " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("EmailTemplateName", EmailTemplateName));
        }

        public int DeleteByEmailTemplateName(DBConnection conn, String EmailTemplateName, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [EmailTemplateName]=@EmailTemplateName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("EmailTemplateName", EmailTemplateName));
        }

        public EmailTemplate FindByTemplateCode(DBConnection conn, String TemplateCode, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[TemplateCode]=@TemplateCode " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("TemplateCode", TemplateCode));
        }

        public List<EmailTemplate> FindsByTemplateCode(DBConnection conn, String TemplateCode, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[TemplateCode]=@TemplateCode " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("TemplateCode", TemplateCode));
        }

        public int DeleteByTemplateCode(DBConnection conn, String TemplateCode, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [TemplateCode]=@TemplateCode " + whereText;
            return conn.executeUpdate(sql,new DBParameter("TemplateCode", TemplateCode));
        }

        public EmailTemplate FindByTemplateBody(DBConnection conn, String TemplateBody, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[TemplateBody]=@TemplateBody " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("TemplateBody", TemplateBody));
        }

        public List<EmailTemplate> FindsByTemplateBody(DBConnection conn, String TemplateBody, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[TemplateBody]=@TemplateBody " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("TemplateBody", TemplateBody));
        }

        public int DeleteByTemplateBody(DBConnection conn, String TemplateBody, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [TemplateBody]=@TemplateBody " + whereText;
            return conn.executeUpdate(sql,new DBParameter("TemplateBody", TemplateBody));
        }

        public EmailTemplate FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<EmailTemplate> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public EmailTemplate FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<EmailTemplate> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public EmailTemplate FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<EmailTemplate> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public EmailTemplate FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<EmailTemplate> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public EmailTemplate FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<EmailTemplate>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<EmailTemplate> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select EmailTemplate.* From [EmailTemplate] EmailTemplate Where EmailTemplate.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<EmailTemplate>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [EmailTemplate] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
