using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.floorno.model;

namespace Library.Winplus.syslib.standard.floorno.repository
{
    public partial class FloorNoRepository : SimpleRepository<FloorNo>
    {
        #region General Query

        public FloorNoRepository()
        {
            SqlInsert = @"Insert Into [FloorNo]([FloorNoName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@FloorNoName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [FloorNo] Set [FloorNoName]=@FloorNoName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [FloorNoId]=@FloorNoId";
            SqlDelete = @"Delete From [FloorNo] Where [FloorNoId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [FloorNo] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<FloorNo> lstImport)
        {
            if (lstImport != null)
            {
                foreach (FloorNo model in lstImport)
                {
                    if (model.FloorNoId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public FloorNo FindByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public List<FloorNo> FindsByFloorNoId(DBConnection conn, Int64 FloorNoId, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[FloorNoId]=@FloorNoId " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("FloorNoId", FloorNoId));
        }

        public int DeleteByFloorNoId(DBConnection conn, Int64 FloorNoId, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [FloorNoId]=@FloorNoId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoId", FloorNoId));
        }

        public FloorNo FindByFloorNoName(DBConnection conn, String FloorNoName, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[FloorNoName]=@FloorNoName " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("FloorNoName", FloorNoName));
        }

        public List<FloorNo> FindsByFloorNoName(DBConnection conn, String FloorNoName, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[FloorNoName]=@FloorNoName " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("FloorNoName", FloorNoName));
        }

        public int DeleteByFloorNoName(DBConnection conn, String FloorNoName, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [FloorNoName]=@FloorNoName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("FloorNoName", FloorNoName));
        }

        public FloorNo FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<FloorNo> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public FloorNo FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<FloorNo> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public FloorNo FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<FloorNo> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public FloorNo FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<FloorNo> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public FloorNo FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 FloorNo.* From [FloorNo] FloorNo Where FloorNo.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<FloorNo>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<FloorNo> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select FloorNo.* From [FloorNo] FloorNo Where FloorNo.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<FloorNo>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [FloorNo] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
