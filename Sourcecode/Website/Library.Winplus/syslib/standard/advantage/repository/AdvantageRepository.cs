using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.advantage.model;

namespace Library.Winplus.syslib.standard.advantage.repository
{
    public partial class AdvantageRepository : SimpleRepository<Advantage>
    {
        #region General Query

        public AdvantageRepository()
        {
            SqlInsert = @"Insert Into [Advantage]([AdvantageName], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@AdvantageName, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [Advantage] Set [AdvantageName]=@AdvantageName, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [AdvantageId]=@AdvantageId";
            SqlDelete = @"Delete From [Advantage] Where [AdvantageId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Advantage] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Advantage> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Advantage model in lstImport)
                {
                    if (model.AdvantageId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Advantage FindByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public List<Advantage> FindsByAdvantageId(DBConnection conn, Int64 AdvantageId, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[AdvantageId]=@AdvantageId " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("AdvantageId", AdvantageId));
        }

        public int DeleteByAdvantageId(DBConnection conn, Int64 AdvantageId, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [AdvantageId]=@AdvantageId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("AdvantageId", AdvantageId));
        }

        public Advantage FindByAdvantageName(DBConnection conn, String AdvantageName, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[AdvantageName]=@AdvantageName " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("AdvantageName", AdvantageName));
        }

        public List<Advantage> FindsByAdvantageName(DBConnection conn, String AdvantageName, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[AdvantageName]=@AdvantageName " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("AdvantageName", AdvantageName));
        }

        public int DeleteByAdvantageName(DBConnection conn, String AdvantageName, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [AdvantageName]=@AdvantageName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("AdvantageName", AdvantageName));
        }

        public Advantage FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<Advantage> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public Advantage FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Advantage> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Advantage FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Advantage> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Advantage FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Advantage> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Advantage FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Advantage.* From [Advantage] Advantage Where Advantage.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Advantage>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Advantage> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Advantage.* From [Advantage] Advantage Where Advantage.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Advantage>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Advantage] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
