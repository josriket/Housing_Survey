using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.smstemplate.model;

namespace Library.Winplus.syslib.standard.smstemplate.search
{
    public partial class SMSTemplateSearch : SimpleSearch<SMSTemplate>
    {
        public SMSTemplateSearch()
        {
            search = new SMSTemplate();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SMSTemplateId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select SMSTemplate.*
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [SMSTemplate] SMSTemplate
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=SMSTemplate.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=SMSTemplate.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=SMSTemplate.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (SMSTemplate.[SMSTemplateName] like '%'+@SearchText+'%' or SMSTemplate.[TemplateCode] like '%'+@SearchText+'%' or SMSTemplate.[TemplateBody] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SMSTemplateId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SMSTemplateId", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[SMSTemplateId] = @SMSTemplateId");
                    param.Add(new DBParameter("SMSTemplateId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SMSTemplateName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SMSTemplateName", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[SMSTemplateName] like '%'+@SMSTemplateName+'%'");
                    param.Add(new DBParameter("SMSTemplateName", StringUtil.ToString(text)));
                }

                /* Advance Search by TemplateCode */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_TemplateCode", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[TemplateCode] like '%'+@TemplateCode+'%'");
                    param.Add(new DBParameter("TemplateCode", StringUtil.ToString(text)));
                }

                /* Advance Search by TemplateBody */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_TemplateBody", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[TemplateBody] like '%'+@TemplateBody+'%'");
                    param.Add(new DBParameter("TemplateBody", StringUtil.ToString(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and SMSTemplate.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and SMSTemplate.SMSTemplateId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
