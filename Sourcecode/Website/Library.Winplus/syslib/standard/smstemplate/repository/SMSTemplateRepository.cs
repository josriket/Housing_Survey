using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.smstemplate.model;

namespace Library.Winplus.syslib.standard.smstemplate.repository
{
    public partial class SMSTemplateRepository : SimpleRepository<SMSTemplate>
    {
        #region General Query

        public SMSTemplateRepository()
        {
            SqlInsert = @"Insert Into [SMSTemplate]([SMSTemplateName], [TemplateCode], [TemplateBody], [ActiveStatusId], [CreatedOn], [CreatedBy]) Values(@SMSTemplateName, @TemplateCode, @TemplateBody, @ActiveStatusId, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SMSTemplate] Set [SMSTemplateName]=@SMSTemplateName, [TemplateCode]=@TemplateCode, [TemplateBody]=@TemplateBody, [ActiveStatusId]=@ActiveStatusId, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SMSTemplateId]=@SMSTemplateId";
            SqlDelete = @"Delete From [SMSTemplate] Where [SMSTemplateId]=@Id";
            SqlActiveStatus = @"Update [SMSTemplate] Set [ActiveStatusId]=@ActiveStatusId Where [SMSTemplateId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SMSTemplate] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SMSTemplate> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SMSTemplate model in lstImport)
                {
                    if (model.SMSTemplateId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SMSTemplate FindBySMSTemplateId(DBConnection conn, Int64 SMSTemplateId, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[SMSTemplateId]=@SMSTemplateId " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("SMSTemplateId", SMSTemplateId));
        }

        public List<SMSTemplate> FindsBySMSTemplateId(DBConnection conn, Int64 SMSTemplateId, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[SMSTemplateId]=@SMSTemplateId " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("SMSTemplateId", SMSTemplateId));
        }

        public int DeleteBySMSTemplateId(DBConnection conn, Int64 SMSTemplateId, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [SMSTemplateId]=@SMSTemplateId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SMSTemplateId", SMSTemplateId));
        }

        public SMSTemplate FindBySMSTemplateName(DBConnection conn, String SMSTemplateName, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[SMSTemplateName]=@SMSTemplateName " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("SMSTemplateName", SMSTemplateName));
        }

        public List<SMSTemplate> FindsBySMSTemplateName(DBConnection conn, String SMSTemplateName, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[SMSTemplateName]=@SMSTemplateName " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("SMSTemplateName", SMSTemplateName));
        }

        public int DeleteBySMSTemplateName(DBConnection conn, String SMSTemplateName, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [SMSTemplateName]=@SMSTemplateName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SMSTemplateName", SMSTemplateName));
        }

        public SMSTemplate FindByTemplateCode(DBConnection conn, String TemplateCode, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[TemplateCode]=@TemplateCode " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("TemplateCode", TemplateCode));
        }

        public List<SMSTemplate> FindsByTemplateCode(DBConnection conn, String TemplateCode, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[TemplateCode]=@TemplateCode " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("TemplateCode", TemplateCode));
        }

        public int DeleteByTemplateCode(DBConnection conn, String TemplateCode, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [TemplateCode]=@TemplateCode " + whereText;
            return conn.executeUpdate(sql,new DBParameter("TemplateCode", TemplateCode));
        }

        public SMSTemplate FindByTemplateBody(DBConnection conn, String TemplateBody, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[TemplateBody]=@TemplateBody " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("TemplateBody", TemplateBody));
        }

        public List<SMSTemplate> FindsByTemplateBody(DBConnection conn, String TemplateBody, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[TemplateBody]=@TemplateBody " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("TemplateBody", TemplateBody));
        }

        public int DeleteByTemplateBody(DBConnection conn, String TemplateBody, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [TemplateBody]=@TemplateBody " + whereText;
            return conn.executeUpdate(sql,new DBParameter("TemplateBody", TemplateBody));
        }

        public SMSTemplate FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<SMSTemplate> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public SMSTemplate FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SMSTemplate> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SMSTemplate FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SMSTemplate> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SMSTemplate FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SMSTemplate> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SMSTemplate FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SMSTemplate>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SMSTemplate> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SMSTemplate.* From [SMSTemplate] SMSTemplate Where SMSTemplate.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SMSTemplate>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SMSTemplate] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
