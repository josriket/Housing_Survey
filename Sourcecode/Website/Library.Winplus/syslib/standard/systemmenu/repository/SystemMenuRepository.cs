using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemmenu.model;

namespace Library.Winplus.syslib.standard.systemmenu.repository
{
    public partial class SystemMenuRepository : SimpleRepository<SystemMenu>
    {
        #region General Query

        public SystemMenuRepository()
        {
            SqlInsert = @"Insert Into [SystemMenu]([SystemMenuName], [SystemParentMenuId], [SystemName], [MenuUrl], [IsHidden], [ActiveStatusId], [DisplayIndex], [CreatedOn], [CreatedBy]) Values(@SystemMenuName, @SystemParentMenuId, @SystemName, @MenuUrl, @IsHidden, @ActiveStatusId, @DisplayIndex, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [SystemMenu] Set [SystemMenuName]=@SystemMenuName, [SystemParentMenuId]=@SystemParentMenuId, [SystemName]=@SystemName, [MenuUrl]=@MenuUrl, [IsHidden]=@IsHidden, [ActiveStatusId]=@ActiveStatusId, [DisplayIndex]=@DisplayIndex, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [SystemMenuId]=@SystemMenuId";
            SqlDelete = @"Delete From [SystemMenu] Where [SystemMenuId]=@Id";
            SqlActiveStatus = @"Update [SystemMenu] Set [ActiveStatusId]=@ActiveStatusId Where [SystemMenuId]=@Id";
            SqlFindAll = "Select sm.*, az.[ActiveStatusName] as [ActiveStatusName], su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [SystemMenu] sm Left Join [ActiveStatus] az on az.[ActiveStatusId]=sm.[ActiveStatusId] Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<SystemMenu> lstImport)
        {
            if (lstImport != null)
            {
                foreach (SystemMenu model in lstImport)
                {
                    if (model.SystemMenuId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public SystemMenu FindBySystemMenuId(DBConnection conn, Int64 SystemMenuId, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemMenuId]=@SystemMenuId " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("SystemMenuId", SystemMenuId));
        }

        public List<SystemMenu> FindsBySystemMenuId(DBConnection conn, Int64 SystemMenuId, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemMenuId]=@SystemMenuId " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("SystemMenuId", SystemMenuId));
        }

        public int DeleteBySystemMenuId(DBConnection conn, Int64 SystemMenuId, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [SystemMenuId]=@SystemMenuId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemMenuId", SystemMenuId));
        }

        public SystemMenu FindBySystemMenuName(DBConnection conn, String SystemMenuName, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemMenuName]=@SystemMenuName " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("SystemMenuName", SystemMenuName));
        }

        public List<SystemMenu> FindsBySystemMenuName(DBConnection conn, String SystemMenuName, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemMenuName]=@SystemMenuName " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("SystemMenuName", SystemMenuName));
        }

        public int DeleteBySystemMenuName(DBConnection conn, String SystemMenuName, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [SystemMenuName]=@SystemMenuName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemMenuName", SystemMenuName));
        }

        public SystemMenu FindBySystemParentMenuId(DBConnection conn, Int64 SystemParentMenuId, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemParentMenuId]=@SystemParentMenuId " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("SystemParentMenuId", SystemParentMenuId));
        }

        public List<SystemMenu> FindsBySystemParentMenuId(DBConnection conn, Int64 SystemParentMenuId, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemParentMenuId]=@SystemParentMenuId " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("SystemParentMenuId", SystemParentMenuId));
        }

        public int DeleteBySystemParentMenuId(DBConnection conn, Int64 SystemParentMenuId, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [SystemParentMenuId]=@SystemParentMenuId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemParentMenuId", SystemParentMenuId));
        }

        public SystemMenu FindBySystemName(DBConnection conn, String SystemName, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemName]=@SystemName " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("SystemName", SystemName));
        }

        public List<SystemMenu> FindsBySystemName(DBConnection conn, String SystemName, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[SystemName]=@SystemName " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("SystemName", SystemName));
        }

        public int DeleteBySystemName(DBConnection conn, String SystemName, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [SystemName]=@SystemName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("SystemName", SystemName));
        }

        public SystemMenu FindByMenuUrl(DBConnection conn, String MenuUrl, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[MenuUrl]=@MenuUrl " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("MenuUrl", MenuUrl));
        }

        public List<SystemMenu> FindsByMenuUrl(DBConnection conn, String MenuUrl, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[MenuUrl]=@MenuUrl " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("MenuUrl", MenuUrl));
        }

        public int DeleteByMenuUrl(DBConnection conn, String MenuUrl, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [MenuUrl]=@MenuUrl " + whereText;
            return conn.executeUpdate(sql,new DBParameter("MenuUrl", MenuUrl));
        }

        public SystemMenu FindByIsHidden(DBConnection conn, Boolean IsHidden, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[IsHidden]=@IsHidden " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("IsHidden", IsHidden));
        }

        public List<SystemMenu> FindsByIsHidden(DBConnection conn, Boolean IsHidden, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[IsHidden]=@IsHidden " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("IsHidden", IsHidden));
        }

        public int DeleteByIsHidden(DBConnection conn, Boolean IsHidden, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [IsHidden]=@IsHidden " + whereText;
            return conn.executeUpdate(sql,new DBParameter("IsHidden", IsHidden));
        }

        public SystemMenu FindByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<SystemMenu> FindsByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[ActiveStatusId]=@ActiveStatusId " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, Int64 ActiveStatusId, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [ActiveStatusId]=@ActiveStatusId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public SystemMenu FindByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<SystemMenu> FindsByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[DisplayIndex]=@DisplayIndex " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, Int32 DisplayIndex, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [DisplayIndex]=@DisplayIndex " + whereText;
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public SystemMenu FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<SystemMenu> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public SystemMenu FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<SystemMenu> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public SystemMenu FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<SystemMenu> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public SystemMenu FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<SystemMenu>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<SystemMenu> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select SystemMenu.* From [SystemMenu] SystemMenu Where SystemMenu.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<SystemMenu>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [SystemMenu] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
