using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemmenu.model;

namespace Library.Winplus.syslib.standard.systemmenu.repository
{
    public partial class SystemMenuRepository
    {
        private static Dictionary<string,SystemMenu> AllMenus = null;
        public string GetSystemMenuName(DBConnection conn, string entity)
        {
            if (AllMenus == null)
            {
                AllMenus = new Dictionary<string,SystemMenu>();
                List<SystemMenu> allMenu = FindAll(conn, null);
                foreach (SystemMenu menu in allMenu)
                {
                    if (!AllMenus.ContainsKey(menu.SystemName.ToUpper()))
                    {
                        AllMenus.Add(menu.SystemName.ToUpper(), menu);
                    }
                }
            }

            if (AllMenus.ContainsKey(entity.ToUpper()))
            {
                return AllMenus[entity.ToUpper()].SystemMenuName;
            }
            else
            {
                return entity;
            }
        }
    }
}
