using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemmenu.model;

namespace Library.Winplus.syslib.standard.systemmenu.search
{
    public partial class SystemMenuSearch : SimpleSearch<SystemMenu>
    {
        public SystemMenuSearch()
        {
            search = new SystemMenu();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "SystemMenuId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select SystemMenu.*
                 , SystemParentMenu_SystemParentMenuId.[SystemParentMenuName] as [SystemParentMenuIdSystemParentMenuName]
                 , ActiveStatus_ActiveStatusId.[ActiveStatusName] as [ActiveStatusIdActiveStatusName]
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [SystemMenu] SystemMenu
                Left Join [SystemParentMenu] SystemParentMenu_SystemParentMenuId on SystemParentMenu_SystemParentMenuId.[SystemParentMenuId]=SystemMenu.[SystemParentMenuId]
                Left Join [ActiveStatus] ActiveStatus_ActiveStatusId on ActiveStatus_ActiveStatusId.[ActiveStatusId]=SystemMenu.[ActiveStatusId]
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=SystemMenu.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=SystemMenu.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (SystemMenu.[SystemMenuName] like '%'+@SearchText+'%' or SystemParentMenu_SystemParentMenuId.[SystemParentMenuName] like '%'+@SearchText+'%' or SystemMenu.[SystemName] like '%'+@SearchText+'%' or SystemMenu.[MenuUrl] like '%'+@SearchText+'%' or ActiveStatus_ActiveStatusId.[ActiveStatusName] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by SystemMenuId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemMenuId", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[SystemMenuId] = @SystemMenuId");
                    param.Add(new DBParameter("SystemMenuId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemMenuName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemMenuName", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[SystemMenuName] like '%'+@SystemMenuName+'%'");
                    param.Add(new DBParameter("SystemMenuName", StringUtil.ToString(text)));
                }

                /* Advance Search by SystemParentMenuId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemParentMenuId", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[SystemParentMenuId] = @SystemParentMenuId");
                    param.Add(new DBParameter("SystemParentMenuId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by SystemName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_SystemName", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[SystemName] like '%'+@SystemName+'%'");
                    param.Add(new DBParameter("SystemName", StringUtil.ToString(text)));
                }

                /* Advance Search by MenuUrl */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_MenuUrl", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[MenuUrl] like '%'+@MenuUrl+'%'");
                    param.Add(new DBParameter("MenuUrl", StringUtil.ToString(text)));
                }

                /* Advance Search by IsHidden */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_IsHidden", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[IsHidden] = @IsHidden");
                    param.Add(new DBParameter("IsHidden", StringUtil.ToBoolean(text)));
                }

                /* Advance Search by ActiveStatusId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ActiveStatusId", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[ActiveStatusId] = @ActiveStatusId");
                    param.Add(new DBParameter("ActiveStatusId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by DisplayIndex */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_DisplayIndex", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[DisplayIndex] = @DisplayIndex");
                    param.Add(new DBParameter("DisplayIndex", StringUtil.ToInt32(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and SystemMenu.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and SystemMenu.SystemMenuId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
