using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.systemmenu.model
{
    [Serializable]
    public partial class SystemMenu : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("SystemMenuId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemMenuId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemMenuName", typeof(MyResources.Winplus.Resource))]
        public String SystemMenuName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemParentMenuId", typeof(MyResources.Winplus.Resource))]
        public Int64? SystemParentMenuId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("SystemName", typeof(MyResources.Winplus.Resource))]
        public String SystemName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("MenuUrl", typeof(MyResources.Winplus.Resource))]
        public String MenuUrl { get; set; }

        [DataColumn]
        [LocalizedDisplayName("IsHidden", typeof(MyResources.Winplus.Resource))]
        public Boolean IsHidden { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ActiveStatusId", typeof(MyResources.Winplus.Resource))]
        public Int64? ActiveStatusId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("DisplayIndex", typeof(MyResources.Winplus.Resource))]
        public Int32? DisplayIndex { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemParentMenuName", typeof(MyResources.Winplus.Resource))]
        public String SystemParentMenuIdSystemParentMenuName { get; set; }

        [LocalizedDisplayName("ActiveStatusName", typeof(MyResources.Winplus.Resource))]
        public String ActiveStatusIdActiveStatusName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
