using System;
using System.Collections.Generic;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.logger.model;

namespace Library.Winplus.syslib.standard.logger.repository
{
    public partial class LoggerRepository : SimpleRepository<Logger>
    {
        #region General Query

        public LoggerRepository()
        {
            SqlInsert = @"Insert Into [Logger]([LoggerName], [LogClass], [LogMethod], [LogLevel], [TokenId], [UserName], [RequestUrl], [Request], [Response], [LogMessage], [ErrorMessage], [ProcessingTime], [CreatedOn], [CreatedBy]) Values(@LoggerName, @LogClass, @LogMethod, @LogLevel, @TokenId, @UserName, @RequestUrl, @Request, @Response, @LogMessage, @ErrorMessage, @ProcessingTime, getdate(), @CreatedBy)";
            SqlUpdate = @"Update [Logger] Set [LoggerName]=@LoggerName, [LogClass]=@LogClass, [LogMethod]=@LogMethod, [LogLevel]=@LogLevel, [TokenId]=@TokenId, [UserName]=@UserName, [RequestUrl]=@RequestUrl, [Request]=@Request, [Response]=@Response, [LogMessage]=@LogMessage, [ErrorMessage]=@ErrorMessage, [ProcessingTime]=@ProcessingTime, [ModifiedOn]=getdate(), [ModifiedBy]=@ModifiedBy Where [LoggerId]=@LoggerId";
            SqlDelete = @"Delete From [Logger] Where [LoggerId]=@Id";
            SqlFindAll = "Select sm.*, su_cb.[SystemUserName] as [CreatedBySystemUserName], sc_mb.[SystemUserName] as [ModifiedBySystemUserName] From [Logger] sm Left Join [SystemUser] su_cb on su_cb.[SystemUserId]=sm.[CreatedBy] Left Join [SystemUser] sc_mb on sc_mb.[SystemUserId]=sm.[ModifiedBy] Where 1=1";
        }

        #endregion

        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<Logger> lstImport)
        {
            if (lstImport != null)
            {
                foreach (Logger model in lstImport)
                {
                    if (model.LoggerId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public Logger FindByLoggerId(DBConnection conn, Int64 LoggerId, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[LoggerId]=@LoggerId " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("LoggerId", LoggerId));
        }

        public List<Logger> FindsByLoggerId(DBConnection conn, Int64 LoggerId, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[LoggerId]=@LoggerId " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("LoggerId", LoggerId));
        }

        public int DeleteByLoggerId(DBConnection conn, Int64 LoggerId, string whereText="")
        {
            String sql = "Delete From [Logger] Where [LoggerId]=@LoggerId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LoggerId", LoggerId));
        }

        public Logger FindByLoggerName(DBConnection conn, String LoggerName, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[LoggerName]=@LoggerName " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("LoggerName", LoggerName));
        }

        public List<Logger> FindsByLoggerName(DBConnection conn, String LoggerName, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[LoggerName]=@LoggerName " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("LoggerName", LoggerName));
        }

        public int DeleteByLoggerName(DBConnection conn, String LoggerName, string whereText="")
        {
            String sql = "Delete From [Logger] Where [LoggerName]=@LoggerName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LoggerName", LoggerName));
        }

        public Logger FindByLogClass(DBConnection conn, String LogClass, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[LogClass]=@LogClass " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("LogClass", LogClass));
        }

        public List<Logger> FindsByLogClass(DBConnection conn, String LogClass, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[LogClass]=@LogClass " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("LogClass", LogClass));
        }

        public int DeleteByLogClass(DBConnection conn, String LogClass, string whereText="")
        {
            String sql = "Delete From [Logger] Where [LogClass]=@LogClass " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogClass", LogClass));
        }

        public Logger FindByLogMethod(DBConnection conn, String LogMethod, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[LogMethod]=@LogMethod " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("LogMethod", LogMethod));
        }

        public List<Logger> FindsByLogMethod(DBConnection conn, String LogMethod, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[LogMethod]=@LogMethod " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("LogMethod", LogMethod));
        }

        public int DeleteByLogMethod(DBConnection conn, String LogMethod, string whereText="")
        {
            String sql = "Delete From [Logger] Where [LogMethod]=@LogMethod " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogMethod", LogMethod));
        }

        public Logger FindByLogLevel(DBConnection conn, String LogLevel, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[LogLevel]=@LogLevel " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("LogLevel", LogLevel));
        }

        public List<Logger> FindsByLogLevel(DBConnection conn, String LogLevel, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[LogLevel]=@LogLevel " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("LogLevel", LogLevel));
        }

        public int DeleteByLogLevel(DBConnection conn, String LogLevel, string whereText="")
        {
            String sql = "Delete From [Logger] Where [LogLevel]=@LogLevel " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogLevel", LogLevel));
        }

        public Logger FindByTokenId(DBConnection conn, String TokenId, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[TokenId]=@TokenId " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("TokenId", TokenId));
        }

        public List<Logger> FindsByTokenId(DBConnection conn, String TokenId, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[TokenId]=@TokenId " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("TokenId", TokenId));
        }

        public int DeleteByTokenId(DBConnection conn, String TokenId, string whereText="")
        {
            String sql = "Delete From [Logger] Where [TokenId]=@TokenId " + whereText;
            return conn.executeUpdate(sql,new DBParameter("TokenId", TokenId));
        }

        public Logger FindByUserName(DBConnection conn, String UserName, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[UserName]=@UserName " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("UserName", UserName));
        }

        public List<Logger> FindsByUserName(DBConnection conn, String UserName, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[UserName]=@UserName " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("UserName", UserName));
        }

        public int DeleteByUserName(DBConnection conn, String UserName, string whereText="")
        {
            String sql = "Delete From [Logger] Where [UserName]=@UserName " + whereText;
            return conn.executeUpdate(sql,new DBParameter("UserName", UserName));
        }

        public Logger FindByRequestUrl(DBConnection conn, String RequestUrl, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[RequestUrl]=@RequestUrl " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("RequestUrl", RequestUrl));
        }

        public List<Logger> FindsByRequestUrl(DBConnection conn, String RequestUrl, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[RequestUrl]=@RequestUrl " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("RequestUrl", RequestUrl));
        }

        public int DeleteByRequestUrl(DBConnection conn, String RequestUrl, string whereText="")
        {
            String sql = "Delete From [Logger] Where [RequestUrl]=@RequestUrl " + whereText;
            return conn.executeUpdate(sql,new DBParameter("RequestUrl", RequestUrl));
        }

        public Logger FindByRequest(DBConnection conn, String Request, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[Request]=@Request " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("Request", Request));
        }

        public List<Logger> FindsByRequest(DBConnection conn, String Request, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[Request]=@Request " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("Request", Request));
        }

        public int DeleteByRequest(DBConnection conn, String Request, string whereText="")
        {
            String sql = "Delete From [Logger] Where [Request]=@Request " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Request", Request));
        }

        public Logger FindByResponse(DBConnection conn, String Response, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[Response]=@Response " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("Response", Response));
        }

        public List<Logger> FindsByResponse(DBConnection conn, String Response, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[Response]=@Response " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("Response", Response));
        }

        public int DeleteByResponse(DBConnection conn, String Response, string whereText="")
        {
            String sql = "Delete From [Logger] Where [Response]=@Response " + whereText;
            return conn.executeUpdate(sql,new DBParameter("Response", Response));
        }

        public Logger FindByLogMessage(DBConnection conn, String LogMessage, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[LogMessage]=@LogMessage " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("LogMessage", LogMessage));
        }

        public List<Logger> FindsByLogMessage(DBConnection conn, String LogMessage, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[LogMessage]=@LogMessage " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("LogMessage", LogMessage));
        }

        public int DeleteByLogMessage(DBConnection conn, String LogMessage, string whereText="")
        {
            String sql = "Delete From [Logger] Where [LogMessage]=@LogMessage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("LogMessage", LogMessage));
        }

        public Logger FindByErrorMessage(DBConnection conn, String ErrorMessage, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[ErrorMessage]=@ErrorMessage " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("ErrorMessage", ErrorMessage));
        }

        public List<Logger> FindsByErrorMessage(DBConnection conn, String ErrorMessage, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[ErrorMessage]=@ErrorMessage " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("ErrorMessage", ErrorMessage));
        }

        public int DeleteByErrorMessage(DBConnection conn, String ErrorMessage, string whereText="")
        {
            String sql = "Delete From [Logger] Where [ErrorMessage]=@ErrorMessage " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ErrorMessage", ErrorMessage));
        }

        public Logger FindByProcessingTime(DBConnection conn, Decimal ProcessingTime, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[ProcessingTime]=@ProcessingTime " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("ProcessingTime", ProcessingTime));
        }

        public List<Logger> FindsByProcessingTime(DBConnection conn, Decimal ProcessingTime, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[ProcessingTime]=@ProcessingTime " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("ProcessingTime", ProcessingTime));
        }

        public int DeleteByProcessingTime(DBConnection conn, Decimal ProcessingTime, string whereText="")
        {
            String sql = "Delete From [Logger] Where [ProcessingTime]=@ProcessingTime " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ProcessingTime", ProcessingTime));
        }

        public Logger FindByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public List<Logger> FindsByCreatedOn(DBConnection conn, DateTime CreatedOn, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[CreatedOn]=@CreatedOn " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, DateTime CreatedOn, string whereText="")
        {
            String sql = "Delete From [Logger] Where [CreatedOn]=@CreatedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public Logger FindByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public List<Logger> FindsByCreatedBy(DBConnection conn, Int64 CreatedBy, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[CreatedBy]=@CreatedBy " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, Int64 CreatedBy, string whereText="")
        {
            String sql = "Delete From [Logger] Where [CreatedBy]=@CreatedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public Logger FindByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<Logger> FindsByModifiedOn(DBConnection conn, DateTime ModifiedOn, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[ModifiedOn]=@ModifiedOn " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, DateTime ModifiedOn, string whereText="")
        {
            String sql = "Delete From [Logger] Where [ModifiedOn]=@ModifiedOn " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public Logger FindByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Top 1 Logger.* From [Logger] Logger Where Logger.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.Find<Logger>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public List<Logger> FindsByModifiedBy(DBConnection conn, Int64 ModifiedBy, string orderBy="")
        {
            String sql = "Select Logger.* From [Logger] Logger Where Logger.[ModifiedBy]=@ModifiedBy " + orderBy;

            return QueryUtil.FindList<Logger>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, Int64 ModifiedBy, string whereText="")
        {
            String sql = "Delete From [Logger] Where [ModifiedBy]=@ModifiedBy " + whereText;
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
