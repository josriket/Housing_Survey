using System;
using System.Text;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.logger.model;

namespace Library.Winplus.syslib.standard.logger.search
{
    public partial class LoggerSearch : SimpleSearch<Logger>
    {
        public LoggerSearch()
        {
            search = new Logger();
        }
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            if (CustomSearch)
            {
                CustomPrepareSql(request, loginUser);
            }
            else
            {
                ParseRequest(request);
                DefaultSearch = "LoggerId desc";

                StringBuilder sqlQuery = new StringBuilder(@"
                Select Logger.*
                 , SystemUser_CreatedBy.[SystemUserName] as [CreatedBySystemUserName]
                 , SystemUser_ModifiedBy.[SystemUserName] as [ModifiedBySystemUserName]
                From [Logger] Logger
                Left Join [SystemUser] SystemUser_CreatedBy on SystemUser_CreatedBy.[SystemUserId]=Logger.[CreatedBy]
                Left Join [SystemUser] SystemUser_ModifiedBy on SystemUser_ModifiedBy.[SystemUserId]=Logger.[ModifiedBy]
                Where 1=1");

                String text = null;
                /* Default text search */
                if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
                {
                    sqlQuery.AppendLine("and (Logger.[LoggerName] like '%'+@SearchText+'%' or Logger.[LogClass] like '%'+@SearchText+'%' or Logger.[LogMethod] like '%'+@SearchText+'%' or Logger.[LogLevel] like '%'+@SearchText+'%' or Logger.[TokenId] like '%'+@SearchText+'%' or Logger.[UserName] like '%'+@SearchText+'%' or Logger.[RequestUrl] like '%'+@SearchText+'%' or Logger.[Request] like '%'+@SearchText+'%' or Logger.[Response] like '%'+@SearchText+'%' or Logger.[LogMessage] like '%'+@SearchText+'%' or Logger.[ErrorMessage] like '%'+@SearchText+'%' or SystemUser_CreatedBy.[SystemUserName] like '%'+@SearchText+'%' or SystemUser_ModifiedBy.[SystemUserName] like '%'+@SearchText+'%')");
                    param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
                }

                #region search condition

                /* Advance Search by LoggerId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LoggerId", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[LoggerId] = @LoggerId");
                    param.Add(new DBParameter("LoggerId", StringUtil.ToInt64(text)));
                }

                /* Advance Search by LoggerName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LoggerName", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[LoggerName] like '%'+@LoggerName+'%'");
                    param.Add(new DBParameter("LoggerName", StringUtil.ToString(text)));
                }

                /* Advance Search by LogClass */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogClass", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[LogClass] like '%'+@LogClass+'%'");
                    param.Add(new DBParameter("LogClass", StringUtil.ToString(text)));
                }

                /* Advance Search by LogMethod */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogMethod", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[LogMethod] like '%'+@LogMethod+'%'");
                    param.Add(new DBParameter("LogMethod", StringUtil.ToString(text)));
                }

                /* Advance Search by LogLevel */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogLevel", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[LogLevel] like '%'+@LogLevel+'%'");
                    param.Add(new DBParameter("LogLevel", StringUtil.ToString(text)));
                }

                /* Advance Search by TokenId */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_TokenId", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[TokenId] like '%'+@TokenId+'%'");
                    param.Add(new DBParameter("TokenId", StringUtil.ToString(text)));
                }

                /* Advance Search by UserName */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_UserName", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[UserName] like '%'+@UserName+'%'");
                    param.Add(new DBParameter("UserName", StringUtil.ToString(text)));
                }

                /* Advance Search by RequestUrl */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_RequestUrl", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[RequestUrl] like '%'+@RequestUrl+'%'");
                    param.Add(new DBParameter("RequestUrl", StringUtil.ToString(text)));
                }

                /* Advance Search by Request */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Request", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[Request] like '%'+@Request+'%'");
                    param.Add(new DBParameter("Request", StringUtil.ToString(text)));
                }

                /* Advance Search by Response */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_Response", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[Response] like '%'+@Response+'%'");
                    param.Add(new DBParameter("Response", StringUtil.ToString(text)));
                }

                /* Advance Search by LogMessage */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_LogMessage", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[LogMessage] like '%'+@LogMessage+'%'");
                    param.Add(new DBParameter("LogMessage", StringUtil.ToString(text)));
                }

                /* Advance Search by ErrorMessage */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ErrorMessage", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[ErrorMessage] like '%'+@ErrorMessage+'%'");
                    param.Add(new DBParameter("ErrorMessage", StringUtil.ToString(text)));
                }

                /* Advance Search by ProcessingTime */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ProcessingTime", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[ProcessingTime] = @ProcessingTime");
                    param.Add(new DBParameter("ProcessingTime", StringUtil.ToDecimal(text)));
                }

                /* Advance Search by CreatedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Logger.`CreatedOn` >= @CreatedOn_From");
                    param.Add(new DBParameter("CreatedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Logger.`CreatedOn` < DATE_ADD(@CreatedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("CreatedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by CreatedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_CreatedBy", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[CreatedBy] = @CreatedBy");
                    param.Add(new DBParameter("CreatedBy", StringUtil.ToInt64(text)));
                }

                /* Advance Search by ModifiedOn From*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_From", ref text))
                {
                    sqlQuery.AppendLine("and Logger.`ModifiedOn` >= @ModifiedOn_From");
                    param.Add(new DBParameter("ModifiedOn_From", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedOn To*/
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedOn_To", ref text))
                {
                    sqlQuery.AppendLine("and Logger.`ModifiedOn` < DATE_ADD(@ModifiedOn_To,INTERVAL 1 DAY)");
                    param.Add(new DBParameter("ModifiedOn_To", StringUtil.ToDateTime(text)));
                }

                /* Advance Search by ModifiedBy */
                if (StringUtil.IsContainsKeyValue(SearchKey, "search_ModifiedBy", ref text))
                {
                    sqlQuery.AppendLine("and Logger.[ModifiedBy] = @ModifiedBy");
                    param.Add(new DBParameter("ModifiedBy", StringUtil.ToInt64(text)));
                }


                if (!string.IsNullOrEmpty(Ids))
                {
                    sqlQuery.AppendLine("and Logger.LoggerId in(" + Ids + ")");
                }

                #endregion search condition

                /* prepare temporary parameter */
                this.sqlQuery = sqlQuery.ToString();

                this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
            }
        }
    }
}
