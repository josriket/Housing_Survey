using System;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.core.attribute;
using Library.Winplus.syslib.standard.fileupload.model;
using System.Web;

namespace Library.Winplus.syslib.standard.logger.model
{
    [Serializable]
    public partial class Logger : SimpleModel
    {
        /* Table column */
        [DataColumn("PK")]
        [LocalizedDisplayName("LoggerId", typeof(MyResources.Winplus.Resource))]
        public Int64? LoggerId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LoggerName", typeof(MyResources.Winplus.Resource))]
        public String LoggerName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LogClass", typeof(MyResources.Winplus.Resource))]
        public String LogClass { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LogMethod", typeof(MyResources.Winplus.Resource))]
        public String LogMethod { get; set; }

        [DataColumn]
        [LocalizedDisplayName("LogLevel", typeof(MyResources.Winplus.Resource))]
        public String LogLevel { get; set; }

        [DataColumn]
        [LocalizedDisplayName("TokenId", typeof(MyResources.Winplus.Resource))]
        public String TokenId { get; set; }

        [DataColumn]
        [LocalizedDisplayName("UserName", typeof(MyResources.Winplus.Resource))]
        public String UserName { get; set; }

        [DataColumn]
        [LocalizedDisplayName("RequestUrl", typeof(MyResources.Winplus.Resource))]
        public String RequestUrl { get; set; }

        [DataColumn]
        [AllowHtml]
        [LocalizedDisplayName("Request", typeof(MyResources.Winplus.Resource))]
        public String Request { get; set; }

        [DataColumn]
        [AllowHtml]
        [LocalizedDisplayName("Response", typeof(MyResources.Winplus.Resource))]
        public String Response { get; set; }

        [DataColumn]
        [AllowHtml]
        [LocalizedDisplayName("LogMessage", typeof(MyResources.Winplus.Resource))]
        public String LogMessage { get; set; }

        [DataColumn]
        [AllowHtml]
        [LocalizedDisplayName("ErrorMessage", typeof(MyResources.Winplus.Resource))]
        public String ErrorMessage { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ProcessingTime", typeof(MyResources.Winplus.Resource))]
        public Decimal? ProcessingTime { get; set; }

        [DataColumn]
        [LocalizedDisplayName("CreatedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? CreatedOn { get; set; }
        public string CreatedOnText
        {
            get { return StringUtil.FormatDate(CreatedOn); }
            set { CreatedOn = StringUtil.ToDate(value); }
        }
        public string CreatedOnTimeText
        {
            get { return StringUtil.FormatTime(CreatedOn); }
            set { CreatedOn = StringUtil.AddedTime(CreatedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("CreatedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? CreatedBy { get; set; }

        [DataColumn]
        [LocalizedDisplayName("ModifiedOn", typeof(MyResources.Winplus.Resource))]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedOnText
        {
            get { return StringUtil.FormatDate(ModifiedOn); }
            set { ModifiedOn = StringUtil.ToDate(value); }
        }
        public string ModifiedOnTimeText
        {
            get { return StringUtil.FormatTime(ModifiedOn); }
            set { ModifiedOn = StringUtil.AddedTime(ModifiedOn, value); }
        }

        [DataColumn]
        [LocalizedDisplayName("ModifiedBy", typeof(MyResources.Winplus.Resource))]
        public Int64? ModifiedBy { get; set; }


        /* Foreign key display model */
        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String CreatedBySystemUserName { get; set; }

        [LocalizedDisplayName("SystemUserName", typeof(MyResources.Winplus.Resource))]
        public String ModifiedBySystemUserName { get; set; }

    }
}
