﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.standard.logger.model;
using Library.Winplus.syslib.standard.logger.repository;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.core.logger;

namespace Library.Winplus.syslib.customize.logger.service
{
    public class LoggerService
    {
        private static Logger NewLogger(string logLevel, string logClass, string logMethod)
        {
            Logger model = new Logger();
            model.LoggerName = logClass + " - " + logMethod;
            model.LogClass = logClass;
            model.LogMethod = logMethod;
            model.LogLevel = logLevel;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = LoginUser.DefaultUser.SystemUserId;

            return model;
        }

        public static void LogError(string logClass, string logMethod, string errorMessage)
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    Logger model = NewLogger("Error", logClass, logMethod);
                    model.ErrorMessage = errorMessage;

                    new LoggerRepository().Create(conn, model);
                }
            }
            catch (Exception ex)
            {
                CriticalLogger.WriteLog("LogError", ex.ToString());
            }
        }

        public static void LogDebug(string logClass, string logMethod, string logMessage)
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    Logger model = NewLogger("Debug", logClass, logMethod);
                    model.LogMessage = logMessage;

                    new LoggerRepository().Create(conn, model);
                }
            }
            catch (Exception ex)
            {
                CriticalLogger.WriteLog("LogDebug", ex.ToString());
            }
        }

        public static void LogInfo(string logClass, string logMethod, string logMessage)
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    Logger model = NewLogger("Info", logClass, logMethod);
                    model.LogMessage = logMessage;

                    new LoggerRepository().Create(conn, model);
                }
            }
            catch (Exception ex)
            {
                CriticalLogger.WriteLog("LogInfo", ex.ToString());
            }
        }
    }
}