﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.systemconfig.keys
{
    public class SystemConfigKey
    {
        // General
        public static readonly string SessionTimeout = "SessionTimeout";
        public static readonly string PublicOrganizationId = "PublicOrganizationId";
        public static string ConfirmRegisterUrl = "ConfirmRegisterUrl";
        public static string MemberDefaultPage = "MemberDefaultPage";

        // Upload Download URL,Folder
        public static readonly string UploadFolder = "UploadFolder";
        public static readonly string UploadFolderUrl = "UploadFolderUrl";

        // SMTP
        public static readonly string SmtpServer = "SmtpServer";
        public static readonly string SmtpSender = "SmtpSender";
        public static readonly string SmtpSenderDisplayName = "SmtpSenderDisplayName";
        public static readonly string SmtpPort = "SmtpPort";
        public static readonly string SmtpPassword = "SmtpPassword";
       
    }
}
