﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemconfig.model;
using Library.Winplus.syslib.standard.systemconfig.repository;

namespace Library.Winplus.syslib.customize.systemconfig.service
{
    public class SystemConfigService
    {
        public static Int32? GetIntConfig(String ConfigKey,Int32? DefaultValue)
        {
            String config = GetStringConfig(ConfigKey);
            if (!string.IsNullOrEmpty(config))
            {
                return Convert.ToInt32(config);
            }
            else
            {
                return DefaultValue;
            }
        }

        public static Int64? GetLongConfig(String ConfigKey, Int64? DefaultValue)
        {
            String config = GetStringConfig(ConfigKey);
            if (!string.IsNullOrEmpty(config))
            {
                return Convert.ToInt64(config);
            }
            else
            {
                return DefaultValue;
            }
        }

        public static String GetStringConfig(String ConfigKey,string DefaultValue=null)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                SystemConfig config = new SystemConfigRepository().FindByConfigKey(conn, ConfigKey);
                if (config!=null)
                {
                    return config.ConfigValue;
                }
                else
                {
                    return DefaultValue;
                }
            }
        }

        public static bool GetBooleanConfig(String ConfigKey)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                SystemConfig config = new SystemConfigRepository().FindByConfigKey(conn, ConfigKey);
                if (config!=null)
                {
                    return config.ConfigValue.ToUpper().Equals("TRUE");
                }
            }
            return false;
        }
    }
}
