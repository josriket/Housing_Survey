using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.choosedialog.model;

namespace Library.Winplus.syslib.standard.choosedialog.repository
{
    public class ChooseDialogRepository : SimpleRepository<ChooseDialog>
    {
        #region Repository method

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<ChooseDialog> lstImport)
        {
            if (lstImport != null)
            {
                foreach (ChooseDialog model in lstImport)
                {
                    if (model.ChooseDialogId.HasValue)
                    {
                        Update(conn, model);
                    }
                    else
                    {
                        Create(conn, model);
                    }
                }
            }
        }

        #endregion

        #region /** Method for search/delete data by column name **/
        public List<ChooseDialog> FindByChooseDialogId(DBConnection conn, LoginUser loginuser, Int64 ChooseDialogId)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[ChooseDialogId]=@ChooseDialogId";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("ChooseDialogId", ChooseDialogId));
        }

        public int DeleteByChooseDialogId(DBConnection conn, LoginUser loginuser, Int64 ChooseDialogId)
        {
            String sql = "delete From [ChooseDialog] where [ChooseDialogId]=@ChooseDialogId";
            return conn.executeUpdate(sql,new DBParameter("ChooseDialogId", ChooseDialogId));
        }

        public List<ChooseDialog> FindByChooseDialogName(DBConnection conn, LoginUser loginuser, String ChooseDialogName)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[ChooseDialogName]=@ChooseDialogName";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("ChooseDialogName", ChooseDialogName));
        }

        public int DeleteByChooseDialogName(DBConnection conn, LoginUser loginuser, String ChooseDialogName)
        {
            String sql = "delete From [ChooseDialog] where [ChooseDialogName]=@ChooseDialogName";
            return conn.executeUpdate(sql,new DBParameter("ChooseDialogName", ChooseDialogName));
        }

        public List<ChooseDialog> FindByName(DBConnection conn, LoginUser loginuser, String Name)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[Name]=@Name";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("Name", Name));
        }

        public int DeleteByName(DBConnection conn, LoginUser loginuser, String Name)
        {
            String sql = "delete From [ChooseDialog] where [Name]=@Name";
            return conn.executeUpdate(sql,new DBParameter("Name", Name));
        }

        public List<ChooseDialog> FindByVersionNumber(DBConnection conn, LoginUser loginuser, Guid VersionNumber)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[VersionNumber]=@VersionNumber";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("VersionNumber", VersionNumber));
        }

        public int DeleteByVersionNumber(DBConnection conn, LoginUser loginuser, Guid VersionNumber)
        {
            String sql = "delete From [ChooseDialog] where [VersionNumber]=@VersionNumber";
            return conn.executeUpdate(sql,new DBParameter("VersionNumber", VersionNumber));
        }

        public List<ChooseDialog> FindByOrganizationId(DBConnection conn, LoginUser loginuser, Int64 OrganizationId)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[OrganizationId]=@OrganizationId";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("OrganizationId", OrganizationId));
        }

        public int DeleteByOrganizationId(DBConnection conn, LoginUser loginuser, Int64 OrganizationId)
        {
            String sql = "delete From [ChooseDialog] where [OrganizationId]=@OrganizationId";
            return conn.executeUpdate(sql,new DBParameter("OrganizationId", OrganizationId));
        }

        public List<ChooseDialog> FindByActiveStatusId(DBConnection conn, LoginUser loginuser, Int64 ActiveStatusId)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[ActiveStatusId]=@ActiveStatusId";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public int DeleteByActiveStatusId(DBConnection conn, LoginUser loginuser, Int64 ActiveStatusId)
        {
            String sql = "delete From [ChooseDialog] where [ActiveStatusId]=@ActiveStatusId";
            return conn.executeUpdate(sql,new DBParameter("ActiveStatusId", ActiveStatusId));
        }

        public List<ChooseDialog> FindByDisplayIndex(DBConnection conn, LoginUser loginuser, Int32 DisplayIndex)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[DisplayIndex]=@DisplayIndex";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("DisplayIndex", DisplayIndex));
        }

        public int DeleteByDisplayIndex(DBConnection conn, LoginUser loginuser, Int32 DisplayIndex)
        {
            String sql = "delete From [ChooseDialog] where [DisplayIndex]=@DisplayIndex";
            return conn.executeUpdate(sql,new DBParameter("DisplayIndex", DisplayIndex));
        }

        public List<ChooseDialog> FindByCreatedOn(DBConnection conn, LoginUser loginuser, DateTime CreatedOn)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[CreatedOn]=@CreatedOn";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("CreatedOn", CreatedOn));
        }

        public int DeleteByCreatedOn(DBConnection conn, LoginUser loginuser, DateTime CreatedOn)
        {
            String sql = "delete From [ChooseDialog] where [CreatedOn]=@CreatedOn";
            return conn.executeUpdate(sql,new DBParameter("CreatedOn", CreatedOn));
        }

        public List<ChooseDialog> FindByCreatedBy(DBConnection conn, LoginUser loginuser, Int64 CreatedBy)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[CreatedBy]=@CreatedBy";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("CreatedBy", CreatedBy));
        }

        public int DeleteByCreatedBy(DBConnection conn, LoginUser loginuser, Int64 CreatedBy)
        {
            String sql = "delete From [ChooseDialog] where [CreatedBy]=@CreatedBy";
            return conn.executeUpdate(sql,new DBParameter("CreatedBy", CreatedBy));
        }

        public List<ChooseDialog> FindByModifiedOn(DBConnection conn, LoginUser loginuser, DateTime ModifiedOn)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[ModifiedOn]=@ModifiedOn";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("ModifiedOn", ModifiedOn));
        }

        public int DeleteByModifiedOn(DBConnection conn, LoginUser loginuser, DateTime ModifiedOn)
        {
            String sql = "delete From [ChooseDialog] where [ModifiedOn]=@ModifiedOn";
            return conn.executeUpdate(sql,new DBParameter("ModifiedOn", ModifiedOn));
        }

        public List<ChooseDialog> FindByModifiedBy(DBConnection conn, LoginUser loginuser, Int64 ModifiedBy)
        {
            String sql = "Select cd.* From [ChooseDialog] cd where cd.[ModifiedBy]=@ModifiedBy";

            return QueryUtil.FindList<ChooseDialog>(conn, sql, new DBParameter("ModifiedBy", ModifiedBy));
        }

        public int DeleteByModifiedBy(DBConnection conn, LoginUser loginuser, Int64 ModifiedBy)
        {
            String sql = "delete From [ChooseDialog] where [ModifiedBy]=@ModifiedBy";
            return conn.executeUpdate(sql,new DBParameter("ModifiedBy", ModifiedBy));
        }

        #endregion

    }
}
