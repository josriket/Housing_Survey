using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;

namespace Library.Winplus.syslib.standard.choosedialog.model
{
    [Serializable]
    public class ChooseDialog : SimpleModel
    {
        /* Table column */
        [LocalizedDisplayName("ChooseDialogId", typeof(MyResources.Winplus.Resource))]
        public Int64? ChooseDialogId { get; set; }

        [LocalizedDisplayName("ChooseDialogName", typeof(MyResources.Winplus.Resource))]
        public String ChooseDialogName { get; set; }

        [LocalizedDisplayName("Name", typeof(MyResources.Winplus.Resource))]
        public String Name { get; set; }


        /* Foreign key display model */
    }
}
