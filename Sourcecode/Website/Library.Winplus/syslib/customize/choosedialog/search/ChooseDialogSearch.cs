using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.choosedialog.model;

namespace Library.Winplus.syslib.standard.choosedialog.search
{
    public class ChooseDialogSearch : SimpleSearch<ChooseDialog>
    {
        public override void PrepareSql(System.Web.HttpRequestBase request, LoginUser loginUser)
        {
            ParseRequest(request);

            String tableName = "";
            String text = null;
            /* Default text search */
            if (StringUtil.IsContainsKeyValue(SearchKey, "entity", ref text))
            {
                tableName = text.Replace("'", "''");
            }

            string filter = "";
            if (StringUtil.IsContainsKeyValue(SearchKey, "flter", ref text))
            {
                if (tableName == "MEModule")
                {
                    filter = string.Format("and JCIModuleId in(select JCIModuleId from IncidentReportJCIModuleAnalyze WHERE IncidentReportJCIModuleAnalyzeId={0})", text);
                }
                else
                {
                    filter = text;
                }
            }

            DefaultSearch = tableName + "Id desc";
            if (StringUtil.IsContainsKeyValue(SearchKey, "orderBy", ref text))
            {
                DefaultSearch = text;
            }



            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("Select entity." + tableName + "Id id,entity." + tableName + "Name Name,entity.CreatedOn,entity.ModifiedOn");
            sqlQuery.AppendLine(", az.[ActiveStatusName] as [ActiveStatusName]");
            sqlQuery.AppendLine(", su_cb.[SystemUserName] as [CreatedBySystemUserName]");
            sqlQuery.AppendLine(", sc_mb.[SystemUserName] as [ModifiedBySystemUserName]");
            sqlQuery.AppendLine("from [" + tableName + "] entity");
            sqlQuery.AppendLine("left join [ActiveStatus] az on az.[ActiveStatusId]=entity.[ActiveStatusId]");
            sqlQuery.AppendLine("left join [SystemUser] su_cb on su_cb.[SystemUserId]=entity.[CreatedBy]");
            sqlQuery.AppendLine("left join [SystemUser] sc_mb on sc_mb.[SystemUserId]=entity.[ModifiedBy]");
            sqlQuery.AppendLine("where 1=1 ");
            if (tableName != "ActiveStatus")
            {
                sqlQuery.AppendLine("and entity.ActiveStatusId=1 ");
            }

            if (StringUtil.IsContainsKeyValue(SearchKey, "SearchText", ref text))
            {
                sqlQuery.AppendLine("and entity.[" + tableName + "Name] like '%'+@SearchText+'%'");
                param.Add(new DBParameter("SearchText", StringUtil.ToString(text)));
            }


            /* prepare temporary parameter */
            this.sqlQuery = sqlQuery.ToString() + filter;

            this.sqlOrderBy = (ColumnOrderByIndex.Count > 0) ? (ColumnOrderByIndex[OrderColumn] + " " + OrderDir) : DefaultSearch;
        }
    }
}
