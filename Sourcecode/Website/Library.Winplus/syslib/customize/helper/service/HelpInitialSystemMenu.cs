﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.systemmenu.model;
using Library.Winplus.syslib.standard.systemmenu.repository;
using Library.Winplus.syslib.standard.systemparentmenu.model;
using Library.Winplus.syslib.standard.systemparentmenu.repository;
using Library.Winplus.syslib.standard.systemrolemenu.model;
using Library.Winplus.syslib.standard.systemrolemenu.repository;
using System;
using System.Collections.Generic;
using System.Data;

namespace Library.Winplus.syslib.customize.helper.service
{
    public class HelpInitialSystemMenu
    {
        public void Initial()
        {
            string sql = "select table_name from information_schema.tables where table_schema='dbo' and table_name not in('sysdiagrams') and table_name not like '$_%' and table_name not like 'Survey_%' order by table_name";
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                conn.beginTrans();
                SystemParentMenu workspace = null;

                List<SystemParentMenu> lstFinds = new SystemParentMenuRepository().FindsBySystemParentMenuName(conn, "WorkSpace");
                if (lstFinds.Count > 0)
                {
                    workspace = lstFinds[0];
                }
                else
                {
                    workspace = new SystemParentMenu();
                    workspace.SystemParentMenuName = "WorkSpace";
                    workspace.MenuUrl = "#";
                    workspace.IconName = "fa-list-ul";
                    workspace.IsHidden = false;
                    workspace.ActiveStatusId = 1;
                    workspace.CreatedOn = DateTime.Now;
                    workspace.CreatedBy = LoginUser.DefaultUser.SystemUserId;

                    workspace.SystemParentMenuId = new SystemParentMenuRepository().Create(conn, workspace);
                }


                bool hasNewTable = false;
                DataSet ds = conn.executeDataSet(sql);
                foreach(DataRow row in ds.Tables[0].Rows)
                {
                    string table_name = row["table_name"].ToString();

                    if (!conn.IsExistsData(@"Select 1 from SystemMenu Where SystemName=@SystemName", new DBParameter("SystemName", table_name)))
                    {
                        SystemMenu menu = new SystemMenu();
                        menu.SystemMenuName = table_name;
                        menu.SystemName = table_name;
                        menu.MenuUrl = "~/"+ table_name;
                        menu.CreatedOn = DateTime.Now;
                        menu.CreatedBy = LoginUser.DefaultUser.SystemUserId;
                        menu.SystemParentMenuId = workspace.SystemParentMenuId;
                        menu.IsHidden = false;
                        menu.ActiveStatusId = 1;
                        menu.DisplayIndex = null;

                        new SystemMenuRepository().Create(conn, menu);
                        hasNewTable = true;
                    }
                }

                if (hasNewTable)
                {
                    new SystemRoleMenuRepository().DeleteBySystemRoleId(conn, 1);

                    List<SystemMenu> menus = new SystemMenuRepository().FindAll(conn);
                    foreach(SystemMenu menu in menus)
                    {
                        SystemRoleMenu roleMenu = new SystemRoleMenu();
                        roleMenu.SystemRoleMenuName = "Developer - " + menu.SystemMenuName;
                        roleMenu.SystemMenuId = menu.SystemMenuId;
                        roleMenu.SystemRoleId = 1;
                        roleMenu.CanAdd = true;
                        roleMenu.CanDelete = true;
                        roleMenu.CanEdit = true;
                        roleMenu.CanExport = true;
                        roleMenu.CanImport = true;
                        roleMenu.CanSearch = true;
                        roleMenu.CanView = true;
                        roleMenu.CreatedOn = DateTime.Now;
                        roleMenu.CreatedBy = LoginUser.DefaultUser.SystemUserId;
                        roleMenu.MenuVisible = true;

                        new SystemRoleMenuRepository().Create(conn, roleMenu);
                    }
                }

                conn.commitTrans();
            }
        }
    }
}
