﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.standard.systemrole.model;
using Library.Winplus.syslib.standard.systemrole.repository;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.standard.systemuser.repository;
using System;

namespace Library.Winplus.syslib.customize.helper.service
{
    public class HelpInitialSystemAdmin
    {
        public void Initial()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                SystemUserRepository systemUserRepository = new SystemUserRepository();
                if (systemUserRepository.FindAll(conn).Count == 0)
                {
                    conn.beginTrans();

                    conn.executeUpdate("Alter table SystemUser Alter Column SystemRoleId bigint");

                    SystemUser user = new SystemUser();
                    user.FirstName = "admin";
                    user.LastName = "system";
                    user.UserName = "admin";
                    user.Password = EncriptionUtil.EncrypOneWay(user.UserName.ToUpper() + "_" + "1234");
                    user.SystemUserName = user.FirstName + " " + user.LastName;
                    user.CreatedBy = 1;
                    user.CreatedOn = DateTime.Now;
                    user.Email = "prasit@v-smart.com";
                    user.ActiveStatusId = 1;
                    user.PhoneNo = "0815981742";
                    user.SystemRoleId = null;
                    user.SystemUserId = systemUserRepository.Create(conn, user);


                    SystemRole role = new SystemRole();
                    role.SystemRoleName = "Developer";
                    role.DefaultMenuUrl = "~/ActiveStatus";
                    role.ActiveStatusId = 1;
                    role.CreatedBy = 1;
                    role.CreatedOn = DateTime.Now;
                    role.SystemRoleId = new SystemRoleRepository().Create(conn, role);

                    user.SystemRoleId = role.SystemRoleId;
                    systemUserRepository.Update(conn, user);



                    conn.executeUpdate("Alter table SystemUser Alter Column SystemRoleId bigint Not NULL");

                    conn.commitTrans();
                }
                
            }
        }
    }
}
