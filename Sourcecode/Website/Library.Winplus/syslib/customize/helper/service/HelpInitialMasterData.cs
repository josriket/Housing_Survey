﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.boardlocation.model;
using Library.Winplus.syslib.standard.boardlocation.repository;
using Library.Winplus.syslib.standard.boardstyle.model;
using Library.Winplus.syslib.standard.boardstyle.repository;
using Library.Winplus.syslib.standard.boardtype.model;
using Library.Winplus.syslib.standard.boardtype.repository;
using Library.Winplus.syslib.standard.bridgematerial.model;
using Library.Winplus.syslib.standard.bridgematerial.repository;
using Library.Winplus.syslib.standard.bridgetype.model;
using Library.Winplus.syslib.standard.bridgetype.repository;
using Library.Winplus.syslib.standard.landparceltype.model;
using Library.Winplus.syslib.standard.landparceltype.repository;
using Library.Winplus.syslib.standard.locationtype.model;
using Library.Winplus.syslib.standard.locationtype.repository;
using Library.Winplus.syslib.standard.polematerial.model;
using Library.Winplus.syslib.standard.polematerial.repository;
using Library.Winplus.syslib.standard.poletype.model;
using Library.Winplus.syslib.standard.poletype.repository;
using Library.Winplus.syslib.standard.roadlane.model;
using Library.Winplus.syslib.standard.roadlane.repository;
using Library.Winplus.syslib.standard.roadmaterial.model;
using Library.Winplus.syslib.standard.roadmaterial.repository;
using Library.Winplus.syslib.standard.roadtype.model;
using Library.Winplus.syslib.standard.roadtype.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.helper.service
{
    public class HelpInitialMasterData
    {
        public void Initial()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                conn.beginTrans();

                List<LocationType> lstLocationType = new LocationTypeRepository().FindAll(conn);
                if (lstLocationType.Count == 0)
                {
                    new LocationTypeRepository().Create(conn, new LocationType() { LocationTypeName= "อาคาร", DisplayIndex=1,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new LocationTypeRepository().Create(conn, new LocationType() { LocationTypeName= "ป้าย", DisplayIndex=2,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new LocationTypeRepository().Create(conn, new LocationType() { LocationTypeName= "ถนน", DisplayIndex=3,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new LocationTypeRepository().Create(conn, new LocationType() { LocationTypeName= "สะพาน", DisplayIndex=4,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new LocationTypeRepository().Create(conn, new LocationType() { LocationTypeName= "เสาไฟฟ้า", DisplayIndex=5,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new LocationTypeRepository().Create(conn, new LocationType() { LocationTypeName= "ที่ดิน", DisplayIndex=6,CreatedOn=DateTime.Now,CreatedBy=1 });
                }

                List<BoardType> lstBoardType = new BoardTypeRepository().FindAll(conn);
                if (lstBoardType.Count == 0)
                {
                    new BoardTypeRepository().Create(conn, new BoardType() { BoardTypeName= "1-อักษรไทยล้วน", DisplayIndex=1,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new BoardTypeRepository().Create(conn, new BoardType() { BoardTypeName= "2-อักษรไทยปนกับอักษรต่างประเภท/ภาพ/เครื่องหมายอื่น", DisplayIndex=2,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new BoardTypeRepository().Create(conn, new BoardType() { BoardTypeName= "3(ก)-ไม่มีอักษรไทย", DisplayIndex=3,CreatedOn=DateTime.Now,CreatedBy=1 });
                    new BoardTypeRepository().Create(conn, new BoardType() { BoardTypeName = "3(ข)-อักษรไทยบางส่วนหรือทั้งหมดอยู่ใต้หรือต่ำกว่าอักษรต่างประเทศ", DisplayIndex=4,CreatedOn=DateTime.Now,CreatedBy=1 });
                }


                List<BoardLocation> lstBoardLocation = new BoardLocationRepository().FindAll(conn);
                if (lstBoardLocation.Count == 0)
                {
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "หน้าร้าน", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "ติดกระจก", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "บนหลังคา", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "หน้าโครงการ", DisplayIndex = 4, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "ริมถนน", DisplayIndex = 5, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "ภายในอาคาร", DisplayIndex = 6, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardLocationRepository().Create(conn, new BoardLocation() { BoardLocationName = "โครงสร้าง", DisplayIndex = 7, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<BoardStyle> lstBoardStyle = new BoardStyleRepository().FindAll(conn);
                if (lstBoardStyle.Count == 0)
                {
                    new BoardStyleRepository().Create(conn, new BoardStyle() { BoardStyleName = "ป้ายทั่วไป", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardStyleRepository().Create(conn, new BoardStyle() { BoardStyleName = "ป้ายอิเล็กทรอนิกส์", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BoardStyleRepository().Create(conn, new BoardStyle() { BoardStyleName = "ป้ายไตรวิชั่น", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<RoadType> lstRoadType = new RoadTypeRepository().FindAll(conn);
                if (lstRoadType.Count == 0)
                {
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ทางหลวงตัวเลข 1 หลัก", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ทางหลวงตัวเลข 2 หลัก", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ทางหลวงตัวเลข 3 หลัก", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ทางหลวงตัวเลข 4 หลัก", DisplayIndex = 4, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ทางหลวงชนบท", DisplayIndex = 5, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ถนนท้องถิ่น", DisplayIndex = 6, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ซอย", DisplayIndex = 7, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadTypeRepository().Create(conn, new RoadType() { RoadTypeName = "ทางเดินเท้า", DisplayIndex = 8, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<RoadMaterial> lstRoadMaterial = new RoadMaterialRepository().FindAll(conn);
                if (lstRoadMaterial.Count == 0)
                {
                    new RoadMaterialRepository().Create(conn, new RoadMaterial() { RoadMaterialName = "ลาดยาง", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadMaterialRepository().Create(conn, new RoadMaterial() { RoadMaterialName = "คอนกรีต", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadMaterialRepository().Create(conn, new RoadMaterial() { RoadMaterialName = "ลูกรัง/หินคลุก", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadMaterialRepository().Create(conn, new RoadMaterial() { RoadMaterialName = "ดิน", DisplayIndex = 4, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<BridgeType> lstBridgeType = new BridgeTypeRepository().FindAll(conn);
                if (lstBridgeType.Count == 0)
                {
                    new BridgeTypeRepository().Create(conn, new BridgeType() { BridgeTypeName = "รถยนต์ข้าม", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BridgeTypeRepository().Create(conn, new BridgeType() { BridgeTypeName = "จักรยานและจักรยานยนต์", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BridgeTypeRepository().Create(conn, new BridgeType() { BridgeTypeName = "คนข้าม", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<BridgeMaterial> lstBridgeMaterial = new BridgeMaterialRepository().FindAll(conn);
                if (lstBridgeMaterial.Count == 0)
                {
                    new BridgeMaterialRepository().Create(conn, new BridgeMaterial() { BridgeMaterialName = "คอนกรีต", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BridgeMaterialRepository().Create(conn, new BridgeMaterial() { BridgeMaterialName = "เหล็ก", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new BridgeMaterialRepository().Create(conn, new BridgeMaterial() { BridgeMaterialName = "ไม้", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<PoleType> lstPoleType = new PoleTypeRepository().FindAll(conn);
                if (lstPoleType.Count == 0)
                {
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาส่งไฟฟ้าแรงสูง", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาไฟฟ้าแรงสูง", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาไฟฟ้าแรงต่ำ", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาไฟส่องสว่าง-ไฮแมสท์", DisplayIndex = 4, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาไฟส่องสว่าง-โพสท์ท็อป", DisplayIndex = 5, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาไฟส่องสว่าง-กิ่งคู่", DisplayIndex = 6, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleTypeRepository().Create(conn, new PoleType() { PoleTypeName = "เสาไฟส่องสว่าง-กิ่งเดี่ยว", DisplayIndex = 7, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<PoleMaterial> lstPoleMaterial = new PoleMaterialRepository().FindAll(conn);
                if (lstPoleMaterial.Count == 0)
                {
                    new PoleMaterialRepository().Create(conn, new PoleMaterial() { PoleMaterialName = "คอนกรีต", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleMaterialRepository().Create(conn, new PoleMaterial() { PoleMaterialName = "เหล็ก", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new PoleMaterialRepository().Create(conn, new PoleMaterial() { PoleMaterialName = "ไม้", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<RoadLane> lstRoadLane = new RoadLaneRepository().FindAll(conn);
                if (lstRoadLane.Count == 0)
                {
                    new RoadLaneRepository().Create(conn, new RoadLane() { RoadLaneName = "1", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadLaneRepository().Create(conn, new RoadLane() { RoadLaneName = "2", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadLaneRepository().Create(conn, new RoadLane() { RoadLaneName = "3", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadLaneRepository().Create(conn, new RoadLane() { RoadLaneName = "4", DisplayIndex = 4, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadLaneRepository().Create(conn, new RoadLane() { RoadLaneName = "5", DisplayIndex = 5, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new RoadLaneRepository().Create(conn, new RoadLane() { RoadLaneName = "6", DisplayIndex = 6, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }

                List<LandParcelType> lstLandParcelType = new LandParcelTypeRepository().FindAll(conn);
                if (lstLandParcelType.Count == 0)
                {
                    new LandParcelTypeRepository().Create(conn, new LandParcelType() { LandParcelTypeName = "ที่อยู่อาศัย", DisplayIndex = 1, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new LandParcelTypeRepository().Create(conn, new LandParcelType() { LandParcelTypeName = "ประกอบการค้า", DisplayIndex = 2, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new LandParcelTypeRepository().Create(conn, new LandParcelType() { LandParcelTypeName = "เกษตรกรรม ปลูกพืช", DisplayIndex = 3, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new LandParcelTypeRepository().Create(conn, new LandParcelType() { LandParcelTypeName = "เกษตรกรรม เลี้ยงสัตว์", DisplayIndex = 4, CreatedOn = DateTime.Now, CreatedBy = 1 });
                    new LandParcelTypeRepository().Create(conn, new LandParcelType() { LandParcelTypeName = "ที่รกร้างว่างเปล่า", DisplayIndex = 5, CreatedOn = DateTime.Now, CreatedBy = 1 });
                }
                conn.commitTrans();
            }
        }
    }
}