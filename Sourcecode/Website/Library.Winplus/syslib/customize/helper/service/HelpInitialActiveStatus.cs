﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.standard.activestatus.model;
using Library.Winplus.syslib.standard.activestatus.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.helper.service
{
    public class HelpInitialActiveStatus
    {
        public void Initial()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                conn.beginTrans();

                ActiveStatusRepository activeStatusRepository = new ActiveStatusRepository();
                if (activeStatusRepository.FindAll(conn).Count == 0)
                {
                    ActiveStatus model = new ActiveStatus();
                    model.ActiveStatusName = "Active";
                    model.CreatedOn = DateTime.Now;
                    model.CreatedBy = LoginUser.DefaultUser.SystemUserId;

                    activeStatusRepository.Create(conn, model);

                    model = new ActiveStatus();
                    model.ActiveStatusName = "Inactive";
                    model.CreatedOn = DateTime.Now;
                    model.CreatedBy = LoginUser.DefaultUser.SystemUserId;

                    activeStatusRepository.Create(conn, model);
                }

                conn.commitTrans();
            }
        }
    }
}