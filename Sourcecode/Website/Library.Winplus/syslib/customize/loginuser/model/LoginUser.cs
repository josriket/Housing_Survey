﻿using System;
using System.Collections.Generic;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.standard.userlogin.model;

namespace Library.Winplus.syslib.customize.loginuser.model
{
    [Serializable]
    public class LoginUser : UserLogin
    {
        public SystemUser User { get; set; }
        public Dictionary<string, bool> AuthorizeMenu { get; set; }

        public List<Int64> LstOrganizationId = new List<Int64>();

        public static new bool HasAuthorize(LoginUser loginUser, String systemName, String method)
        {
            if (loginUser == null || loginUser.AuthorizeMenu == null) return false;
            if (loginUser.AuthorizeMenu.ContainsKey(systemName + "-Can" + method))
            {
                return loginUser.AuthorizeMenu[systemName + "-Can" + method];
            }
            return false;
        }

        public static LoginUser DefaultUser
        {
            get
            {
                LoginUser loginUser = new LoginUser();
                loginUser.SystemUserId = 1;
                return loginUser;
            }
        }

    }
}