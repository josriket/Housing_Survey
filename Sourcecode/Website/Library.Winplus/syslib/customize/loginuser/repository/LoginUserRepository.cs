﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.homepage.model;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.systemuser.model;
using Library.Winplus.syslib.standard.userlogin.repository;

namespace Library.Winplus.syslib.customize.loginuser.repository
{
    public class LoginUserRepository : SimpleRepository<LoginUser>
    {

        public LoginUser FindByTokenId(DBConnection conn, string TokenId)
        {
            UserLoginRepository ulrRepo = new UserLoginRepository();

            String sql = @"select u.* From [UserLogin] u where u.[TokenId]=@TokenId";

            LoginUser model = QueryUtil.Find<LoginUser>(conn, sql, new DBParameter("TokenId", TokenId));

            if (model != null)
            {
                // Find SystemUser
                sql = @"select u.*
                        from SystemUser u
                        where u.SystemUserId=@SystemUserId";
                model.User = QueryUtil.Find<SystemUser>(conn, sql, new DBParameter("SystemUserId", model.SystemUserId.Value));

                if (model.User != null && model.User.SystemUserId.HasValue)
                {
                    sql = @"select sm.[SystemName]
                            ,srm.[CanSearch]
                            ,srm.[CanView]
                            ,srm.[CanAdd]
                            ,srm.[CanEdit]
                            ,srm.[CanDelete]
                            ,srm.[CanExport]
                            ,srm.[CanImport]
                            ,sr.[SystemRoleName]
                        from [SystemRoleMenu] srm 
                        inner join [SystemRole] sr on sr.[SystemRoleId]=srm.[SystemRoleId]
                        inner join [SystemMenu] sm on sm.[SystemMenuId]=srm.[SystemMenuId]
                        inner join [SystemUser] su on su.[SystemRoleId]=sr.[SystemRoleId]
                        where 1=1
                        and su.[SystemUserId]=@SystemUserId
                        and su.[ActiveStatusId]=1
                        and sm.[ActiveStatusId]=1
                        and sr.[ActiveStatusId]=1";

                    model.AuthorizeMenu = new Dictionary<String, bool>();
                    DataSet ds = conn.executeDataSet(sql, new DBParameter("SystemUserId", model.SystemUserId));
                    String systemName = null;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        model.User.SystemRoleIdSystemRoleName = row["SystemRoleName"].ToString();

                        systemName = row["SystemName"].ToString();
                        model.AuthorizeMenu.Add(systemName + "-CanSearch", Convert.ToBoolean(row["CanSearch"]));
                        model.AuthorizeMenu.Add(systemName + "-CanView", Convert.ToBoolean(row["CanView"]));
                        model.AuthorizeMenu.Add(systemName + "-CanAdd", Convert.ToBoolean(row["CanAdd"]));
                        model.AuthorizeMenu.Add(systemName + "-CanEdit", Convert.ToBoolean(row["CanEdit"]));
                        model.AuthorizeMenu.Add(systemName + "-CanDelete", Convert.ToBoolean(row["CanDelete"]));
                        model.AuthorizeMenu.Add(systemName + "-CanExport", Convert.ToBoolean(row["CanExport"]));
                        model.AuthorizeMenu.Add(systemName + "-CanImport", Convert.ToBoolean(row["CanImport"]));
                    }

                }
                else
                {
                    model = null;
                }
            }

            return model;
        }

        public LoginUser FindByTokenId(string TokenId)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                return FindByTokenId(conn, TokenId);
            }
        }

        public List<DisplaySystemParentMenu> LoadDisplayMenu(DBConnection conn, LoginUser loginUser)
        {
            List<DisplaySystemParentMenu> lstDisplaySystemParentMenu = new List<DisplaySystemParentMenu>();

            string sql = @"select spm.[SystemParentMenuId]
                                ,spm.[SystemParentMenuName]
                                ,spm.[MenuUrl] ParentMenuUrl
                                ,spm.[IsHidden] ParentIsHidden
                                ,sm.[SystemMenuName]
                                ,sm.[MenuUrl]
                                ,sm.[IsHidden]
                                ,spm.[IconName]
                                ,srm.[MenuVisible]
                            from [SystemRoleMenu] srm 
                            inner join [SystemRole] sr on sr.[SystemRoleId]=srm.[SystemRoleId]
                            inner join [SystemMenu] sm on sm.[SystemMenuId]=srm.[SystemMenuId]
                            inner join [SystemUser] su on su.[SystemRoleId]=sr.[SystemRoleId]
				            inner join [SystemParentMenu] spm on spm.[SystemParentMenuId]=sm.[SystemParentMenuId]
                            where 1=1
                            and su.[SystemUserId]=@SystemUserId
                            and su.[ActiveStatusId]=1
                            and sm.[ActiveStatusId]=1
                            and sr.[ActiveStatusId]=1
				            order by spm.[DisplayIndex],sm.[DisplayIndex],sm.SystemMenuName";

            DisplaySystemParentMenu parentMenu = null;
            DisplaySystemMenu menu = null;
            Int64 SystemParentMenuId = 0;

            DataSet ds = conn.executeDataSet(sql, new DBParameter("SystemUserId", loginUser.SystemUserId));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (Convert.ToInt64(row["SystemParentMenuId"]) != SystemParentMenuId)
                {
                    SystemParentMenuId = Convert.ToInt64(row["SystemParentMenuId"]);
                    parentMenu = new DisplaySystemParentMenu();
                    parentMenu.lstDisplaySystemMenu = new List<DisplaySystemMenu>();
                    parentMenu.SystemParentMenuId = SystemParentMenuId;
                    parentMenu.SystemParentMenuName = row["SystemParentMenuName"] == DBNull.Value ? null : row["SystemParentMenuName"].ToString();
                    parentMenu.ParentMenuUrl = row["ParentMenuUrl"]==DBNull.Value?null: row["ParentMenuUrl"].ToString();
                    parentMenu.ParentIsHidden = Convert.ToBoolean(row["ParentIsHidden"]);
                    parentMenu.IconName = row["IconName"].ToString();

                    lstDisplaySystemParentMenu.Add(parentMenu);
                }

                menu = new DisplaySystemMenu();
                menu.SystemMenuName = row["SystemMenuName"] == DBNull.Value ? null : row["SystemMenuName"].ToString();
                menu.MenuUrl = row["MenuUrl"]==DBNull.Value?null: row["MenuUrl"].ToString();
                menu.IsHidden = Convert.ToBoolean(row["IsHidden"]) || !Convert.ToBoolean(row["MenuVisible"]);

                parentMenu.lstDisplaySystemMenu.Add(menu);
            }

            foreach (DisplaySystemParentMenu parent in lstDisplaySystemParentMenu)
            {
                bool hasDisplay = false;
                foreach (DisplaySystemMenu sub in parent.lstDisplaySystemMenu)
                {
                   
                    if (!sub.IsHidden)
                    {
                        hasDisplay = true;
                        break;
                    }
                }
                if (!hasDisplay)
                {
                    parent.ParentIsHidden = true;
                }
            }

            return lstDisplaySystemParentMenu;
        }

        public void Logout(DBConnection conn, string TokenId)
        {
            new UserLoginRepository().DeleteByTokenId(conn, TokenId);
        }

        public override void ImportExcel(DBConnection conn, LoginUser loginUser, List<LoginUser> lstImport)
        {
        }
    }
}
