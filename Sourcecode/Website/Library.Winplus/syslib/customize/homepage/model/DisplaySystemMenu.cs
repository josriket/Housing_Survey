﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.homepage.model
{
    public class DisplaySystemMenu
    {
        public string SystemMenuName { get; set; }
        public string MenuUrl { get; set; }
        public bool IsHidden { get; set; }
    }
}
