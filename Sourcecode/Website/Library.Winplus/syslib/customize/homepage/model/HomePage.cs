﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.customize.loginuser.model;

namespace Library.Winplus.syslib.customize.homepage.model
{
    public class HomePage
    {
        public List<DisplaySystemParentMenu> lstDisplaySystemParentMenu { get; set; }
        public LoginUser loginUser { get; set; }
        public String DefaultMenuUrl { get; set; }
    }
}
