﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.homepage.model
{
    public class DisplaySystemParentMenu
    {
        public Int64 SystemParentMenuId { get; set; }
        public string SystemParentMenuName { get; set; }
        public string ParentMenuUrl { get; set; }
        public bool ParentIsHidden { get; set; }
        public string IconName { get; set; }

        public List<DisplaySystemMenu> lstDisplaySystemMenu { get; set; }

    }
}
