﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.choosetree.model.submodel
{
    public class JTreeAttr
    {
        //'onclick':'alert(this.id);'
        public string onclick { get; set; } // alert(this.id);
        public string title { get; set; }
        public JTreeAttr(string methodName, string id, int level, string text,string title,bool hasChild)
        {
            //chooseJTreeItem(id, level, name)
            onclick = string.Format(methodName+"({0},{1},\"{2}\",{3});", id, level, text,hasChild.ToString().ToLower());
            this.title = title.Replace("<br/>","\n");
        }
    }
}