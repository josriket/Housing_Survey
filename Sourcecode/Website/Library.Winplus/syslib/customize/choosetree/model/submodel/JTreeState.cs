﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.choosetree.model.submodel
{
    public class JTreeState
    {
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }

        public JTreeState(bool opened, bool disabled, bool selected)
        {
            this.opened = opened;
            this.disabled = disabled;
            this.selected = selected;
        }
    }
}
