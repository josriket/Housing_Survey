﻿using Library.Winplus.syslib.customize.choosetree.model.submodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Library.Winplus.syslib.customize.choosetree.model
{
    public class JTree
    {
        public string id { get; set; }
        public string text{get;set;}
        public string title { get; set; }
        public string icon { get; set; } //fa fa-file-image-o
        public JTreeState state{get;set;}
        public List<JTree> children{get;set;}
        public JTreeAttr a_attr { get; set; }

        [ScriptIgnore]
        public JTree parentTree { get; set; }
    }
}
