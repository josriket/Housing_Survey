﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.choosetree.model;
using Library.Winplus.syslib.customize.choosetree.model.submodel;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.choosetree.repository
{
    public class JTreeRepository
    {
        public string TableName { get; set; }
        public List<string> SelectedIds { get; set; }
        public JTreeRepository(string TableName, List<string> SelectedIds)
        {
            this.TableName = TableName;
            this.SelectedIds = SelectedIds;
        }

        public List<JTree> FindChild(DBConnection conn, JTree parentTree, string id, int level,string jsMethod,bool isOpenAll,string filter)
        {
            level++;
            string titleColumn = TableName+"Name";
            if (TableName.ToUpper() == "JCIMODULE")
            {
                titleColumn = "Description";
            }
            else if (TableName.ToUpper() == "INCIDENTCASE")
            {
                titleColumn = "CaseDescription";
            }

            string sql = string.Format("select cast({0}Id as varchar(50)) id, {0}Name [text] ,{2} [title] From {0} where Parent{0}Id {1}"
                , TableName
                , (id == null ? "is null" : "=" + id)
                , titleColumn);

            if (!string.IsNullOrEmpty(filter))
            {
                sql += " "+filter;
            }

            List<JTree> lstJtree = QueryUtil.FindList<JTree>(conn, sql);
            if (lstJtree.Count > 0)
            {
                if (parentTree != null)
                {
                    parentTree.icon = "fa fa-folder";
                }

                foreach (JTree jTree in lstJtree)
                {
                    jTree.icon = "fa fa-file-text-o";
                    if (jTree.title == null) jTree.title = jTree.text;
                    jTree.state = new JTreeState(false || isOpenAll, false, false);
                    jTree.children = FindChild(conn, jTree, jTree.id, level, jsMethod, isOpenAll, filter);
                    jTree.a_attr = new JTreeAttr(jsMethod, jTree.id, level, jTree.text, jTree.title,jTree.children.Count>0);
                    jTree.parentTree = parentTree;
                    jTree.text = StringUtil.ShortTextTree(SplitText(jTree.text));

                    if (SelectedIds!=null && SelectedIds.Contains(jTree.id))
                    {
                        jTree.state.selected = true;
                        if (jTree.parentTree != null)
                        {
                            OpenParent(jTree.parentTree);
                        }
                    }
                }
            }

            return lstJtree;
        }

        private string[] _temp;
        private string SplitText(string text)
        {
            _temp = text.Split(new string[] { " > " }, StringSplitOptions.None);
            if (_temp.Length > 0)
            {
                return _temp[_temp.Length - 1].Trim();
            }
            return text;
        }

        private void OpenParent(JTree jTree)
        {
            jTree.state.opened = true;
            if (jTree.parentTree != null)
            {
                OpenParent(jTree.parentTree);
            }
        }
    }
}