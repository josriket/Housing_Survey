﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.taskschedule.service.interf
{
    public abstract class BasicTask
    {
        private Thread Thread = null;
        private ThreadStart ThreadStart = null;
        private object InitialLock = new object();

        public BasicTask()
        {
            ThreadStart = new ThreadStart(PerformTask);
            Thread = new Thread(ThreadStart);
        }

        public void StartThread()
        {
            Thread.Start();
        }

        public abstract void PerformTask();

        public void StopThread()
        {
            try
            {
                Thread.Abort();
            }
            catch
            {
            }
        }
    }
}
