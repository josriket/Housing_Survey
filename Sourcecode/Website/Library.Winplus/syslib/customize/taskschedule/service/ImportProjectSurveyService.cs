﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.locationtype.model;
using Library.Winplus.syslib.standard.locationtype.repository;
using Library.Winplus.syslib.standard.project.model;
using Library.Winplus.syslib.standard.survey.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Library.Winplus.syslib.customize.taskschedule.service
{
    public class ImportProjectSurveyService
    {
        private string filePath { get; set; }
        private bool fileExists { get; set; }
        private Int64 ProjectId { get; set; }
        private Int64 SystemUserId { get; set; }
        private string fileName { get; set; }
        private Project project { get; set; }

        public ImportProjectSurveyService(Project model, string filePath)
        {
            this.project = model;
            this.SystemUserId = SystemUserId;
            this.filePath = filePath;
            if (File.Exists(filePath))
            {
                fileExists = true;
                FileInfo finfo = new FileInfo(filePath);
                fileName = finfo.Name;
                ProjectId = Convert.ToInt64(finfo.Name.Split('_')[1]);
                SystemUserId = Convert.ToInt64(finfo.Name.Split('_')[2]);
            }
        }

        public void CreateIfNotExists(DBConnection conn, Int64 ProjectId)
        {
            string tableName = $"Survey_{ProjectId}";
            if (!DataSetUtil.IsNotEmpty(conn.executeDataSet($"Select * From Information_Schema.tables Where TABLE_NAME='{tableName}'")))
            {
                conn.executeUpdate($"Select * Into dbo.{tableName} From dbo.Survey Where 1<>1");
                conn.executeUpdate($"Alter Table dbo.{tableName} Add Default(0) For UpdateFlag");
                conn.executeUpdate($"Alter Table dbo.{tableName} Add Default(0) For DeleteFlag");
                conn.executeUpdate($"Alter Table dbo.{tableName} Add Default(0) For NewPoint");
                conn.executeUpdate($"Alter Table dbo.{tableName} Add Default(getdate()) For CreatedOn");
            }
        }

        public void PrepareTable(DBConnection conn, Int64 ProjectId)
        {
            string tableName = $"Survey_{ProjectId}";
            bool createNew = false;
            if (DataSetUtil.IsNotEmpty(conn.executeDataSet($"Select * From Information_Schema.tables Where TABLE_NAME='{tableName}'")))
            {
                if (DataSetUtil.IsNotEmpty(conn.executeDataSet($"SELECT Top 1 * From {tableName}")))
                {
                    // กรณีมี Table อยู่แล้ว
                    if (project.OverrideData)
                    {
                        conn.executeUpdate($"Exec sp_rename '{tableName}','$_{tableName}_{StringUtil.NewTimeStamp()}'");
                        createNew = true;
                    }
                }
            }
            else
            {
                createNew = true;
            }

            if (createNew)
            {
                CreateIfNotExists(conn, ProjectId);
            }
        }

        public void PerformTask()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                if (fileExists)
                {
                    StreamReader file = new StreamReader(filePath);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("SurveyCode", typeof(string));
                    dt.Columns.Add("LocationTypeId", typeof(long));
                    dt.Columns.Add("PlaceOwner", typeof(string));
                    dt.Columns.Add("Latitude", typeof(decimal));
                    dt.Columns.Add("Longitude", typeof(decimal));
                    dt.Columns.Add("ProjectId", typeof(long));
                    dt.Columns.Add("CreatedBy", typeof(long));
                    dt.Columns.Add("LandParcelNo", typeof(string));
                    dt.Columns.Add("LandNo", typeof(string));
                    dt.Columns.Add("LandRai", typeof(int));
                    dt.Columns.Add("LandNgan", typeof(int));
                    dt.Columns.Add("LandWa", typeof(int));
                    dt.Columns.Add("LandSubWa", typeof(decimal));

                    string headerText = file.ReadLine(); //Skip Header
                    string[] headers = headerText.Split(',');
                    int? idxLongitude = null;
                    int? idxLatitude = null;
                    int? idxLocationType = null;
                    int? idxPlaceOwner = null;
                    int? idxLandParcelNo = null;
                    int? idxLandNo = null;
                    int? idxLandRai = null;
                    int? idxLandNgan = null;
                    int? idxLandWa = null;
                    int? idxLandSubWa = null;
                    int? i = 0;
                    foreach (string header in headers)
                    {
                        switch (header.ToUpper())
                        {
                            case "Y": { idxLatitude = i; break; }
                            case "X": { idxLongitude = i; break; }
                            case "TYPE": { idxLocationType = i; break; }
                            case "OWNER": { idxPlaceOwner = i; break; }
                            case "PARCEL_NO": { idxLandParcelNo = i; break; }
                            case "LAND_NO": { idxLandNo = i; break; }
                            case "RAI": { idxLandRai = i; break; }
                            case "NGAN": { idxLandNgan = i; break; }
                            case "WA": { idxLandWa = i; break; }
                            case "SUBWA": { idxLandSubWa = i; break; }
                        }
                        i++;
                    }

                    List<LocationType> lstLocationType = new LocationTypeRepository().FindAll(conn);
                    Dictionary<string, long> dicLocationType = new Dictionary<string, long>();
                    foreach (LocationType l in lstLocationType) dicLocationType.Add(l.LocationTypeName, l.LocationTypeId.Value);

                    string[] split = null;
                    DataRow row = null;
                    string line = null;
                    i = 1;
                    while ((line = file.ReadLine()) != null)
                    {
                        try
                        {
                            row = dt.NewRow();
                            dt.Rows.Add(row);

                            split = line.Split(',');
                            row["SurveyCode"] = (i++).ToString();
                            if (idxLongitude.HasValue) row["Longitude"] = DataSetUtil.ParseDecimal(split[idxLongitude.Value]);
                            if (idxLatitude.HasValue) row["Latitude"] = DataSetUtil.ParseDecimal(split[idxLatitude.Value]);

                            string type = split[idxLocationType.Value];
                            if (dicLocationType.TryGetValue(type, out long value)) row["LocationTypeId"] = value;
                            else row["LocationTypeId"] = DBNull.Value;

                            if (idxPlaceOwner.HasValue) row["PlaceOwner"] = DataSetUtil.ParseString(split[idxPlaceOwner.Value]);
                            if (idxLandParcelNo.HasValue) row["LandParcelNo"] = DataSetUtil.ParseString(split[idxLandParcelNo.Value]);
                            if (idxLandNo.HasValue) row["LandNo"] = DataSetUtil.ParseString(split[idxLandNo.Value]);
                            if (idxLandRai.HasValue) row["LandRai"] = DataSetUtil.ParseInt(split[idxLandRai.Value]);
                            if (idxLandNgan.HasValue) row["LandNgan"] = DataSetUtil.ParseInt(split[idxLandNgan.Value]);
                            if (idxLandWa.HasValue) row["LandWa"] = DataSetUtil.ParseInt(split[idxLandWa.Value]);
                            if (idxLandSubWa.HasValue) row["LandSubWa"] = DataSetUtil.ParseDecimal(split[idxLandSubWa.Value]);
                            row["ProjectId"] = ProjectId;
                            row["CreatedBy"] = SystemUserId;
                        }
                        catch(Exception ex)
                        {
                            throw ex;
                        }
                    }

                    file.Close();

                    string tableName = $"Survey_{ProjectId}";

                    PrepareTable(conn, ProjectId);

                    conn.bulkInsert(tableName, dt);

                    string resourcePath = SystemConfigService.GetStringConfig("ResourcePath", @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Resource");
                    string newFileName = $@"{resourcePath}\csv\import\completed\{fileName}";

                    File.Move(filePath, newFileName);
                }
            }
        }
    }
}