﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.customize.taskschedule.service.interf;
using Library.Winplus.syslib.standard.bglogger.model;
using Library.Winplus.syslib.standard.bglogger.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.customize.taskschedule.service
{
    public class ClearTempService : BasicTask
    {
        public override void PerformTask()
        {
            while (true)
            {
                BGLogger logger = new BGLogger();
                logger.LogClass = "ClearTempService";
                logger.LogMethod = "PerformTask";
                logger.BGLoggerName = logger.LogClass +  " - "+logger.LogMethod;
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    try
                    {
                        List<string> message = new List<string>();
                        string sql = string.Empty;
                        int foundUpdate = 0;

                        // Delete Logger
                        int? LoggerTime = SystemConfigService.GetIntConfig("LoggerTime",45);
                        
                        sql = string.Format("delete From Logger where CreatedOn<dateadd(day,-{0},getdate())",LoggerTime);
                        foundUpdate = conn.executeUpdate(sql);
                        message.Add(string.Format("Remove Logger[-{0} Days]:{1} Recored",LoggerTime, foundUpdate));

                        // Delete BG-Logger
                        sql = string.Format("delete From BGLogger where CreatedOn<dateadd(day,-{0},getdate())",LoggerTime);
                        foundUpdate = conn.executeUpdate(sql);
                        message.Add(string.Format("Remove BGLogger[-{0} Days]:{1} Recored",LoggerTime, foundUpdate));

                        logger.LogMessage = string.Join(", ", message);
                        logger.LogLevel = "Info";
                    }
                    catch (Exception ex)
                    {
                        logger.LogLevel = "Error";
                        logger.LogError = ex.ToString();
                    }
                    finally
                    {
                        logger.CreatedOn = DateTime.Now;
                        logger.CreatedBy = LoginUser.DefaultUser.SystemUserId;
                        new BGLoggerRepository().Create(conn, logger);
                    }
                }

                
                System.Threading.Thread.Sleep(1000 * 60 * 60 * 24); // 60 Minute
            }
        }
    }
}