﻿using Library.Winplus.syslib.customize.systemconfig.service;
using System;
using System.IO;

namespace Library.Winplus.syslib.core.logger
{
    public class CriticalLogger
    {
        public static string LoggerFolder = null;
        public static void WriteLog(string System,string message)
        {
            StreamWriter CrititalLog = null;
            try
            {
                if(LoggerFolder==null) LoggerFolder = SystemConfigService.GetStringConfig("LoggerFolder", @"D:\Temp");

                CrititalLog = File.AppendText(LoggerFolder + string.Format("\\{0}_{1}.log", System, DateTime.Now.ToString("yyyyMMdd")));

                CrititalLog.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " {0}", message);
                CrititalLog.Flush();
                CrititalLog.Close();
                CrititalLog = null;
            }
            catch
            {
                if(CrititalLog!=null) CrititalLog.Close();
            }
        }
    }
}
