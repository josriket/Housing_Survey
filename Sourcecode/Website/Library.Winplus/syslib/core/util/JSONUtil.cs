﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Library.Winplus.syslib.core.util
{
    //Library.Winplus.syslib.core.util.JSONUtil.Serialize(xx)
    public class JSONUtil
    {
        public static string Serialize(object value)
        {
            if (value == null) return null;
            return JsonConvert.SerializeObject(value, Formatting.None,
                           new JsonSerializerSettings()
                           {
                               ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                           });
            //return JsonConvert.SerializeObject(value);
        }

        public static T DeSerialize<T>(string strJSON)
        {
            return JsonConvert.DeserializeObject<T>(strJSON);
        }

        public static List<SelectListItem> ParseSelectListItem(DataSet ds, string TextColumnName, string ValueColumnName)
        {
            List<SelectListItem> LstSelectListItem = new List<SelectListItem>();
            SelectListItem item = null;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                item = new SelectListItem();
                item.Text = row[TextColumnName].ToString();
                item.Value = row[ValueColumnName].ToString();
                LstSelectListItem.Add(item);
            }
            return LstSelectListItem;
        }
    }
}