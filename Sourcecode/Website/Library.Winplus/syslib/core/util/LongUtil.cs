﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util
{
    public class LongUtil
    {
        public static bool IsEqual(long? a, long? b)
        {
            if (!a.HasValue && !a.HasValue) return true;

            if (a.HasValue && !b.HasValue
                ||!a.HasValue && b.HasValue
                || a.Value!=b.Value
                )
            {
                return false;
            }

            return true;
        }
    }
}
