﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util.sampleobject
{
    public class WysiwygUtil
    {
        public static bool IsEqual(string p1, string p2)
        {
            p1 = StringUtil.ReplaceHtml(p1);
            p2 = StringUtil.ReplaceHtml(p2);

            return StringUtil.IsEqual(p1, p2);
        }

        public static string StringValue(string a)
        {
            return StringUtil.ReplaceHtml(a);
        }
    }
}
