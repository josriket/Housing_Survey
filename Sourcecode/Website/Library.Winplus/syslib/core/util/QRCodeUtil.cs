﻿using Gma.QrCodeNet.Encoding;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util
{
    public class QRCodeUtil
    {
        //pictureBox1.Image = GenerateQrCode("Hello world", Color.Black, Color.White, 100);
        //Length of data must over 8 charactor
        public static Bitmap GenerateQrCode(string text, System.Drawing.Color darkColor, System.Drawing.Color lightColor, int size)
        {
            QrEncoder encoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode code = encoder.Encode(text);
            
            // Check your text is shortter
            if (size % code.Matrix.Width != 0 || size % code.Matrix.Height != 0)
            {
                throw new InvalidOperationException("Width/Height not divisible with size");
            }
            int multiplier = size / code.Matrix.Width;
            Bitmap tempBmp = new Bitmap(size, size);
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    var originalX = x / multiplier;
                    var originalY = y / multiplier;
                    if (code.Matrix.InternalArray[originalX, originalY])
                    {
                        tempBmp.SetPixel(x, y, darkColor);
                    }
                    else
                    {
                        tempBmp.SetPixel(x, y, lightColor);
                    }
                }
            }
            return tempBmp;
        }
    }
}
