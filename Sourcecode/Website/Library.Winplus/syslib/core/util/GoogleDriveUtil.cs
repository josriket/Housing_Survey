﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Library.Winplus.syslib.core.logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Google.Apis.Drive.v3.FilesResource;

namespace Library.Winplus.syslib.core.util
{
    public class GoogleDriveUtil
    {
        static string[] Scopes = { DriveService.Scope.Drive };
        static string ApplicationName = "wecollect";

        public DriveService service;
        public GoogleDriveUtil()
        {
            using (System.IO.FileStream stream = new System.IO.FileStream("credentials.json", System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                //string credPath = "token.json";
                string credPath = @"token.json";
                UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });
            }
        }

        public GoogleDriveUtil(string credentials, string credPath)
        {
            using (System.IO.FileStream stream = new System.IO.FileStream(credentials, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                //string credPath = "token.json";
                UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });
            }
        }

        public string UploadFile(byte[] fileData, string fileName, string contentType= "image/jpg", string dirId = "1hSkTzYDuxzqiYw2q9ithDYY7bPA2Fkqg")
        {
            using (System.IO.Stream stream = new System.IO.MemoryStream(fileData))
            {
                var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = fileName,
                    Parents = new List<string>() { dirId },

                };

                FilesResource.CreateMediaUpload request;
                request = service.Files.Create(fileMetadata, stream, contentType);
                request.ChunkSize = ResumableUpload.MinimumChunkSize * 4;
                request.Fields = "id";
                var result = request.Upload();
                if (result.Status != UploadStatus.Completed)
                {
                    CriticalLogger.WriteLog("GoogleDriveUtil.UploadFile", JSONUtil.Serialize(result));
                    var rdn = new Random();
                    var count = 0;
                    do
                    {
                        var waitTime = (Convert.ToInt32(Math.Pow(2, count)) * 1000) + rdn.Next(0, 1000);
                        Thread.Sleep(waitTime);

                        result = request.Upload();
                        count++;
                    } while (count < 5 && (result.Status != UploadStatus.Completed));
                }

                var file = request.ResponseBody;
                string sharedLink = GetSharedLink(file.Id);

                return sharedLink;
            }
        }

        public string CreateDirectory(string parentId , string dirName)
        {
            var fileMetadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = dirName,
                Parents = new List<string>() { parentId },
                MimeType = "application/vnd.google-apps.folder"
            };

            FilesResource.CreateRequest file = service.Files.Create(fileMetadata);

            return GetFolderId(dirName);
        }

        public string GetFolderId(string folderName)
        {
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 1000;
            listRequest.Fields = "nextPageToken, files(id, name)";
            listRequest.Q = "";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute().Files;
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    if (file.Name == folderName)
                    {
                        return file.Id;
                    }
                }
            }
            return null;
        }

        public void ListAllFile()
        {
            //Define parameters of request.
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 1000;
            listRequest.Fields = "nextPageToken, files(id, name)";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute().Files;
            Console.WriteLine("Files:");
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    Console.WriteLine("{0} ({1})", file.Name, file.Id);
                }
            }
            else
            {
                Console.WriteLine("No files found.");
            }
            Console.Read();
        }

        public string GetSharedLink(string fileId)
        {
            Google.Apis.Drive.v3.Data.Permission permission = new Google.Apis.Drive.v3.Data.Permission();
            permission.Type = "anyone";
            permission.Role = "reader";
            permission.AllowFileDiscovery = true;

            PermissionsResource.CreateRequest request = service.Permissions.Create(permission, fileId);
            request.Fields = "*";
            request.Execute();
            //FilesResource.ListRequest listRequest = service.Files.List();
            //listRequest.Fields = "*";
            //List<Google.Apis.Drive.v3.Data.File>  files = listRequest.Execute().Files.ToList();
            //Google.Apis.Drive.v3.Data.File myFile = files.Single(f => f.Id == fileId);
            //return myFile.WebContentLink;

            return $"https://drive.google.com/uc?id={fileId}&export=download";
        }
    }
}
