﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util
{
    public class BooleanUtil
    {
        public static bool IsEqual(bool p1, bool p2)
        {
            return p1 == p2;
        }

        public static string StringValue(bool p)
        {
            return p ? "True" : "False";
        }
    }
}
