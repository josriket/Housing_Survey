﻿using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.standard.project.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util
{
    public class CSVOutputUtil
    {
        private static readonly string SPACE_BAR = " ";

        public string ExportData(Int64 ProjectId,DataTable dt)
        {
            dt.Columns.Remove("SurveyId"); // Remove Column-> SuveryId
            dt.Columns.Remove("RowNr"); // Remove Column-> SuveryId

            string resourcePath = SystemConfigService.GetStringConfig("ResourcePath", @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Resource");
            //resourcePath = @"D:\Workspace\P_Joe\Housing_Survey\Sourcecode\Website\Admin.Winplus\Resource";
            string fileName = $"Survey{ProjectId}_{StringUtil.NewTimeStamp()}.csv";

            using(StreamWriter sw = new StreamWriter(new FileStream($@"{resourcePath}\csv\export\{fileName}", FileMode.OpenOrCreate, FileAccess.Write),Encoding.UTF8))
            {
                sw.WriteLine(string.Join(",", dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName)));

                IEnumerable<string> list = null;
                foreach (DataRow row in dt.Rows)
                {
                    list = replaceCommas(row.ItemArray.Select(field => field.ToString()));
                    sw.WriteLine(string.Join(",", list));
                }
            }
           

            return fileName;
        }
        private IEnumerable<string> replaceCommas(IEnumerable<string> enumerable)
        {
            List<string> lst = new List<string>();
            foreach(string s in enumerable)
            {
                if (s.Contains(","))
                {
                    lst.Add("\"" + s.Replace(System.Environment.NewLine, SPACE_BAR).Replace('\n', ' ').Trim() + "\"");
                }
                else
                {
                    lst.Add(s.Replace(System.Environment.NewLine, SPACE_BAR).Replace('\n',' ').Trim());
                }
            }

            return lst;
        }
    }
}
