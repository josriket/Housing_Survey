﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Winplus.syslib.core.domain;

namespace Library.Winplus.syslib.core.util
{
    public class GenCodeKey
    {
        public string GenKey { get; set; }
        public string Prefix { get; set; }
        public int Length { get; set; }

        public GenCodeKey(string GenKey, string Prefix, int Length)
        {
            this.GenKey = GenKey;
            this.Prefix = Prefix;
            this.Length = Length;
        }

        // T-YYMM-XXX
        public static GenCodeKey TempNo()
        {
            string yyMM = DateTime.Now.Date.ToString("yyMM", BeanParameter.GetInstance().ThaiCultureInfo);
            return new GenCodeKey("TempNo", "T-" + yyMM + "-", 3);
        }


        // R-YYMM-XXX
        public static GenCodeKey RegNo()
        {
            string yyMM = DateTime.Now.Date.ToString("yyMM", BeanParameter.GetInstance().ThaiCultureInfo);
            return new GenCodeKey("RegNo", "R-" + yyMM + "-", 3);
        }

        // TYYMMXXX
        public static GenCodeKey TemplateCode()
        {
            string yyMM = DateTime.Now.Date.ToString("yyMM", BeanParameter.GetInstance().ThaiCultureInfo);
            return new GenCodeKey("TemplateCode", "T" + yyMM, 3);
        }
    }
}
