﻿using System.Data.SqlClient;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NPOI.SS.Util;
using System.Drawing;

namespace Library.Winplus.syslib.core.util
{
    public class ExcelOutputUtil
    {
        public String ReportName { get; set; }
        public Dictionary<string, CellProperties> TemplateStyle = new Dictionary<string, CellProperties>();
        public IWorkbook wb {get;set;}
        public int RowStartIndex =0;
        public ExcelOutputUtil(string ReportName,string templatePath)
        {
            this.ReportName = ReportName;

            MemoryStream fs = null;
            try
            {
                fs = new MemoryStream(File.ReadAllBytes(templatePath));
                wb = new XSSFWorkbook(fs);

                ISheet sheet = wb.GetSheetAt(0);
                //Header
                //ColumnHeader
                //CellString
                //CellNumeric
                //CellDecimal
                //CellDateTime
                //CellDate
                //CellMonth
                for (int i = 0; i < 20; i++)
                {
                    IRow xRow = sheet.GetRow(i);
                    if (xRow == null) break;
                    ICell xCell0 = xRow.GetCell(0);
                    if (xCell0 == null) break;
                    ICell xCell1 = xRow.GetCell(1);
                    if (xCell1 == null) break;

                    TemplateStyle.Add(xCell0.StringCellValue, new CellProperties(xCell1.CellStyle,xCell1.CellType));
                }
                wb.RemoveSheetAt(0);
            }
            finally
            {
                if (fs != null) fs.Close();
            }

        }

        public ICellStyle ColumnHeaderCellStyle { get; set; }
        public ICellStyle HeaderCellStyle { get; set; }

        public ICellStyle ColumnHeaderStyle(IWorkbook IWorkBook)
        {
            if (ColumnHeaderCellStyle == null)
            {
                ColumnHeaderCellStyle = IWorkBook.CreateCellStyle();

                // cell background
                ColumnHeaderCellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.BLUE.index;
                ColumnHeaderCellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

                // font color
                IFont font1 = IWorkBook.CreateFont();
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD; 
                ColumnHeaderCellStyle.SetFont(font1);
            }

            return ColumnHeaderCellStyle;
        }

        public ICellStyle HeaderStyle(IWorkbook IWorkBook)
        {
            if (HeaderCellStyle == null)
            {
                HeaderCellStyle = IWorkBook.CreateCellStyle();

                // font color
                IFont font1 = IWorkBook.CreateFont();
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
                HeaderCellStyle.SetFont(font1);
            }

            return HeaderCellStyle;
        }

        public void ToWorkBook(DataTable dt)
        {
            string sheetName = "Result";
            int rowIndex = RowStartIndex;
            int sheetIndex = 0;
            ICell xCell = null;
            ISheet sheet = wb.CreateSheet(sheetName + "_" + (sheetIndex + 1));

            // Report Header
            IRow xRow = sheet.CreateRow(rowIndex);

            int cellIndex = 0;
            foreach (DataColumn dtCol in dt.Columns)
            {
                xCell = xRow.CreateCell(cellIndex);
                xCell.CellStyle = TemplateStyle["ColumnHeader"].CellStyle;
                xCell.SetCellType(TemplateStyle["ColumnHeader"].CellType);
                xCell.SetCellValue(dtCol.ColumnName);
                cellIndex++;
            }

            rowIndex++;
            foreach (DataRow row in dt.Rows)
            {
                if (rowIndex != 0 && rowIndex % 1048576 == 0)
                {
                    sheet = wb.CreateSheet(sheetName + "_" + (++sheetIndex + 1));
                    rowIndex = 0;
                }

                xRow = sheet.CreateRow(rowIndex);
                cellIndex = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    if (!string.IsNullOrEmpty(dc.ColumnName) && dt.Columns.Contains(dc.ColumnName))
                    {
                        xCell = xRow.CreateCell(cellIndex);
                        if (dc.ColumnName.Equals("row_index"))
                        {
                            xCell.SetCellValue(rowIndex);
                        }
                        else
                        {
                            if (row[dc.ColumnName] != DBNull.Value)
                            {
                                object o = row[dc.ColumnName];
                                if (o is DateTime)
                                {
                                    xCell.SetCellType(TemplateStyle["CellDateTime"].CellType);
                                    xCell.CellStyle = TemplateStyle["CellDateTime"].CellStyle;
                                    xCell.CellStyle.DataFormat = TemplateStyle["CellDateTime"].CellStyle.DataFormat;
                                    xCell.SetCellValue(Convert.ToDateTime(o));
                                }
                                else if (o is decimal)
                                {
                                    xCell.SetCellType(TemplateStyle["CellDecimal"].CellType);
                                    xCell.CellStyle = TemplateStyle["CellDecimal"].CellStyle;
                                    xCell.CellStyle.DataFormat = TemplateStyle["CellDecimal"].CellStyle.DataFormat;
                                    xCell.SetCellValue(Convert.ToDouble(o));
                                }
                                else if (o is Int32)
                                {
                                    xCell.SetCellType(TemplateStyle["CellNumeric"].CellType);
                                    xCell.CellStyle = TemplateStyle["CellNumeric"].CellStyle;
                                    xCell.CellStyle.DataFormat = TemplateStyle["CellNumeric"].CellStyle.DataFormat;
                                    xCell.SetCellValue(Convert.ToInt32(o));
                                }
                                else if (o is Int64)
                                {
                                    xCell.CellStyle = TemplateStyle["CellString"].CellStyle;
                                    xCell.SetCellType(TemplateStyle["CellString"].CellType);
                                    xCell.CellStyle.DataFormat = TemplateStyle["CellString"].CellStyle.DataFormat;
                                    xCell.SetCellValue(Convert.ToInt64(o));
                                }
                                else
                                {
                                    xCell.SetCellType(TemplateStyle["CellString"].CellType);
                                    xCell.CellStyle = TemplateStyle["CellString"].CellStyle;
                                    xCell.SetCellValue(o.ToString());
                                }

                            }
                            else
                            {
                                xCell.SetCellType(TemplateStyle["CellGeneral"].CellType);
                                xCell.CellStyle = TemplateStyle["CellGeneral"].CellStyle;
                            }
                        }
                        cellIndex++;
                    }
                }
                rowIndex++;
            }

            // Resize column
            int i = 0;
            foreach (DataColumn dtCol in dt.Columns)
            {
                try
                {
                    sheet.AutoSizeColumn(i++);
                }
                catch{ }
            }
        }

        private void CloneStyle(XSSFCellStyle _xssfStyle, XSSFCellStyle _hssfStyle)
        {
            _xssfStyle.BorderLeft = _hssfStyle.BorderLeft;
            _xssfStyle.BorderRight = _hssfStyle.BorderRight;
            _xssfStyle.BorderTop = _hssfStyle.BorderTop;
            _xssfStyle.BorderBottom = _hssfStyle.BorderBottom;
            _xssfStyle.WrapText = _hssfStyle.WrapText;
            _xssfStyle.VerticalAlignment = _hssfStyle.VerticalAlignment;
            _xssfStyle.SetFont(_hssfStyle.GetFont());
        }

        public void ReponseFile(string outPutFile)
        {
            if (File.Exists(outPutFile)) File.Delete(outPutFile);

            ResponseFile(outPutFile, wb);
        }


        public void ResponseMemory(MemoryStream memStream)
        {
            wb.Write(memStream);
        }


        public static void ResponseFile(string fileName, IWorkbook wb)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(fileName, FileMode.CreateNew);
                wb.Write(fs);
            }
            finally
            {
                if (fs != null) fs.Close();
            }

        }


        internal static void ToWorkBook(IWorkbook wb, ISheet sheet, DataTable dt, int rowIndex, List<CellProperties> TemplateStyle)
        {
            object o = null;
            ICell xCell = null;
            IRow xRow = null;

            int cellIndex = 0;
            int rowNumber = 1;

            foreach (DataRow row in dt.Rows)
            {
                xRow = sheet.CreateRow(rowIndex);
                cellIndex = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    try
                    {
                        xCell = xRow.CreateCell(cellIndex);
                        xCell.SetCellType(TemplateStyle[cellIndex].CellType);
                        xCell.CellStyle = TemplateStyle[cellIndex].CellStyle;
                        xCell.CellStyle.DataFormat = TemplateStyle[cellIndex].CellStyle.DataFormat;

                        if (dc.ColumnName.Equals("RowNr"))
                        {
                            xCell.SetCellValue(rowNumber++);
                        }
                        else
                        {

                            o = row[dc.ColumnName];

                            if (o == DBNull.Value)
                            {

                            }
                            else if (o is DateTime)
                            {
                                xCell.SetCellValue(Convert.ToDateTime(o));
                            }
                            else if (o is decimal)
                            {
                                xCell.SetCellValue(Convert.ToDouble(o));
                            }
                            else if (o is Int32)
                            {
                                xCell.SetCellValue(Convert.ToInt32(o));
                            }
                            else if (o is Int64)
                            {
                                xCell.SetCellValue(Convert.ToInt64(o));
                            }
                            else
                            {
                                xCell.SetCellValue(o.ToString());
                            }
                        }
                        cellIndex++;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                rowIndex++;
            }
        }
    }

    public class CellProperties
    {
        public ICellStyle CellStyle { get; set; }
        public CellType CellType { get; set; }

        public CellProperties(ICellStyle CellStyle, CellType CellType)
        {
            this.CellStyle = CellStyle;
            this.CellType = CellType;
        }
    }
}
