﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Mail;
using System.IO;
using System.Data;
using System.Configuration;
using Library.Winplus.syslib.core.domain;
using Library.Winplus.syslib.customize.logger.service;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.keys;

namespace Library.Winplus.syslib.core.util
{
    public class SendMailUtil
    {
        private string smsTO;
        private string smsCC;
        private string subject;
        private string message;
        private List<AttachFile> lstStream;

        private SendMailUtil(string smsTO, string smsCC, string subject, string message, List<AttachFile> lstStream)
        {
            this.smsTO = smsTO;
            this.smsCC = smsCC;
            this.subject = subject;
            this.message = message;
            this.lstStream = lstStream;
        }

        public static void Send(string smsTO, string smsCC, string subject, string message, List<AttachFile> lstStream)
        {
            if (smsTO != null && smsTO.Length > 0)
            {
                SendMailUtil sender = new SendMailUtil(smsTO, smsCC, subject, message, lstStream);
                Thread oThread = new Thread(new ThreadStart(sender.Sendsms));
                oThread.Start();
            }
        }

        private void Sendsms()
        {
            string status = "";
            for (int i = 0; i < 5; i++)
            {
                status = TryToSend(i + 1);
                if (status.Equals("Success"))
                {
                   
                    break;
                }
                else
                {
                    Thread.Sleep(1000 * 30);
                }
            }
            
            LoggerService.LogInfo("SendMailUtil", "Sendsms", string.Format("SendMail {0} To:{1} ,Cc:{2} [{3}]", subject, smsTO, smsCC, status));
        }

        public string TryToSend(int round)
        {
            try
            {
                String smtpServer = SystemConfigService.GetStringConfig(SystemConfigKey.SmtpServer);
                String smtpSender = SystemConfigService.GetStringConfig(SystemConfigKey.SmtpSender);
                String smtpSenderDisplayName = SystemConfigService.GetStringConfig(SystemConfigKey.SmtpSenderDisplayName);
                int? smtpPort = SystemConfigService.GetIntConfig(SystemConfigKey.SmtpPort,587);
                String smtpPassword = SystemConfigService.GetStringConfig(SystemConfigKey.SmtpPassword);

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtpServer);
                mail.From = new MailAddress(smtpSender, smtpSenderDisplayName);
                
                foreach (string _smsTo in smsTO.Replace(",", ";").Replace(":", ";").Split(';'))
                {
                    if (!string.IsNullOrEmpty(_smsTo) && _smsTo.Trim().Length > 2)
                    {
                        mail.To.Add(new MailAddress(_smsTo.Trim(), _smsTo.Trim()));
                    }
                }

                if (smsCC != null)
                {
                    foreach (string _smsCc in smsCC.Replace(",", ";").Replace(":", ";").Split(';'))
                    {
                        if (!string.IsNullOrEmpty(_smsCc) && _smsCc.Trim().Length > 2)
                        {
                            mail.CC.Add(new MailAddress(_smsCc.Trim(), _smsCc.Trim()));
                        }
                    }
                }

                if (lstStream != null && lstStream.Count>0)
                {
                    foreach (AttachFile ms in lstStream)
                    {
                        Attachment attach = new Attachment(ms.Stream, ms.FileName);
                        mail.Attachments.Add(attach);
                    }
                }


                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;

                SmtpServer.Port = smtpPort.Value;
                if (!string.IsNullOrEmpty(smtpPassword))
                {
                    SmtpServer.Credentials = new System.Net.NetworkCredential(smtpSender, smtpPassword);
                    SmtpServer.EnableSsl = true;
                }

                SmtpServer.Send(mail);


                return "Success";
            }
            catch (Exception ex)
            {
                LoggerService.LogError("SendMailUtil", "TryToSend", ex.ToString());
                return "Fail";
            }
        }
    }
}