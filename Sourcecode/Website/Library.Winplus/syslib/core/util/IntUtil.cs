﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util
{
    public class IntUtil
    {
        public static bool IsEqual(int? a, int? b)
        {
            if (!a.HasValue && !a.HasValue) return true;

            if (a.HasValue && !b.HasValue
                || !a.HasValue && b.HasValue
                || a.Value != b.Value
                )
            {
                return false;
            }

            return true;
        }

        public static string StringValue(int? p)
        {
            return p.HasValue ? p.Value.ToString() : "";
        }
    }
}
