﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Web;

namespace Library.Winplus.syslib.core.util
{
    public class EncriptionUtil
    {
        public static readonly string InternalKey = "iRm#1";

        public static string EncrypOneWay(string originalPassword)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;

            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return Encrypt(Convert.ToBase64String(encodedBytes), InternalKey);
        }

        public static string Encrypt(string ToEncrypt,string key)
        {
            return doEncrypt(ToEncrypt, key);
        }

        public static string Decrypt(string cypherString, string key)
        {
            return doDecrypt(cypherString, key);
        }

        public static string SimpleEncrypt(string ToEncrypt, string tempKey)
        {
            return doEncrypt(ToEncrypt, tempKey);
        }

        public static string SimpleEncryptBase64(string ToEncrypt, string tempKey)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(doEncrypt(ToEncrypt, tempKey)));
        }

        public static string SimpleDecryptBase64(string cypherString, string tempKey)
        {
            return doDecrypt(Encoding.UTF8.GetString(Convert.FromBase64String(cypherString)), tempKey);
        }

        public static string SimpleDecrypt(string cypherString, string tempKey)
        {
            return doDecrypt(cypherString, tempKey);
        }


        private static string doEncrypt(string PlainText, string key)
        {
            TripleDES des = CreateDES(key);
            ICryptoTransform ct = des.CreateEncryptor();
            byte[] input = Encoding.Unicode.GetBytes(PlainText);
            return Convert.ToBase64String(ct.TransformFinalBlock(input, 0, input.Length));
        }

        private static string doDecrypt(string cypherString, string key)
        {
            byte[] b = Convert.FromBase64String(cypherString);
            TripleDES des = CreateDES(key);
            ICryptoTransform ct = des.CreateDecryptor();
            byte[] output = ct.TransformFinalBlock(b, 0, b.Length);
            return Encoding.Unicode.GetString(output);
        }

        static TripleDES CreateDES(string key)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            TripleDES des = new TripleDESCryptoServiceProvider();
            des.Key = md5.ComputeHash(Encoding.Unicode.GetBytes(key));
            des.IV = new byte[des.BlockSize / 8];
            return des;
        }
    }
}