﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.domain;

namespace Library.Winplus.syslib.core.util
{
    public class GenCodeUtil
    {

        public static string GenCodeTemp(GenCodeKey genKey)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                conn.beginTrans();

                string tempCode = GenCode(conn, genKey);

                conn.rollbackTrans();

                return tempCode;
            }
        }

        public static string GenCode(DBConnection conn, GenCodeKey genKey)
        {
            int GenCodeRunning = 0;

            List<DBParameter> lstParam = new List<DBParameter>();
            lstParam.Add(new DBParameter("GenCodeName", genKey.GenKey));
            lstParam.Add(new DBParameter("GenKey", genKey.GenKey));
            lstParam.Add(new DBParameter("Prefix", genKey.Prefix));
            lstParam.Add(new DBParameter("Length", genKey.Length));

            string sql = "update GenCode Set GenCodeRunning=GenCodeRunning+1 where GenKey=@GenKey and Prefix=@Prefix";
          

            int found = conn.executeUpdate(sql, lstParam.ToArray());
            if (found == 0)
            {
                sql = " INSERT INTO GenCode(GenCodeName,GenKey,Prefix,GenCodeRunning,Length,VersionNumber,OrganizationId,ActiveStatusId,CreatedOn,CreatedBy,YYMMDD) VALUES(@GenCodeName,@GenKey,@Prefix,1,@Length,newid(),1,1,getdate(),1,'')";
                conn.executeUpdate(sql, lstParam.ToArray());
                GenCodeRunning=1;
            }
            else
            {
                sql = "select max(GenCodeRunning) GenCodeRunning from GenCode where GenKey=@GenKey and Prefix=@Prefix";
             
                GenCodeRunning = Convert.ToInt32(conn.executeScalar(sql, lstParam));
            }

            return genKey.Prefix+GenCodeRunning.ToString("D" + genKey.Length);
        }
    }
}
