﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.domain;
using System.Web.Mvc;
using Library.Winplus.syslib.data;
using System.Collections;
using Library.Winplus.syslib.standard.userlogin.model;
using System.Globalization;
using System.Web;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;

namespace Library.Winplus.syslib.core.util
{
    public class QueryUtil
    {
        public static string PreFixBegin = "[";
        public static string PreFixEnd = "]";

        static QueryUtil()
        {
        }
        public static string QueryCount(String sql)
        {
            return "SELECT COUNT(1) AMOUNT FROM(\n" + sql + "\n) RESULT_";
        }

        public static string PagingQuery(string sql,string OrderBy,int Start,int Lpp)
        {
            if (DBConnection.DBType == "Sql")
            {
                // Sql
                return string.Format(@"{0}
                            ORDER BY {1}
                            limit {2},{3}", sql, OrderBy, Start, Lpp);
            }
            else
            {
                if (OrderBy == null)
                {
                    return sql;
                }
                else
                {
                    // SQLSERVER
                    sql = string.Format(@"SELECT *
                                            FROM (
                                            SELECT ROW_NUMBER() OVER(ORDER BY {0}) RowNr, t.* FROM (
                	                            {1}
                                            ) t
                                            ) R
                                            WHERE RowNr between {2} and {3}
                                            ORDER BY " + OrderBy, OrderBy, sql, Start + 1, (Start + Lpp));
                }
            }

            return sql;
        }

        public static IEnumerable GetSelectedItems(DBConnection conn, string sql, string firstValue)
        {
            DataSet ds = conn.executeDataSet(sql.ToString());
            return GetSelectedItems(ds, firstValue);
        }

        public static IEnumerable GetSelectedItems(DataSet ds, string firstValue)
        {
            List<SelectItem> lstItems = new List<SelectItem>();
            if (firstValue != null)
            {
                lstItems.Add(new SelectItem("", firstValue));
            }
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string key_ = row["Id"].ToString();
                string val_ = row["Name"].ToString();

                lstItems.Add(new SelectItem(key_, val_));
            }

            return lstItems;
        }

        public static IEnumerable GetSelectedItemsForMonth(string firstValue)
        {
            CultureInfo culture = new CultureInfo("th-TH");
            List<SelectItem> lstItems = new List<SelectItem>();
            if (firstValue != null)
            {
                lstItems.Add(new SelectItem("", firstValue));
            }

            DateTime firstMonth = new DateTime(DateTime.Now.Date.Year, 1, 1);
            for (int i = 0; i < 12; i++)
            {
                lstItems.Add(new SelectItem((i + 1).ToString(), firstMonth.ToString("MMMM", culture)));

                firstMonth = firstMonth.AddMonths(1);
            }

            return lstItems;
        }

        public static IEnumerable GetSelectedItemsForQuarter(string firstValue)
        {
            List<SelectItem> lstItems = new List<SelectItem>();
            if (firstValue != null)
            {
                lstItems.Add(new SelectItem("", firstValue));
            }

            for (int i = 0; i < 4; i++)
            {
                lstItems.Add(new SelectItem((i + 1).ToString(), "ไตรมาสที่ "+(i+1)));
            }

            return lstItems;
        }


        public static IEnumerable GetSelectedItemsForYear(string firstValue)
        {
            CultureInfo culture = new CultureInfo("th-TH");
            List<SelectItem> lstItems = new List<SelectItem>();
            if (firstValue != null)
            {
                lstItems.Add(new SelectItem("", firstValue));
            }

            DateTime firstMonth = new DateTime(2016, 1, 1);
            do
            {
                lstItems.Add(new SelectItem(firstMonth.Year.ToString(), firstMonth.ToString("yyyy", culture)));

                firstMonth = firstMonth.AddYears(1);
            } while (firstMonth.Year <= DateTime.Now.Year);

            return lstItems;
        }

        public static IEnumerable GetSelectedItems(DBConnection conn, string tableName, Int64? id, string firstValue, string whereCondition, string orderBy)
        {
            if (tableName == "Month")
            {
                return GetSelectedItemsForMonth(firstValue);
            }
            else if (tableName == "Year")
            {
                return GetSelectedItemsForYear(firstValue);
            }
            else if (tableName == "Quarter")
            {
                return GetSelectedItemsForQuarter(firstValue);
            }

            List<SelectItem> lstItems = new List<SelectItem>();
            if (firstValue != null)
            {
                lstItems.Add(new SelectItem("", firstValue));
            }

            string columnId = tableName + "Id";
            string columnValue = tableName + "Name";

            if (string.IsNullOrEmpty(orderBy))
            {
                if (DisplayIndex.Has(tableName))
                {
                    orderBy = "Order by DisplayIndex";
                }
                else
                {
                    orderBy = "Order by " + columnValue;
                }
            }
            else
            {
                orderBy = "Order by " + orderBy;
            }

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select " + columnId + "," + columnValue);
            sql.AppendLine("from " + PreFixBegin + tableName + PreFixEnd);
            sql.AppendLine("where (1=1");
            if (tableName != "ActiveStatus" && ActiveStatusId.Has(tableName))
            {
                sql.AppendLine("and ActiveStatusId=1");
            }
            if (!string.IsNullOrEmpty(whereCondition))
            {
                sql.AppendLine(whereCondition);
            }
            if (tableName == "SystemRole" || tableName == "SystemUser" || tableName == "Project")
            {
                LoginUser loginUser = new LoginUserRepository().FindByTokenId(conn, HttpContext.Current.Session.SessionID);
                if(tableName == "SystemRole")
                {
                    long? SupervisorRoleId = SystemConfigService.GetLongConfig("SupervisorRoleId", 3);
                    if (loginUser != null && loginUser.SystemUserId > 1 && loginUser.User.SystemRoleId == SupervisorRoleId)
                    {
                        long? MobileClientRoleId = SystemConfigService.GetLongConfig("MobileClientRoleId", 4);
                        sql.AppendLine($"and SystemRoleId={MobileClientRoleId}");
                    }
                }else if (tableName == "SystemUser")
                {
                    sql.AppendLine($"and CreatedBy={loginUser.SystemUserId}");
                }
                else if (tableName == "Project")
                {
                    sql.AppendLine($"and ProjectId IN (Select ProjectId from UserProject Where SystemUserId={loginUser.SystemUserId}) or {loginUser.SystemUserId}=1");
                }
            }
            sql.AppendLine(")");
            if (id.HasValue && id.Value != 0)
            {
                sql.AppendLine("or " + columnId + "=" + id.Value);
            }
            sql.AppendLine(orderBy);

            DataSet ds = conn.executeDataSet(sql.ToString());

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string key_ = row[columnId].ToString();
                string val_ = row[columnValue].ToString();

                lstItems.Add(new SelectItem(key_, val_));
            }

            return lstItems;
        }

        // Method parameter example : GetSelectedItems("Customer","-- Please select--","and customerid=1","firstName asc")
        public static IEnumerable GetSelectedItems(string tableName,Int64? id, string firstValue, string whereCondition,string orderBy)
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                return GetSelectedItems(conn, tableName, id, firstValue, whereCondition, orderBy);
            }
        }

        public static T Find<T>(DBConnection conn, string sql, params DBParameter[] param)
        {
            return SetModel<T>(conn.executeDataSet(sql, param));
        }

        public static T SetModel<T>(DataSet ds)
        {
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];
                return SetModel<T>(dt, row);
            }
            return default(T);
        }

        public static T SetModel<T>(DataTable dt, DataRow row)
        {
            T domain = ReflectionUtil.CreateInstance<T>();
            PropertyInfo[] properties = ReflectionUtil.getProperties(domain);
            foreach (PropertyInfo prop in properties)
            {
                if (dt.Columns.Contains(prop.Name) && row[prop.Name] != DBNull.Value)
                {
                    ReflectionUtil.setProperty(domain, prop.Name, row[prop.Name]);
                }
            }
            return domain;
        }

        public static List<T> SetList<T>(DataSet ds)
        {
            List<T> Lst = new List<T>();
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    T domain =  ReflectionUtil.CreateInstance<T>();

                    PropertyInfo[] properties = ReflectionUtil.getProperties(domain);
                    foreach (PropertyInfo prop in properties)
                    {
                        if (dt.Columns.Contains(prop.Name) && row[prop.Name] != DBNull.Value)
                        {
                            ReflectionUtil.setProperty(domain, prop.Name, row[prop.Name]);
                        }
                    }

                    Lst.Add(domain);
                }
            }
            return Lst;
        }

        public static List<T> FindList<T>(DBConnection conn, string sql, params DBParameter[] param)
        {
            return SetList<T>(conn.executeDataSet(sql, param));
        }

        
        public static List<T> FindList<T>(DBConnection conn, string storedName, Dictionary<string, object> lstParam)
        {
            DataSet dsResult = conn.executeStoredProcedure(storedName, lstParam);
            return SetList<T>(dsResult);
        }

        public static byte[] GetByteData(DBConnection conn, string tableName, string pkName, string value, string fieldName)
        {
            string sql = "Select " + PreFixBegin + fieldName + PreFixEnd+" from "+PreFixBegin + tableName + PreFixEnd+" where "+PreFixEnd + pkName + PreFixEnd+"=@PkValue";
            DataSet ds = conn.executeDataSet(sql, new DBParameter("PkValue", value));
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];

                if (row[fieldName] != DBNull.Value)
                {
                    return (byte[])row[fieldName];
                }
            }
            return null;
        }

        public static string GetStringData(DBConnection conn, string tableName, string pkName, string value, string fieldName)
        {
            string sql = "Select " + PreFixBegin + fieldName + PreFixEnd+" From " + PreFixBegin + tableName + PreFixEnd+" where " + PreFixBegin + pkName + PreFixEnd+"=@PkValue";
            DataSet ds = conn.executeDataSet(sql, new DBParameter("PkValue", value));
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];

                if (row[fieldName] != DBNull.Value)
                {
                    return row[fieldName].ToString();
                }
            }
            return null;
        }

        public static Int64? GetInt64Data(DBConnection conn, string tableName, string pkName, string value, string fieldName)
        {
            string sql = "Select " + PreFixBegin + fieldName + PreFixEnd+" From " + PreFixBegin + tableName + PreFixEnd;
            if (!string.IsNullOrEmpty(pkName))
            {
                sql += " Where " + PreFixBegin + pkName + PreFixEnd+"=@PkValue";
            }
            DataSet ds = conn.executeDataSet(sql, new DBParameter("PkValue", value));
            if (DataSetUtil.IsNotEmpty(ds))
            {
                DataTable dt = ds.Tables[0];
                DataRow row = dt.Rows[0];

                if (row[fieldName] != DBNull.Value)
                {
                    return Convert.ToInt64(row[fieldName]);
                }
            }
            return null;
        }

        public static Int64? Create(DBConnection conn,string sqlInsert,Object model)
        {
            List<DBParameter> lstParam = new List<DBParameter>();
            object valSource = null;
            List<string> zz = new List<string>();
            foreach (PropertyInfo prop in ReflectionUtil.getProperties(model))
            {
                Attribute _colAttr = prop.GetCustomAttribute(typeof(Library.Winplus.syslib.core.attribute.DataColumn));
                if (_colAttr != null)
                {
                    zz.Add(prop.Name);
                    Library.Winplus.syslib.core.attribute.DataColumn colAttr = (Library.Winplus.syslib.core.attribute.DataColumn)_colAttr;
                    valSource = ReflectionUtil.getPropertyValue(model, prop.Name);
                    lstParam.Add(new DBParameter(prop.Name, valSource == null ? DBNull.Value : valSource));
                }
            }
            string x = String.Join(",", zz);
            Int64 genkey = 0;
            conn.executeUpdate(sqlInsert, ref genkey, lstParam.ToArray());

            return genkey;
        }

        public static void Update(DBConnection conn,string SqlUpdate, Object model)
        {
            List<DBParameter> lstParam = new List<DBParameter>();
            object valSource = null;
            foreach (PropertyInfo prop in ReflectionUtil.getProperties(model))
            {
                Attribute _colAttr = prop.GetCustomAttribute(typeof(Library.Winplus.syslib.core.attribute.DataColumn));
                if (_colAttr != null)
                {
                    valSource = ReflectionUtil.getPropertyValue(model, prop.Name);
                    if (valSource is System.Web.HttpPostedFileWrapper)
                    {
                        var FileDataUpload = (System.Web.HttpPostedFileWrapper)valSource;
                        byte[] buffer = new byte[FileDataUpload.ContentLength];
                        FileDataUpload.InputStream.Read(buffer, 0, buffer.Length);
                        valSource = buffer;
                    }
                    lstParam.Add(new DBParameter(prop.Name, valSource == null ? DBNull.Value : valSource));
                }
            }
            conn.executeUpdate(SqlUpdate, lstParam.ToArray());
        }

        public static bool IsExists(DBConnection conn,string sql, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(sql, conn.conn);
            if (conn.tran != null)
            {
                cmd.Transaction = conn.tran;
            }

            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return (int)cmd.ExecuteScalar() > 0;
        }


        public static List<SelectListItem> FindSelectListItem(DBConnection conn, string sql, params DBParameter[] param)
        {
            List<SelectListItem> lstItem = new List<SelectListItem>();
            DataSet ds = conn.executeDataSet(sql, param);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                SelectListItem item = new SelectListItem();
                if (row["Text"] != DBNull.Value)
                {
                    item.Text = Convert.ToString(row["Text"]);
                }

                if (row["Value"] != DBNull.Value)
                {
                    item.Value = Convert.ToString(row["Value"]);
                }

                if (row["Selected"] != DBNull.Value)
                {
                    item.Selected = Convert.ToBoolean(row["Selected"]);
                }

                lstItem.Add(item);
            }

            return lstItem;
        }
    }
}
