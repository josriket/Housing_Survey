﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.util
{
    public class ReflectionUtil
    {/// <summary> 

        /// copy ALL the values of public properties, that have the same name, from source to dest. 

        /// ignores properties that have different names. 

        ///  

        /// note - this would also perform shallow copy of a collection property, if is has the same name! 
        /// </summary> 

        /// <param name="source"></param> 

        /// <param name="dest"></param> 

        /// returns: list of properties that were copied 
        public static List<string> CopyProperties(object source, object dest, string[] propertiesToExclude = null)
        {

            List<string> propsCopied = new List<string>();

            PropertyInfo[] properties = getProperties(source);

            if (propertiesToExclude == null)

                propertiesToExclude = new string[] { };

            foreach (var prop in properties)
            {

                if (!propertiesToExclude.Contains(prop.Name) && hasProperty(dest, prop))
                {

                    object val = getPropertyValue(source, prop.Name);

                    //trim strings (especially for Excel source which tends to have lots of whitespace) 

                    if (val != null && val.GetType() == typeof(string))
                    {

                        string str = (string)val;

                        val = str.Trim();

                    }

                    setProperty(dest, prop.Name, val);

                    propsCopied.Add(prop.Name);

                }

            }

            return propsCopied;

        }

        private static bool hasProperty(object dest, PropertyInfo prop)
        {

            var props = getProperties(dest);

            //detect case where property has same name, but different value (usually error in client code): 

            var propWithName = props.Where(p => p.Name == prop.Name);

            if (propWithName.Count() == 1)
            {

                string destPropTypeName = propWithName.First().PropertyType.FullName;

                string sourcePropTypeName = prop.PropertyType.FullName;

                if (destPropTypeName != sourcePropTypeName)

                    throw new ApplicationException("Two properties have same name " + prop.Name + " but different types: dest=" + destPropTypeName + " source:" + sourcePropTypeName);

            }

            return props.Select(p => p.Name).Contains(prop.Name); //need to compare by name (not same objects!) 

        }

        public static object getPropertyValue(object item, string propName)
        {

            return item.GetType().InvokeMember(propName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty, Type.DefaultBinder, item, new object[] { });

        }

        /// <summary> 

        /// note: if this call fails, it usually means the 2 properties have the same name, but different type. 

        /// </summary> 

        /// <param name="item"></param> 

        /// <param name="prop"></param> 

        /// <param name="val"></param> 

        /// <param name="copyMode"></param> 

        public static void setProperty(object item, string propName, object val)
        {
            item.GetType().InvokeMember(propName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty, Type.DefaultBinder, item, new object[] { val });

        }

        /// <summary> 

        /// get all the public properties of the given object. 

        /// </summary> 

        /// <param name="obj"></param> 

        /// <returns></returns> 

        public static PropertyInfo[] getProperties(object obj)
        {
            //Edit By Jo
            //return obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            return obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

        }

        /// <summary> 

        /// compare all the values of the public properties that have same name. 

        /// ignores properties that have different names. 

        /// </summary> 

        /// <param name="existingLicWEB"></param> 

        /// <param name="prsLic"></param> 

        /// <returns></returns> 

        public static bool IsEqual(object one, object two)
        {
            if (one == null && two != null)
            {
                return false;
            }
            else if (one != null && two == null)
            {
                return false;
            }
            else if (one == null && two == null)
            {
                return true;
            }

            PropertyInfo[] props = getProperties(one);
            if (props.Length > 0)
            {
                foreach (var prop in props)
                {
                    if (hasProperty(two, prop))
                    {
                        return one.Equals(two);
                        //object propVal1 = getPropertyValue(one, prop);
                        //object propVal2 = getPropertyValue(two, prop);
                        //if (propVal1 == null && propVal2 != null)
                        //    return false;
                        //if (propVal1 != null && !propVal1.Equals(propVal2))
                        //{
                        //    return false;
                        //}
                    }
                }
            }
            else
            {
                return one.ToString().Equals(two.ToString());
            }
            return true;
        }

        public static object GetPropertyValue(object item, string propertyName)
        {

            var prop = getProperties(item).Where(p => p.Name == propertyName).First();

            return getPropertyValue(item, prop.Name);

        }

        public static void CopyBaseClass(object source, object target)
        {
            // get property list of the target object.
            // this is a reflection extension which simply gets properties (CanWrite = true).
            var tprops = getProperties(target);

            tprops.ToList().ForEach(prop =>
            {
                // check whether source object has the the property
                PropertyInfo sp = source.GetType().GetProperty(prop.Name);
                if (sp != null)
                {
                    // if yes, copy the value to the matching property
                    var value = sp.GetValue(source, null);
                    target.GetType().GetProperty(prop.Name).SetValue(target, value);
                }
            });
        }


        public static T CreateInstance<T>()
        {
            return (T)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(typeof(T).FullName);
        }
    }
}
