﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Library.Winplus.syslib.core.util
{
    public class ImportExcelUtil
    {
        public static List<T> ParseExcel<T>(MemoryStream fs)
        {
            IWorkbook wb = wb = new XSSFWorkbook(fs);
            ISheet sheet = wb.GetSheetAt(0); ;

            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            // Get Column Header
            rows.MoveNext();
            List<String> lstColumnName = new List<string>();
            IRow row = (XSSFRow)rows.Current;
            int ColumnAmout = row.LastCellNum;
            for (int j = 0; j < ColumnAmout; j++)
            {
                lstColumnName.Add(row.GetCell(j).ToString());
            }

            // Load Model properties
            T model = default(T);
            model = ReflectionUtil.CreateInstance<T>();
            PropertyInfo[] properties = ReflectionUtil.getProperties(model);
            Dictionary<String, PropertyInfo> modelProperties = new Dictionary<string, PropertyInfo>();
            foreach (PropertyInfo prop in properties)
            {
                modelProperties.Add(prop.Name, prop);
            }

            List<T> lstImport = new List<T>();

            ICell cell = null;
            object value = null;
            PropertyInfo colProp = null;
            Type type = null;
            while (rows.MoveNext())
            {
                model = ReflectionUtil.CreateInstance<T>();

                PropertyInfo[] prop = ReflectionUtil.getProperties(model);
                Dictionary<string, PropertyInfo> dicPropertyInfo = new Dictionary<string, PropertyInfo>();
                foreach (PropertyInfo p in prop)
                {
                    dicPropertyInfo.Add(p.Name, p);
                }

                row = (XSSFRow)rows.Current;
                int i = 0;
                foreach (String columnName in lstColumnName)
                {
                    cell = row.GetCell(i++);
                    value = null;
                    if (cell != null && modelProperties.ContainsKey(columnName))
                    {
                        colProp = modelProperties[columnName];
                        type = colProp.PropertyType;
                        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            type = type.GetGenericArguments()[0];
                        }

                        if (cell.CellType == CellType.NUMERIC && HSSFDateUtil.IsCellDateFormatted(cell))
                        {
                            // Date Type -> cell.DateCellValue;
                            value = cell.DateCellValue;
                        }
                        else if (cell.CellType == CellType.STRING)
                        {
                            if (type == typeof(Guid))
                            {
                                value = Guid.Parse(cell.ToString().Trim());
                            }
                            else
                            {
                                value = cell.ToString().Trim();
                            }
                        }
                        else if (cell.CellType == CellType.NUMERIC)
                        {

                            if (type == typeof(Int32))
                            {
                                value = Convert.ToInt32(cell.NumericCellValue);
                            }
                            else if (type == typeof(Int64))
                            {
                                value = Convert.ToInt64(cell.NumericCellValue);
                            }
                            else if (type == typeof(Decimal))
                            {
                                value = Convert.ToDecimal(cell.NumericCellValue);
                            }
                            else if (type == typeof(string))
                            {
                                value = cell.ToString().Trim();
                            }
                        }
                        else if (cell.CellType == CellType.BOOLEAN)
                        {
                            value = cell.BooleanCellValue;
                        }
                        else if (cell.CellType == CellType.BLANK)
                        {
                            if (type == typeof(Int32))
                            {
                                value = null;
                            }
                            else if (type == typeof(Int64))
                            {
                                value = null;
                            }
                            else if (type == typeof(Decimal))
                            {
                                value = null;
                            }
                            else if (type == typeof(string))
                            {
                                value = null;
                            }
                        }
                        else
                        {
                            throw new NotImplementedException("Excel parsing not support for CellType:" + cell.CellType.ToString());
                        }

                        if (value != null)
                        {
                            if (dicPropertyInfo[columnName].PropertyType == typeof(bool))
                            {
                                value = (value.ToString().ToUpper() == "TRUE");
                            }

                            ReflectionUtil.setProperty(model, columnName, value);
                        }
                    }

                }
                lstImport.Add(model);
            }

            return lstImport;
        }

        public static DataTable ParseExcel(MemoryStream fs)
        {
            DataTable dt = new DataTable();

            IWorkbook wb = wb = new XSSFWorkbook(fs);
            ISheet sheet = wb.GetSheetAt(0); ;

            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            // Get Column Header
            rows.MoveNext();
            List<String> lstColumnName = new List<string>();
            IRow row = (XSSFRow)rows.Current;
            int ColumnAmout = row.LastCellNum;
            for (int j = 0; j < ColumnAmout; j++)
            {
                dt.Columns.Add(row.GetCell(j).ToString());
                lstColumnName.Add(row.GetCell(j).ToString());
            }

            ICell cell = null;
            object value = null;
            while (rows.MoveNext())
            {
                row = (XSSFRow)rows.Current;

                DataRow dsRow = dt.NewRow();

                int i = 0;
                foreach (String columnName in lstColumnName)
                {
                    cell = row.GetCell(i);
                    value = null;
                    if (cell != null)
                    {
                        if (cell.CellType == CellType.NUMERIC && HSSFDateUtil.IsCellDateFormatted(cell))
                        {
                            // Date Type -> cell.DateCellValue;
                            value = cell.DateCellValue;
                        }
                        else if (cell.CellType == CellType.STRING)
                        {
                            value = cell.ToString().Trim();
                        }
                        else if (cell.CellType == CellType.NUMERIC)
                        {
                            value = cell.NumericCellValue;
                        }
                        else if (cell.CellType == CellType.BOOLEAN)
                        {
                            value = cell.BooleanCellValue;
                        }
                        else
                        {
                            throw new NotImplementedException("Excel parsing not support for CellType:"+cell.CellType.ToString());
                        }

                        if (value != null)
                        {
                            dsRow[i] = value;
                        }
                        i++;
                    }

                }
                dt.Rows.Add(dsRow);
            }

            return dt;
        }


        public static void ExampleParseExcelToDataTable()
        {
            string filename = "D:\\Agency.xlsx";

            DataTable dtResult = null;
            using (System.IO.MemoryStream fs = new System.IO.MemoryStream(File.ReadAllBytes(filename)))
            {
                dtResult = ImportExcelUtil.ParseExcel(fs);
            }

            if (dtResult != null)
            {
                foreach (DataRow row in dtResult.Rows)
                {
                    Console.WriteLine(string.Format("AgencyId:{0} ,AgencyName:{1}", row["AgencyId"], row["AgencyName"]));
                }
            }
        }
    }
}
