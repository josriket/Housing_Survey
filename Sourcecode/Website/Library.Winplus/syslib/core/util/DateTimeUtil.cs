﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.domain;

namespace Library.Winplus.syslib.core.util
{
    public class DateTimeUtil
    {
        public static Object strUiToDate(string data)
        {
            try
            {
                if (StringUtil.notEmpty(data))
                {
                    return DateTime.ParseExact(data, BeanParameter.GetInstance().OutPutDateFormat, BeanParameter.GetInstance().DefaultCultureInfo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DBNull.Value;
        }


        public static string ShowThaiDate(DateTime dateTime)
        {
            return dateTime.ToString("d MMMM yyyy", BeanParameter.GetInstance().ThaiCultureInfo);
        }

        public static bool IsEqual(DateTime? a, DateTime? b)
        {
            if (!a.HasValue && !b.HasValue) return true;

            if (a.HasValue && !b.HasValue
                || b.HasValue && !a.HasValue
                || a.Value != b.Value
                )
            {
                return false;
            }
            return true;
        }

        public static string StringValue(DateTime? a)
        {
            if (a.HasValue)
            {
                return StringUtil.FormatDateTime(a.Value);
            }

            return string.Empty;
        }

        //123, 456, 789,10 11 12
        public static int QuaterFromMonth(int month)
        {
            if (month >= 1 && month <= 3)
            {
                return 1;
            }
            else if (month >= 4 && month <= 6)
            {
                return 2;
            }
            else if (month >= 7 && month <= 9)
            {
                return 3;
            }
            else
            {
                return 4;
            }
        }

        //123, 456, 789,10 11 12
        public static int MonthFromQuarter(int Quarter)
        {
            if (Quarter == 1)
            {
                return 1;
            }
            else if (Quarter == 2)
            {
                return 4;
            }
            else if (Quarter == 3)
            {
                return 7;
            }
            else
            {
                return 10;
            }
        }

        public static DateTime? CurrentDate()
        {
            return DateTime.Now;
        }

        public static DateTime? SetFileUploadDate(DateTime? fileUploadDate, string ids)
        {
            if (!fileUploadDate.HasValue
               && !string.IsNullOrEmpty(ids)
               )
            {
                return DateTime.Now;
            }
            return null;
        }
    }
}
