﻿using System;
using System.Threading;
using Library.Winplus.syslib.customize.logger.service;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.core.connection;

namespace Library.Winplus.syslib.core.util
{
    public class SendSMSUtil
    {
        private string phoneNos;
        private string message;

        private SendSMSUtil(string phoneNos, string message)
        {
            this.phoneNos = phoneNos;
            this.message = message;
        }

        public static void Send(string phoneNos, string message)
        {
            if (phoneNos != null && phoneNos.Length > 0)
            {
                SendSMSUtil sender = new SendSMSUtil(phoneNos, message);
                Thread oThread = new Thread(new ThreadStart(sender.SendSMS));
                oThread.Start();
            }
        }

        private void SendSMS()
        {
            string status = "";
            for (int i = 0; i < 5; i++)
            {
                status = TryToSend(i + 1);
                if (status.Equals("Success"))
                {
                    break;
                }
                else
                {
                    Thread.Sleep(1000 * 30);
                }
            }

            LoggerService.LogInfo("SendSMSUtil", "SendSMS", string.Format("SendSMS {0} To:{1} [{2}]", message, phoneNos, status));
        }

        public string TryToSend(int round)
        {
            try
            {
                return "Success";
            }
            catch (Exception ex)
            {
                LoggerService.LogError("SendSMSUtil", "TryToSend", ex.ToString());
                return "Fail";
            }
        }
    }
}