﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Winplus.syslib.core.exception
{
    public class InvalidDataException : Exception
    {
        public string ErrorMessage { get; set; }
        public string SelectTab { get; set; }
        public string SelectElement { get; set; }

        public InvalidDataException(string ErrorMessage)
        {
            this.ErrorMessage = ErrorMessage;
        }

        public InvalidDataException(string SelectTab,string SelectElement, string ErrorMessage)
        {
            this.SelectTab = SelectTab;
            this.SelectElement = SelectElement;
            this.ErrorMessage = ErrorMessage;
        }

        public override string ToString(){
            return ErrorMessage;
        }
    }
}
