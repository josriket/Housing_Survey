﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.systemconfig.service;

namespace Library.Winplus.syslib.core.simple
{
    [Serializable]
    public abstract class SimpleSearch<T>
    {
        public LoginUser loginUser { get; set; }
        public SimpleSearch() { }
        public T search { get; set; }
        public Int32 TotalRecord { get; set; }
	    public Int32 Draw { get; set; }
	    public Int32 Start { get; set; }
	    public Int32 Length { get; set; }
	    public String Search { get; set; }
	    public Int32 OrderColumn { get; set; }
	    public String OrderDir { get; set; }
        public Dictionary<Int32, String> ColumnSearchIndex = new Dictionary<Int32, String>();
        public Dictionary<String, String> SearchKey = new Dictionary<String, String>();
        public Dictionary<Int32, String> ColumnOrderByIndex = new Dictionary<Int32, String>();
        public List<DBParameter> param = new List<DBParameter>();
        public String DefaultSearch = "";
        public String Ids { get; set; }
	
	    // SQL
	    public String sqlQuery { get; set; }
	    public String sqlOrderBy { get; set; }

        public void ParseRequest(HttpRequestBase request)
        {
            if (request != null)
            {
                foreach (string key in request.QueryString.AllKeys)
                {
                    if (!string.IsNullOrEmpty(key))
                    {
                        if (!string.IsNullOrEmpty(request.QueryString[key]))
                        {
                            if (key.Equals("ids"))
                            {
                                // Checking all ids are integer for protection sql injection
                                Ids = request.QueryString[key];
                                String[] ids = Ids.Split(',');
                                long _tmpResult = 0;
                                foreach (String id in ids)
                                {
                                    if (!Int64.TryParse(id, out _tmpResult))
                                    {
                                        Ids = string.Empty;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                SearchKey.Add(key, request.QueryString[key]);
                            }
                        }
                    }
                }

                // BootStrap Ajax parameter (Post)
                string json = new StreamReader(request.InputStream).ReadToEnd();
                string[] test = HttpUtility.UrlDecode(json).Split('&');
                string[] tmp = json.Split('&');
                foreach (string x in tmp)
                {
                    string[] y = x.Split('=');
                    string key = HttpUtility.UrlDecode(y[0]);

                    if (key.Equals("order[0][column]"))
                    {
                        string val = HttpUtility.UrlDecode(y[1]);
                        OrderColumn = Convert.ToInt32(val);
                    }
                    else if (key.Equals("order[0][dir]"))
                    {
                        string val = HttpUtility.UrlDecode(y[1]);
                        OrderDir = val;
                    }
                    else if (key.Equals("start"))
                    {
                        string val = HttpUtility.UrlDecode(y[1]);
                        Start = Convert.ToInt32(val);
                    }
                    else if (key.Equals("length"))
                    {
                        string val = HttpUtility.UrlDecode(y[1]);
                        Length = Convert.ToInt32(val);
                    }
                    //else if (key.Equals("search[value]"))
                    //{
                    //    string val = HttpUtility.UrlDecode(y[1]);
                    //    if (!string.IsNullOrEmpty(val) && !SearchKey.ContainsKey("SearchText"))
                    //    {
                    //        SearchKey.Add("SearchText", val);
                    //    }
                    //}
                    else if (key.StartsWith("columns[") && key.EndsWith("][name]"))
                    {
                        string val = HttpUtility.UrlDecode(y[1]);
                        ColumnOrderByIndex.Add(Convert.ToInt32(key.Replace("columns[", "").Replace("][name]", "")), val);
                    }
                }
            }
        }

        public abstract void PrepareSql(HttpRequestBase request, LoginUser loginUser);

        public IEnumerable<System.Web.Mvc.SelectListItem> GetSelectedItems(string tableName,Int64? id, string firstValue = null, string whereCondition = "", string orderBy = null)
        {
            return new SelectList(QueryUtil.GetSelectedItems(tableName, id, firstValue, whereCondition, orderBy), "Value", "Text");
        }

        public Boolean HasAuthorize(LoginUser loginUser, String systemName, String method)
        {
            return LoginUser.HasAuthorize(loginUser, systemName, method);
        }
    }
}
