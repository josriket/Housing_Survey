﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.system.common.model;
using Library.Winplus.syslib.customize.systemconfig.service;

namespace Library.Winplus.syslib.core.simple
{
    public abstract class SimpleRepository<T>
    {
        public string SqlInsert;
        public string SqlUpdate;
        public string SqlDelete;
        public string SqlActiveStatus;
        public string SqlFindAll;

        public virtual Int64? Create(DBConnection conn,T model)
        {
            return QueryUtil.Create(conn,SqlInsert, model);
        }

        public virtual void Update(DBConnection conn, T model)
        {
            QueryUtil.Update(conn, SqlUpdate, model);
        }

        public Int64 Delete(DBConnection conn, params string[] Ids)
        {
            int updated = 0;
            foreach(String _id in Ids)
            {
                updated += conn.executeUpdate(SqlDelete
                    , new DBParameter("Id", Convert.ToInt64(_id))
                    );
            }
            return updated;
        }

        public Int64 Active(DBConnection conn, long activeStatusId, string[] Ids)
        {
            int updated = 0;
            foreach (String _id in Ids)
            {
                updated += conn.executeUpdate(SqlActiveStatus
                    , new DBParameter("ActiveStatusId", activeStatusId)
                    , new DBParameter("Id", Convert.ToInt64(_id))
                    );
            }
            return updated;
        }

        public T FindById(DBConnection conn, Int64 Id)
        {
            return QueryUtil.Find<T>(conn, SqlFindAll + " And sm.[" + typeof(T).Name + "Id]=@Id", new DBParameter("Id", Id));
        }

        public List<T> FindAll(DBConnection conn, string appendQuery=null)
        {
            return QueryUtil.FindList<T>(conn, SqlFindAll + (appendQuery==null?"": " "+appendQuery.ToString()));
        }


        public SearchResultModel AdvanceSearch(DBConnection conn, SimpleSearch<T> search)
        {
            int recordsTotal = Convert.ToInt32(conn.executeScalar(QueryUtil.QueryCount(search.sqlQuery), search.param));

            String sql = QueryUtil.PagingQuery(search.sqlQuery, search.sqlOrderBy, search.Start, search.Length);
            DataSet ds = conn.executeDataSet(sql, search.param.ToArray());

            if (DBConnection.DBType == "Sql")
            {
                ds.Tables[0].Columns.Add("RowNr", typeof(int));
                int i = search.Start;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row["RowNr"] = ++i;
                }
            }

            SearchResultModel result = new SearchResultModel();
            result.data = ds.Tables[0];
            result.options = new string[0];
            result.files = new string[0];
            result.draw = 0;
            result.recordsTotal = recordsTotal.ToString();
            result.recordsFiltered = result.recordsTotal;

            return result;
        }

        public DataSet ExportExcel(DBConnection conn, SimpleSearch<T> search)
        {
            int MaxExcelExport = SystemConfigService.GetIntConfig("MaxExcelExport",10000).Value;
            String sql = QueryUtil.PagingQuery(search.sqlQuery, search.sqlOrderBy, 0, MaxExcelExport);
            return conn.executeDataSet(sql, search.param.ToArray());
        }

        public DataSet ExportCSV(DBConnection conn, SimpleSearch<T> search)
        {
            int MaxExcelExport = SystemConfigService.GetIntConfig("MaxCSVExport", 200000).Value;
            String sql = QueryUtil.PagingQuery(search.sqlQuery, search.sqlOrderBy, 0, MaxExcelExport);
            return conn.executeDataSet(sql, search.param.ToArray());
        }

        public abstract void ImportExcel(DBConnection conn, LoginUser loginUser, List<T> lstImport);
    }
}
