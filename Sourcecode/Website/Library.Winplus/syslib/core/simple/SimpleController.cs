﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.core.logger;
using Library.Winplus.syslib.core.simple;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.logger.service;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.customize.systemconfig.keys;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.data.Response;
using Library.Winplus.syslib.standard.logger.model;
using Library.Winplus.syslib.standard.logger.repository;
using MyResources.Winplus;

namespace Library.Winplus.syslib.core.simple
{
    public abstract class SimpleController<T,S> : BaseController
    {
        #region Standard Action Method (Search,Add,Edit,Delete,Export,Import)

        [NonAction]
        public abstract ActionResult _Index(DBConnection conn, LoginUser loginUser,S model);
        public ActionResult Index()
        {
            try
            {
                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    LoginUser loginUser = GetLogin();
                    if (loginUser != null && loginUser.SystemUserId.HasValue && HasAuthorize(typeof(T).Name, loginUser, "Search"))
                    {
                        S model = ReflectionUtil.CreateInstance<S>();
                        //MethodInfo methodInfo = model.GetType().GetMethod("PrepareSql");
                        //methodInfo.Invoke(model, new object[] { Request, loginUser });

                        return _Index(conn, loginUser, model);
                    }
                    else
                    {
                        return View(NotAuthorizeView);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerService.LogError("SimpleController", "Index", ex.ToString());
                return View(InternalErrorView);
            }
        }
        
        [NonAction]
        public abstract ActionResult _Search(DBConnection conn, LoginUser loginUser, S model);
        public ActionResult Search()
        {
            try
            {

                LoginUser loginUser = GetLogin();
                if (loginUser != null && (loginUser.UserLoginId.HasValue && (HasAuthorize(typeof(T).Name, loginUser, "Search")
                    || typeof(T).Name == "ChooseDialog"
                    || typeof(T).Name == "IncidentReportJCIModule"
                    || typeof(T).Name == "IncidentReportCloseApprove"
                    || typeof(T).Name == "IncidentReportCloseApproveTemplate"
                    || typeof(T).Name == "DocumentUpload"
                    || typeof(T).Name == "IncidentReportAuditLog"
                    || typeof(T).Name == "SystemUserIRAuthorize"
                    || typeof(T).Name == "IncidentReportAuditLogDetail"
                    )))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {

                        S model = ReflectionUtil.CreateInstance<S>();
                        //MethodInfo methodInfo = model.GetType().GetMethod("PrepareSql");
                        //methodInfo.Invoke(model, new object[] { Request, loginUser });

                        return _Search(conn, loginUser, model);

                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Search", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public abstract ActionResult _Find(DBConnection conn, LoginUser loginUser, Int64? id);
        public ActionResult Find(Int64? id)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue && HasAuthorize(typeof(T).Name, loginUser, "View"))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        return _Find(conn, loginUser, id);
                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Find", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public abstract ActionResult _Copy(DBConnection conn, LoginUser loginUser, Int64? id);
        public ActionResult Copy(Int64? id)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue && HasAuthorize(typeof(T).Name, loginUser, "View"))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        return _Copy(conn, loginUser, id);
                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Copy", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public abstract ActionResult _Save(DBConnection conn, LoginUser loginUser, T model);

        [HttpPost, ValidateInput(false)]
        public ActionResult Save(T model)
        {
            try
            {
                Int64? Id = (Int64?)ReflectionUtil.getPropertyValue(model, typeof(T).Name + "Id");

                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue && ((!Id.HasValue && HasAuthorize(typeof(T).Name, loginUser, "Add"))
                || Id.HasValue && HasAuthorize(typeof(T).Name, loginUser, "Edit")))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        try
                        {
                            return _Save(conn, loginUser, model);

                        }
                        catch (InvalidDataException ex)
                        {
                            if (!string.IsNullOrEmpty(ex.SelectTab) || !string.IsNullOrEmpty(ex.SelectElement))
                            {
                                return ResponseJsonElementError(ex.SelectTab,ex.SelectElement, ex.ToString());
                            }
                            else
                            {
                                return ResponseJsonError(ex.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            return ResponseJsonError(ex.ToString());
                        }
                    }
                }
                else
                {
                    return ResponseJsonNotAuthorize();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Save", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public abstract ActionResult _Delete(DBConnection conn, LoginUser loginUser, string ids);
        public ActionResult Delete(string ids)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue &&
                    (
                       HasAuthorize(typeof(T).Name, loginUser, "Delete")
                    || typeof(T).Name == "DocumentUpload"
                    || typeof(T).Name == "IncidentReportCloseApproveTemplate"
                    )
                    )
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        try
                        {
                            return _Delete(conn, loginUser, ids);
                        }
                        catch (Exception ex)
                        {
                            return ResponseJsonError(ex.ToString());
                        }
                    }
                }
                else
                {
                    return ResponseJsonNotAuthorize();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Delete", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public abstract ActionResult _ExportExcel(DBConnection conn, LoginUser loginUser, S model);
        public ActionResult ExportExcel()
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue && HasAuthorize(typeof(T).Name, loginUser, "Export") || typeof(T).Name == "RptIncidentReport")
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        S model = ReflectionUtil.CreateInstance<S>();
                        MethodInfo methodInfo = model.GetType().GetMethod("PrepareSql");
                        methodInfo.Invoke(model, new object[] { Request, loginUser });

                        return _ExportExcel(conn, loginUser, model);
                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "ExportExcel", ex.ToString());
                return View(InternalErrorView);
            }
        }

        [NonAction]
        public abstract ActionResult _Import(DBConnection conn, LoginUser loginUser, List<T> lstImport);
        public ActionResult Import(HttpPostedFileBase FileDataUpload)
        {
            LoginUser loginUser = GetLogin();
            try
            {
                if (loginUser != null && loginUser.UserLoginId.HasValue && HasAuthorize(typeof(T).Name, loginUser, "Import"))
                {

                }
                else
                {
                    return View(NotAuthorizeView);
                }

                if (FileDataUpload != null && FileDataUpload.ContentLength > 0)
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {

                        byte[] buffer = new byte[FileDataUpload.ContentLength];
                        FileDataUpload.InputStream.Read(buffer, 0, buffer.Length);

                        List<T> lstImport = null;
                        using (System.IO.MemoryStream fs = new System.IO.MemoryStream(buffer))
                        {
                            lstImport = ImportExcelUtil.ParseExcel<T>(fs);
                        }
                        return _Import(conn, loginUser, lstImport);
                    }
                }
                else
                {
                    return ResponseJsonError( @MyResources.Winplus.Resource.PleaseChooseFileImport);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();

                return ResponseJsonError( ex.ToString());
            }
        }


        [NonAction]
        public abstract ActionResult _Active(DBConnection conn, LoginUser loginUser,Int64 activeStatusId, string ids);
        public ActionResult Active(Int64 id,string ids)
        {
            try
            {
                LoginUser loginUser = GetLogin();
                if (loginUser != null && loginUser.UserLoginId.HasValue && HasAuthorize(typeof(T).Name, loginUser, "Edit"))
                {
                    using (DBConnection conn = DBConnection.DefaultConnection)
                    {
                        try
                        {
                            return _Active(conn, loginUser, id, ids);
                        }
                        catch (Exception ex)
                        {
                            return ResponseJsonError(ex.ToString());
                        }
                    }
                }
                else
                {
                    return View(NotAuthorizeView);
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.ToString();
                LoggerService.LogError("SimpleController", "Delete", ex.ToString());
                return View(InternalErrorView);
            }
        }

        #endregion

       
    }
}
