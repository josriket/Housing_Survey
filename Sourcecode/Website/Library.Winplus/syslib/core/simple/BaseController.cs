﻿using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.logger;
using Library.Winplus.syslib.customize.loginuser.model;
using Library.Winplus.syslib.customize.loginuser.repository;
using Library.Winplus.syslib.customize.systemconfig.service;
using Library.Winplus.syslib.data.Response;
using Library.Winplus.syslib.standard.logger.model;
using Library.Winplus.syslib.standard.logger.repository;
using MyResources.Winplus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Library.Winplus.syslib.core.simple
{
    public class BaseController:Controller
    {
        #region Shared Variable

        protected readonly String NotAuthorizeView = "~/Views/_shared/NotAuthorize.cshtml";
        protected readonly String InternalErrorView = "~/Views/_shared/InternalError.cshtml";

        protected readonly global::System.Resources.ResourceManager rm = new global::System.Resources.ResourceManager("MyResources.Winplus.Resource", typeof(Resource).Assembly);
        //protected readonly ResourceManager rm = new ResourceManager("MyResources.Winplus.Resource", Assembly.GetExecutingAssembly());

        #endregion

        #region Authorize helper method

        [NonAction]
        public bool HasAuthorize(string systemName, LoginUser loginUser, string method)
        {
            return LoginUser.HasAuthorize(loginUser, systemName, method);
        }

        [NonAction]
        protected LoginUser GetLogin()
        {
            using (DBConnection conn = DBConnection.DefaultConnection)
            {
                return new LoginUserRepository().FindByTokenId(conn, Session.SessionID);
            }
        }

        #endregion

        #region Json Response Result

        [NonAction]
        protected JsonResult ResponseJsonElementError(string SelectTab, string SelectElement, string Message = null)
        {
            JsonStatusResponse response = new JsonStatusResponse();
            response.StatusCode = "-1";
            response.Message = Message;
            response.SelectTab = SelectTab;
            response.SelectElement = SelectElement;

            return ResponseJson(response, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        protected JsonResult ResponseJsonError(string Message = null, object jsonData=null)
        {
            JsonStatusResponse response = new JsonStatusResponse();
            response.StatusCode = "-1";
            response.Message = Message;
            response.JsonData = jsonData;

            return ResponseJson(response, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        protected JsonResult ResponseJsonNotAuthorize(string Message = null, object jsonData = null)
        {
            JsonStatusResponse response = new JsonStatusResponse();
            response.StatusCode = "-1";
            string ErrorNotAuthorize = SystemConfigService.GetStringConfig("ErrorNotAuthorize");
            if (ErrorNotAuthorize==null)
            {
                ErrorNotAuthorize = MyResources.Winplus.Resource.ErrorNotAuthorize;
            }
            response.Message = ErrorNotAuthorize;
            response.JsonData = jsonData;

            return ResponseJson(response, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        protected JsonResult ResponseJsonSuccess(string Message = null,object jsonData=null)
        {
            JsonStatusResponse response = new JsonStatusResponse();
            response.StatusCode = "0";
            response.Message = Message;
            response.JsonData = jsonData;

            return ResponseJson(response, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private JsonResult ResponseJson(object model, JsonRequestBehavior AllowGet)
        {
            JsonResult jsonResult = Json(model, AllowGet);

            jsonResult.MaxJsonLength = Int32.MaxValue;

            return jsonResult;
        }


        #endregion

        #region Request and Response filter

        /* Base Action Event */
        protected Logger logger = new Logger();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);
                HttpRequestBase request = filterContext.HttpContext.Request;
                logger.RequestUrl = request.Url.ToString();
                logger.LogClass = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName + "Controller";
                logger.LogMethod = filterContext.ActionDescriptor.ActionName;
                logger.TokenId = Session.SessionID;
                logger.CreatedOn = DateTime.Now;
            }
            catch (Exception ex)
            {
                // Need to write log to text file
                CriticalLogger.WriteLog("BaseController", "SimpleController:OnActionExecuting Error->" + ex.ToString());
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            try
            {
                if (logger.LogClass != "LoggerController")
                {
                    logger.LoggerName = logger.LogClass + " - " + logger.LogMethod;
                    logger.ProcessingTime = Convert.ToDecimal((DateTime.Now - logger.CreatedOn.Value).TotalMilliseconds);
                    if (logger.Response != null && logger.Response.Length > 2000)
                    {
                        logger.Response = logger.Response.Substring(0, 2000) + "...";
                    }
                    //using (DBConnection conn = DBConnection.DefaultConnection)
                    //{
                    //    new LoggerRepository().Create(conn, logger);
                    //}
                }
            }
            catch (Exception ex)
            {
                // Need to write log to text file
                CriticalLogger.WriteLog("BaseController","SimpleController:OnActionExecuted Error->" + ex.ToString());
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            try
            {
                logger.LoggerName = logger.LogClass + " - " + logger.LogMethod;
                logger.ErrorMessage = filterContext.Exception.ToString();
                logger.ProcessingTime = Convert.ToDecimal((DateTime.Now - logger.CreatedOn.Value).TotalMilliseconds);

                using (DBConnection conn = DBConnection.DefaultConnection)
                {
                    new LoggerRepository().Create(conn, logger);
                }
            }
            catch (Exception ex)
            {
                // Need to write log to text file
                CriticalLogger.WriteLog("BaseController","SimpleController:OnException Error->" + ex.ToString());
            }
        }


        #endregion

    }
}
