﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using Library.Winplus.syslib.customize.loginuser.model;
using System.Web.Script.Serialization;

namespace Library.Winplus.syslib.core.simple
{
    [Serializable]
    public class SimpleModel
    {
        [LocalizedDisplayName("IsHiddenName", typeof(MyResources.Winplus.Resource))]
        public bool IsHiddenName { get; set; }

        [ScriptIgnore]
        public LoginUser loginUser { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> GetSelectedItems(DBConnection conn, string sql, string firstValue = null)
        {
            return new SelectList(QueryUtil.GetSelectedItems(conn, sql, firstValue), "Value", "Text");
        }

        public IEnumerable<System.Web.Mvc.SelectListItem> GetSelectedItems(DataSet ds, string firstValue=null)
        {
            return new SelectList(QueryUtil.GetSelectedItems(ds,firstValue), "Value", "Text");
        }

        public IEnumerable<System.Web.Mvc.SelectListItem> GetSelectedItems(string tableName, Int64? id, string firstValue = null, string whereCondition = "", string orderBy = null)
        {
            return new SelectList(QueryUtil.GetSelectedItems(tableName, id, firstValue, whereCondition, orderBy), "Value", "Text");
        }

        public IEnumerable<System.Web.Mvc.SelectListItem> GetSelectedItems(DBConnection conn,string tableName, Int64? id, string firstValue = null, string whereCondition = "", string orderBy = null)
        {
            return new SelectList(QueryUtil.GetSelectedItems(conn,tableName, id, firstValue, whereCondition, orderBy), "Value", "Text");
        }

        public Boolean HasAuthorize(LoginUser loginUser, String systemName, String method)
        {
            return LoginUser.HasAuthorize(loginUser, systemName, method);
        }
    }
}
