using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.domain
{
    public class DisplayIndex
    {
        private static HashSet<string> Tables = new HashSet<string>();
        static DisplayIndex()
        {
            Tables.Clear();
            Tables.Add("Advantage");
            Tables.Add("BoardLocation");
            Tables.Add("BoardStyle");
            Tables.Add("BoardType");
            Tables.Add("BridgeMaterial");
            Tables.Add("BridgeType");
            Tables.Add("BuildingType");
            Tables.Add("FloorNo");
            Tables.Add("LandParcelType");
            Tables.Add("LocationType");
            Tables.Add("Material");
            Tables.Add("Ownership");
            Tables.Add("PoleMaterial");
            Tables.Add("PoleType");
            Tables.Add("RoadLane");
            Tables.Add("RoadMaterial");
            Tables.Add("RoadType");
            Tables.Add("SystemMenu");
            Tables.Add("SystemParentMenu");
        }

        public static bool Has(string TableName)
        {
            return Tables.Contains(TableName);
        }
    }
}
