using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.domain
{
    public class ActiveStatusId
    {
        private static HashSet<string> Tables = new HashSet<string>();
        private static HashSet<string> IgnoreTables = new HashSet<string>();
        static ActiveStatusId()
        {
            Tables.Clear();
            IgnoreTables.Clear();

            Tables.Add("BuildingType");
            Tables.Add("EmailTemplate");
            Tables.Add("FileUpload");
            Tables.Add("GenCode");
            Tables.Add("MVCSetting");
            Tables.Add("Project");
            Tables.Add("SMSTemplate");
            Tables.Add("SystemConfig");
            Tables.Add("SystemMenu");
            Tables.Add("SystemParentMenu");
            Tables.Add("SystemRole");
            Tables.Add("SystemUser");

            IgnoreTables.Add("ActiveStatus");
            IgnoreTables.Add("Advantage");
            IgnoreTables.Add("BGLogger");
            IgnoreTables.Add("BoardLocation");
            IgnoreTables.Add("BoardStyle");
            IgnoreTables.Add("BoardType");
            IgnoreTables.Add("BridgeMaterial");
            IgnoreTables.Add("BridgeType");
            IgnoreTables.Add("FloorNo");
            IgnoreTables.Add("LandParcelType");
            IgnoreTables.Add("LocationType");
            IgnoreTables.Add("Logger");
            IgnoreTables.Add("Material");
            IgnoreTables.Add("Ownership");
            IgnoreTables.Add("PoleMaterial");
            IgnoreTables.Add("PoleType");
            IgnoreTables.Add("RoadLane");
            IgnoreTables.Add("RoadMaterial");
            IgnoreTables.Add("RoadType");
            IgnoreTables.Add("Survey");
            IgnoreTables.Add("SystemRoleMenu");
            IgnoreTables.Add("UserLogin");
            IgnoreTables.Add("UserProject");
        }

        public static bool Has(string TableName)
        {
            return Tables.Contains(TableName);
        }

        public static bool NotHas(string TableName)
        {
            return IgnoreTables.Contains(TableName);
        }
    }
}
