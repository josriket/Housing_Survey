﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Winplus.syslib.core.domain;

namespace Library.Winplus.syslib.core.domain
{
    public class TBColumn
    {
        public ColumnStyle Style;
        public string ColumnName;
        public string SortingIndexDesc;
        public string SortingIndexAsc;
        public string Label;
        public int Width;
        public string PageIndex;
        public string aHrefLink;

        public TBColumn() { }

        public TBColumn(string ColumnName, string SortingIndexDesc, string SortingIndexAsc, string Label, int Width, ColumnStyle Style)
        {
            this.ColumnName = ColumnName;
            this.SortingIndexDesc = SortingIndexDesc;
            this.SortingIndexAsc = SortingIndexAsc;
            this.Label = Label;
            this.Width = Width;
            this.Style = Style;
        }

        public bool CanSort { get; set; }


        public static TBColumn CheckBox
        {
            get
            {
                TBColumn col = new TBColumn();
                col.Style = ColumnStyle.CheckBox;
                col.Width = 5;
                col.CanSort = false;
                return col;
            }
        }

        public static TBColumn View
        {
            get
            {
                TBColumn col = new TBColumn();
                col.Style = ColumnStyle.View;
                col.Width = 5;
                col.CanSort = false;
                return col;
            }
        }

    }

    public enum ColumnStyle
    {
        CheckBox, Date, DateTime, Text, Decimal, Numeric,View
    }
}

