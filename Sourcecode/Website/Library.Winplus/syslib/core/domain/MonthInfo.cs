﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.domain
{
    public class MonthInfo
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string Label { get; set; }

        public MonthInfo(int Year, int Month, string Label)
        {
            this.Year = Year;
            this.Month = Month;
            this.Label = Label;
        }
    }
}
