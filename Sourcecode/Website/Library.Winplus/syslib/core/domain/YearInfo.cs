﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.domain
{
    public class YearInfo
    {
        public int Year { get; set; }
        public string Label { get; set; }

        public YearInfo(int Year, string Label)
        {
            this.Year = Year;
            this.Label = Label;
        }
    }
}
