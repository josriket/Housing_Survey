﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.domain
{
    public class SelectItem
    {
        public SelectItem(string Value, string Text)
        {
            this.Value = Value;
            this.Text = Text;
        }
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
