﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Winplus.syslib.core.domain
{
    public class HeaderSort
    {
        private List<TBColumn> lstColumn = new List<TBColumn>();
        private String defalultOrder; // Order default

        public HeaderSort(String defalultOrder, List<TBColumn> lstColumn)
        {
            this.defalultOrder = defalultOrder;
            this.lstColumn = lstColumn;
        }

      
        public String getActiveColumn(int index,string OrderType)
        {
            if (OrderType == null) OrderType = "asc";
            if (index == 0)
            {
                return defalultOrder;
            }
            else
            {
                return OrderType.Equals("asc") ? lstColumn[index].SortingIndexAsc : lstColumn[index].SortingIndexDesc;
            }
        }


        public List<TBColumn> getLstColumn()
        {
            return lstColumn;
        }
    }
}