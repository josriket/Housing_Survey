﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.Winplus.syslib.core.domain;
using System.Web.UI.WebControls;
using System.Data;
using Library.Winplus.syslib.core.connection;
using Library.Winplus.syslib.core.util;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Library.Winplus.syslib.data;
using System.Reflection;
using System.Web;


namespace Library.Winplus.syslib.core.domain
{
    public abstract class SearchCriteria
    {
        public DataTable dtResult { get; set; }
        public static string BLANK_COLUMN = "_____BLANK_COLUMN_____";
        public int _colHideCol = -1;
        public int _colHideColStart = 0;
        public int _colHideColEnd = 0;
        public bool _canEdit;
        public bool _canView;
        public string DownloadFileName;
        public string[] ids { get; set; }
        public bool ShowAdvance { get; set; }


        public string TbName { get; set; }
        public string PkName { get; set; }

        public void HidleRow(int colHideCol, int colHideColStart, int colHideColEnd)
        {
            _colHideCol = colHideCol;
            _colHideColStart = colHideColStart;
            _colHideColEnd = colHideColEnd;
        }


        public bool ShowResult { get; set; }
        private int totRecord; //จำนวนผลลัพธ์การค้นหา

        private List<Int32> pages = new List<Int32>(); // list of string to set navigator

        private bool searchFlag; // เก็บ สถานะการค้น false(ยังไม่ได้กดปุ่ม ค้นหา ก็ไม่จำเป็นต้องค้นผลหาในกรณีอยู่หน้าค้น), true(เคยกดค้นหาแล้ว ตอนกลับมาหน้าค้น ก็ต้องค้นหาผลลัพธ์มาแสดง)

        private HeaderSort headSort;

        private bool ignoreLimit; // Not need to generate query limt x,y

        //public string COLUMN_UNIQ; // Uniq column id for link,Edit,View,CheckboxValue
        public Dictionary<string, string> ReqParams = new Dictionary<string, string>();

        public abstract List<string> GetRPTMappingColumn();

        public bool disabledSorting;
        public bool AllowRowDbClick;
        public string TRClassNameColumn;

        public int Lpp { get; set; }

        public SearchCriteria()
        {
            Lpp = Convert.ToInt32(50);
        }

        private bool chkMaxExceed = true; // false:uncheck  ,true:check (default=true)

        public void setTotRecord(int totRecord, int maxPage)
        {
            pages.Clear();
            int totalPage = Lpp == 0 ? 1 : (int)Math.Ceiling((double)totRecord / Lpp);
            int maxpage = maxPage;
            int i = 0;
            for (i = _CurrentPage; i < _CurrentPage + maxpage / 2 && i < totalPage; i++)
            {
                pages.Add(i + 1);
            }
            int temp = _CurrentPage;

            while (pages.Count != maxpage)
            {
                if (temp <= 0)
                {
                    break;
                }
                pages.Add(--temp + 1);
            }

            while (pages.Count != maxpage && pages.Count < totalPage)
            {
                pages.Add((i++) + 1);
            }
            pages.Sort();
            // Collections.sort(pages);

            this.totRecord = totRecord;
        }
        /**
         * set total record from search count and will be calculate totalPage and initial list of pageString
         */
        public void setTotRecord(int totRecord)
        {
            setTotRecord(totRecord, 10);
        }

        /**
         * จำนวนหน้าทั้งหมดที่ได้จากการคำนวน
         */
        public int getTotalPage()
        {
            return Lpp == 0 ? 1 : (int)Math.Ceiling((double)totRecord / Lpp);
        }

        private int _CurrentPage;
        public int CurrentPage
        {
            get { return _CurrentPage; }
            set
            {
                pages.Clear();
                totRecord = 0;
                this._CurrentPage = value;
            }
        }

        private int _orderIndex;
        public int OrderIndex
        {
            get { return _orderIndex; }
            set
            {
                this._orderIndex = value;
            }
        }

        private string _orderType;
        public string OrderType
        {
            get { return _orderType; }
            set
            {
                this._orderType = value;
            }
        }

        public List<Int32> getPages()
        {
            return pages;
        }

        public void setPages(List<Int32> pages)
        {
            this.pages = pages;
        }


        public int getStart()
        {
            return _CurrentPage * Lpp + 1;
        }

        public int getTotRecord()
        {
            return totRecord;
        }

        public bool isChkMaxExceed()
        {
            return chkMaxExceed;
        }

        public void setChkMaxExceed(bool chkMaxExceed)
        {
            this.chkMaxExceed = chkMaxExceed;
        }

        public bool isSearchFlag()
        {
            return searchFlag;
        }

        public void setSearchFlag(bool searchFlag)
        {
            this.searchFlag = searchFlag;
        }

        public HeaderSort getHeadSort()
        {
            return headSort;
        }

        public void setHeadSort(HeaderSort headSort)
        {
            this.headSort = headSort;
        }

        public void setIgnoreLimit(bool ignoreLimit)
        {
            this.ignoreLimit = ignoreLimit;
        }

        public bool isIgnoreLimit()
        {
            return ignoreLimit;
        }


        public string _COLUMN_UNIQ { get; set; }

        public bool DisabledSorting { get; set; }

        public List<string> RowClickParams = new List<string>();

        public bool AllowRowClick { get; set; }

        public List<string> RowDbClickParams = new List<string>();

    }
}