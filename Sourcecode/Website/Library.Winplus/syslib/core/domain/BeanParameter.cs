﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Globalization;
using Library.Winplus.syslib.core.connection;
using System.Data;
using Library.Winplus.syslib.core.util;
using System.Web;

namespace Library.Winplus.syslib.core.domain
{
    public class BeanParameter
    {
        public string DB_CONNECTION;
        public string MemberShipDBConnectionString;

        public readonly DateTime TemplateDate = new DateTime(2000, 1, 1);
        public readonly string SQLDateFormat = "yyyy-MM-dd";
        public readonly string OutPutDateFormat = "dd/MM/yyyy";
        public readonly string OutPutDateTimeFormat = "dd/MM/yyyy HH:mm:ss";
        public readonly string OUTPUT_NUMBER_FORMAT = "{0:#,##0}";
        public readonly string OUTPUT_MONEY_FORMAT = "{0:#,##0.00}";
        public readonly string OutPutTimeFormat = "HH:mm";

        public CultureInfo ThaiCultureInfo
        {
            get
            {
                return CultureInfo.CreateSpecificCulture("th-TH");
            }
        }

        public CultureInfo EngCultureInfo
        {
            get
            {
                return CultureInfo.CreateSpecificCulture("en-US");
            }
        }

        public CultureInfo DefaultCultureInfo
        {
            get
            {
                return EngCultureInfo;
            }
        }


        private static BeanParameter param = null;
        public static BeanParameter GetInstance(){
            if (param == null)
            {
                param = new BeanParameter();
            }
            return param;
        }

        private BeanParameter()
        {
            DB_CONNECTION = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["DBName"].ToString()].ConnectionString;
            if (ConfigurationManager.ConnectionStrings["MemberShipDBConnectionString"] != null)
            {
                MemberShipDBConnectionString = ConfigurationManager.ConnectionStrings["MemberShipDBConnectionString"].ConnectionString;
            }
        }


        public string ContextUrl = "/";
        public string FullContextUrl = "/";

        public void InitialContextUrl(System.Web.Mvc.UrlHelper Url)
        {
            Uri url = new Uri(HttpContext.Current.Request.Url, ContextUrl);
            ContextUrl = Url.Content("~/");
            FullContextUrl = url.AbsoluteUri;
        }


    }
}
