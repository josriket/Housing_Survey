﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.domain
{
    public class QuarterInfo
    {
        public int Year { get; set; }
        public int Quarter { get; set; }
        public string Label { get; set; }

        public QuarterInfo(int Year, int Quarter, string Label)
        {
            this.Year = Year;
            this.Quarter = Quarter;
            this.Label = Label;
        }
    }
}
