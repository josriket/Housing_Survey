﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Winplus.syslib.core.domain;
using Library.Winplus.syslib.core.exception;
using Library.Winplus.syslib.core.util;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Library.Winplus.syslib.core.connection
{
    public class DBConnection : IDisposable
    {
        public static string DBType { get; set; }
        public SqlConnection conn;
        public SqlTransaction tran;

        static DBConnection()
        {
            DBType = ConfigurationManager.AppSettings["DBType"].ToString();
        }
        // Example : DBConnection conn = DBConnection.NewGWConnection;
        public static DBConnection DefaultConnection
        {
            get
            {
                DBConnection conn = new DBConnection(BeanParameter.GetInstance().DB_CONNECTION);
                return conn;
            }
        }

        public static DBConnection CustomConnection(string connections)
        {
            return new DBConnection(connections);
        }

        // New connection for Choose Database Connection "CRM","GW"
        // Example : DBConnection conn = new DBConnection(DBConnection.CRM);
        private DBConnection(string connectionstring)
        {
            openCon(connectionstring);
        }


        private void openCon(string connectionstring)
        {
            if (conn != null) throw new DBException("Connection not yet close, Please close connection before re-create.");

            conn = new SqlConnection(connectionstring);
            conn.Open();
        }


        public DataSet executeStoredProcedure(string storedName, Dictionary<string, object> lstParam)
        {
            using (SqlCommand cmd = new SqlCommand(storedName, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                if (tran != null)
                {
                    cmd.Transaction = tran;
                }

                if (lstParam != null)
                {
                    foreach (KeyValuePair<string, object> p in lstParam)
                    {
                        cmd.Parameters.Add("@" + p.Key, SqlDbType.VarChar).Value = p.Value;
                    }
                }

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                ds.Dispose();
                return ds;
            }
        }

        public DataSet executeDataSet(string query, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }

        internal void bulkInsert(string tableName, DataTable dt)
        {
            SqlBulkCopy bulkCopy =new SqlBulkCopy(this.conn, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);

            foreach(DataColumn col in dt.Columns)
            {
                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            }

            bulkCopy.DestinationTableName = tableName;
            bulkCopy.WriteToServer(dt);
        }

        public object executeScalar(string query, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            return cmd.ExecuteScalar();
        }
        public object executeScalar(string query, List<DBParameter> list)
        {
            return executeScalar(query, list.ToArray());
        }

        public SqlDataReader executeDataReader(string query, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            return cmd.ExecuteReader();
        }

        public string QueryDataString(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(query, param);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return null;
            }
        }

        public byte[] QueryDataBinary(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(query, param);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (byte[])ds.Tables[0].Rows[0][0];
            }
            else
            {
                return null;
            }
        }

        public decimal QueryDataDecimal(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(query, param);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return (decimal)ds.Tables[0].Rows[0][0];
            }
            else
            {
                return 0;
            }
        }

        public int QueryDataInt(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(query, param);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public Int64? QueryDataInt64(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(query, param);
            if (DataSetUtil.IsNotEmpty(ds) && ds.Tables[0].Rows[0][0]!=DBNull.Value)
            {
                return Convert.ToInt64(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return null;
            }
        }


        public DateTime QueryDataDateTime(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(query, param);
            if (DataSetUtil.IsNotEmpty(ds))
            {
                return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        public int getTotResut(string query, params DBParameter[] param)
        {
            DataSet ds = executeDataSet(QueryUtil.QueryCount(query), param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            return 0;
        }

        public int executeUpdate(string query, params DBParameter[] param)
        {
            Int64 generateKey = 0;
            return executeUpdate(query, ref generateKey, param);
        }

        public int executeUpdate(string query, ref Int64 generateKey, params DBParameter[] param)
        {
            SqlCommand cmd = new SqlCommand(FormatSql(query));
            cmd.Connection = conn;
            cmd.CommandTimeout = 0;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    cmd.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }
            int result = cmd.ExecuteNonQuery();

            // SQL SERVER
            cmd.CommandText = "SELECT @@IDENTITY";
            Object pkId = cmd.ExecuteScalar();
            if (pkId != DBNull.Value)
            {
                generateKey = Convert.ToInt64(pkId);
            }

            // Sql SQL
            //generateKey =cmd.LastInsertedId;

            return result;

        }

        public DataSet getSchema(string tableName)
        {
            SqlCommand cmd = new SqlCommand(FormatSql("SELECT * FROM [" + tableName + "] WHERE 1<>1"));
            cmd.Connection = conn;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            ds.Dispose();
            return ds;
        }


        public void beginTrans()
        {
            if (conn == null) throw new DBException("Can't begin tranaction, Please initial connection before use.");
            tran = conn.BeginTransaction();
        }

        public void commitTrans()
        {
            if (conn == null && tran == null) throw new DBException("Can't commit tranaction, Please begin transaction before use.");
            if (tran != null)
            {
                tran.Commit();
                tran = null;
            }
        }

        public void rollbackTrans()
        {
            if (tran != null)
            {
                tran.Rollback();
                tran = null;
            }
        }

        public void closeCon()
        {
            if (tran != null)
            {
                rollbackTrans();
            }
            if (conn != null) conn.Close();
            conn = null;
        }


        /* Delete data for search page*/
        public void delete(string sqlDelete, string[] values)
        {
            executeUpdate(String.Format(sqlDelete, StringUtil.arrToString(values)), null);
        }

        public static void Close(DBConnection conn)
        {
            if (conn != null) conn.closeCon();
        }

        public void Dispose()
        {
            DBConnection.Close(this);
        }

        public bool IsExistsData(string sql, params DBParameter[] param)
        {
            SqlCommand command = new SqlCommand(FormatSql(sql), conn);
            if (tran != null)
            {
                command.Transaction = tran;
            }

            if (param != null)
            {
                foreach (DBParameter p in param)
                {
                    command.Parameters.Add(new SqlParameter(p.ParameterName, p.Value == null ? DBNull.Value : p.Value));
                }
            }

            return Convert.ToInt32(command.ExecuteScalar())>0;
        }


        private string FormatSql(string sql)
        {
            if (DBType == "Sql")
            {
                // Sql
                return Regex.Replace(sql.Replace("[", "`").Replace("]", "`"), "ISNULL", "COALESCE", RegexOptions.IgnoreCase);
            }
            else
            {
                // SQLSERVER
                return sql;
            }

        }
    }
}
