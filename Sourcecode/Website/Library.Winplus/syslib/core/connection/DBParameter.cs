﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.connection
{
    public class DBParameter
    {
        public string ParameterName { get; set; }
        public object Value { get; set; }

        public DBParameter(string ParameterName, object Value)
        {
            // TODO: Complete member initialization
            this.ParameterName = ParameterName;
            this.Value = Value;
        }

    }
}
