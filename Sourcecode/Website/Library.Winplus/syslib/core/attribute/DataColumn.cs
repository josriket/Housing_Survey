﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Winplus.syslib.core.attribute
{
    public class DataColumn : Attribute
    {
        public string Type { get; set; }
        public DataColumn() { }
        public DataColumn(string type)
        {
            this.Type = type;
        }
    }
}