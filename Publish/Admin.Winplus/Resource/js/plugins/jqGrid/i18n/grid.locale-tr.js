﻿;(function($){
/**
 * jqGrid Turkish Translation
 * Erhan Gรผndoฤan (erhan@trposta.net)
 * http://blog.zakkum.com
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "{0}-{1} listeleniyor. Toplam:{2}",
	    emptyrecords: "Kayฤฑt bulunamadฤฑ",
		loadtext: "Yรผkleniyor...",
		pgtext : "{0}/{1}. Sayfa"
	},
	search : {
	    caption: "Arama...",
	    Find: "Bul",
	    Reset: "Temizle",	    
	    odata: [{ oper:'eq', text:"eลit"},{ oper:'ne', text:"eลit deฤil"},{ oper:'lt', text:"daha az"},{ oper:'le', text:"daha az veya eลit"},{ oper:'gt', text:"daha fazla"},{ oper:'ge', text:"daha fazla veya eลit"},{ oper:'bw', text:"ile baลlayan"},{ oper:'bn', text:"ile baลlamayan"},{ oper:'in', text:"iรงinde"},{ oper:'ni', text:"iรงinde deฤil"},{ oper:'ew', text:"ile biten"},{ oper:'en', text:"ile bitmeyen"},{ oper:'cn', text:"iรงeren"},{ oper:'nc', text:"iรงermeyen"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
	    groupOps: [	{ op: "VE", text: "tรผm" },	{ op: "VEYA",  text: "herhangi" }],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
	    addCaption: "Kayฤฑt Ekle",
	    editCaption: "Kayฤฑt Dรผzenle",
	    bSubmit: "Gรถnder",
	    bCancel: "ฤฐptal",
		bClose: "Kapat",
		saveData: "Veriler deฤiลti! Kayฤฑt edilsin mi?",
		bYes : "Evet",
		bNo : "Hayฤฑt",
		bExit : "ฤฐptal",
	    msg: {
	        required:"Alan gerekli",
	        number:"Lรผtfen bir numara giriniz",
	        minValue:"girilen deฤer daha bรผyรผk ya da buna eลit olmalฤฑdฤฑr",
	        maxValue:"girilen deฤer daha kรผรงรผk ya da buna eลit olmalฤฑdฤฑr",
	        email: "geรงerli bir e-posta adresi deฤildir",
	        integer: "Lรผtfen bir tamsayฤฑ giriniz",
			url: "Geรงerli bir URL deฤil. ('http://' or 'https://') รถn eki gerekli.",
			nodefined : " is not defined!",
			novalue : " return value is required!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
		}
	},
	view : {
	    caption: "Kayฤฑt Gรถrรผntรผle",
	    bClose: "Kapat"
	},
	del : {
	    caption: "Sil",
	    msg: "Seรงilen kayฤฑtlar silinsin mi?",
	    bSubmit: "Sil",
	    bCancel: "ฤฐptal"
	},
	nav : {
		edittext: " ",
	    edittitle: "Seรงili satฤฑrฤฑ dรผzenle",
		addtext:" ",
	    addtitle: "Yeni satฤฑr ekle",
	    deltext: " ",
	    deltitle: "Seรงili satฤฑrฤฑ sil",
	    searchtext: " ",
	    searchtitle: "Kayฤฑtlarฤฑ bul",
	    refreshtext: "",
	    refreshtitle: "Tabloyu yenile",
	    alertcap: "Uyarฤฑ",
	    alerttext: "Lรผtfen bir satฤฑr seรงiniz",
		viewtext: "",
		viewtitle: "Seรงilen satฤฑrฤฑ gรถrรผntรผle"
	},
	col : {
	    caption: "Sรผtunlarฤฑ gรถster/gizle",
	    bSubmit: "Gรถnder",
	    bCancel: "ฤฐptal"	
	},
	errors : {
		errcap : "Hata",
		nourl : "Bir url yapฤฑlandฤฑrฤฑlmamฤฑล",
		norecords: "ฤฐลlem yapฤฑlacak bir kayฤฑt yok",
	    model : "colNames uzunluฤu <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"Paz", "Pts", "Sal", "รar", "Per", "Cum", "Cts",
				"Pazar", "Pazartesi", "Salฤฑ", "รarลamba", "Perลembe", "Cuma", "Cumartesi"
			],
			monthNames: [
				"Oca", "ลub", "Mar", "Nis", "May", "Haz", "Tem", "Aฤu", "Eyl", "Eki", "Kas", "Ara",
				"Ocak", "ลubat", "Mart", "Nisan", "Mayฤฑs", "Haziran", "Temmuz", "Aฤustos", "Eylรผl", "Ekim", "Kasฤฑm", "Aralฤฑk"
			],
			AmPm : ["am","pm","AM","PM"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
	            ISO8601Long:"Y-m-d H:i:s",
	            ISO8601Short:"Y-m-d",
	            ShortDate: "n/j/Y",
	            LongDate: "l, F d, Y",
	            FullDateTime: "l, F d, Y g:i:s A",
	            MonthDay: "F d",
	            ShortTime: "g:i A",
	            LongTime: "g:i:s A",
	            SortableDateTime: "Y-m-d\\TH:i:s",
	            UniversalSortableDateTime: "Y-m-d H:i:sO",
	            YearMonth: "F, Y"
	        },
	        reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
