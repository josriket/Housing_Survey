﻿;(function($){
/**
 * jqGrid Polish Translation
 * ลukasz Schab lukasz@freetree.pl
 * http://FreeTree.pl
 *
 * Updated names, abbreviations, currency and date/time formats for Polish norms (also corresponding with CLDR v21.0.1 --> http://cldr.unicode.org/index) 
 * Tomasz Pฤczek tpeczek@gmail.com
 * http://tpeczek.blogspot.com; http://tpeczek.codeplex.com
 *
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Pokaลผ {0} - {1} z {2}",
		emptyrecords: "Brak rekordรณw do pokazania",
		loadtext: "ลadowanie...",
		pgtext : "Strona {0} z {1}"
	},
	search : {
		caption: "Wyszukiwanie...",
		Find: "Szukaj",
		Reset: "Czyลฤ",
		odata: [{ oper:'eq', text:"dokลadnie"},{ oper:'ne', text:"rรณลผne od"},{ oper:'lt', text:"mniejsze od"},{ oper:'le', text:"mniejsze lub rรณwne"},{ oper:'gt', text:"wiฤksze od"},{ oper:'ge', text:"wiฤksze lub rรณwne"},{ oper:'bw', text:"zaczyna siฤ od"},{ oper:'bn', text:"nie zaczyna siฤ od"},{ oper:'in', text:"jest w"},{ oper:'ni', text:"nie jest w"},{ oper:'ew', text:"koลczy siฤ na"},{ oper:'en', text:"nie koลczy siฤ na"},{ oper:'cn', text:"zawiera"},{ oper:'nc', text:"nie zawiera"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "oraz" },	{ op: "OR",  text: "lub" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "Dodaj rekord",
		editCaption: "Edytuj rekord",
		bSubmit: "Zapisz",
		bCancel: "Anuluj",
		bClose: "Zamknij",
		saveData: "Dane zostaลy zmienione! Zapisaฤ zmiany?",
		bYes: "Tak",
		bNo: "Nie",
		bExit: "Anuluj",
		msg: {
			required: "Pole jest wymagane",
			number: "Proszฤ wpisaฤ poprawnฤ liczbฤ",
			minValue: "wartoลฤ musi byฤ wiฤksza lub rรณwna od",
			maxValue: "wartoลฤ musi byฤ mniejsza lub rรณwna od",
			email: "nie jest poprawnym adresem e-mail",
			integer: "Proszฤ wpisaฤ poprawnฤ liczbฤ",
			date: "Proszฤ podaj poprawnฤ datฤ",
			url: "jest niewลaลciwym adresem URL. Pamiฤtaj o prefiksie ('http://' lub 'https://')",
			nodefined: " niezdefiniowane!",
			novalue: " wymagana jest wartoลฤ zwracana!",
			customarray: "Funkcja niestandardowa powinna zwracaฤ tablicฤ!",
			customfcheck: "Funkcja niestandardowa powinna byฤ obecna w przypadku niestandardowego sprawdzania!"
		}
	},
	view : {
		caption: "Pokaลผ rekord",
		bClose: "Zamknij"
	},
	del : {
		caption: "Usuล",
		msg: "Czy usunฤฤ wybrany rekord(y)?",
		bSubmit: "Usuล",
		bCancel: "Anuluj"
	},
	nav : {
		edittext: "",
		edittitle: "Edytuj wybrany wiersz",
		addtext: "",
		addtitle: "Dodaj nowy wiersz",
		deltext: "",
		deltitle: "Usuล wybrany wiersz",
		searchtext: "",
		searchtitle: "Wyszukaj rekord",
		refreshtext: "",
		refreshtitle: "Przeลaduj",
		alertcap: "Uwaga",
		alerttext: "Proszฤ wybraฤ wiersz",
		viewtext: "",
		viewtitle: "Pokaลผ wybrany wiersz"
	},
	col : {
		caption: "Pokaลผ/Ukryj kolumny",
		bSubmit: "Zatwierdลบ",
		bCancel: "Anuluj"
	},
	errors : {
		errcap: "Bลฤd",
		nourl: "Brak adresu url",
		norecords: "Brak danych",
		model : "Dลugoลฤ colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0,00'},
		currency : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:" zล", defaultValue: '0,00'},
		date : {
			dayNames:   [
				"niedz.", "pon.", "wt.", "ลr.", "czw.", "pt.", "sob.",
				"niedziela", "poniedziaลek", "wtorek", "ลroda", "czwartek", "piฤtek", "sobota"
			],
			monthNames: [
				"sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paลบ", "lis", "gru",
				"styczeล", "luty", "marzec", "kwiecieล", "maj", "czerwiec", "lipiec", "sierpieล", "wrzesieล", "paลบdziernik", "listopad", "grudzieล"
				],
			AmPm : ["","","",""],
			S: function (j) {return '';},
			srcformat: 'Y-m-d',
			newformat: 'd.m.Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long: "Y-m-d H:i:s",
				ISO8601Short: "Y-m-d",
				ShortDate: "d.m.y",
				LongDate: "l, j F Y",
				FullDateTime: "l, j F Y H:i:s",
				MonthDay: "j F",
				ShortTime: "H:i",
				LongTime: "H:i:s",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);