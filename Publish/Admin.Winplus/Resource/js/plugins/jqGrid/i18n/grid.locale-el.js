﻿;(function($){
/**
 * jqGrid Greek (el) Translation
 * Alex Cicovic
 * http://www.alexcicovic.com
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "View {0} - {1} of {2}",
	    emptyrecords: "No records to view",
		loadtext: "ฮฆฯฯฯฯฯฮท...",
		pgtext : "Page {0} of {1}"
	},
	search : {
	    caption: "ฮฮฝฮฑฮถฮฎฯฮทฯฮท...",
	    Find: "ฮฯฯฮตฯฮท",
	    Reset: "ฮฯฮฑฮฝฮฑฯฮฟฯฮฌ",
	    odata: [{ oper:'eq', text:'equal'},{ oper:'ne', text:'not equal'},{ oper:'lt', text:'less'},{ oper:'le', text:'less or equal'},{ oper:'gt', text:'greater'},{ oper:'ge', text:'greater or equal'},{ oper:'bw', text:'begins with'},{ oper:'bn', text:'does not begin with'},{ oper:'in', text:'is in'},{ oper:'ni', text:'is not in'},{ oper:'ew', text:'ends with'},{ oper:'en', text:'does not end with'},{ oper:'cn', text:'contains'},{ oper:'nc', text:'does not contain'},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
	    groupOps: [	{ op: "AND", text: "all" },	{ op: "OR",  text: "any" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
	    addCaption: "ฮฮนฯฮฑฮณฯฮณฮฎ ฮฮณฮณฯฮฑฯฮฎฯ",
	    editCaption: "ฮฯฮตฮพฮตฯฮณฮฑฯฮฏฮฑ ฮฮณฮณฯฮฑฯฮฎฯ",
	    bSubmit: "ฮฮฑฯฮฑฯฯฯฮทฯฮท",
	    bCancel: "ฮฮบฯฯฮฟ",
		bClose: "ฮฮปฮตฮฏฯฮนฮผฮฟ",
		saveData: "Data has been changed! Save changes?",
		bYes : "Yes",
		bNo : "No",
		bExit : "Cancel",
	    msg: {
	        required:"ฮคฮฟ ฯฮตฮดฮฏฮฟ ฮตฮฏฮฝฮฑฮน ฮฑฯฮฑฯฮฑฮฏฯฮทฯฮฟ",
	        number:"ฮคฮฟ ฯฮตฮดฮฏฮฟ ฮดฮญฯฮตฯฮฑฮน ฮผฯฮฝฮฟ ฮฑฯฮนฮธฮผฮฟฯฯ",
	        minValue:"ฮ ฯฮนฮผฮฎ ฯฯฮญฯฮตฮน ฮฝฮฑ ฮตฮฏฮฝฮฑฮน ฮผฮตฮณฮฑฮปฯฯฮตฯฮท ฮฎ ฮฏฯฮท ฯฮฟฯ ",
	        maxValue:"ฮ ฯฮนฮผฮฎ ฯฯฮญฯฮตฮน ฮฝฮฑ ฮตฮฏฮฝฮฑฮน ฮผฮนฮบฯฯฯฮตฯฮท ฮฎ ฮฏฯฮท ฯฮฟฯ ",
	        email: "ฮ ฮดฮนฮตฯฮธฯฮฝฯฮท e-mail ฮดฮตฮฝ ฮตฮฏฮฝฮฑฮน ฮญฮณฮบฯฯฮท",
	        integer: "ฮคฮฟ ฯฮตฮดฮฏฮฟ ฮดฮญฯฮตฯฮฑฮน ฮผฯฮฝฮฟ ฮฑฮบฮญฯฮฑฮนฮฟฯฯ ฮฑฯฮนฮธฮผฮฟฯฯ",
			url: "is not a valid URL. Prefix required ('http://' or 'https://')",
			nodefined : " is not defined!",
			novalue : " return value is required!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
		}
	},
	view : {
	    caption: "View Record",
	    bClose: "Close"
	},
	del : {
	    caption: "ฮฮนฮฑฮณฯฮฑฯฮฎ",
	    msg: "ฮฮนฮฑฮณฯฮฑฯฮฎ ฯฯฮฝ ฮตฯฮนฮปฮตฮณฮผฮญฮฝฯฮฝ ฮตฮณฮณฯฮฑฯฯฮฝ;",
	    bSubmit: "ฮฮฑฮน",
	    bCancel: "ฮฮบฯฯฮฟ"
	},
	nav : {
		edittext: " ",
	    edittitle: "ฮฯฮตฮพฮตฯฮณฮฑฯฮฏฮฑ ฮตฯฮนฮปฮตฮณฮผฮญฮฝฮทฯ ฮตฮณฮณฯฮฑฯฮฎฯ",
		addtext:" ",
	    addtitle: "ฮฮนฯฮฑฮณฯฮณฮฎ ฮฝฮญฮฑฯ ฮตฮณฮณฯฮฑฯฮฎฯ",
	    deltext: " ",
	    deltitle: "ฮฮนฮฑฮณฯฮฑฯฮฎ ฮตฯฮนฮปฮตฮณฮผฮญฮฝฮทฯ ฮตฮณฮณฯฮฑฯฮฎฯ",
	    searchtext: " ",
	    searchtitle: "ฮฯฯฮตฯฮท ฮฮณฮณฯฮฑฯฯฮฝ",
	    refreshtext: "",
	    refreshtitle: "ฮฮฝฮฑฮฝฮญฯฯฮท ฮ ฮฏฮฝฮฑฮบฮฑ",
	    alertcap: "ฮ ฯฮฟฯฮฟฯฮฎ",
	    alerttext: "ฮฮตฮฝ ฮญฯฮตฯฮต ฮตฯฮนฮปฮญฮพฮตฮน ฮตฮณฮณฯฮฑฯฮฎ",
		viewtext: "",
		viewtitle: "View selected row"
	},
	col : {
	    caption: "ฮฮผฯฮฌฮฝฮนฯฮท / ฮฯฯฮบฯฯฯฮท ฮฃฯฮทฮปฯฮฝ",
	    bSubmit: "ฮฮ",
	    bCancel: "ฮฮบฯฯฮฟ"
	},
	errors : {
		errcap : "ฮฃฯฮฌฮปฮผฮฑ",
		nourl : "ฮฮตฮฝ ฮญฯฮตฮน ฮดฮฟฮธฮตฮฏ ฮดฮนฮตฯฮธฯฮฝฯฮท ฯฮตฮนฯฮนฯฮผฮฟฯ ฮณฮนฮฑ ฯฮท ฯฯฮณฮบฮตฮบฯฮนฮผฮญฮฝฮท ฮตฮฝฮญฯฮณฮตฮนฮฑ",
		norecords: "ฮฮตฮฝ ฯฯฮฌฯฯฮฟฯฮฝ ฮตฮณฮณฯฮฑฯฮญฯ ฯฯฮฟฯ ฮตฯฮตฮพฮตฯฮณฮฑฯฮฏฮฑ",
		model : "ฮฮฝฮนฯฮฟฯ ฮฑฯฮนฮธฮผฯฯ ฯฮตฮดฮฏฯฮฝ colNames/colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"ฮฯฯ", "ฮฮตฯ", "ฮคฯฮน", "ฮคฮตฯ", "ฮ ฮตฮผ", "ฮ ฮฑฯ", "ฮฃฮฑฮฒ",
				"ฮฯฯฮนฮฑฮบฮฎ", "ฮฮตฯฯฮญฯฮฑ", "ฮคฯฮฏฯฮท", "ฮคฮตฯฮฌฯฯฮท", "ฮ ฮญฮผฯฯฮท", "ฮ ฮฑฯฮฑฯฮบฮตฯฮฎ", "ฮฃฮฌฮฒฮฒฮฑฯฮฟ"
			],
			monthNames: [
				"ฮฮฑฮฝ", "ฮฆฮตฮฒ", "ฮฮฑฯ", "ฮฯฯ", "ฮฮฑฮน", "ฮฮฟฯฮฝ", "ฮฮฟฯฮป", "ฮฯฮณ", "ฮฃฮตฯ", "ฮฮบฯ", "ฮฮฟฮต", "ฮฮตฮบ",
				"ฮฮฑฮฝฮฟฯฮฌฯฮนฮฟฯ", "ฮฆฮตฮฒฯฮฟฯฮฌฯฮนฮฟฯ", "ฮฮฌฯฯฮนฮฟฯ", "ฮฯฯฮฏฮปฮนฮฟฯ", "ฮฮฌฮนฮฟฯ", "ฮฮฟฯฮฝฮนฮฟฯ", "ฮฮฟฯฮปฮนฮฟฯ", "ฮฯฮณฮฟฯฯฯฮฟฯ", "ฮฃฮตฯฯฮญฮผฮฒฯฮนฮฟฯ", "ฮฮบฯฯฮฒฯฮนฮฟฯ", "ฮฮฟฮญฮผฮฒฯฮนฮฟฯ", "ฮฮตฮบฮญฮผฮฒฯฮนฮฟฯ"
			],
			AmPm : ["ฯฮผ","ฮผฮผ","ฮ ฮ","ฮฮ"],
			S: function (j) {return j == 1 || j > 1 ? ['ฮท'][Math.min((j - 1) % 10, 3)] : ''},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
	            ISO8601Long:"Y-m-d H:i:s",
	            ISO8601Short:"Y-m-d",
	            ShortDate: "n/j/Y",
	            LongDate: "l, F d, Y",
	            FullDateTime: "l, F d, Y g:i:s A",
	            MonthDay: "F d",
	            ShortTime: "g:i A",
	            LongTime: "g:i:s A",
	            SortableDateTime: "Y-m-d\\TH:i:s",
	            UniversalSortableDateTime: "Y-m-d H:i:sO",
	            YearMonth: "F, Y"
	        },
	        reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
