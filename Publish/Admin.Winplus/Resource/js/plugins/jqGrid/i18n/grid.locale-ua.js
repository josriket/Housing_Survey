﻿;(function($){
/**
 * jqGrid Ukrainian Translation v1.0 02.07.2009
 * Sergey Dyagovchenko
 * http://d.sumy.ua
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "ะะตัะตะณะปัะด {0} - {1} ะท {2}",
	  emptyrecords: "ะะตะผะฐั ะทะฐะฟะธััะฒ ะดะปั ะฟะตัะตะณะปัะดั",
		loadtext: "ะะฐะฒะฐะฝัะฐะถะตะฝะฝั...",
		pgtext : "ะกัะพั. {0} ะท {1}"
	},
	search : {
    caption: "ะะพััะบ...",
    Find: "ะะฝะฐะนัะธ",
    Reset: "ะกะบะธะดะฐะฝะฝั",
    odata: [{ oper:'eq', text:"ััะฒะฝะพ"},{ oper:'ne', text:"ะฝะต ััะฒะฝะพ"},{ oper:'lt', text:"ะผะตะฝัะต"},{ oper:'le', text:"ะผะตะฝัะต ะฐะฑะพ ััะฒะฝะต"},{ oper:'gt', text:"ะฑัะปััะต"},{ oper:'ge', text:"ะฑัะปััะต ะฐะฑะพ ััะฒะฝะต"},{ oper:'bw', text:"ะฟะพัะธะฝะฐััััั ะท"},{ oper:'bn', text:"ะฝะต ะฟะพัะธะฝะฐััััั ะท"},{ oper:'in', text:"ะทะฝะฐัะพะดะธัััั ะฒ"},{ oper:'ni', text:"ะฝะต ะทะฝะฐัะพะดะธัััั ะฒ"},{ oper:'ew', text:"ะทะฐะบัะฝััััััั ะฝะฐ"},{ oper:'en', text:"ะฝะต ะทะฐะบัะฝััััััั ะฝะฐ"},{ oper:'cn', text:"ะผัััะธัั"},{ oper:'nc', text:"ะฝะต ะผัััะธัั"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
    groupOps: [	{ op: "AND", text: "ะฒัะต" },	{ op: "OR",  text: "ะฑัะดั-ัะบะธะน" }],
	operandTitle : "Click to select search operation.",
	resetTitle : "Reset Search Value"
	},
	edit : {
    addCaption: "ะะพะดะฐัะธ ะทะฐะฟะธั",
    editCaption: "ะะผัะฝะธัะธ ะทะฐะฟะธั",
    bSubmit: "ะะฑะตัะตะณัะธ",
    bCancel: "ะัะดะผัะฝะฐ",
		bClose: "ะะฐะบัะธัะธ",
		saveData: "ะะพ ะดะฐะฝะฝะธั ะฑัะปะธ ะฒะฝะตัะตะฝั ะทะผัะฝะธ! ะะฑะตัะตะณัะธ ะทะผัะฝะธ?",
		bYes : "ะขะฐะบ",
		bNo : "ะั",
		bExit : "ะัะดะผัะฝะฐ",
	    msg: {
        required:"ะะพะปะต ั ะพะฑะพะฒ'ัะทะบะพะฒะธะผ",
        number:"ะัะดั ะปะฐัะบะฐ, ะฒะฒะตะดััั ะฟัะฐะฒะธะปัะฝะต ัะธัะปะพ",
        minValue:"ะทะฝะฐัะตะฝะฝั ะฟะพะฒะธะฝะฝะต ะฑััะธ ะฑัะปััะต ะฐะฑะพ ะดะพััะฒะฝัั",
        maxValue:"ะทะฝะฐัะตะฝะฝั ะฟะพะฒะธะฝะฝะพ ะฑััะธ ะผะตะฝัะต ะฐะฑะพ ะดะพััะฒะฝัั",
        email: "ะฝะตะบะพัะตะบัะฝะฐ ะฐะดัะตัะฐ ะตะปะตะบััะพะฝะฝะพั ะฟะพััะธ",
        integer: "ะัะดั ะปะฐัะบะฐ, ะฒะฒะตะดะตะฝะฝั ะดัะนัะฝะต ััะปะต ะทะฝะฐัะตะฝะฝั",
        date: "ะัะดั ะปะฐัะบะฐ, ะฒะฒะตะดะตะฝะฝั ะดัะนัะฝะต ะทะฝะฐัะตะฝะฝั ะดะฐัะธ",
        url: "ะฝะต ะดัะนัะฝะธะน URL. ะะตะพะฑััะดะฝะฐ ะฟัะธััะฐะฒะบะฐ ('http://' or 'https://')",
		nodefined : " is not defined!",
		novalue : " return value is required!",
		customarray : "Custom function should return array!",
		customfcheck : "Custom function should be present in case of custom checking!"
		}
	},
	view : {
	    caption: "ะะตัะตะณะปัะฝััะธ ะทะฐะฟะธั",
	    bClose: "ะะฐะบัะธัะธ"
	},
	del : {
	    caption: "ะะธะดะฐะปะธัะธ",
	    msg: "ะะธะดะฐะปะธัะธ ะพะฑัะฐะฝะธะน ะทะฐะฟะธั(ะธ)?",
	    bSubmit: "ะะธะดะฐะปะธัะธ",
	    bCancel: "ะัะดะผัะฝะฐ"
	},
	nav : {
  		edittext: " ",
	    edittitle: "ะะผัะฝะธัะธ ะฒะธะฑัะฐะฝะธะน ะทะฐะฟะธั",
  		addtext:" ",
	    addtitle: "ะะพะดะฐัะธ ะฝะพะฒะธะน ะทะฐะฟะธั",
	    deltext: " ",
	    deltitle: "ะะธะดะฐะปะธัะธ ะฒะธะฑัะฐะฝะธะน ะทะฐะฟะธั",
	    searchtext: " ",
	    searchtitle: "ะะฝะฐะนัะธ ะทะฐะฟะธัะธ",
	    refreshtext: "",
	    refreshtitle: "ะะฝะพะฒะธัะธ ัะฐะฑะปะธัั",
	    alertcap: "ะะพะฟะตัะตะดะถะตะฝะฝั",
	    alerttext: "ะัะดั ะปะฐัะบะฐ, ะฒะธะฑะตัััั ะทะฐะฟะธั",
  		viewtext: "",
  		viewtitle: "ะะตัะตะณะปัะฝััะธ ะพะฑัะฐะฝะธะน ะทะฐะฟะธั"
	},
	col : {
	    caption: "ะะพะบะฐะทะฐัะธ/ะัะธัะพะฒะฐัะธ ััะพะฒะฟัั",
	    bSubmit: "ะะฑะตัะตะณัะธ",
	    bCancel: "ะัะดะผัะฝะฐ"
	},
	errors : {
		errcap : "ะะพะผะธะปะบะฐ",
		nourl : "URL ะฝะต ะทะฐะดะฐะฝ",
		norecords: "ะะตะผะฐั ะทะฐะฟะธััะฒ ะดะปั ะพะฑัะพะฑะบะธ",
    model : "ะงะธัะปะพ ะฟะพะปัะฒ ะฝะต ะฒัะดะฟะพะฒัะดะฐั ัะธัะปั ััะพะฒะฟััะฒ ัะฐะฑะปะธัั!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0,00'},
		currency : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0,00'},
		date : {
			dayNames:   [
				"ะะด", "ะะฝ", "ะั", "ะกั", "ะงั", "ะั", "ะกะฑ",
				"ะะตะดัะปั", "ะะพะฝะตะดัะปะพะบ", "ะัะฒัะพัะพะบ", "ะกะตัะตะดะฐ", "ะงะตัะฒะตั", "ะ'ััะฝะธัั", "ะกัะฑะพัะฐ"
			],
			monthNames: [
				"ะกัั", "ะัั", "ะะตั", "ะะฒั", "ะขัะฐ", "ะงะตั", "ะะธะฟ", "ะกะตั", "ะะตั", "ะะพะฒ", "ะะธั", "ะัั",
				"ะกััะตะฝั", "ะััะธะน", "ะะตัะตะทะตะฝั", "ะะฒััะตะฝั", "ะขัะฐะฒะตะฝั", "ะงะตัะฒะตะฝั", "ะะธะฟะตะฝั", "ะกะตัะฟะตะฝั", "ะะตัะตัะตะฝั", "ะะพะฒัะตะฝั", "ะะธััะพะฟะฐะด", "ะััะดะตะฝั"
			],
			AmPm : ["am","pm","AM","PM"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd.m.Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
	            ISO8601Long:"Y-m-d H:i:s",
	            ISO8601Short:"Y-m-d",
	            ShortDate: "n.j.Y",
	            LongDate: "l, F d, Y",
	            FullDateTime: "l, F d, Y G:i:s",
	            MonthDay: "F d",
	            ShortTime: "G:i",
	            LongTime: "G:i:s",
	            SortableDateTime: "Y-m-d\\TH:i:s",
	            UniversalSortableDateTime: "Y-m-d H:i:sO",
	            YearMonth: "F, Y"
	        },
	        reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	  target: '',
	  checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
