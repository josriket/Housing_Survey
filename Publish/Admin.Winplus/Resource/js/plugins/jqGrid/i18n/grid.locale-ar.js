﻿;(function($){
/**
 * jqGrid Arabic Translation
 * 
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "ุชุณุฌูู {0} - {1} ุนูู {2}",
		emptyrecords: "ูุง ููุฌุฏ ุชุณุฌูู",
		loadtext: "ุชุญููู...",
		pgtext : "ุตูุญุฉ {0} ุนูู {1}"
	},
	search : {
		caption: "ุจุญุซ...",
		Find: "ุจุญุซ",
		Reset: "ุฅูุบุงุก",
		odata: [{ oper:'eq', text:"ูุณุงูู"},{ oper:'ne', text:"ูุฎุชูู"},{ oper:'lt', text:"ุฃูู"},{ oper:'le', text:"ุฃูู ุฃู ูุณุงูู"},{ oper:'gt', text:"ุฃูุจุฑ"},{ oper:'ge', text:"ุฃูุจุฑ ุฃู ูุณุงูู"},{ oper:'bw', text:"ูุจุฏุฃ ุจู"},{ oper:'bn', text:"ูุง ูุจุฏุฃ ุจู"},{ oper:'in', text:"est dans"},{ oper:'ni', text:"n'est pas dans"},{ oper:'ew', text:"ููุชู ุจู"},{ oper:'en', text:"ูุง ููุชู ุจู"},{ oper:'cn', text:"ูุญุชูู"},{ oper:'nc', text:"ูุง ูุญุชูู"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "ูุน", text: "ุงููู" },	{ op: "ุฃู",  text: "ูุง ุฃุญุฏ" }],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
},
	edit : {
		addCaption: "ุงุถุงูุฉ",
		editCaption: "ุชุญุฏูุซ",
		bSubmit: "ุชุซุจูุซ",
		bCancel: "ุฅูุบุงุก",
		bClose: "ุบูู",
		saveData: "ุชุบูุฑุช ุงููุนุทูุงุช ูู ุชุฑูุฏ ุงูุชุณุฌูู ?",
		bYes: "ูุนู",
		bNo: "ูุง",
		bExit: "ุฅูุบุงุก",
		msg: {
			required: "ุฎุงูุฉ ุฅุฌุจุงุฑูุฉ",
			number: "ุณุฌู ุฑูู ุตุญูุญ",
			minValue: "ูุฌุจ ุฃู ุชููู ุงููููุฉ ุฃูุจุฑ ุฃู ุชุณุงูู 0",
			maxValue: "ูุฌุจ ุฃู ุชููู ุงููููุฉ ุฃูู ุฃู ุชุณุงูู 0",
			email: "ุจุฑูุฏ ุบูุฑ ุตุญูุญ",
			integer: "ุณุฌู ุนุฏุฏ ุทุจููุนู ุตุญูุญ",
			url: "ููุณ ุนููุงูุง ุตุญูุญุง. ุงูุจุฏุงูุฉ ุงูุตุญูุญุฉ ('http://' ุฃู 'https://')",
			nodefined : " ููุณ ูุญุฏุฏ!",
			novalue : " ูููุฉ ุงูุฑุฌูุน ูุทููุจุฉ!",
			customarray : "ูุฌุจ ุนูู ุงูุฏุงูุฉ ุงูุดุฎุตูุฉ ุฃู ุชูุชุฌ ุฌุฏููุง",
			customfcheck : "ุงูุฏุงูุฉ ุงูุดุฎุตูุฉ ูุทููุจุฉ ูู ุญุงูุฉ ุงูุชุญูู ุงูุดุฎุตู"
		}
	},
	view : {
		caption: "ุฑุฃูุช ุงูุชุณุฌููุงุช",
		bClose: "ุบูู"
	},
	del : {
		caption: "ุญุฐู",
		msg: "ุญุฐู ุงูุชุณุฌููุงุช ุงููุฎุชุงุฑุฉ ?",
		bSubmit: "ุญุฐู",
		bCancel: "ุฅูุบุงุก"
	},
	nav : {
		edittext: " ",
		edittitle: "ุชุบููุฑ ุงูุชุณุฌูู ุงููุฎุชุงุฑ",
		addtext:" ",
		addtitle: "ุฅุถุงูุฉ ุชุณุฌูู",
		deltext: " ",
		deltitle: "ุญุฐู ุงูุชุณุฌูู ุงููุฎุชุงุฑ",
		searchtext: " ",
		searchtitle: "ุจุญุซ ุนู ุชุณุฌูู",
		refreshtext: "",
		refreshtitle: "ุชุญุฏูุซ ุงูุฌุฏูู",
		alertcap: "ุชุญุฐูุฑ",
		alerttext: "ูุฑุฌู ุฅุฎุชูุงุฑ ุงูุณุทุฑ",
		viewtext: "",
		viewtitle: "ุฅุธูุงุฑ ุงูุณุทุฑ ุงููุฎุชุงุฑ"
	},
	col : {
		caption: "ุฅุธูุงุฑ/ุฅุฎูุงุก ุงูุฃุนูุฏุฉ",
		bSubmit: "ุชุซุจูุซ",
		bCancel: "ุฅูุบุงุก"
	},
	errors : {
		errcap : "ุฎุทุฃ",
		nourl : "ูุง ููุฌุฏ ุนููุงู ูุญุฏุฏ",
		norecords: "ูุง ููุฌุฏ ุชุณุฌูู ูููุนุงูุฌุฉ",
		model : "ุนุฏุฏ ุงูุนูุงููู (colNames) <> ุนุฏุฏ ุงูุชุณุฌููุงุช (colModel)!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0,00'},
		currency : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0,00'},
		date : {
			dayNames:   [
				"ุงูุฃุญุฏ", "ุงูุฅุซููู", "ุงูุซูุงุซุงุก", "ุงูุฃุฑุจุนุงุก", "ุงูุฎููุณ", "ุงูุฌูุนุฉ", "ุงูุณุจุช",
				"ุงูุฃุญุฏ", "ุงูุฅุซููู", "ุงูุซูุงุซุงุก", "ุงูุฃุฑุจุนุงุก", "ุงูุฎููุณ", "ุงูุฌูุนุฉ", "ุงูุณุจุช"
			],
			monthNames: [
				"ุฌุงููู", "ูููุฑู", "ูุงุฑุณ", "ุฃูุฑูู", "ูุงู", "ุฌูุงู", "ุฌููููุฉ", "ุฃูุช", "ุณุจุชูุจุฑ", "ุฃูุชูุจุฑ", "ููููุจุฑ", "ุฏูุณูุจุฑ",
				"ุฌุงููู", "ูููุฑู", "ูุงุฑุณ", "ุฃูุฑูู", "ูุงู", "ุฌูุงู", "ุฌููููุฉ", "ุฃูุช", "ุณุจุชูุจุฑ", "ุฃูุชูุจุฑ", "ููููุจุฑ", "ุฏูุณูุจุฑ"
			],
			AmPm : ["ุตุจุงุญุง","ูุณุงุกุง","ุตุจุงุญุง","ูุณุงุกุง"],
			S: function (j) {return j == 1 ? 'er' : 'e';},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n/j/Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
