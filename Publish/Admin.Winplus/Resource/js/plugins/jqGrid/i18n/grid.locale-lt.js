﻿;(function($){
/**
 * jqGrid Lithuanian Translation
 * aur1mas aur1mas@devnet.lt
 * http://aur1mas.devnet.lt
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Perลพiลซrima {0} - {1} iลก {2}",
		emptyrecords: "ฤฎraลกลณ nฤra",
		loadtext: "Kraunama...",
		pgtext : "Puslapis {0} iลก {1}"
	},
	search : {
		caption: "Paieลกka...",
		Find: "Ieลกkoti",
		Reset: "Atstatyti",
		odata: [{ oper:'eq', text:"lygu"},{ oper:'ne', text:"nelygu"},{ oper:'lt', text:"maลพiau"},{ oper:'le', text:"maลพiau arba lygu"},{ oper:'gt', text:"daugiau"},{ oper:'ge', text:"daugiau arba lygu"},{ oper:'bw', text:"prasideda"},{ oper:'bn', text:"neprasideda"},{ oper:'in', text:"reikลกmฤ yra"},{ oper:'ni', text:"reikลกmฤs nฤra"},{ oper:'ew', text:"baigiasi"},{ oper:'en', text:"nesibaigia"},{ oper:'cn', text:"yra sudarytas"},{ oper:'nc', text:"nฤra sudarytas"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "visi" },	{ op: "OR",  text: "bet kuris" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "Sukurti ฤฏraลกฤ",
		editCaption: "Redaguoti ฤฏraลกฤ",
		bSubmit: "Iลกsaugoti",
		bCancel: "Atลกaukti",
		bClose: "Uลพdaryti",
		saveData: "Duomenys buvo pakeisti! Iลกsaugoti pakeitimus?",
		bYes : "Taip",
		bNo : "Ne",
		bExit : "Atลกaukti",
		msg: {
			required:"Privalomas laukas",
			number:"ฤฎveskite tinkamฤ numerฤฏ",
			minValue:"reikลกmฤ turi bลซti didesnฤ arba lygi ",
			maxValue:"reikลกmฤ turi bลซti maลพesnฤ arba lygi",
			email: "neteisingas el. paลกto adresas",
			integer: "ฤฎveskite teisingฤ sveikฤjฤฏ skaiฤiลณ",
			date: "ฤฎveskite teisingฤ datฤ",
			url: "blogas adresas. Nepamirลกkite pridฤti ('http://' arba 'https://')",
			nodefined : " nฤra apibrฤลพta!",
			novalue : " turi bลซti graลพinama kokia nors reikลกmฤ!",
			customarray : "Custom f-ja turi grฤลพinti masyvฤ!",
			customfcheck : "Custom f-ja tลซrฤtลณ bลซti sukurta, prieลก bandant jฤ naudoti!"
			
		}
	},
	view : {
		caption: "Perลพiลซrฤti ฤฏraลกus",
		bClose: "Uลพdaryti"
	},
	del : {
		caption: "Iลกtrinti",
		msg: "Iลกtrinti paลพymฤtus ฤฏraลกus(-ฤ)?",
		bSubmit: "Iลกtrinti",
		bCancel: "Atลกaukti"
	},
	nav : {
		edittext: "",
		edittitle: "Redaguoti paลพymฤtฤ eilutฤ",
		addtext:"",
		addtitle: "Pridฤti naujฤ eilutฤ",
		deltext: "",
		deltitle: "Iลกtrinti paลพymฤtฤ eilutฤ",
		searchtext: "",
		searchtitle: "Rasti ฤฏraลกus",
		refreshtext: "",
		refreshtitle: "Perkrauti lentelฤ",
		alertcap: "ฤฎspฤjimas",
		alerttext: "Pasirinkite eilutฤ",
		viewtext: "",
		viewtitle: "Perลพiลซrฤti pasirinktฤ eilutฤ"
	},
	col : {
		caption: "Pasirinkti stulpelius",
		bSubmit: "Gerai",
		bCancel: "Atลกaukti"
	},
	errors : {
		errcap : "Klaida",
		nourl : "Url reikลกmฤ turi bลซti perduota",
		norecords: "Nฤra ฤฏraลกลณ, kuriuos bลซtลณ galima apdoroti",
		model : "colNames skaiฤius <> colModel skaiฤiui!"
	},
	formatter : {
		integer : {thousandsSeparator: "", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: "", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:",", thousandsSeparator: "", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"Sek", "Pir", "Ant", "Tre", "Ket", "Pen", "ล eลก",
				"Sekmadienis", "Pirmadienis", "Antradienis", "Treฤiadienis", "Ketvirtadienis", "Penktadienis", "ล eลกtadienis"
			],
			monthNames: [
				"Sau", "Vas", "Kov", "Bal", "Geg", "Bir", "Lie", "Rugj", "Rugs", "Spa", "Lap", "Gru",
				"Sausis", "Vasaris", "Kovas", "Balandis", "Geguลพฤ", "Birลพelis", "Liepa", "Rugpjลซtis", "Rugsฤjis", "Spalis", "Lapkritis", "Gruodis"
			],
			AmPm : ["am","pm","AM","PM"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n/j/Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
