﻿;(function($){
/**
 * jqGrid Czech Translation
 * Pavel Jirak pavel.jirak@jipas.cz
 * doplnil Thomas Wagner xwagne01@stud.fit.vutbr.cz
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Zobrazeno {0} - {1} z {2} zรกznamลฏ",
	    emptyrecords: "Nenalezeny ลพรกdnรฉ zรกznamy",
		loadtext: "Naฤรญtรกm...",
		pgtext : "Strana {0} z {1}"
	},
	search : {
		caption: "Vyhledรกvรกm...",
		Find: "Hledat",
		Reset: "Reset",
	    odata: [{ oper:'eq', text:"rovno"},{ oper:'ne', text:"nerovno"},{ oper:'lt', text:"menลกรญ"},{ oper:'le', text:"menลกรญ nebo rovno"},{ oper:'gt', text:"vฤtลกรญ"},{ oper:'ge', text:"vฤtลกรญ nebo rovno"},{ oper:'bw', text:"zaฤรญnรก s"},{ oper:'bn', text:"nezaฤรญnรก s"},{ oper:'in', text:"je v"},{ oper:'ni', text:"nenรญ v"},{ oper:'ew', text:"konฤรญ s"},{ oper:'en', text:"nekonฤรญ s"},{ oper:'cn', text:"obsahuje"},{ oper:'nc', text:"neobsahuje"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
	    groupOps: [	{ op: "AND", text: "vลกech" },	{ op: "OR",  text: "nฤkterรฉho z" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "Pลidat zรกznam",
		editCaption: "Editace zรกznamu",
		bSubmit: "Uloลพit",
		bCancel: "Storno",
		bClose: "Zavลรญt",
		saveData: "Data byla zmฤnฤna! Uloลพit zmฤny?",
		bYes : "Ano",
		bNo : "Ne",
		bExit : "Zruลกit",
		msg: {
		    required:"Pole je vyลพadovรกno",
		    number:"Prosรญm, vloลพte validnรญ ฤรญslo",
		    minValue:"hodnota musรญ bรฝt vฤtลกรญ neลพ nebo rovnรก ",
		    maxValue:"hodnota musรญ bรฝt menลกรญ neลพ nebo rovnรก ",
		    email: "nenรญ validnรญ e-mail",
		    integer: "Prosรญm, vloลพte celรฉ ฤรญslo",
			date: "Prosรญm, vloลพte validnรญ datum",
			url: "nenรญ platnou URL. Vyลพadovรกn prefix ('http://' or 'https://')",
			nodefined : " nenรญ definovรกn!",
			novalue : " je vyลพadovรกna nรกvratovรก hodnota!",
			customarray : "Custom function mฤlรก vrรกtit pole!",
			customfcheck : "Custom function by mฤla bรฝt pลรญtomna v pลรญpadฤ custom checking!"
		}
	},
	view : {
	    caption: "Zobrazit zรกznam",
	    bClose: "Zavลรญt"
	},
	del : {
		caption: "Smazat",
		msg: "Smazat vybranรฝ(รฉ) zรกznam(y)?",
		bSubmit: "Smazat",
		bCancel: "Storno"
	},
	nav : {
		edittext: " ",
		edittitle: "Editovat vybranรฝ ลรกdek",
		addtext:" ",
		addtitle: "Pลidat novรฝ ลรกdek",
		deltext: " ",
		deltitle: "Smazat vybranรฝ zรกznam ",
		searchtext: " ",
		searchtitle: "Najรญt zรกznamy",
		refreshtext: "",
		refreshtitle: "Obnovit tabulku",
		alertcap: "Varovรกnรญ",
		alerttext: "Prosรญm, vyberte ลรกdek",
		viewtext: "",
		viewtitle: "Zobrazit vybranรฝ ลรกdek"
	},
	col : {
		caption: "Zobrazit/Skrรฝt sloupce",
		bSubmit: "Uloลพit",
		bCancel: "Storno"	
	},
	errors : {
		errcap : "Chyba",
		nourl : "Nenรญ nastavena url",
		norecords: "ลฝรกdnรฉ zรกznamy ke zpracovรกnรญ",
		model : "Dรฉlka colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"Ne", "Po", "รt", "St", "ฤt", "Pรก", "So",
				"Nedฤle", "Pondฤlรญ", "รterรฝ", "Stลeda", "ฤtvrtek", "Pรกtek", "Sobota"
			],
			monthNames: [
				"Led", "รno", "Bลe", "Dub", "Kvฤ", "ฤer", "ฤvc", "Srp", "Zรกล", "ลรญj", "Lis", "Pro",
				"Leden", "รnor", "Bลezen", "Duben", "Kvฤten", "ฤerven", "ฤervenec", "Srpen", "Zรกลรญ", "ลรญjen", "Listopad", "Prosinec"
			],
			AmPm : ["do","od","DO","OD"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
		        ISO8601Long:"Y-m-d H:i:s",
		        ISO8601Short:"Y-m-d",
		        ShortDate: "n/j/Y",
		        LongDate: "l, F d, Y",
		        FullDateTime: "l, F d, Y g:i:s A",
		        MonthDay: "F d",
		        ShortTime: "g:i A",
		        LongTime: "g:i:s A",
		        SortableDateTime: "Y-m-d\\TH:i:s",
		        UniversalSortableDateTime: "Y-m-d H:i:sO",
		        YearMonth: "F, Y"
		    },
		    reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
