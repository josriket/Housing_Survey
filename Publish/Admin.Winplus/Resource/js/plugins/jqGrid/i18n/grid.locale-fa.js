﻿;(function ($) {
/**
 * jqGrid Persian Translation
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
	$.jgrid = $.jgrid || {};
	$.extend($.jgrid,{
        defaults: {
            recordtext: "ููุงุจุด {0} - {1} ุงุฒ {2}",
            emptyrecords: "ุฑฺฉูุฑุฏ฻ ฻ุงูุช ูุดุฏ",
            loadtext: "ุจุงุฑฺฏุฒุงุฑู...",
            pgtext: "ุตูุญู {0} ุงุฒ {1}"
        },
        search: {
            caption: "ุฌุณุชุฌู...",
            Find: "ูุงูุชู ูุง",
            Reset: "ุงุฒ ูู",
            odata: [{ oper:'eq', text:"ุจุฑุงุจุฑ"},{ oper:'ne', text:"ูุง ุจุฑุงุจุฑ"},{ oper:'lt', text:"ุจู"},{ oper:'le', text:"ฺฉูฺฺฉุชุฑ"},{ oper:'gt', text:"ุงุฒ"},{ oper:'ge', text:"ุจุฒุฑฺฏุชุฑ"},{ oper:'bw', text:"ุดุฑูุน ุจุง"},{ oper:'bn', text:"ุดุฑูุน ูุดูุฏ ุจุง"},{ oper:'in', text:"ูุจุงุดุฏ"},{ oper:'ni', text:"ุนุถู ุง฻ู ูุจุงุดุฏ"},{ oper:'ew', text:"ุงุชูุงู ุจุง"},{ oper:'en', text:"ุชูุงู ูุดูุฏ ุจุง"},{ oper:'cn', text:"ุญุงู฻"},{ oper:'nc', text:"ูุจุงุดุฏ ุญุงู฻"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
            groupOps: [{
                op: "AND",
                text: "ฺฉู"
            },
            {
                op: "OR",
                text: "ูุฌููุน"
            }],
			operandTitle : "Click to select search operation.",
			resetTitle : "Reset Search Value"
        },
        edit: {
            addCaption: "ุงุถุงูู ฺฉุฑุฏู ุฑฺฉูุฑุฏ",
            editCaption: "ููุฑุงูุด ุฑฺฉูุฑุฏ",
            bSubmit: "ุซุจุช",
            bCancel: "ุงูุตุฑุงู",
            bClose: "ุจุณุชู",
            saveData: "ุฏ฻ุชุง ุชุน฻฻ุฑ ฺฉุฑุฏ! ุฐุฎ฻ุฑู ุดูุฏุ",
            bYes: "ุจูู",
            bNo: "ุฎ฻ุฑ",
            bExit: "ุงูุตุฑุงู",
            msg: {
                required: "ูููุฏูุง ุจุงูุฏ ุฎุชูุง ูพุฑ ุดููุฏ",
                number: "ูุทูุง ุนุฏุฏ ูุนุชุจุฑ ูุงุฑุฏ ฺฉููุฏ",
                minValue: "ููุฏุงุฑ ูุงุฑุฏ ุดุฏู ุจุงูุฏ ุจุฒุฑฺฏุชุฑ ูุง ูุณุงูู ุจุง",
                maxValue: "ููุฏุงุฑ ูุงุฑุฏ ุดุฏู ุจุงูุฏ ฺฉูฺฺฉุชุฑ ูุง ูุณุงูู",
                email: "ูพุณุช ุงูฺฉุชุฑูููฺฉ ูุงุฑุฏ ุดุฏู ูุนุชุจุฑ ููุณุช",
                integer: "ูุทูุง ูฺฉ ุนุฏุฏ ุตุญูุญ ูุงุฑุฏ ฺฉููุฏ",
                date: "ูุทูุง ูฺฉ ุชุงุฑูุฎ ูุนุชุจุฑ ูุงุฑุฏ ฺฉููุฏ",
                url: "ุง฻ู ุขุฏุฑุณ ุตุญ฻ุญ ูู฻ ุจุงุดุฏ. ูพ฻ุดููุฏ ู฻ุงุฒ ุงุณุช ('http://' ฻ุง 'https://')",
                nodefined: " ุชุนุฑ฻ู ูุดุฏู!",
                novalue: " ููุฏุงุฑ ุจุฑฺฏุดุช฻ ุงุฌุจุงุฑ฻ ุงุณุช!",
                customarray: "ุชุงุจุน ุดูุง ุจุง฻ุฏ ููุฏุงุฑ ุขุฑุง฻ู ุฏุงุดุชู ุจุงุดุฏ!",
                customfcheck: "ุจุฑุง฻ ุฏุงุดุชู ูุชุฏ ุฏูุฎูุงู ุดูุง ุจุง฻ุฏ ุณุทูู ุจุง ฺฺฉ฻ูฺฏ ุฏูุฎูุงู ุฏุงุดุชู ุจุงุด฻ุฏ!"
            }
        },
        view: {
            caption: "ููุง฻ุด ุฑฺฉูุฑุฏ",
            bClose: "ุจุณุชู"
        },
        del: {
            caption: "ุญุฐู",
            msg: "ุงุฒ ุญุฐู ฺฏุฒููู ูุงู ุงูุชุฎุงุจ ุดุฏู ูุทูุฆู ูุณุชูุฏุ",
            bSubmit: "ุญุฐู",
            bCancel: "ุงุจุทุงู"
        },
        nav: {
            edittext: " ",
            edittitle: "ููุฑุงูุด ุฑุฏูู ูุงู ุงูุชุฎุงุจ ุดุฏู",
            addtext: " ",
            addtitle: "ุงูุฒูุฏู ุฑุฏูู ุฌุฏูุฏ",
            deltext: " ",
            deltitle: "ุญุฐู ุฑุฏุจู ูุงู ุงูุช฻ุงุจ ุดุฏู",
            searchtext: " ",
            searchtitle: "ุฌุณุชุฌูู ุฑุฏูู",
            refreshtext: "",
            refreshtitle: "ุจุงุฒูุงุจู ูุฌุฏุฏ ุตูุญู",
            alertcap: "ุงุฎุทุงุฑ",
            alerttext: "ูุทูุง ูฺฉ ุฑุฏูู ุงูุชุฎุงุจ ฺฉููุฏ",
            viewtext: "",
            viewtitle: "ููุง฻ุด ุฑฺฉูุฑุฏ ูุง฻ ุงูุชุฎุงุจ ุดุฏู"
        },
        col: {
            caption: "ููุงูุด/ุนุฏู ููุงูุด ุณุชูู",
            bSubmit: "ุซุจุช",
            bCancel: "ุงูุตุฑุงู"
        },
        errors: {
            errcap: "ุฎุทุง",
            nourl: "ููฺ ุขุฏุฑุณู ุชูุธูู ูุดุฏู ุงุณุช",
            norecords: "ููฺ ุฑฺฉูุฑุฏู ุจุฑุงู ูพุฑุฏุงุฒุด ููุฌูุฏ ููุณุช",
            model: "ุทูู ูุงู ุณุชูู ูุง ูุญุงูู ุณุชูู ูุงู ูุฏู ูู ุจุงุดุฏ!"
        },
        formatter: {
            integer: {
                thousandsSeparator: " ",
                defaultValue: "0"
            },
            number: {
                decimalSeparator: ".",
                thousandsSeparator: " ",
                decimalPlaces: 2,
                defaultValue: "0.00"
            },
            currency: {
                decimalSeparator: ".",
                thousandsSeparator: " ",
                decimalPlaces: 2,
                prefix: "",
                suffix: "",
                defaultValue: "0"
            },
            date: {
                dayNames: ["ูฺฉ", "ุฏู", "ุณู", "ฺูุงุฑ", "ูพูุฌ", "ุฌูุน", "ุดูุจ", "ูฺฉุดูุจู", "ุฏูุดูุจู", "ุณู ุดูุจู", "ฺูุงุฑุดูุจู", "ูพูุฌุดูุจู", "ุฌูุนู", "ุดูุจู"],
                monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "ฺุงูููู", "ููุฑูู", "ูุงุฑุณ", "ุขูุฑูู", "ูู", "ฺูุฆู", "ฺูุฆูู", "ุงูุช", "ุณูพุชุงูุจุฑ", "ุงฺฉุชุจุฑ", "ููุงูุจุฑ", "December"],
                AmPm: ["ุจ.ุธ", "ุจ.ุธ", "ู.ุธ", "ู.ุธ"],
                S: function (b) {
                    return b < 11 || b > 13 ? ["st", "nd", "rd", "th"][Math.min((b - 1) % 10, 3)] : "th"
                },
                srcformat: "Y-m-d",
                newformat: "d/m/Y",
				parseRe : /[#%\\\/:_;.,\t\s-]/,
                masks: {
                    ISO8601Long: "Y-m-d H:i:s",
                    ISO8601Short: "Y-m-d",
                    ShortDate: "n/j/Y",
                    LongDate: "l, F d, Y",
                    FullDateTime: "l, F d, Y g:i:s A",
                    MonthDay: "F d",
                    ShortTime: "g:i A",
                    LongTime: "g:i:s A",
                    SortableDateTime: "Y-m-d\\TH:i:s",
                    UniversalSortableDateTime: "Y-m-d H:i:sO",
                    YearMonth: "F, Y"
                },
                reformatAfterEdit: false
            },
            baseLinkUrl: "",
            showAction: "ููุงูุด",
            target: "",
            checkbox: {
                disabled: true
            },
            idName: "id"
        }
    });
})(jQuery);