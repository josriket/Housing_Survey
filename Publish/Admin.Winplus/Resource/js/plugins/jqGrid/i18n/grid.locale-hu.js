﻿;(function($){
/**
 * jqGrid Hungarian Translation
 * ลrszigety รdรกm udx6bs@freemail.hu
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/

$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Oldal {0} - {1} / {2}",
		emptyrecords: "Nincs talรกlat",
		loadtext: "Betรถltรฉs...",
		pgtext : "Oldal {0} / {1}"
	},
	search : {
		caption: "Keresรฉs...",
		Find: "Keres",
		Reset: "Alapรฉrtelmezett",
		odata: [{ oper:'eq', text:"egyenlล"},{ oper:'ne', text:"nem egyenlล"},{ oper:'lt', text:"kevesebb"},{ oper:'le', text:"kevesebb vagy egyenlล"},{ oper:'gt', text:"nagyobb"},{ oper:'ge', text:"nagyobb vagy egyenlล"},{ oper:'bw', text:"ezzel kezdลdik"},{ oper:'bn', text:"nem ezzel kezdลdik"},{ oper:'in', text:"tartalmaz"},{ oper:'ni', text:"nem tartalmaz"},{ oper:'ew', text:"vรฉgzลdik"},{ oper:'en', text:"nem vรฉgzลdik"},{ oper:'cn', text:"tartalmaz"},{ oper:'nc', text:"nem tartalmaz"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "all" },	{ op: "OR",  text: "any" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "รj tรฉtel",
		editCaption: "Tรฉtel szerkesztรฉse",
		bSubmit: "Mentรฉs",
		bCancel: "Mรฉgse",
		bClose: "Bezรกrรกs",
		saveData: "A tรฉtel megvรกltozott! Tรฉtel mentรฉse?",
		bYes : "Igen",
		bNo : "Nem",
		bExit : "Mรฉgse",
		msg: {
			required:"Kรถtelezล mezล",
			number:"Kรฉrjรผk, adjon meg egy helyes szรกmot",
			minValue:"Nagyobb vagy egyenlลnek kell lenni mint ",
			maxValue:"Kisebb vagy egyenlลnek kell lennie mint",
			email: "hibรกs emailcรญm",
			integer: "Kรฉrjรผk adjon meg egy helyes egรฉsz szรกmot",
			date: "Kรฉrjรผk adjon meg egy helyes dรกtumot",
			url: "nem helyes cรญm. Elลtag kรถtelezล ('http://' vagy 'https://')",
			nodefined : " nem definiรกlt!",
			novalue : " visszatรฉrรฉsi รฉrtรฉk kรถtelezล!!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
			
		}
	},
	view : {
		caption: "Tรฉtel megtekintรฉse",
		bClose: "Bezรกrรกs"
	},
	del : {
		caption: "Tรถrlรฉs",
		msg: "Kivรกlaztott tรฉtel(ek) tรถrlรฉse?",
		bSubmit: "Tรถrlรฉs",
		bCancel: "Mรฉgse"
	},
	nav : {
		edittext: "",
		edittitle: "Tรฉtel szerkesztรฉse",
		addtext:"",
		addtitle: "รj tรฉtel hozzรกadรกsa",
		deltext: "",
		deltitle: "Tรฉtel tรถrlรฉse",
		searchtext: "",
		searchtitle: "Keresรฉs",
		refreshtext: "",
		refreshtitle: "Frissรญtรฉs",
		alertcap: "Figyelmeztetรฉs",
		alerttext: "Kรฉrem vรกlasszon tรฉtelt.",
		viewtext: "",
		viewtitle: "Tรฉtel megtekintรฉse"
	},
	col : {
		caption: "Oszlopok kivรกlasztรกsa",
		bSubmit: "Ok",
		bCancel: "Mรฉgse"
	},
	errors : {
		errcap : "Hiba",
		nourl : "Nincs URL beรกllรญtva",
		norecords: "Nincs feldolgozรกsra vรกrรณ tรฉtel",
		model : "colNames รฉs colModel hossza nem egyenlล!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0,00'},
		currency : {decimalSeparator:",", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0,00'},
		date : {
			dayNames:   [
				"Va", "Hรฉ", "Ke", "Sze", "Csรผ", "Pรฉ", "Szo",
				"Vasรกrnap", "Hรฉtfล", "Kedd", "Szerda", "Csรผtรถrtรถk", "Pรฉntek", "Szombat"
			],
			monthNames: [
				"Jan", "Feb", "Mรกr", "รpr", "Mรกj", "Jรบn", "Jรบl", "Aug", "Szep", "Okt", "Nov", "Dec",
				"Januรกr", "Februรกr", "Mรกrcius", "รprili", "Mรกjus", "Jรบnius", "Jรบlius", "Augusztus", "Szeptember", "Oktรณber", "November", "December"
			],
			AmPm : ["de","du","DE","DU"],
			S: function (j) {return '.-ik';},
			srcformat: 'Y-m-d',
			newformat: 'Y/m/d',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "Y/j/n",
				LongDate: "Y. F hรณ d., l",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "a g:i",
				LongTime: "a g:i:s",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "Y, F"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
