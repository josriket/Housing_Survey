﻿;(function($){
/**
 * jqGrid Hebrew Translation
 * Shuki Shukrun shukrun.shuki@gmail.com
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "ืืฆืื {0} - {1} ืืชืื {2}",
		emptyrecords: "ืืื ืจืฉืืืืช ืืืฆืื",
		loadtext: "ืืืขื...",
		pgtext : "ืืฃ {0} ืืชืื {1}"
	},
	search : {
		caption: "ืืืคืฉ...",
		Find: "ืืคืฉ",
		Reset: "ืืชืื",
		odata: [{ oper:'eq', text:"ืฉืืื"},{ oper:'ne', text:"ืื ืฉืืื"},{ oper:'lt', text:"ืงืื"},{ oper:'le', text:"ืงืื ืื ืฉืืื"},{ oper:'gt', text:"ืืืื"},{ oper:'ge', text:"ืืืื ืื ืฉืืื"},{ oper:'bw', text:"ืืชืืื ื"},{ oper:'bn', text:"ืื ืืชืืื ื"},{ oper:'in', text:"ื ืืฆื ื"},{ oper:'ni', text:"ืื ื ืืฆื ื"},{ oper:'ew', text:"ืืกืชืืื ื"},{ oper:'en', text:"ืื ืืกืชืืื ื"},{ oper:'cn', text:"ืืืื"},{ oper:'nc', text:"ืื ืืืื"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "ืืื" },	{ op: "OR",  text: "ืืื ื" }],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "ืืืกืฃ ืจืฉืืื",
		editCaption: "ืขืจืื ืจืฉืืื",
		bSubmit: "ืฉืื",
		bCancel: "ืืื",
		bClose: "ืกืืืจ",
		saveData: "ื ืชืื ืื ืืฉืชื ื! ืืฉืืืจ?",
		bYes : "ืื",
		bNo : "ืื",
		bExit : "ืืื",
		msg: {
			required:"ืฉืื ืืืื",
			number:"ืื ื, ืืื ืก ืืกืคืจ ืชืงืื",
			minValue:"ืขืจื ืฆืจืื ืืืืืช ืืืื ืื ืฉืืื ื ",
			maxValue:"ืขืจื ืฆืจืื ืืืืืช ืงืื ืื ืฉืืื ื ",
			email: "ืืื ืื ืืชืืืช ืืืืื ืชืงืื ื",
			integer: "ืื ื, ืืื ืก ืืกืคืจ ืฉืื",
			date: "ืื ื, ืืื ืก ืชืืจืื ืชืงืื",
			url: "ืืืชืืืช ืืื ื ืชืงืื ื. ืืจืืฉื ืชืืืืืช ('http://' ืื 'https://')",
			nodefined : " is not defined!",
			novalue : " return value is required!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
		}
	},
	view : {
		caption: "ืืฆื ืจืฉืืื",
		bClose: "ืกืืืจ"
	},
	del : {
		caption: "ืืืง",
		msg: "ืืื ืืืืืง ืืช ืืจืฉืืื/ืืช ืืืกืืื ืืช?",
		bSubmit: "ืืืง",
		bCancel: "ืืื"
	},
	nav : {
		edittext: "",
		edittitle: "ืขืจืื ืฉืืจื ืืกืืื ืช",
		addtext:"",
		addtitle: "ืืืกืฃ ืฉืืจื ืืืฉื",
		deltext: "",
		deltitle: "ืืืง ืฉืืจื ืืกืืื ืช",
		searchtext: "",
		searchtitle: "ืืคืฉ ืจืฉืืืืช",
		refreshtext: "",
		refreshtitle: "ืืขื ืืจืื ืืืืฉ",
		alertcap: "ืืืืจื",
		alerttext: "ืื ื, ืืืจ ืฉืืจื",
		viewtext: "",
		viewtitle: "ืืฆื ืฉืืจื ืืกืืื ืช"
	},
	col : {
		caption: "ืืฆื/ืืกืชืจ ืขืืืืืช",
		bSubmit: "ืฉืื",
		bCancel: "ืืื"
	},
	errors : {
		errcap : "ืฉืืืื",
		nourl : "ืื ืืืืืจื ืืชืืืช url",
		norecords: "ืืื ืจืฉืืืืช ืืขืื",
		model : "ืืืจื ืฉื colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"ื", "ื", "ื", "ื", "ื", "ื", "ืฉ",
				"ืจืืฉืื", "ืฉื ื", "ืฉืืืฉื", "ืจืืืขื", "ืืืืฉื", "ืฉืืฉื", "ืฉืืช"
			],
			monthNames: [
				"ืื ื", "ืคืืจ", "ืืจืฅ", "ืืคืจ", "ืืื", "ืืื ", "ืืื", "ืืื", "ืกืคื", "ืืืง", "ื ืื", "ืืฆื",
				"ืื ืืืจ", "ืคืืจืืืจ", "ืืจืฅ", "ืืคืจืื", "ืืื", "ืืื ื", "ืืืื", "ืืืืืกื", "ืกืคืืืืจ", "ืืืงืืืืจ", "ื ืืืืืจ", "ืืฆืืืจ"
			],
			AmPm : ["ืืคื ื ืืฆืืจืื","ืืืจ ืืฆืืจืื","ืืคื ื ืืฆืืจืื","ืืืจ ืืฆืืจืื"],
			S: function (j) {return j < 11 || j > 13 ? ['', '', '', ''][Math.min((j - 1) % 10, 3)] : ''},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "n/j/Y",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
