﻿;(function($){
/**
 * jqGrid Chinese (Taiwan) Translation for v4.2
 * linquize
 * https://github.com/linquize/jqGrid
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * 
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "{0} - {1} ๅฑ {2} ๆข",
		emptyrecords: "ๆฒๆ่จ้",
		loadtext: "่ผๅฅไธญ...",
		pgtext : " {0} ๅฑ {1} ้ "
	},
	search : {
		caption: "ๆๅฐ...",
		Find: "ๆๅฐ",
		Reset: "้่จญ",
		odata: [{ oper:'eq', text:"็ญๆผ "},{ oper:'ne', text:"ไธ็ญๆผ "},{ oper:'lt', text:"ๅฐๆผ "},{ oper:'le', text:"ๅฐๆผ็ญๆผ "},{ oper:'gt', text:"ๅคงๆผ "},{ oper:'ge', text:"ๅคงๆผ็ญๆผ "},{ oper:'bw', text:"้ๅงๆผ "},{ oper:'bn', text:"ไธ้ๅงๆผ "},{ oper:'in', text:"ๅจๅถไธญ "},{ oper:'ni', text:"ไธๅจๅถไธญ "},{ oper:'ew', text:"็ตๆๆผ "},{ oper:'en', text:"ไธ็ตๆๆผ "},{ oper:'cn', text:"ๅๅซ "},{ oper:'nc', text:"ไธๅๅซ "},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
		groupOps: [	{ op: "AND", text: "ๆๆ" },	{ op: "OR",  text: "ไปปไธ" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "ๆฐๅข่จ้",
		editCaption: "็ทจ่ผฏ่จ้",
		bSubmit: "ๆไบค",
		bCancel: "ๅๆถ",
		bClose: "้้",
		saveData: "่ณๆๅทฒๆน่ฎ๏ผๆฏๅฆๅฒๅญ๏ผ",
		bYes : "ๆฏ",
		bNo : "ๅฆ",
		bExit : "ๅๆถ",
		msg: {
			required:"ๆญคๆฌๅฟ่ฆ",
			number:"่ซ่ผธๅฅๆๆ็ๆธๅญ",
			minValue:"ๅผๅฟ้ ๅคงๆผ็ญๆผ ",
			maxValue:"ๅผๅฟ้ ๅฐๆผ็ญๆผ ",
			email: "ไธๆฏๆๆ็e-mailๅฐๅ",
			integer: "่ซ่ผธๅฅๆๆๆดๆฐ",
			date: "่ซ่ผธๅฅๆๆๆ้",
			url: "็ถฒๅ็กๆใๅ็ถดๅฟ้ ็บ ('http://' ๆ 'https://')",
			nodefined : " ๆชๅฎ็พฉ๏ผ",
			novalue : " ้่ฆๅณๅๅผ๏ผ",
			customarray : "่ช่จๅฝๆธๆๅณๅ้ฃๅ๏ผ",
			customfcheck : "่ช่จๆชขๆฅๆๆ่ช่จๅฝๆธ๏ผ"
			
		}
	},
	view : {
		caption: "ๆฅ็่จ้",
		bClose: "้้"
	},
	del : {
		caption: "ๅช้ค",
		msg: "ๅช้คๅทฒ้ธ่จ้๏ผ",
		bSubmit: "ๅช้ค",
		bCancel: "ๅๆถ"
	},
	nav : {
		edittext: "",
		edittitle: "็ทจ่ผฏๅทฒ้ธๅ",
		addtext:"",
		addtitle: "ๆฐๅขๅ",
		deltext: "",
		deltitle: "ๅช้คๅทฒ้ธๅ",
		searchtext: "",
		searchtitle: "ๆๅฐ่จ้",
		refreshtext: "",
		refreshtitle: "้ๆฐๆด็่กจๆ ผ",
		alertcap: "่ญฆๅ",
		alerttext: "่ซ้ธๆๅ",
		viewtext: "",
		viewtitle: "ๆชข่ฆๅทฒ้ธๅ"
	},
	col : {
		caption: "้ธๆๆฌ",
		bSubmit: "็ขบๅฎ",
		bCancel: "ๅๆถ"
	},
	errors : {
		errcap : "้ฏ่ชค",
		nourl : "ๆช่จญๅฎURL",
		norecords: "็ก้่ฆ่็็่จ้",
		model : "colNames ๅ colModel ้ทๅบฆไธๅ๏ผ"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"ๆฅ", "ไธ", "ไบ", "ไธ", "ๅ", "ไบ", "ๅญ",
		         "ๆๆๆฅ", "ๆๆไธ", "ๆๆไบ", "ๆๆไธ", "ๆๆๅ", "ๆๆไบ", "ๆๆๅญ"
			],
			monthNames: [
				"ไธ", "ไบ", "ไธ", "ๅ", "ไบ", "ๅญ", "ไธ", "ๅซ", "ไน", "ๅ", "ๅไธ", "ๅไบ",
				"ไธๆ", "ไบๆ", "ไธๆ", "ๅๆ", "ไบๆ", "ๅญๆ", "ไธๆ", "ๅซๆ", "ไนๆ", "ๅๆ", "ๅไธๆ", "ๅไบๆ"
			],
			AmPm : ["ไธๅ","ไธๅ","ไธๅ","ไธๅ"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th';},
			srcformat: 'Y-m-d',
			newformat: 'm-d-Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
				ISO8601Long:"Y-m-d H:i:s",
				ISO8601Short:"Y-m-d",
				ShortDate: "Y/j/n",
				LongDate: "l, F d, Y",
				FullDateTime: "l, F d, Y g:i:s A",
				MonthDay: "F d",
				ShortTime: "g:i A",
				LongTime: "g:i:s A",
				SortableDateTime: "Y-m-d\\TH:i:s",
				UniversalSortableDateTime: "Y-m-d H:i:sO",
				YearMonth: "F, Y"
			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
