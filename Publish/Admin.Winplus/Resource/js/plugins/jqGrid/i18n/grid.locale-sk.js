﻿;(function($){
/**
 * jqGrid Slovak Translation
 * Milan Cibulka
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = $.jgrid || {};
$.extend($.jgrid,{
	defaults : {
		recordtext: "Zobrazenรฝch {0} - {1} z {2} zรกznamov",
	    emptyrecords: "Neboli nรกjdenรฉ ลพiadne zรกznamy",
		loadtext: "Naฤรญtรกm...",
		pgtext : "Strana {0} z {1}"
	},
	search : {
		caption: "Vyhฤพadรกvam...",
		Find: "Hฤพadaลฅ",
		Reset: "Reset",
	    odata: [{ oper:'eq', text:"rovnรก sa"},{ oper:'ne', text:"nerovnรก sa"},{ oper:'lt', text:"menลกie"},{ oper:'le', text:"menลกie alebo rovnajรบce sa"},{ oper:'gt', text:"vรคฤลกie"},{ oper:'ge', text:"vรคฤลกie alebo rovnajรบce sa"},{ oper:'bw', text:"zaฤรญna s"},{ oper:'bn', text:"nezaฤรญna s"},{ oper:'in', text:"je v"},{ oper:'ni', text:"nie je v"},{ oper:'ew', text:"konฤรญ s"},{ oper:'en', text:"nekonฤรญ s"},{ oper:'cn', text:"obahuje"},{ oper:'nc', text:"neobsahuje"},{ oper:'nu', text:'is null'},{ oper:'nn', text:'is not null'}],
	    groupOps: [	{ op: "AND", text: "vลกetkรฝch" },	{ op: "OR",  text: "niektorรฉho z" }	],
		operandTitle : "Click to select search operation.",
		resetTitle : "Reset Search Value"
	},
	edit : {
		addCaption: "Pridaลฅ zรกznam",
		editCaption: "Editรกcia zรกznamov",
		bSubmit: "Uloลพiลฅ",
		bCancel: "Storno",
		bClose: "Zavrieลฅ",
		saveData: "รdaje boli zmenenรฉ! Uloลพiลฅ zmeny?",
		bYes : "Ano",
		bNo : "Nie",
		bExit : "Zruลกiลฅ",
		msg: {
		    required:"Pole je poลพadovanรฉ",
		    number:"Prosรญm, vloลพte valรญdne ฤรญslo",
		    minValue:"hodnota musรญ bรฝลฅ vรคฤลกia ako alebo rovnรก ",
		    maxValue:"hodnota musรญ bรฝลฅ menลกia ako alebo rovnรก ",
		    email: "nie je valรญdny e-mail",
		    integer: "Prosรญm, vloลพte celรฉ ฤรญslo",
			date: "Prosรญm, vloลพte valรญdny dรกtum",
			url: "nie je platnou URL. Poลพadovanรฝ prefix ('http://' alebo 'https://')",
			nodefined : " nie je definovanรฝ!",
			novalue : " je vyลพadovanรก nรกvratovรก hodnota!",
			customarray : "Custom function mala vrรกtiลฅ pole!",
			customfcheck : "Custom function by mala byลฅ prรญtomnรก v prรญpade custom checking!"
		}
	},
	view : {
	    caption: "Zobraziลฅ zรกznam",
	    bClose: "Zavrieลฅ"
	},
	del : {
		caption: "Zmazaลฅ",
		msg: "Zmazaลฅ vybranรฝ(รฉ) zรกznam(y)?",
		bSubmit: "Zmazaลฅ",
		bCancel: "Storno"
	},
	nav : {
		edittext: " ",
		edittitle: "Editovaลฅ vybranรฝ riadok",
		addtext:" ",
		addtitle: "Pridaลฅ novรฝ riadek",
		deltext: " ",
		deltitle: "Zmazaลฅ vybranรฝ zรกznam ",
		searchtext: " ",
		searchtitle: "Nรกjsลฅ zรกznamy",
		refreshtext: "",
		refreshtitle: "Obnoviลฅ tabuฤพku",
		alertcap: "Varovanie",
		alerttext: "Prosรญm, vyberte riadok",
		viewtext: "",
		viewtitle: "Zobraziลฅ vybranรฝ riadok"
	},
	col : {
		caption: "Zobrazit/Skrรฝลฅ stฤบpce",
		bSubmit: "Uloลพiลฅ",
		bCancel: "Storno"	
	},
	errors : {
		errcap : "Chyba",
		nourl : "Nie je nastavenรก url",
		norecords: "ลฝiadne zรกznamy k spracovaniu",
		model : "Dฤบลพka colNames <> colModel!"
	},
	formatter : {
		integer : {thousandsSeparator: " ", defaultValue: '0'},
		number : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, defaultValue: '0.00'},
		currency : {decimalSeparator:".", thousandsSeparator: " ", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
		date : {
			dayNames:   [
				"Ne", "Po", "Ut", "St", "ล t", "Pi", "So",
				"Nedela", "Pondelok", "Utorok", "Streda", "ล tvrtok", "Piatek", "Sobota"
			],
			monthNames: [
				"Jan", "Feb", "Mar", "Apr", "Mรกj", "Jรบn", "Jรบl", "Aug", "Sep", "Okt", "Nov", "Dec",
				"Januรกr", "Februรกr", "Marec", "Aprรญl", "Mรกj", "Jรบn", "Jรบl", "August", "September", "Oktรณber", "November", "December"
			],
			AmPm : ["do","od","DO","OD"],
			S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'},
			srcformat: 'Y-m-d',
			newformat: 'd/m/Y',
			parseRe : /[#%\\\/:_;.,\t\s-]/,
			masks : {
		        ISO8601Long:"Y-m-d H:i:s",
		        ISO8601Short:"Y-m-d",
		        ShortDate: "n/j/Y",
		        LongDate: "l, F d, Y",
		        FullDateTime: "l, F d, Y g:i:s A",
		        MonthDay: "F d",
		        ShortTime: "g:i A",
		        LongTime: "g:i:s A",
		        SortableDateTime: "Y-m-d\\TH:i:s",
		        UniversalSortableDateTime: "Y-m-d H:i:sO",
		        YearMonth: "F, Y"
		    },
		    reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
});
})(jQuery);
