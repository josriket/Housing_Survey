﻿// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode("apl", function() {
  var builtInOps = {
    ".": "innerProduct",
    "\\": "scan",
    "/": "reduce",
    "โฟ": "reduce1Axis",
    "โ": "scan1Axis",
    "ยจ": "each",
    "โฃ": "power"
  };
  var builtInFuncs = {
    "+": ["conjugate", "add"],
    "โ": ["negate", "subtract"],
    "ร": ["signOf", "multiply"],
    "รท": ["reciprocal", "divide"],
    "โ": ["ceiling", "greaterOf"],
    "โ": ["floor", "lesserOf"],
    "โฃ": ["absolute", "residue"],
    "โณ": ["indexGenerate", "indexOf"],
    "?": ["roll", "deal"],
    "โ": ["exponentiate", "toThePowerOf"],
    "โ": ["naturalLog", "logToTheBase"],
    "โ": ["piTimes", "circularFuncs"],
    "!": ["factorial", "binomial"],
    "โน": ["matrixInverse", "matrixDivide"],
    "<": [null, "lessThan"],
    "โค": [null, "lessThanOrEqual"],
    "=": [null, "equals"],
    ">": [null, "greaterThan"],
    "โฅ": [null, "greaterThanOrEqual"],
    "โ ": [null, "notEqual"],
    "โก": ["depth", "match"],
    "โข": [null, "notMatch"],
    "โ": ["enlist", "membership"],
    "โท": [null, "find"],
    "โช": ["unique", "union"],
    "โฉ": [null, "intersection"],
    "โผ": ["not", "without"],
    "โจ": [null, "or"],
    "โง": [null, "and"],
    "โฑ": [null, "nor"],
    "โฒ": [null, "nand"],
    "โด": ["shapeOf", "reshape"],
    ",": ["ravel", "catenate"],
    "โช": [null, "firstAxisCatenate"],
    "โฝ": ["reverse", "rotate"],
    "โ": ["axis1Reverse", "axis1Rotate"],
    "โ": ["transpose", null],
    "โ": ["first", "take"],
    "โ": [null, "drop"],
    "โ": ["enclose", "partitionWithAxis"],
    "โ": ["diclose", "pick"],
    "โท": [null, "index"],
    "โ": ["gradeUp", null],
    "โ": ["gradeDown", null],
    "โค": ["encode", null],
    "โฅ": ["decode", null],
    "โ": ["format", "formatByExample"],
    "โ": ["execute", null],
    "โฃ": ["stop", "left"],
    "โข": ["pass", "right"]
  };

  var isOperator = /[\.\/โฟโยจโฃ]/;
  var isNiladic = /โฌ/;
  var isFunction = /[\+โรรทโโโฃโณ\?โโโ!โน<โค=>โฅโ โกโขโโทโชโฉโผโจโงโฑโฒโด,โชโฝโโโโโโโทโโโคโฅโโโฃโข]/;
  var isArrow = /โ/;
  var isComment = /[โ#].*$/;

  var stringEater = function(type) {
    var prev;
    prev = false;
    return function(c) {
      prev = c;
      if (c === type) {
        return prev === "\\";
      }
      return true;
    };
  };
  return {
    startState: function() {
      return {
        prev: false,
        func: false,
        op: false,
        string: false,
        escape: false
      };
    },
    token: function(stream, state) {
      var ch, funcName, word;
      if (stream.eatSpace()) {
        return null;
      }
      ch = stream.next();
      if (ch === '"' || ch === "'") {
        stream.eatWhile(stringEater(ch));
        stream.next();
        state.prev = true;
        return "string";
      }
      if (/[\[{\(]/.test(ch)) {
        state.prev = false;
        return null;
      }
      if (/[\]}\)]/.test(ch)) {
        state.prev = true;
        return null;
      }
      if (isNiladic.test(ch)) {
        state.prev = false;
        return "niladic";
      }
      if (/[ยฏ\d]/.test(ch)) {
        if (state.func) {
          state.func = false;
          state.prev = false;
        } else {
          state.prev = true;
        }
        stream.eatWhile(/[\w\.]/);
        return "number";
      }
      if (isOperator.test(ch)) {
        return "operator apl-" + builtInOps[ch];
      }
      if (isArrow.test(ch)) {
        return "apl-arrow";
      }
      if (isFunction.test(ch)) {
        funcName = "apl-";
        if (builtInFuncs[ch] != null) {
          if (state.prev) {
            funcName += builtInFuncs[ch][1];
          } else {
            funcName += builtInFuncs[ch][0];
          }
        }
        state.func = true;
        state.prev = false;
        return "function " + funcName;
      }
      if (isComment.test(ch)) {
        stream.skipToEnd();
        return "comment";
      }
      if (ch === "โ" && stream.peek() === ".") {
        stream.next();
        return "function jot-dot";
      }
      stream.eatWhile(/[\w\$_]/);
      word = stream.current();
      state.prev = true;
      return "keyword";
    }
  };
});

CodeMirror.defineMIME("text/apl", "apl");

});
