﻿function AdezySearch() { }

AdezySearch.prototype.Lpp = 5;
AdezySearch.prototype.Page = 0;
AdezySearch.prototype.SearchUrl = "";
AdezySearch.prototype.TotalRecords = 0;
AdezySearch.prototype.VariableName;
AdezySearch.prototype.SearchResults = {}
AdezySearch.prototype.TotalPage;
AdezySearch.prototype.ShowPageFrom;
AdezySearch.prototype.ShowPageTo;
AdezySearch.prototype.CallBackName;
AdezySearch.prototype.Filter = "";


AdezySearch.prototype.NextPage = function (Page, CallBackFunction) {
    this.Page = Page;
    this.ApplyFilter();
    var params = "Lpp=" + this.Lpp + "&Page=" + this.Page + this.Filter;

    ajaxUrlRequest(this.SearchUrl + "?" + params,
        function (result) {
            CallBackFunction(result);
        }
    );
}

AdezySearch.prototype.ParseResult = function (JsonData) {
    this.TotalRecords = JsonData.Result.TotalRecords;
    this.SearchResults = JsonData.Result.SearchResults;
    this.TotalPage = Math.ceil(this.TotalRecords / this.Lpp);

    var searchFrom = this.Page - 3;
    var searchTo = this.Page + 3;
    if (searchFrom < 0) {
        searchTo = searchTo + (-1 * searchFrom);
        searchFrom = 0;
    }
    if (searchTo > this.TotalPage) {
        searchFrom = searchFrom - (searchTo - this.TotalPage);
        searchTo = this.TotalPage;
        if (searchFrom < 0) searchFrom = 0;
    }

    this.ShowPageFrom = searchFrom;
    this.ShowPageTo = searchTo;

}